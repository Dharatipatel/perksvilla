var viewModelName;
var profileViewModel;

$(document).ready(function(){
	initializeKnockout();
    getAdminDetail();
    var rules = {
        txtEmail: {
            'required' :true,
            'email' :true,
        },
    };
    var messages = {
            
        txtEmail: {
            'required' : 'Please Enter Email',
            'email' : 'Please enter a valid email address'
        },
    };
    $("#form_admin").validate({
        rules:rules ,
        messages: messages,
    });
});


function getAdminDetail(){
	var setting = {};
	setting.url  = viewProfileApiPath;
	setting.type = 'GET';
	setting.data = {'id':id};
	setting.token = window.btoa(id+"|"+token);
	setting.success = function(response){
		profileViewModel.id(response.data.id);
		profileViewModel.email(response.data.email);
	};
	setting.error = function(jqXHR, textStatus, error){
		swal(jqXHR.responseJSON.message,"",'error');
		return false;
	};
	apiAjax(setting);
}
function initializeKnockout(){
	profileViewModel = new ProfileViewModel();
	viewModelName  = profileViewModel;
	ko.applyBindings(profileViewModel);
}

function ProfileViewModel(){
	self = this;
	self.id =ko.observable("");
	
	self.email =ko.observable("");
	
	self.updateProfileClick = function(){
        if(!$("#form_admin").valid()){
            return false
        }
        var setting = {};
        var datavalue = {};
       
        var email = profileViewModel.email();      
        datavalue['id'] = profileViewModel.id();
        datavalue['email'] = profileViewModel.email();
        
        setting.url  = updateProfileApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
      		swal("Update Successfully","",'success');
            return false;
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"",'error');
            return false;
        };
        apiAjax(setting);
    
	}

}