var viewModelName;
var offerViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#offerTab').addClass('active');
    initlizeKnockout();
    if(type==1){
        $('#offerActiveTab').addClass('active');
        $('#offerUpcomingTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        $('#offerRejectTab').removeClass('active');
        offerViewModel.status(1);
    }
    else if(type==2){
        $('#offerUpcomingTab').addClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        $('#offerRejectTab').removeClass('active');
        offerViewModel.status(2);   
    }
    else if(type==3){
        $('#offerPendingTab').addClass('active');
        $('#offerUpcomingTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPastTab').removeClass('active');
        $('#offerRejectTab').removeClass('active');
        offerViewModel.status(3);
    }
    else if(type==6){
        $('#offerPastTab').addClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        $('#offerRejectTab').removeClass('active');
        offerViewModel.status(6);
    } else if(type==5){
        $('#offerRejectTab').addClass('active');
        $('#offerPastTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerActiveTab').removeClass('active');
        $('#offerPendingTab').removeClass('active');
        offerViewModel.status(5);
    }
    $("#form_Offer").validate({
        rules:{
            txtTitle : "required",
            txtDescription : "required",
            txtValue: {
                'required' :true,
                'digits' :true,
            },
            txtPrice: {
                'required' :true,
                'digits': true
            },
            txtApplicableOn:"required",
            fixDate : { 
                required:'#txtApplicableOn1[value=1]:checked',
            },
            validTo : { 
                required :'#txtApplicableOn2[value=2]:checked',
            },
            'address[]' :{
                  required: true,
            },
            count :{
                "required" :true,
                "digits" :true,
            },
            publishDate : "required",
            notes : "required",
        },
        messages: {
            txtTitle: "Please enter Offer title",
            txtDescription: "Please enter Offer description",
            txtValue: {
                'required' : 'Please Enter Offer Value',
                'digits' : 'Please enter Offer Value in digits'
            },
            txtPrice: {
                'required' : 'Please Enter Offer Price',
                'digits' : 'Offer Price should be only in digits'
            },
            txtApplicableOn : "Please select Applicable Place",
            fixDate : "Please select Fix date",
            validTo : "Please select Valid To date",
            'address[]': {
                required: "You must check at least 1 Address",
            },
            count:{
                'required' : 'Please Enter Offer Count',
                'digits' : 'Offer Count should be only in digits'
            },
            publishDate:"Please Select Publish Date",
            notes:"Please Enter Important Notes",

        },
    });
    $('#fixDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#fixDate').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){
                    if($("#publishDate-error").length){
                            $('#publishDate-error').css("display","none");
                    }     
                    $("#fixDate").after(function() {
                        return "<label id='fixDate'-error' class='error' for='fixDate'> Fix Date can not less than Publish Date </label>";
                    });
                    return false;        
                } else {
                    if($("#publishDate-error").length){
                        $('#publishDate-error').css("display","none");
                    }
                }
            }
        });
    $('#validTo').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#validTo').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){     
                    $("#validTo").after(function() {
                        return "<label id='validTo'-error' class='error' for='validTo'>Expire Date can not less than Publish Date </label>";
                    });
                    return false;        
                }
            }
        });
    $('#publishDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            var applicableOn = $("input[name='txtApplicableOn']:checked").val()
            if(applicableOn == 1){
                applicableDate = $('#fixDate').val();
            } else {
                applicableDate = $('#validTo').val();
            }
            if(applicableDate!=''){
                if(!moment(applicableDate, 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){  
                    if($("#fixDate-error").length){
                            $('#fixDate-error').css("display","none");
                    } 
                    if($("#validTo-error").length){
                            $('#validTo-error').css("display","none");
                    }    
                    $("#publishDate").after(function() {
                        return "<label id='publishDate-error' class='error' for='publishDate'>Publish Date can not greater than Fix Date or Expire Date</label>";
                    });
                    return false;        
                }
            }
        });
    getOffers();
});
function initlizeKnockout(){
    offerViewModel = new OfferViewModel();
    viewModelName  = offerViewModel;
    ko.applyBindings(offerViewModel);
}
function OfferViewModel(){
    self=this;
    self.status=ko.observable(1);
    self.total=ko.observable(0);
    self.offers=ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.offerNoData = ko.observable(false);
    self.offerListContainer = ko.observable(true);
    self.editContainer = ko.observable(false);
    self.stepSubTitle = ko.observable("");
    self.step1Container =ko.observable(true);
    self.step2Container =ko.observable(false);
    self.fixDateVisibilty  = ko.observable(true);
    self.validToVisibilty =ko.observable(false);
    self.address = ko.observableArray([]);
    self.choosenAddress = ko.observableArray([]);
    self.action = ko.observable("add");
    self.action_title = ko.observable("Create Offer");
    self.offer_id = ko.observable("");
    self.isReject = ko.observable(true);
    self.approveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
            'publish_date' :offer.publish_date()
        };
        setting.url  = offerApproveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            // getOffers();
            location.href = offerActiveWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerDeactiveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            // getOffers();
            location.href = offerActiveWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.rejectOfferClick  = function(data,event){
        var setting = {};
        var datavalue = {
            'id' : event.target.getAttribute('data-id'),
        };
        setting.url  = offerRejectApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            // getOffers();
            location.href = offerRejectWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.editOfferClick= function(offer){
        getAddress(offer.brand_id());
        offerViewModel.editContainer(true);
        offerViewModel.offerListContainer(false);
        offerViewModel.step1Container(false);
        offerViewModel.step2Container(true);
        offerViewModel.action('edit');
        offerViewModel.action_title("Edit Offer");
        offerViewModel.offer_id(offer.id());
        $('#txtContact').val(offer.contact());
        if(offer.apply_on()==1){
            offerViewModel.fixDateVisibilty(true);
            offerViewModel.validToVisibilty(false);
            $('#fixDate').val(offer.valid_to());
        } else {
            offerViewModel.validToVisibilty(true);
            offerViewModel.fixDateVisibilty(false);
            $('#validTo').val(offer.valid_to());
        }
        $('#txtTitle').val(offer.name());
        $('#txtDescription').val(offer.description());
        $('#txtValue').val(offer.value());
        if(offer.price()!=0){
            $("#txtPrice").attr("disabled", false);
            $("#txtPrice").attr("placeholder", "Enter Buy price");
            $('#txtPrice').val(offer.price());
        } else {
            $("#txtPrice").attr("disabled", true);
            $("#txtPrice").attr("placeholder", "Rs 0");
        }
        $('input:radio[name="txtApplicableOn"]').filter('[value="'+offer.apply_on()+'"]').attr('checked', true);
        $('#count').val(offer.remaining_count());
        $('#publishDate').val(offer.publish_date());
        $('#notes').val(offer.notes());
        var address_array = offer.brand_address_id().split(',')
        offerViewModel.choosenAddress([]);
        $.each(address_array, function(index, value) {
          offerViewModel.choosenAddress.push(value); 
          // offerViewModel.choosenAddress.push(parseInt(value)); 
        });
    }
    self.cancelOfferClick = function(){
        offerViewModel.editContainer(false);
        offerViewModel.offerListContainer(true);
        offerViewModel.step1Container(false);
        offerViewModel.step2Container(false);
        offerViewModel.action('add');
        offerViewModel.action_title("Create Offer");
        resetOffer();
    }
    self.createOfferClick = function(){
        if(!$("#form_Offer").valid()){
            return false;
        }
        var datavalue = {};
        var applicableDate = '';
        var title = $('#txtTitle').val();
        var description = $('#txtDescription').val();
        var value = $('#txtValue').val();
        var price = $('#txtPrice').val();
        var count = $('#count').val();
        var publish_date = $('#publishDate').val();
        var notes = $('#notes').val();
        var applicableOn = $("input[name='txtApplicableOn']:checked").val()
        if(applicableOn == 1){
            applicableDate = $('#fixDate').val();
        } else {
            applicableDate = $('#validTo').val();
        }
        var setting = {};
        datavalue['title'] = title;
        datavalue['description'] = description;
        datavalue['value'] = value;
        datavalue['price'] = (price==""||price==null) ? 0 : price;
        datavalue['brand_address_id'] = offerViewModel.choosenAddress();
        datavalue['apply_on'] = applicableOn;
        datavalue['valid_to'] = applicableDate;
        datavalue['count'] = count;
        datavalue['publish_date'] = publish_date;
        datavalue['notes'] = notes;
        datavalue['contact'] = $('#txtContact').val();
        if(offerViewModel.action()=="edit"){
            offerApiPath =updateOfferApiPath
            datavalue['id'] =  offerViewModel.offer_id()
        }
        else if(offerViewModel.action()=="add"){
            offerApiPath = addOfferApiPath
        }

        setting.url  = offerApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            if(offerViewModel.action()=="add"){
               resetOffers();
               offerViewModel.offerAddContainer(false);
               getOffers();
            }else {
                swal("Offer Updated SuccessFully","",'success');
                return false;
            }
        };
        setting.error = function(jqXHR, textStatus, error){
             if(jqXHR.status==422){
                $.each(jqXHR.responseJSON.errors,function(i,item){
                    $("#publishDate" ).after(function() {
                        return "<label id='publishDate'-error' class='error' for='publishDate'>"+item+"</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            }    
        };
        apiAjax(setting);
    };
}
function getOffers(){

    var setting = {};
    var datavalue = {};
    if(brand_id != ""){
        datavalue['bid'] = brand_id;

    }
    datavalue['status'] = offerViewModel.status();
    setting.url  = offerListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        offerViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if(offer_list.length>0){
            offerViewModel.offers([]);
            $.each(offer_list, function(i,item) {
                offerViewModel.offers.push(new OfferModel(item));
            });
            $("#offer_tbl").DataTable( { responsive: true } );
            offerViewModel.offerList(true);
            offerViewModel.offerNoData(false);

        } else {
            offerViewModel.offerList(false);
            offerViewModel.offerNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            offerViewModel.address([]);
            $.each(address_list, function(i,item) {
                offerViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
$('input[type=radio][name=txtApplicableOn]').change(function() {
    if(this.value == 1){
        offerViewModel.fixDateVisibilty(true);
        offerViewModel.validToVisibilty(false);
    } else {
        offerViewModel.fixDateVisibilty(false);
        offerViewModel.validToVisibilty(true);
    }       
});
 function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }

 function resetOffer(){
    offerViewModel.offer_id("");
    $('#fixDate').val("");
    $('#validTo').val("");
    $('#txtTitle').val("");
    $('#txtDescription').val("");
    $('#txtValue').val("");
    $('#txtPrice').val("");
    $('input:radio[name="txtApplicableOn"]').filter('[value=1]').attr('checked', true);
    $('#count').val("");
    $('#publishDate').val("");
    $('#notes').val("");
 }