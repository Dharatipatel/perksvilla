var viewModelName;
var categoryViewModel;
var categoryApiPath;
var categoryPath;
function getCategories(){
    var setting = {};
    var datavalue = {
        'limit' : categoryViewModel.limit(),
        'type' : type,
        // 'user':true,
    };
    setting.url  = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        categoryViewModel.total(response.data.total);
        var category_list = response.data.data;
        if(category_list.length>0){
            categoryViewModel.categories([]);
            $.each(category_list, function(i,item) {
                categoryViewModel.categories.push(new CategoryModel(item));
            });

            $("#category_tbl").DataTable( { responsive: true } );
            $("#user_created_tbl").DataTable( { responsive: true } );
            categoryViewModel.categoryList(true);
            categoryViewModel.categoryNoData(false);

        } else {
            categoryViewModel.categoryList(false);
            categoryViewModel.categoryNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message);
        return false;
    };
    apiAjax(setting);
}
       
function CategoryViewModel(){
    self = this;
    self.limit =ko.observable(25);
    self.categories = ko.observableArray([]);
    self.categoryname = ko.observable("");
    self.selectedStatus= ko.observable(1);
    self.availbleStatus= [{
        text: "Active",
        value: 1
      }, {
        text: "Deactive",
        value: 0
      }];
    self.description = ko.observable("");
    self.total = ko.observable(0);
    self.categoryList = ko.observable(false);
    self.addListCategory =ko.observable(true);
    self.updateCategory =ko.observable(false);
    self.categoryNoData = ko.observable(true);
    self.deleteId =ko.observable();
    self.current_id =ko.observable();
    self.action_title =ko.observable();
    self.action_name =ko.observable();
    self.action =ko.observable();
    self.categoryListContainer = ko.observable(true);
    self.categoryAddContainer = ko.observable(false);
    self.cancelCategoryClick = function(){
        categoryViewModel.categoryAddContainer(false);
        categoryViewModel.categoryListContainer(true);
        resetAll();
    }
    self.addCategoryClick = function(){
        categoryViewModel.categoryAddContainer(true);
        categoryViewModel.categoryListContainer(false);
        categoryViewModel.action_title("Add");
        categoryViewModel.action_name("Add Category");
        categoryViewModel.action("add");
    }
    self.createCategoryClick =function(){
        if(!$("#form_category").valid()){
            return false;
        }
        categoryViewModel.categoryname($('#txtCategoryName').val());
        categoryViewModel.description($('#txtDescription').val());
        var datavalue = {
            'type' : type,
            'name' : categoryViewModel.categoryname(),
            'description' : categoryViewModel.description(),
            'status' : categoryViewModel.selectedStatus(),
        };

        if(categoryViewModel.action()=="edit"){
            datavalue['id'] = categoryViewModel.current_id();
            categoryApiPath = updateCategoryPath;
        } else if(categoryViewModel.action()=="add"){
            categoryApiPath = addCategoryPath;
        }

        var setting = {};
        setting.url  = categoryApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           location.href = categoryPath;
           resetAll();
        };
        setting.error = function(jqXHR, textStatus, error){
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            };
        apiAjax(setting);

    }
    self.editCategoryClick = function(category){
        categoryViewModel.categoryListContainer(false);
        categoryViewModel.categoryAddContainer(true);
        categoryViewModel.action_title("Edit");
        categoryViewModel.action_name("Edit Category");
        categoryViewModel.action("edit");
        $('#txtCategoryName').val(category.name());
        $('#txtDescription').val(category.description());
        categoryViewModel.selectedStatus(category.status());
        categoryViewModel.current_id(category.id());  
    };
    self.deleteCategoryClick=function(category){
        categoryViewModel.deleteId(category.id());
        $('#deleteModal').modal('show');
    };
    
    self.deleteConfirm = function(){
        var setting = {};
        var datavalue = {
            'id' : categoryViewModel.deleteId(),
        };
        setting.url  = deleteCategoriesApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            $('#deleteModal').modal('hide');
            location.href = categoryPath;
            // getCategories();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
    self.activeClick =function(category){
        var setting = {};
        var datavalue = {
            'id' : category.id(),
        };
        setting.url  = activeCategoryApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCategories();
            location.href = categoryPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
    self.deactiveClick =function(category){
        var setting = {};
        var datavalue = {
            'id' : category.id(),
        };
        setting.url  = deactiveCategoryApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCategories();
            location.href = categoryPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
}
function initlizeKnockout(){
    categoryViewModel = new CategoryViewModel();
    viewModelName = categoryViewModel;
    ko.applyBindings(categoryViewModel);
}
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#categoryTab').addClass('active');
    if(type==1){
        $('#categoryBrandTab').addClass('active');
        $('#categoryEmployeeTab').removeClass('active');
        $('#categoryRewardTab').removeClass('active');
        categoryPath = brandcategoryPath;
    }
    else if(type==2){
        $('#categoryEmployeeTab').addClass('active');
        $('#categoryBrandTab').removeClass('active');
        $('#categoryRewardTab').removeClass('active');
        categoryPath = employeecategoryPath;
    }
    else if(type==3){
        $('#categoryRewardTab').addClass('active');
        $('#categoryEmployeeTab').removeClass('active');
        $('#categoryBrandTab').removeClass('active');
        categoryPath = rewardcategoryPath;
    }
    initlizeKnockout();
    getCategories();
    $("#form_category").validate({
        rules: {
            name: "required",
            status : "required",
            txtDescription : "required",
        },
        messages: {
            name: "Please enter Category Name",
            status: "Please Select Status",
            txtDescription : "Please Enter Description",
        },
    });
});
function resetAll(){
    $('#txtCategoryName').val("");
    $('#txtDescription').val("");
}