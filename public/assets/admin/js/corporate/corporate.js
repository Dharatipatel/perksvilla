var viewModelName;
var corporateViewModel;
var corporateApiPath;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#corporateTab').addClass('active');
    initlizeKnockout();
    getCategories();
    if(type == 'add'){
        $('#corporateAddTab').addClass('active');
        $('#corporateListTab').removeClass('active');
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Add");
        corporateViewModel.action_name("Add Corporate");
        corporateViewModel.action("add");
    } else if(type == 'list'){
        $('#corporateListTab').addClass('active');
        $('#corporateAddTab').removeClass('active');
        corporateViewModel.corporateAddContainer(false);
        corporateViewModel.corporateListContainer(true);
        getCorporates();
        
    }
    var rules = {
            
            txtCorporateName : "required",
            txtCompany_size :{
                'required' :true,
                'digits': true
            },
            txtDescription : "required",
            txtCategory : "required",
            txtEmail: {
                'required' :true,
                'email' :true,
            },
            txtContact: {
                'required' :true,
                'digits': true
            },
            txtPassword : "required",
            txtBusinessHours: {
                'required' :true,
            },
            txtPersonName : "required",
            // txtWebsiteUrl :{
            //     "required" :true,
            // },
            // txtFacebookPage : "required",
            // txtLinkedInPage : "required",
            txtAddress : "required",
            txtStatus : "required",
        };
    var messages = {
            txtBrandName: "Please enter a Corporate name",
            txtCompany_size: {
                'required' : 'Please Enter Company size',
                'digits' : 'Company size should be only in digits'
            },
            txtDescription: "Please enter  a description",
            txtCategory: "Please select category",
            txtEmail: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter a valid email address'
            },
            txtContact: {
                'required' : 'Please Enter mobile',
                'digits' : 'Mobile should be only in digits'
            },
            txtPassword: "Please enter password",
            txtPersonName: "Please enter person name",
            // txtWebsiteUrl: {
            //     'required':"Please Enter url",
            // },
            // txtFacebookPage: "Please Enter facebook page",
            // txtLinkedInPage: "Please Enter LinkedIn page",
            txtAddress: "Please Enter address",
            txtStatus: "Please select  a Status",
        };
    $("#form_corporate").validate({
        rules:rules ,
        messages: messages,
    });
    $("#form_employee").validate({
        rules:{
            txtFirstName : "required",
            txtLastName : "required",
            gender : "required",
            txtDesignation : "required",
            txtEmailEmployee: {
                'required' :true,
                'email' :true,
            },
            txtContactEmployee: {
                'required' :true,
                'digits': true
            },
            dateBirth : { 
                required:true,
            },
            dateJoin : { 
                required :true,
            },
            /*fileInputEmployee:{
                required :function(element) {
                            if (corporateViewModel.employeeViewModel.action()=='edit') {
                                return false;
                            } else {
                                return true;
                            }
                    },
            }*/
        },
        messages: {
            txtFirstName: "Please enter First name",
            txtLastName: "Please enter Last name",
            gender: "Please select gender ",
            txtDesignation: "Please enter Designation",
            txtEmailEmployee: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter valid Email'
            },
            txtContactEmployee: {
                'required' : 'Please Enter Mobile Number',
                'digits' : 'Mobile Number should be only in digits'
            },
            dateBirth : "Please select Date of Birth",
            dateJoin : "Please select Date of join",
            /*fileInputEmployee:{
                required :"Please Select Profile Image",
            }*/
        },
    });
    $("#form_import").validate({
        rules:{
            fileImport: 'required',
        },
        message:{
            fileImport: 'Csv File is required',
        }
    });
    $('#dateBirth').datepicker({format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true,endDate: Date()});
    $('#dateJoin').datepicker({format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true});
});
function initlizeKnockout(){

    corporateViewModel = new corporateViewModel();
    viewModelName = corporateViewModel;
    ko.applyBindings(corporateViewModel);
}
function EmployeeViewModel(){
    self= this;
    self.imagePathEmployee = ko.observable(pathEmployee);
    self.current_id = ko.observable(); 
    self.status=ko.observable(1);
    self.total=ko.observable(0);
    self.action=ko.observable("add");
    self.selectedGender =ko.observable("Male");
    self.employee=ko.observableArray([]);
    self.button_title = ko.observable("ADD");
    self.imageUri = ko.observable("");
    self.profile = ko.observable("");
    self.employees = ko.observableArray([]);
    self.employeeList = ko.observable(true);
    self.employeeNoData = ko.observable(false);
    self.addEmployeeContainer = ko.observable(false);
    self.importEmployeeContainer = ko.observable(false);
    self.employeeAction = ko.observable(true);
    self.selectedStatus = ko.observable(0);
    self.availbleStatus = [{
            text: "Active",
            value: 1
        }, {
            text: "Deactive",
            value: 0
        }];
    self.deleteEmployeeClick = function (employee) {
        corporateViewModel.employeeViewModel.current_id(employee.id());
        $('#deleteEmployeeModal').modal('show');
    };
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': corporateViewModel.employeeViewModel.current_id()
        };
        setting.url = deleteEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteEmployeeModal').modal('hide');
            getEmployees();
        };
        setting.error = function (jqXHR, textStatus, error) {
            $('#deleteEmployeeModal').modal('hide');
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.createEmployeeClick = function(){
        if(!$("#form_employee").valid()){
            return false;
        }
        var datavalue = {};
        var setting = {};
        datavalue['first_name'] = $('#txtFirstName').val();
        datavalue['last_name'] = $('#txtLastName').val();
        datavalue['gender'] = corporateViewModel.employeeViewModel.selectedGender();
        datavalue['designation'] = $('#txtDesignation').val();
        datavalue['contact'] = $('#txtContactEmployee').val();
        datavalue['email'] = $('#txtEmailEmployee').val();
        datavalue['joining_date'] = $('#dateJoin').val();
        datavalue['birth_date'] = $('#dateBirth').val();
        datavalue['status'] = corporateViewModel.employeeViewModel.selectedStatus();
        if(corporateViewModel.employeeViewModel.imageUri()!=""){

            datavalue['profile_pic'] = corporateViewModel.employeeViewModel.imageUri();
        }
        datavalue['corporate_id'] = corporateViewModel.current_id();
        if(corporateViewModel.employeeViewModel.action()=='add'){
            employeeApi = employeeAddApi
        } else if(corporateViewModel.employeeViewModel.action()=='edit'){
            employeeApi = employeeUpdateApi
            datavalue['id'] = corporateViewModel.employeeViewModel.current_id();
        }

        setting.url  = employeeApi;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           resetEmployee();
           corporateViewModel.employeeViewModel.employeeList(true);
           corporateViewModel.employeeViewModel.employeeAction(true);
           corporateViewModel.employeeViewModel.addEmployeeContainer(false);
           if(corporateViewModel.employeeViewModel.status() ==1){
                corporateViewModel.employeeViewModel.activeEmployeeClick();
           } else if(corporateViewModel.employeeViewModel.status() == 0) {
                corporateViewModel.employeeViewModel.deactiveEmployeeClick();
           }
           // getEmployees();
        };
        setting.error = function(jqXHR, textStatus, error){
            if(jqXHR.status==422){
                $.each(jqXHR.responseJSON.errors,function(i,item){
                    var fieldname;
                    tail = i.substring( 1 );
                    fieldName = i.substring( 0, 1 ).toUpperCase() + tail; 
                    fieldname ="txt"+fieldName+"Employee";
                    $("#"+fieldname ).after(function() {
                        return "<label id='"+fieldname+"'-error' class='error' for='"+fieldname+"'>"+item+"</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            }
        };
        apiAjax(setting);
    }
    self.editEmployeeClick   = function(employee){
    
        corporateViewModel.employeeViewModel.addEmployeeContainer(true);
        corporateViewModel.employeeViewModel.employeeList(false);
        corporateViewModel.employeeViewModel.importEmployeeContainer(false);
        corporateViewModel.employeeViewModel.employeeAction(false);
        $('#txtFirstName').val(employee.first_name());
        $('#txtLastName').val(employee.last_name());
        $('#txtDesignation').val(employee.designation());
        $('#txtContactEmployee').val(employee.contact());
        $('#txtEmailEmployee').val(employee.email());
        $('#dateJoin').val(employee.joining_date());
        $('#dateBirth').val(employee.birth_date());

        if(employee.profile_pic()==null){
            if(employee.gender()=="Male"){    
                corporateViewModel.employeeViewModel.profile(static_image_male);
            } else if(employee.gender()=="Female"){
                corporateViewModel.employeeViewModel.profile(static_image_female);
            }
        }else{
            corporateViewModel.employeeViewModel.profile(employee.profile_pic());
        }
        corporateViewModel.employeeViewModel.selectedStatus(employee.status());
        corporateViewModel.employeeViewModel.current_id(employee.id());
        corporateViewModel.employeeViewModel.action("edit");
        corporateViewModel.employeeViewModel.button_title("UPDATE");
        corporateViewModel.employeeViewModel.selectedGender(employee.gender());
    }
    self.cancelEmployeeClick = function(){
        resetEmployee();
        corporateViewModel.employeeViewModel.employeeList(true);
        corporateViewModel.employeeViewModel.employeeAction(true);
        corporateViewModel.employeeViewModel.employeeNoData(false);
        corporateViewModel.employeeViewModel.addEmployeeContainer(false);
        corporateViewModel.employeeViewModel.importEmployeeContainer(false); 
    }
    self.addEmployeeClick = function(){
        corporateViewModel.employeeViewModel.employeeList(false);
        corporateViewModel.employeeViewModel.employeeAction(false);
        corporateViewModel.employeeViewModel.employeeNoData(false);
        corporateViewModel.employeeViewModel.addEmployeeContainer(true);
        corporateViewModel.employeeViewModel.importEmployeeContainer(false);
    };
    self.importCsvClick = function(){
        corporateViewModel.employeeViewModel.employeeList(false);
        corporateViewModel.employeeViewModel.employeeAction(false);
        corporateViewModel.employeeViewModel.employeeNoData(false);
        corporateViewModel.employeeViewModel.addEmployeeContainer(false);
        corporateViewModel.employeeViewModel.importEmployeeContainer(true);
    }
    self.importEmployeeClick = function(){
        if(!$("#form_import").valid()){
            return false;
        }
        var fileUpload = $('#fileImport').val();
        if (fileUpload != null) {
            var uploadFile = new FormData();
            var files = $("#fileImport").get(0).files;
            // Add the uploaded file content to the form data collection
            if (files.length > 0) {
                var setting = {};
                uploadFile.append("employeecsv", files[0]);
                uploadFile.append("corporate_id",corporateViewModel.current_id());
                setting.url  = employeeImportApi;
                setting.type = 'POST';
                setting.data = uploadFile;
                setting.processData = false;
                setting.contentType = false;
                setting.token = window.btoa(id+"|"+token);
                setting.success = function(response){
                   resetEmployee();
                   corporateViewModel.employeeViewModel.employeeList(true);
                   corporateViewModel.employeeViewModel.employeeAction(true);
                   corporateViewModel.employeeViewModel.importEmployeeContainer(false);
                   corporateViewModel.employeeViewModel.addEmployeeContainer(false);
                   corporateViewModel.employeeViewModel.deactiveEmployeeClick();
                };
                setting.error = function(jqXHR, textStatus, error){
                    $('#fileImport').val("");
                    if(jqXHR.status==422){
                        var error_msg="Following email and contact are already exist";
                        $.each(jqXHR.responseJSON.errors,function(i,item){                
                            if(item.email){
                                error_msg += item.email +" ";
                            }
                            if(item.contact){
                                error_msg += item.contact +" ";
                            }
                        })
                            swal(error_msg,"","error");
                    } else {
                        swal(jqXHR.responseJSON.message,"test","error");
                        return false;
                    }
                };
                apiAjax(setting);
            }
        }
    }
    self.activeEmployeeClick = function(item, event){
        $('.employeelist').removeClass('active'); 
        // $(event.target).addClass('active'); 
        $('#activeEmployee').addClass('active'); 
        corporateViewModel.employeeViewModel.status(1);
        getEmployees();
    }
    self.deactiveEmployeeClick = function(item, event){
        $('.employeelist').removeClass('active'); 
        // $(event.target).addClass('active'); 
        $('#deactiveEmployee').addClass('active'); 
        corporateViewModel.employeeViewModel.status(0);
        getEmployees();
    }
}
function corporateViewModel(){
    self = this;
    self.current_id = ko.observable(""); 
    self.current_uid = ko.observable("");
    self.limit =ko.observable(200);
    self.active_corporate_count=ko.observable(0);
    self.active_employee_count=ko.observable(0);
    self.issue_count=ko.observable(0);
    self.corporates = ko.observableArray([]);
    self.selectedCategory= ko.observable("");
    self.availbleCategory= ko.observableArray([]);
    self.selectedStatus= ko.observable(0);
    self.availbleStatus= [{
        text: "Active",
        value: 1
      }, {
        text: "Deactive",
        value: 0
      }];
    self.total = ko.observable(0);
    self.corporateListContainer =  ko.observable(true);
    self.corporateList =  ko.observable(false);
    self.corporateNoData = ko.observable(true);
    self.corporateAddContainer =ko.observable(false);
    self.addCorporateClick = function(){
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Add");
        corporateViewModel.action_name("Add Corporate");
        corporateViewModel.action("add");
    };
    self.action =ko.observable("");
    self.action_title = ko.observable("");
    self.employeeCountClick = function(corporate){
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Edit");
        corporateViewModel.action_name("Edit Corporate");
        corporateViewModel.action("edit");
        $('#txtCorporateName').val(corporate.name());
        $('#txtCompany_size').val(corporate.company_size());
        $('#txtDescription').val(corporate.description());
        $('#txtEmail').val(corporate.email());
        $('#txtContact').val(corporate.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled","disabled");
        $('#txtPersonName').val(corporate.contact_person_name());
        $('#txtWebsiteUrl').val(corporate.website_url());
        $('#txtFacebookPage').val(corporate.facebook_page());
        $('#txtLinkedInPage').val(corporate.linkedin_page());
        $('#txtAddress').val(corporate.address());
        corporateViewModel.selectedCategory(corporate.category_id());
        corporateViewModel.selectedStatus(corporate.status());
        corporateViewModel.current_id(corporate.id());
        corporateViewModel.current_uid(corporate.uid());
        corporateViewModel.profile(corporate.logo());
        $("#employeetab").attr("data-toggle", "tab");
        $("#order_historytab").attr("data-toggle", "tab");
        $("#employee").addClass("in active");
        $("#employeetab").parent().addClass("active");
        $("#corporate_details").removeClass("in active");
        $("#corporate_detailstab").parent().removeClass("active");
        getEmployees();
        getOrderHistory();
        // location.href=employeeWebList+"?id="+corporate.id();
    }
    self.cancelCorporateClick = function(){
        location.href = corporateWebList;
    }
    self.editCorporateClick =function(corporate){
        corporateViewModel.corporateListContainer(false);
        corporateViewModel.corporateAddContainer(true);
        corporateViewModel.action_title("Edit");
        corporateViewModel.action_name("Edit Corporate");
        corporateViewModel.action("edit");
        $('#txtCorporateName').val(corporate.name());
        $('#txtCompany_size').val(corporate.company_size());
        $('#txtDescription').val(corporate.description());
        $('#txtEmail').val(corporate.email());
        $('#txtContact').val(corporate.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled","disabled");
       
        $('#txtPersonName').val(corporate.contact_person_name());
        $('#txtWebsiteUrl').val(corporate.website_url());
        $('#txtFacebookPage').val(corporate.facebook_page());
        $('#txtLinkedInPage').val(corporate.linkedin_page());
        $('#txtAddress').val(corporate.address());
        if(corporate.logo()==null){
            corporateViewModel.profile(static_image);
        }else{
            corporateViewModel.profile(corporate.logo());
        }
        corporateViewModel.selectedCategory(corporate.category_id());
        corporateViewModel.selectedStatus(corporate.status());
        corporateViewModel.current_id(corporate.id());
        corporateViewModel.current_uid(corporate.uid());
        
        $("#employeetab").attr("data-toggle", "tab");
        $("#order_historytab").attr("data-toggle", "tab");
        getEmployees();
        getOrderHistory();
    };
    self.deleteCorporateClick = function(corporate){
        corporateViewModel.current_uid(corporate.uid());
        $('#deleteModal').modal('show');
    }
    self.deleteConfirm = function(){
        var setting = {};
        var datavalue = {
            'id' : corporateViewModel.current_uid(),
        };
        setting.url  = deleteCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            $('#deleteModal').modal('hide');
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;

            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };

    self.createCorporateClick = function(){
        if(!$("#form_corporate").valid()){
            return false;
        }
            
        var datavalue = {};
        
       
        if(corporateViewModel.action()=="edit"){
            datavalue['id'] = corporateViewModel.current_id();
            datavalue['uid'] = corporateViewModel.current_uid();
            corporateApiPath = updateCorporateApiPath;
        } else if(corporateViewModel.action()=="add"){
            corporateApiPath = addCorporateApiPath;
            datavalue['password'] = $.md5($('#txtPassword').val());
        }
        var setting = {};
        datavalue['name'] = $('#txtCorporateName').val();
        datavalue['company_size'] = $('#txtCompany_size').val();
        datavalue['description'] = $('#txtDescription').val();
        datavalue['email'] = $('#txtEmail').val();
        datavalue['contact'] = $('#txtContact').val();
        datavalue['person_name'] = $('#txtPersonName').val();
        datavalue['website_url'] = $('#txtWebsiteUrl').val();
        datavalue['facebook_page'] = $('#txtFacebookPage').val();
        datavalue['linkedin_page'] = $('#txtLinkedInPage').val();
        datavalue['address'] = $('#txtAddress').val();
        datavalue['category'] = corporateViewModel.selectedCategory();
        datavalue['status'] = corporateViewModel.selectedStatus();
        if(corporateViewModel.imageUri()!=""){
            datavalue['image'] = corporateViewModel.imageUri();
        }
        setting.url  = corporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            if (corporateViewModel.action() == "add") {
                location.href = corporateWebList;
                resetAll();
            } else if(corporateViewModel.action() == "edit"){
                swal("Corporate Details Updated SuccessFully" ,"", "success");
            }
           // resetAll();
            // corporateViewModel.current_id(response.data.id);
            // $("#employeetab").attr("data-toggle", "tab");
            // $("#employee").addClass("in active");
            // $("#employeetab").parent().addClass("active");
            // $("#corporate_details").removeClass("in active");
            // $("#corporate_detailstab").parent().removeClass("active");
            // getEmployees();
        };
        setting.error = function(jqXHR, textStatus, error){
                if(jqXHR.status==422){                    
                    $.each(jqXHR.responseJSON.errors,function(i,item){
                        var fieldname;
                        tail = i.substring( 1 );
                        fieldName = i.substring( 0, 1 ).toUpperCase() + tail; 
                        fieldname ="txt"+fieldName;
                        $("#"+fieldname ).after(function() {
                            return "<label id='"+fieldname+"'-error' class='error' for='"+fieldname+"'>"+item+"</label>";
                        });
                    })
                } else {
                    swal(jqXHR.responseJSON.message,"","error");
                    return false;
                }
            };
        apiAjax(setting);
    };
    self.activeClick =function(corporate){
        var setting = {};
        var datavalue = {
            'id' : corporate.uid(),
        };
        setting.url  = activeCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick =function(corporate){
        var setting = {};
        var datavalue = {
            'id' : corporate.uid(),
        };
        setting.url  = deactiveCorporateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // getCorporates();
            location.href = corporateWebList;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.emailCorporateClick = function(corporate){
       var id = corporate.id();
        $("#labelEmail").html(corporate.email());
        $("#hiddenCorporateId").val(id);
        $("#emailModal").modal("toggle");
        $("#txtSendPassword").val("");
    };
    
    self.emailSendClick = function (corporate) {
        var setting = {};
        var datavalue = {
            'id': $("#hiddenCorporateId").val(),
            "password": $("#txtSendPassword").val()
        };
        setting.url = emailSendApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#emailModal').modal('toggle');
            swal(response.message, "", "success");
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.action_name = ko.observable("");
     /*Image Upload*/
    self.imageUri = ko.observable("");
    self.imagePath = ko.observable(path);
    self.profile = ko.observable("");
    /*order History*/
    self.orderhistory = ko.observableArray([]);
    self.orderHistoryListContainer  = ko.observable(false);
    self.orderHistoryList  = ko.observable(false);
    self.orderHistoryNoData  = ko.observable(false);
    self.offer_id  = ko.observable("");
    self.offer = ko.observableArray([]);
    self.offers = ko.observableArray([]);
    self.offerDetailClick = function(data,event){
        corporateViewModel.offer_id(event.target.getAttribute('data-id'));
        getOfferDetail();
        // $('#viewModel').modal();
    };
    self.address= ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.contact = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    /*Employee*/
    self.employeeViewModel = new EmployeeViewModel;
}
function resetAll(){
    $('#txtCorporateName').val("");
    $('#fileInput').val("");
    $('#txtCategory').val("");
    $('#txtCompany_size').val("");
    $('#txtDescription').val("");
    $('#txtPersonName').val("");
    $('#txtEmail').val("");
    $('#txtContact').val("");
    $('#txtPassword').removeAttr("disabled");
    $('#txtPassword').val("");
    $('#txtWebsiteUrl').val("");
    $('#txtLinkedInPage').val("");
    $('#txtFacebookPage').val("");
    $('#txtAddress').val("");
}
function getCorporates(){
    var setting = {};
    var datavalue = {
        'limit' : corporateViewModel.limit(),
    };
    setting.url  = corporateListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        corporateViewModel.total(response.data.total);
        corporateViewModel.active_corporate_count(response.active_corporate);
        corporateViewModel.active_employee_count(response.active_employee);
        corporateViewModel.issue_count(response.issue_coupen);
        var corporate_list = response.data.data;
        if(corporate_list.length>0){
            corporateViewModel.corporates([]);
            $.each(corporate_list, function(i,item) {
                corporateViewModel.corporates.push(new CorporateModel(item));
            });

            $("#corporate_tbl").DataTable( { responsive: true } );
            corporateViewModel.corporateListContainer(true);
            corporateViewModel.corporateAddContainer(false);
            corporateViewModel.corporateList(true);
            corporateViewModel.corporateNoData(false);

        } else {
            corporateViewModel.corporateListContainer(true);
            corporateViewModel.corporateAddContainer(false);
            corporateViewModel.corporateList(false);
            corporateViewModel.corporateNoData(true);
        }
        if(cid != ""){
            $.each(corporateViewModel.corporates(), function (i, item) {
                if(item.id() == cid){
                    corporateViewModel.employeeCountClick(item);
                }
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getOfferDetail(){
    var setting = {};
    var datavalue = {
       'offer_id':corporateViewModel.offer_id(),
    };
    
    setting.url  = offerDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            corporateViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                corporateViewModel.offers.push(new OfferModel(item));
            });
            var offer = corporateViewModel.offers()[0];
            corporateViewModel.offer_title(offer.name());
            corporateViewModel.offer_description(offer.description());
            corporateViewModel.coupon_value(offer.value());
            corporateViewModel.valid_to(offer.valid_to());
            corporateViewModel.coupon_code("");
            corporateViewModel.applicable_on(offer.apply_on_text());
            corporateViewModel.contact(offer.contact());
            corporateViewModel.address_array(offer.brand_address_id().split(','));
            corporateViewModel.business_hours(offer.business_hours());
            corporateViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
            corporateViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
            corporateViewModel.notes(offer.notes());
            corporateViewModel.brand_logo(pathBrand+""+offer.logo());
            // corporateViewModel.coupon_code(reward.coupon());
            getAddress(offer.brand_id());
        }
    
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            $.each(address_list, function(i,item) {
                corporateViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            corporateViewModel.selected_address([]);
            for (var i = 0; i < corporateViewModel.address().length ; i++) {
                if(corporateViewModel.address_array.indexOf(corporateViewModel.address()[i].value().toString()) != -1){
                    corporateViewModel.selected_address.push(corporateViewModel.address()[i].textaddress());
                }
            }
            $('#viewModel').modal('toggle');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
}
$('#fileInput').change(function(){
    var file = $('#fileInput').prop('files')[0];
    var ImageURL;
    var FR = new FileReader();
    FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        
        corporateViewModel.imageUri(realData);
    }, false);
    if (file) {
        FR.readAsDataURL(file);
    }
});
$('#fileInputEmployee').change(function(){
   var file = $('#fileInputEmployee').prop('files')[0];
   var ImageURL;
   var FR = new FileReader();
   FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        // var blob = b64toBlob(    , contentType);
        corporateViewModel.employeeViewModel.imageUri(realData);
   }, false);
   if (file) {
        FR.readAsDataURL(file);
   }
});
function getEmployees() {
    var setting = {};
    var datavalue = {
        'limit': 1000,
        'cid':corporateViewModel.current_id(),
        'status': corporateViewModel.employeeViewModel.status()
    };
    
    setting.url = employeeListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var employee_list = response.data.data;
        if ($.fn.DataTable.isDataTable("#employeeList")) {
                $('#employeeList').DataTable().clear().destroy();
            } 
        if (employee_list.length > 0) {
            corporateViewModel.employeeViewModel.employees([]);
            $.each(employee_list, function (i, item) {
                corporateViewModel.employeeViewModel.employees.push(new EmployeeModel(item));
            });
            corporateViewModel.employeeViewModel.employeeList(true);
            corporateViewModel.employeeViewModel.employeeNoData(false);
             $('#employeeList').DataTable({});
        } else {
            corporateViewModel.employeeViewModel.employeeList(false);
            corporateViewModel.employeeViewModel.employeeNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function resetEmployee(){
    $('#txtFirstName').val("");
    $('#txtLastName').val("");
    $('#txtDesignation').val("");
    $('#txtContactEmployee').val("");
    $('#txtEmailEmployee').val("");
    $('#dateJoin').val("");
    $('#dateBirth').val("");
    $('#fileInput').val("");
}
function getCategories() {
    var setting = {};
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit': 1000, 'type': 2, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var category_list = response.data.data;
        if (category_list.length > 0) {
            $.each(category_list, function (i, item) {
                corporateViewModel.availbleCategory.push(new availableCategory(item.id, item.name));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getOrderHistory(){
    var setting = {};
    var datavalue = {
       'status':1,
       'cid':corporateViewModel.current_id(),
    };
    
    setting.url  = orderHistoryListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data.data;
            corporateViewModel.orderhistory([]);
        if(order_list.length>0){
            $.each(order_list, function(i,item) {
                corporateViewModel.orderhistory.push(new OrderHistoryCorporateModel(item));
                corporateViewModel.orderHistoryNoData(false);
                corporateViewModel.orderHistoryListContainer(true);
                corporateViewModel.orderHistoryList(true);
            });
            $('#history_tbl').DataTable({});
        } else {
            corporateViewModel.orderHistoryNoData(true);
            corporateViewModel.orderHistoryListContainer(false);
            corporateViewModel.orderHistoryList(false);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}