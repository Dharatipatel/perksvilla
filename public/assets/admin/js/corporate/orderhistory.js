var viewModelName;
var orderHistoryViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#orderHistoryTab').addClass('active');
    $('#summaryTab').addClass('active');
    initializeKnockout();
    getOrderHistory();
});
function initializeKnockout(){
    orderHistoryViewModel = new OrderHistoryViewModel();
    viewModelName  = orderHistoryViewModel;
    ko.applyBindings(orderHistoryViewModel);
}

function OrderHistoryViewModel(){
    self = this;
    self.total = ko.observable("");
    self.orderhistory = ko.observableArray([]);
    self.offers = ko.observableArray([]);
    self.reward_issued= ko.observable(0);
    self.total_save = ko.observable(0);
    self.total_invest = ko.observable(0);
    self.orderHistoryListContainer  = ko.observable(false);
    self.orderHistoryList  = ko.observable(false);
    self.orderHistoryNoData  = ko.observable(false);
    self.offer_id  = ko.observable("");
    self.offerDetailClick = function(data,event){
        orderHistoryViewModel.offer_id(event.target.getAttribute('data-id'));
        getOfferDetail();
    };
    self.address= ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.contact = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
   
}

function getOrderHistory(){
    var setting = {};
    var datavalue = {
       'status':1,
    };
    
    setting.url  = orderHistoryListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data.data;
            orderHistoryViewModel.orderhistory([]);
        if(order_list.length>0){
            $.each(order_list, function(i,item) {
                orderHistoryViewModel.orderhistory.push(new OrderHistoryCorporateModel(item)); 
            });
            orderHistoryViewModel.orderHistoryNoData(false);
            orderHistoryViewModel.orderHistoryListContainer(true);
            orderHistoryViewModel.orderHistoryList(true);
        } else {
            orderHistoryViewModel.orderHistoryNoData(true);
            orderHistoryViewModel.orderHistoryListContainer(false);
            orderHistoryViewModel.orderHistoryList(false);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getOfferDetail(){
    var setting = {};
    var datavalue = {
       'offer_id':orderHistoryViewModel.offer_id(),
    };
    
    setting.url  = offerDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            orderHistoryViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                orderHistoryViewModel.offers.push(new OfferModel(item));
            });
            var offer = orderHistoryViewModel.offers()[0];
            orderHistoryViewModel.offer_title(offer.name());
            orderHistoryViewModel.offer_description(offer.description());
            orderHistoryViewModel.coupon_value(offer.value());
            orderHistoryViewModel.valid_to(offer.valid_to());
            orderHistoryViewModel.coupon_code("");
            orderHistoryViewModel.applicable_on(offer.apply_on_text());
            orderHistoryViewModel.contact(offer.contact());
            orderHistoryViewModel.address_array(offer.brand_address_id().split(','));
            orderHistoryViewModel.business_hours(offer.business_hours());
            orderHistoryViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
            orderHistoryViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
            orderHistoryViewModel.notes(offer.notes().replace(/\n/g, "<br />"));
            orderHistoryViewModel.brand_logo(pathBrand+""+offer.logo());
            // orderHistoryViewModel.coupon_code(reward.coupon());
            getAddress(offer.brand_id());
        }
    
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            $.each(address_list, function(i,item) {
                orderHistoryViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            orderHistoryViewModel.selected_address([]);
            for (var i = 0; i < orderHistoryViewModel.address().length ; i++) {
                if(orderHistoryViewModel.address_array.indexOf(orderHistoryViewModel.address()[i].value().toString()) != -1){
                    orderHistoryViewModel.selected_address.push(orderHistoryViewModel.address()[i].textaddress());
                }
            }
            $('#viewModel').modal('toggle');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
}
