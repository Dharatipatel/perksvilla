var viewModelName;
var brandViewModel;
var brandApiPath;
$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#brandTab').addClass('active');

    initlizeKnockout();
    if (type == 'add') {
        $('#brandAddTab').addClass('active');
        $('#brandListTab').removeClass('active');
        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Add");
        brandViewModel.action_name("Add Brand");
        brandViewModel.action("add");
        getCountries(0);
    } else if (type == 'list') {
        $('#brandListTab').addClass('active');
        $('#brandAddTab').removeClass('active');
        brandViewModel.brandAddContainer(false);
        brandViewModel.brandListContainer(true);
        getBrands();
    }
    getCategories();
    var rules = {
        txtBrandName: "required",
        txtCategory: "required",
        txtDescription: "required",
        txtEmail: {
            'required': true,
            'email': true,
        },
        txtContact: {
            'required': true,
            'digits': true
        },
        txtPassword: "required",
        txtBusinessHours: "required",
        txtPersonName: "required",
        txtCommision: {
            required:true,
            min:0,
            max:100,
        },
        txtStatus: "required",
    };
    var messages = {
        txtBrandName: "Please Enter a brand name",
        txtCategory: "Please select  a category",
        txtDescription: "Please Enter  a description",
        txtEmail: {
            'required': 'Please Enter Email',
            'email': 'Please Enter a valid email address'
        },
        txtContact: {
            'required': 'Please Enter mobile',
            'digits': 'Mobile should be only in digits'
        },
        txtPassword: "Please Enter password",
        txtBusinessHours: "Please Enter Business Hours",
        txtPersonName: "Please Enter person name",
        txtStatus: "Please select  a Status",
        txtCommision: {
            required:"Please Enter Commision",
            min:"Commision shuod not less than 1",
            max:"Commision shuod not grater than 100",
        },
    };
    $("#form_brand").validate({
        rules: rules,
        messages: messages,
    });
    $("#form_location").validate({
        rules: {
            txtAddress: "required",
            txtPincode: "required",
            txtCountry: "required",
            txtState: "required",
            txtCity: "required",
        },
        messages: {
            txtAddress: "Please Enter address",
            txtPincode: "Please Enter pincode",
            txtCountry: "Please Enter Country name",
            txtState: "Please Enter State name",
            txtCity: "Please Enter City name",
        },
    });
    $("#form_bank").validate({
        rules: {
            txtBank_name: "required",
            txtAccount_number: {
                'required': true,
                'digits': true,
            },
            txtIfsc: "required",
            txtBranch_details: "required",
            txtAccount_type: "required",
        },
        messages: {
            txtBank_name: "Please Enter Bank name",
            txtAccount_number: {
                'required': "Please Enter Account Number",
                'digits': "Account Number should be in digits",
            },
            txtIfsc: "Please Enter IFSC Code",
            txtBranch_details: "Please Enter Branch Details",
            txtAccount_type: "Please Enter Account Type",
        },
    });
    $("#form_Offer").validate({
        rules:{
            txtTitle : "required",
            txtOfferDescription : "required",
            txtValue: {
                'required' :true,
                'digits' :true,
            },
            txtPrice: {
                'required' :true,
                'digits': true
            },
            txtApplicableOn:"required",
            fixDate : { 
                required:'#txtApplicableOn1[value=1]:checked',
            },
            validTo : { 
                required :'#txtApplicableOn2[value=2]:checked',
            },
            'address[]' :{
                  required: true,
            },
            count :{
                "required" :true,
                "digits" :true,
            },
            txtOfferContact :{
                "required" :true,
                "digits" :true,
            },
            publishDate : "required",
            notes : "required",
        },
        messages: {
            txtTitle: "Please enter Offer title",
            txtOfferDescription: "Please enter Offer description",
            txtValue: {
                'required' : 'Please Enter Offer Value',
                'digits' : 'Please enter Offer Value in digits'
            },
            txtPrice: {
                'required' : 'Please Enter Offer Price',
                'digits' : 'Offer Price should be only in digits'
            },
            txtApplicableOn : "Please select Applicable Place",
            fixDate : "Please select Fix date",
            validTo : "Please select Valid To date",
            'address[]': {
                required: "You must check at least 1 Address",
            },
            count:{
                'required' : 'Please Enter Offer Count',
                'digits' : 'Offer Count should be only in digits'
            },
            txtOfferContact:{
                'required' : 'Please Enter Contact Number',
                'digits' : 'Contact Number should be only in digits'
            },
            publishDate:"Please Select Publish Date",
            notes:"Please Enter Important Notes",

        },
    });
    $('#fixDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#fixDate').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){
                    if($("#publishDate-error").length){
                            $('#publishDate-error').css("display","none");
                    }     
                    $("#fixDate").after(function() {
                        return "<label id='fixDate'-error' class='error' for='fixDate'> Fix Date can not less than Publish Date </label>";
                    });
                    return false;        
                } else {
                    if($("#publishDate-error").length){
                        $('#publishDate-error').css("display","none");
                    }
                }
            }
        });
    $('#validTo').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#validTo').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){     
                    $("#validTo").after(function() {
                        return "<label id='validTo'-error' class='error' for='validTo'>Expire Date can not less than Publish Date </label>";
                    });
                    return false;        
                }
            }
        });
    $('#publishDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            var applicableOn = $("input[name='txtApplicableOn']:checked").val()
            if(applicableOn == 1){
                applicableDate = $('#fixDate').val();
            } else {
                applicableDate = $('#validTo').val();
            }
            if(applicableDate!=''){
                if(!moment(applicableDate, 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){  
                    if($("#fixDate-error").length){
                            $('#fixDate-error').css("display","none");
                    } 
                    if($("#validTo-error").length){
                            $('#validTo-error').css("display","none");
                    }    
                    $("#publishDate").after(function() {
                        return "<label id='publishDate-error' class='error' for='publishDate'>Publish Date can not greater than Fix Date or Expire Date</label>";
                    });
                    return false;        
                }
            }
        });
});
function initlizeKnockout() {
    brandViewModel = new BrandViewModel();
    viewModelName = brandViewModel;
    ko.applyBindings(brandViewModel);
}
function OfferViewModel(){
    self=this;
    self.status=ko.observable(1);
    self.editContainer = ko.observable(false);
    self.listContainer = ko.observable(true);
    self.offerListContainer =ko.observable(true);
    self.activeOfferContainer =ko.observable(true);
    self.pastOfferContainer =ko.observable(false);
    self.pendingOfferContainer =ko.observable(false);
    self.upcomingOfferContainer =ko.observable(false);
    self.rejectedOfferContainer =ko.observable(false);
    self.offerAddContainer = ko.observable(false);
    self.offers=ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.offerNoData = ko.observable(false);
    self.stepTitle = ko.observable("Create Offer -> Step 1");
    self.stepSubTitle = ko.observable("");
    self.step1Container =ko.observable(true);
    self.step2Container =ko.observable(false);
    self.fixDateVisibilty =ko.observable(true);
    self.validToVisibilty =ko.observable(false);
    self.address = ko.observableArray([]);
    self.choosenAddress = ko.observableArray([]);
    self.action = ko.observable("add");
    self.action_title = ko.observable("Create Offer");
    self.offer_id = ko.observable("");
    self.addOffer = ko.observable(true);
    self.isReject = ko.observable(false);

    self.backToStep1Click = function(){
        brandViewModel.offerViewModel.step1Container(true);
        brandViewModel.offerViewModel.step2Container(false);
        resetOffers();
    }
    self.fixValueOfferCreateClick = function(){
        brandViewModel.offerViewModel.stepTitle("Create Offer -> Step 2");
        brandViewModel.offerViewModel.stepSubTitle("Fix Value Offer");
        brandViewModel.offerViewModel.step1Container(false);
        brandViewModel.offerViewModel.step2Container(true);
        $("#txtPrice").attr("disabled", false);
        $("#txtPrice").attr("placeholder", "Enter Buy price");
        resetOffers();
    };
    self.freeOfferCreateClick = function(){
        brandViewModel.offerViewModel.stepTitle("Create Offer -> Step 2");
        brandViewModel.offerViewModel.stepSubTitle("Free Offer");
        
        brandViewModel.offerViewModel.step1Container(false);
        brandViewModel.offerViewModel.step2Container(true);
        $("#txtPrice").attr("disabled", true);
        $("#txtPrice").attr("placeholder", "Rs 0");
        resetOffers();
    };
    self.cancelOfferClick = function(){
        brandViewModel.offerViewModel.offerAddContainer(false);
        brandViewModel.offerViewModel.offerListContainer(true);
        brandViewModel.offerViewModel.step1Container(true);
        brandViewModel.offerViewModel.step2Container(false);
        resetOffers();
        brandViewModel.offerViewModel.offerPendingClick();
    }
    self.rejectOfferClick  = function(data,event){
        var setting = {};
        var datavalue = {
            'id' : event.target.getAttribute('data-id'),
        };
        setting.url  = offerRejectApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            brandViewModel.offerViewModel.offerAddContainer(false);
            swal(response.message,"",'success');
            brandViewModel.offerViewModel.offerRejectedClick();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.editOfferClick= function(offer){
        brandViewModel.offerViewModel.offerAddContainer(true);
        brandViewModel.offerViewModel.isReject(true);
        brandViewModel.offerViewModel.addOffer(false);
        brandViewModel.offerViewModel.offerListContainer(false);
        brandViewModel.offerViewModel.step1Container(false);
        brandViewModel.offerViewModel.step2Container(true);
        brandViewModel.offerViewModel.action('edit');
        brandViewModel.offerViewModel.action_title("Edit Offer");
        brandViewModel.offerViewModel.offer_id(offer.id());
        if(offer.apply_on()==1){
            brandViewModel.offerViewModel.fixDateVisibilty(true);
            brandViewModel.offerViewModel.validToVisibilty(false);
            $('#fixDate').val(offer.valid_to());
        } else {
            brandViewModel.offerViewModel.validToVisibilty(true);
            brandViewModel.offerViewModel.fixDateVisibilty(false);
            $('#validTo').val(offer.valid_to());
        }
        $('#txtTitle').val(offer.name());
        $('#txtOfferDescription').val(offer.description());
        $('#txtValue').val(offer.value());
        $('#txtOfferContact').val(offer.contact());
        if(offer.price()!=0){
            $("#txtPrice").attr("disabled", false);
            $("#txtPrice").attr("placeholder", "Enter Buy price");
            $('#txtPrice').val(offer.price());
        } else {
            $("#txtPrice").attr("disabled", true);
            $("#txtPrice").attr("placeholder", "Rs 0");
        }
        $('input:radio[name="txtApplicableOn"]').filter('[value="'+offer.apply_on()+'"]').attr('checked', true);
        $('#count').val(offer.remaining_count());
        $('#publishDate').val(offer.publish_date());
        $('#notes').val(offer.notes());
        var address_array = offer.brand_address_id().split(',')
        $.each(address_array, function(index, value) {
          // brandViewModel.offerViewModel.choosenAddress.push(parseInt(value)); 
          brandViewModel.offerViewModel.choosenAddress.push(value); 
        });
    }
    self.createOfferClick = function(){
        if(!$("#form_Offer").valid()){
            return false;
        }
        var datavalue = {};
        var applicableDate = '';
        var title = $('#txtTitle').val();
        var description = $('#txtOfferDescription').val();
        var value = $('#txtValue').val();
        var price = $('#txtPrice').val();
        var count = $('#count').val();
        var publish_date = $('#publishDate').val();
        var notes = $('#notes').val();
        var applicableOn = $("input[name='txtApplicableOn']:checked").val()
            if(applicableOn == 1){
                applicableDate = $('#fixDate').val();
            } else {
                applicableDate = $('#validTo').val();
            }
        var setting = {};
        datavalue['title'] = title;
        datavalue['description'] = description;
        datavalue['value'] = value;
        datavalue['price'] = (price==""||price==null) ? 0 : price;
        datavalue['brand_address_id'] = brandViewModel.offerViewModel.choosenAddress();
        datavalue['apply_on'] = applicableOn;
        datavalue['valid_to'] = applicableDate;
        datavalue['count'] = count;
        datavalue['publish_date'] = publish_date;
        datavalue['notes'] = notes;
        datavalue['brand_id'] = brandViewModel.current_id();
        datavalue['contact'] =$('#txtOfferContact').val();
        if(brandViewModel.offerViewModel.action()=="edit"){
            offerApiPath =updateOfferApiPath
            datavalue['id'] =  brandViewModel.offerViewModel.offer_id()
        }
        else if(brandViewModel.offerViewModel.action()=="add"){
            offerApiPath = addOfferApiPath
        }

        setting.url  = offerApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            if(brandViewModel.offerViewModel.action()=="add"){
               resetOffers();
               brandViewModel.offerViewModel.offerAddContainer(false);
               brandViewModel.offerViewModel.offerPendingClick();
            }else {
                swal("Offer Updated SuccessFully","",'success');
                return false;
            }
        };
        setting.error = function(jqXHR, textStatus, error){
            if(jqXHR.status==422){
                $.each(jqXHR.responseJSON.errors,function(i,item){
                    $("#publishDate" ).after(function() {
                        return "<label id='publishDate'-error' class='error' for='publishDate'>"+item+"</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            }  
        };
        apiAjax(setting);
    };
    self.addOfferClick = function(){
        brandViewModel.offerViewModel.addOffer(true);
        brandViewModel.offerViewModel.offerAddContainer(true);
        brandViewModel.offerViewModel.step1Container(true);
        brandViewModel.offerViewModel.step2Container(false);
        brandViewModel.offerViewModel.action_title("Add Offer");
        brandViewModel.offerViewModel.offerListContainer(false);
        brandViewModel.offerViewModel.action("add");
        resetOffers();
    };    
    self.offerActiveClick = function(item, event){
        $('.offerstatus').removeClass('active'); 
        $('#activeOffer').addClass('active'); 
        brandViewModel.offerViewModel.status(1);
        getOffers();
    }
    self.offerPastClick = function(item, event){
        $('.offerstatus').removeClass('active'); 
        $('#pastOffer').addClass('active'); 
        brandViewModel.offerViewModel.status(6);
        getOffers();
    }
    self.offerPendingClick = function(item, event){
        $('.offerstatus').removeClass('active'); 
        $('#pendingOffer').addClass('active'); 
        brandViewModel.offerViewModel.status(3);
        getOffers();
    }
    self.offerUpcomingClick = function(item, event){
        $('.offerstatus').removeClass('active'); 
        $('#upcomingOffer').addClass('active'); 
        brandViewModel.offerViewModel.status(2);
        getOffers();
    }
    self.offerRejectedClick = function(item, event){
        $('.offerstatus').removeClass('active'); 
        $('#rejectedOffer').addClass('active'); 
        brandViewModel.offerViewModel.status(5);
        getOffers();
    }
    self.approveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
            'publish_date' :offer.publish_date()
        };
        setting.url  = offerApproveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            brandViewModel.offerViewModel.offerUpcomingClick();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function(offer){
        var setting = {};
        var datavalue = {
            'id' : offer.id(),
        };
        setting.url  = offerDeactiveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            getOffers();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
}
function BrandViewModel() {
    self = this;
    self.current_id = ko.observable("");
    self.current_uid = ko.observable("");
    self.limit = ko.observable(200);
    self.brands = ko.observableArray([]);
    self.selectedCategory = ko.observable("");
    self.availbleCategory = ko.observableArray([]);
    self.selectedCountry = ko.observable("");
    self.availbleCountries = ko.observableArray([]);
    self.Countries = ko.observableArray([]);
    self.States = ko.observableArray([]);
    self.Cities = ko.observableArray([]);
    self.address = ko.observableArray([]);
    self.selectedState = ko.observable("");
    self.availbleStates = ko.observableArray([]);
    self.selectedCity = ko.observable("");
    self.availbleCities = ko.observableArray([]);
    self.selectedStatus = ko.observable(0);
    self.availbleStatus = [{
            text: "Active",
            value: 1
        }, {
            text: "Deactive",
            value: 0
        }];

    self.total = ko.observable(0);
    self.brandListContainer = ko.observable(true);
    self.brandList = ko.observable(false);
    self.brandNoData = ko.observable(true);
    self.brandAddContainer = ko.observable(true);
    self.action = ko.observable("");
    self.action_title = ko.observable("");
    self.actionAddress = ko.observable("Add Address");
    self.actionBankDetail = ko.observable("Add Bank Details");
    
    self.emailBrandClick = function (brand) {
        var id = brand.id();
        $("#labelEmail").html(brand.email());
        $("#hiddenBrandId").val(id);
        $("#emailModal").modal("toggle");
        $("#txtSendPassword").val("");
    };
    
    self.emailSendClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': $("#hiddenBrandId").val(),
            "password": $("#txtSendPassword").val()
        };
        setting.url = emailSendApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#emailModal').modal('toggle');
            swal(response.message, "", "success");
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.addBrandClick = function () {

        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Add");
        brandViewModel.action_name("Add Brand");
        brandViewModel.action("add");
        getCountries(0);

    };
    self.cancelBrandClick = function(){
        location.href = brandListWebPath;
    }

    self.editBrandClick = function (brand) {
        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Edit");
        brandViewModel.action_name("Edit Brand");
        brandViewModel.actionAddress("Edit Address");
        brandViewModel.actionBankDetail("Edit Bank Details");
        brandViewModel.action("edit");
        $('#txtBrandName').val(brand.name());
        $('#txtDescription').val(brand.description());
        $('#txtEmail').val(brand.email());
        $('#txtContact').val(brand.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled", "disabled");
        $('#txtBusinessHours').val(brand.business_hours());
        $('#txtPersonName').val(brand.contact_person_name());
        $('#txtWebsiteUrl').val(brand.website_url());
        $('#txtFacebookPage').val(brand.facebook_page());
        $('#txtCommision').val(brand.commision());
        $("#locationtab").attr("data-toggle", "tab");
        $("#offerstab").attr("data-toggle", "tab");
        $("#bank_detailstab").attr("data-toggle", "tab");
        $("#payment_requesttab").attr("data-toggle", "tab");
        $("#total_salestab").attr("data-toggle", "tab");
        getAddress(brand.id());
        brandViewModel.selectedCategory(brand.categoryid());
        brandViewModel.selectedStatus(brand.status());
        brandViewModel.current_id(brand.id());
        brandViewModel.current_uid(brand.uid());
        if(brand.logo()==null){
            brandViewModel.profile(static_image);
        }else{
            brandViewModel.profile(brand.logo());
        }
        getOffers();
        getAddressList();
        getBankDetails();
        getPaymentRequest();
        getTotalSales();
    };
    self.offerListClick = function (brand) {
        brandViewModel.brandListContainer(false);
        brandViewModel.brandAddContainer(true);
        brandViewModel.action_title("Edit");
        brandViewModel.action_name("Edit Brand");
        brandViewModel.actionAddress("Edit Address");
        brandViewModel.actionBankDetail("Edit Bank Details");
        brandViewModel.action("edit");
        $('#txtBrandName').val(brand.name());
        $('#txtDescription').val(brand.description());
        $('#txtEmail').val(brand.email());
        $('#txtContact').val(brand.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled", "disabled");
        $('#txtBusinessHours').val(brand.business_hours());
        $('#txtPersonName').val(brand.contact_person_name());
        $('#txtWebsiteUrl').val(brand.website_url());
        $('#txtFacebookPage').val(brand.facebook_page());
        $("#brand_detailstab").attr("data-toggle", "tab");
        $("#bank_detailstab").attr("data-toggle", "tab");
        $("#locationtab").attr("data-toggle", "tab");
        $("#offerstab").attr("data-toggle", "tab");
        $("#brand_details").removeClass("in active");
        $("#brand_detailstab").parent().removeClass("active");
        $("#offers").addClass("in active");
        $("#offerstab").parent().addClass("active");
        getAddress(brand.id());
        brandViewModel.selectedCategory(brand.categoryid());
        brandViewModel.selectedStatus(brand.status());
        brandViewModel.current_id(brand.id());
        brandViewModel.current_uid(brand.uid());
        brandViewModel.profile(brand.logo());
        getOffers();
        getAddressList();
        getBankDetails();
        // location.href = offerListWebPath + "?id=" + brand.id();
    }
    self.removeAddress = function (index) {
        brandViewModel.fields.splice(index, 1);
    }
    self.deleteBrandClick = function (category) {
        brandViewModel.current_uid(category.uid());
        $('#deleteModal').modal('show');
    }
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': brandViewModel.current_uid(),
        };
        setting.url = deleteBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteModal').modal('hide');
            swal(response.message, "", "success");
            // getBrands();
            location.href = brandListWebPath;
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
    self.createAddressClick = function () {
        // if (!$("#form_location").valid()) {
        //     return false;
        // }
        if (brandViewModel.action() == "edit") {
            brandAddressApiPath = updateAddressApiPath;
        } else if (brandViewModel.action() == "add") {
            datavalue['password'] = $.md5(password);
            brandAddressApiPath = addAddressApiPath;
        }
        var address_fields = $("[id^=addressDiv]").length;
        var address = {};
        for (var i = 0; i < address_fields; i++) {
            address[i] = {
                "id": brandViewModel.fields()[i].items[0].id(),
                "address": brandViewModel.fields()[i].items[0].address(),
                "pincode": brandViewModel.fields()[i].items[0].pincode(),
                "state": brandViewModel.fields()[i].items[0].state(),
                "city": brandViewModel.fields()[i].items[0].city(),
                "country": brandViewModel.fields()[i].items[0].country()
            };
        }
        var setting = {};
        var datavalue = {};
        datavalue['bid'] = brandViewModel.current_id();
        datavalue['addressArray'] = address;

        setting.url = brandAddressApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            if (brandViewModel.action() == "add") {
                location.href = brandListWebPath;
                resetAll();
            } else if(brandViewModel.action() == "edit"){
                swal("Brand Location Updated SuccessFully" ,"", "success");
            }
            // getBrands();
            // location.href = brandListWebPath;
            // $("#location").removeClass("in active");
            // $("#locationtab").parent().removeClass("active");
            // $("#offers").addClass("in active");
            // $("#offerstab").parent().addClass("active");
            // resetAll();
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
                return false;
            } else {
                swal(jqXHR.responseJSON.message, "", 'error');
                return false;
            }
        };
        apiAjax(setting);
    }
    self.createBrandClick = function () {

        if (!$("#form_brand").valid()) {
            return false;
        }
        var datavalue = {};
        var brand_name = $('#txtBrandName').val();
        var category_name = brandViewModel.selectedCategory();
        var description = $('#txtDescription').val();
        var email = $('#txtEmail').val();
        var mobile = $('#txtContact').val();
        var password = $('#txtPassword').val();
        var business_hours = $('#txtBusinessHours').val();
        var person_name = $('#txtPersonName').val();
        var website_url = $('#txtWebsiteUrl').val();
        var facebook_page = $('#txtFacebookPage').val();
        var commision = $('#txtCommision').val();
        if(website_url != ""){

        }
        if(facebook_page != ""){

        }
        if (brandViewModel.action() == "edit") {
            datavalue['id'] = brandViewModel.current_id();
            datavalue['uid'] = brandViewModel.current_uid();
            brandApiPath = updateBrandApiPath;
        } else if (brandViewModel.action() == "add") {
            datavalue['password'] = $.md5(password);
            brandApiPath = addBrandApiPath;
        }
        var setting = {};
        datavalue['brand_name'] = brand_name;
        datavalue['category_name'] = category_name;
        datavalue['description'] = description;
        datavalue['email'] = email;
        datavalue['contact'] = mobile;
        datavalue['business_hours'] = business_hours;
        datavalue['person_name'] = person_name;
        datavalue['website_url'] = website_url;
        datavalue['facebook_page'] = facebook_page;
        datavalue['commision'] = commision;
        datavalue['status'] = brandViewModel.selectedStatus();
        datavalue['image'] = brandViewModel.imageUri();
        setting.url = brandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            // getBrands();
            if (brandViewModel.action() == "add") {
                location.href = brandListWebPath;
                resetAll();
            } else {
                swal("Brand Details Updated SuccessFully" ,"", "success");
            }
            // brandViewModel.current_id(response.data.id);
            // $("#locationtab").attr("data-toggle", "tab");
            // $("#offerstab").attr("data-toggle", "tab");
            // $("#bank_detailstab").attr("data-toggle", "tab");
            // $("#location").addClass("in active");
            // $("#locationtab").parent().addClass("active");
            // $("#brand_details").removeClass("in active");
            // $("#brand_detailstab").parent().removeClass("active");
            // getOffers();
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
                return false;
            } else {
                swal(jqXHR.responseJSON.message, "", 'error');
                return false;
            }
        };
        apiAjax(setting);
    };
    self.activeClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': brand.uid(),
        };
        setting.url = activeBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", 'success');
            location.href = brandListWebPath;
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function (brand) {
        var setting = {};
        var datavalue = {
            'id': brand.uid(),
        };
        setting.url = deactiveBrandApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", 'success');
            location.href = brandListWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
    self.countryChanged = function (c, event) {
        getStates($("#" + event.target.id).val(), event.target.getAttribute('data-id'));
    }
    self.stateChanged = function (c, event) {
        getCities($("#" + event.target.id).val(), event.target.getAttribute('data-id'));
    }
    // brandViewModel.fields()[1].items[0].country()
    self.fields = ko.observableArray([
        {
            items: [
                {
                    address: ko.observable(""),
                    pincode: ko.observable(""),
                    country: ko.observable(""),
                    state: ko.observable(""),
                    city: ko.observable(""),
                    id: ko.observable("")
                }
            ]
        },
    ]);
    self.index = ko.observable(0);
    self.addLocationClick = function () {

        brandViewModel.fields.push({items: [{id: ko.observable(""),address: ko.observable(""), pincode: ko.observable(""), country: ko.observable(""), state: ko.observable(""),
                    city: ko.observable("")}
            ]
        });
        getCountries(brandViewModel.index(brandViewModel.index() + 1));
    }
    
    self.bank_detail_id = ko.observable("");
    self.addBankDetails = function () {
        if (!$("#form_bank").valid()) {
            return false;
        }
        var setting = {};
        var datavalue = {};
        datavalue['brand_id'] = brandViewModel.current_id();
        datavalue['name'] = brandViewModel.bank_name();
        datavalue['ifsc'] = brandViewModel.ifsc();
        datavalue['account_number'] = brandViewModel.account_number();
        datavalue['branch_details'] = brandViewModel.branch_details();
        datavalue['account_type'] = brandViewModel.account_type();
        if(brandViewModel.bank_detail_id()==""){
            bankApiPath = addBankDetailApiPath;
        } else{
            datavalue['bank_detail_id'] = brandViewModel.bank_detail_id();
            bankApiPath = updateBankDetailApiPath;
        }
        setting.url = bankApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal("Bank Details Updated SuccessFully","","success");
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
                return false;
            } else {
                swal(jqXHR.responseJSON.message, "", 'error');
                return false;
            }
        };
        apiAjax(setting);
    };
    self.account_type = ko.observable("");
    self.branch_details = ko.observable("");
    self.ifsc = ko.observable("");
    self.account_number = ko.observable("");
    self.bank_name = ko.observable("");
    self.active_count = ko.observable("");
    self.active_offer_count = ko.observable("");
    self.redeem_count = ko.observable("");
    
    /*Image Upload*/
    self.imageUri = ko.observable("");
    self.imagePath = ko.observable(path);
    self.profile = ko.observable("");

    

    /*payment request*/
    self.paymentRequests = ko.observableArray([]);
    self.paymentRequestsTotal = ko.observable(0);
    self.paymentListContainer = ko.observable(true);
    self.paymentList = ko.observable(false);
    self.paymentNoData = ko.observable(true);
    self.paymentAddContainer = ko.observable(false);
    self.activePaymentClick = function(request) {
        var setting = {};
        var datavalue = {
            'id': request.id(),
            'brand_uid': request.user_id(),
            'amount' :request.withdraw_amount(),
        };
        setting.url = paymentPaidApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            window.location.reload();
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
    apiAjax(setting);
    }
    /*Totsl sales*/
    self.totalSales = ko.observableArray([]);
    self.totalSalesTotal = ko.observable(0);
    self.totalSalesListContainer = ko.observable(true);
    self.totalSalesList = ko.observable(false);
    self.totalSalesNoData = ko.observable(true);
    /*offer*/
    self.offerViewModel = new OfferViewModel;

}
function getPaymentRequest() {
    var setting = {};
    var datavalue = {
        'limit': brandViewModel.limit(),
        'user_id' : brandViewModel.current_uid,
    };
    setting.url = paymentListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        var payment_list = response.data;
        if (payment_list.length > 0) {
            brandViewModel.paymentRequests([]);
            $.each(payment_list, function (i, item) {
                brandViewModel.paymentRequests.push(new TransactionModel(item));
            });

            $("#payment_tbl").DataTable( { responsive: true } );
            brandViewModel.paymentListContainer(true);
            brandViewModel.paymentAddContainer(false);
            brandViewModel.paymentList(true);
            brandViewModel.paymentNoData(false);

        } else {
            brandViewModel.paymentListContainer(true);
            brandViewModel.paymentAddContainer(false);
            brandViewModel.paymentList(false);
            brandViewModel.paymentNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getTotalSales() {
    var setting = {};
    var datavalue = {
        'limit': brandViewModel.limit(),
        'brand_id':brandViewModel.current_id(),
    };
    setting.url = totalSalesApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        var payment_list = response.data.data;
        if (payment_list.length > 0) {
            brandViewModel.totalSales([]);
            $.each(payment_list, function (i, item) {
                brandViewModel.totalSales.push(new TotalSalesModel(item));
            });

            $("#sales_tbl").DataTable( { responsive: true } );
            brandViewModel.totalSalesListContainer(true);
            brandViewModel.totalSalesList(true);
            brandViewModel.totalSalesNoData(false);

        } else {
            brandViewModel.totalSalesListContainer(true);
            brandViewModel.totalSalesList(false);
            brandViewModel.totalSalesNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function resetAll() {
    $('#txtBrandName').val("");
    $('#txtCategory').val("");
    $('#txtDescription').val("");
    $('#txtEmail').val("");
    $('#txtContact').val("");
    $('#txtPassword').val("");
    $('#txtPassword').removeAttr("disabled");
    $('#txtBusinessHours').val("");
    $('#txtPersonName').val("");
    $('#txtWebsiteUrl').val("");
    $('#txtFacebookPage').val("");
    $('#txtAddress').val("");
    $('#txtPincode').val("");
}
function getBrands() {
    var setting = {};
    var datavalue = {
        'limit': brandViewModel.limit(),
    };
    setting.url = brandListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        brandViewModel.active_count(response.active_brand_count);
        brandViewModel.active_offer_count(response.active_offer_count);
        brandViewModel.redeem_count(response.coupon_redeem_count);
        var brannd_list = response.data.data;
        if (brannd_list.length > 0) {
            brandViewModel.brands([]);
            $.each(brannd_list, function (i, item) {
                brandViewModel.brands.push(new BrandModel(item));
            });

            $("#brand_tbl").DataTable({responsive: true});
            brandViewModel.brandListContainer(true);
            brandViewModel.brandAddContainer(false);
            brandViewModel.brandList(true);
            brandViewModel.brandNoData(false);

        } else {
            brandViewModel.brandListContainer(true);
            brandViewModel.brandAddContainer(false);
            brandViewModel.brandList(false);
            brandViewModel.brandNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getCategories() {
    var setting = {};
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit': 1000, 'type': 1, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var category_list = response.data.data;
        if (category_list.length > 0) {
            $.each(category_list, function (i, item) {
                brandViewModel.availbleCategory.push(new availableCategory(item.id, item.name));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getCountries(index) {

    var setting = {};
    setting.url = countryListApiPath;
    setting.type = 'GET';
    setting.data = {'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var country_list = response.data.data;
        // brandViewModel.availbleCountries([]);
        if (country_list.length > 0) {
            brandViewModel.availbleCountries()[index] = ko.observableArray([]);
            $.each(country_list, function (i, item) {
                brandViewModel.availbleCountries()[index].push(new availbleCountries(item.id, item.name));
            });
            brandViewModel.Countries.splice(index, 0, brandViewModel.availbleCountries()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getStates(country, index) {
    var setting = {};
    setting.url = stateListApiPath;
    setting.type = 'GET';
    setting.data = {'country': country, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var state_list = response.data.data;
        //brandViewModel.availbleStates([]);
        if (state_list.length > 0) {
            brandViewModel.availbleStates()[index] = ko.observableArray([]);
            $.each(state_list, function (i, item) {
                brandViewModel.availbleStates()[index].push(new availbleStates(item.id, item.name));
            });
            brandViewModel.States.splice(index, 0, brandViewModel.availbleStates()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function getCities(state, index) {

    var setting = {};
    setting.url = cityListApiPath;
    setting.type = 'GET';
    setting.data = {'state': state, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {

        var city_list = response.data.data;
        // brandViewModel.availbleCities([]);
        if (city_list.length > 0) {
            brandViewModel.availbleCities()[index] = ko.observableArray([]);
            $.each(city_list, function (i, item) {
                brandViewModel.availbleCities()[index].push(new availbleCities(item.id, item.name));
            });
            brandViewModel.Cities.splice(index, 0, brandViewModel.availbleCities()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
$('#fileInput').change(function(){
    var file = $('#fileInput').prop('files')[0];
    var ImageURL;
    var FR = new FileReader();
    FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        
        brandViewModel.imageUri(realData);
    }, false);
    if (file) {
        FR.readAsDataURL(file);
    }
});
function availbleCountries(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function availbleStates(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function availbleCities(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getAddress(bid) {
    var setting = {};
    var datavalue = {
        'bid': bid,
    };
    setting.url = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var address_list = response.data.data;
        if (address_list.length > 0) {
            $.each(address_list, function (i, item) {
                brandViewModel.address.push(new availableAddress(item.id, item.address, item.city, item.state, item.country, item.pincode, item.city_id, item.state_id, item.country_id));
                brandViewModel.fields.push({items: [{id: ko.observable(""),address: ko.observable(""), pincode: ko.observable(""), country: ko.observable(""), state: ko.observable(""),
                            city: ko.observable("")}
                    ]
                });
            });
            brandViewModel.fields.pop();
            var address_fields = brandViewModel.fields().length;
            for (var i = 0; i < address_fields; i++) {
                getCountries(i);
                getStates(brandViewModel.address()[i].country_id(), i);
                getCities(brandViewModel.address()[i].state_id(), i);
                brandViewModel.fields()[i].items[0].id(brandViewModel.address()[i].value()),
                brandViewModel.fields()[i].items[0].address(brandViewModel.address()[i].address()),
                brandViewModel.fields()[i].items[0].pincode(brandViewModel.address()[i].pincode()),
                brandViewModel.fields()[i].items[0].state(brandViewModel.address()[i].state_id()),
                brandViewModel.fields()[i].items[0].city(brandViewModel.address()[i].city_id()),
                brandViewModel.fields()[i].items[0].country(brandViewModel.address()[i].country_id())
            }

        } else {
            getCountries(0);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k, address, city, state, country, pincode, city_id, state_id, country_id) {
    var self = this;
    self.value = ko.observable(k);
    self.address = ko.observable(address);
    self.city = ko.observable(city);
    self.state = ko.observable(state);
    self.country = ko.observable(country);
    self.city_id = ko.observable(city_id);
    self.state_id = ko.observable(state_id);
    self.country_id = ko.observable(country_id);
    self.pincode = ko.observable(pincode);
}
function getOffers() {

    var setting = {};
    var datavalue = {};
    datavalue['bid'] = brandViewModel.current_id();  
    datavalue['status'] = brandViewModel.offerViewModel.status();    
    setting.url  = offerListApiPath;

    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        brandViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if(brandViewModel.offerViewModel.status() == 1){
            if ($.fn.DataTable.isDataTable("#active_offer_tbl")) {
               $('#active_offer_tbl').DataTable().clear().destroy();
            } 

        }else if(brandViewModel.offerViewModel.status() == 2){
            if ($.fn.DataTable.isDataTable("#upcoming_offer_tbl")) {
                $('#upcoming_offer_tbl').DataTable().clear().destroy();
            } 
           
        }else if(brandViewModel.offerViewModel.status() == 3){
            if ($.fn.DataTable.isDataTable("#pending_offer_tbl")) {
               $('#pending_offer_tbl').DataTable().clear().destroy();
            } 
            
        }else if(brandViewModel.offerViewModel.status() == 5){
            if ($.fn.DataTable.isDataTable("#rejected_offer_tbl")) {
                $('#rejected_offer_tbl').DataTable().clear().destroy();
            } 
           
        }else if(brandViewModel.offerViewModel.status() == 6){
            if ($.fn.DataTable.isDataTable("#past_offer_tbl")) {
                $('#past_offer_tbl').DataTable().clear().destroy();
            } 
        }
        if (offer_list.length > 0) {
            brandViewModel.offerViewModel.offers([]);
            $.each(offer_list, function (i, item) {
                brandViewModel.offerViewModel.offers.push(new OfferModel(item));
            });
            brandViewModel.offerViewModel.offerListContainer(true);
            brandViewModel.offerViewModel.offerList(true);
            brandViewModel.offerViewModel.offerNoData(false);
        } else {
            brandViewModel.offerViewModel.offerListContainer(true);
            brandViewModel.offerViewModel.offerList(false);
            brandViewModel.offerViewModel.offerNoData(false);
        }
        if(brandViewModel.offerViewModel.status() == 1){
                brandViewModel.offerViewModel.activeOfferContainer(true);
                brandViewModel.offerViewModel.pendingOfferContainer(false);
                brandViewModel.offerViewModel.pastOfferContainer(false);
                brandViewModel.offerViewModel.upcomingOfferContainer(false);
                brandViewModel.offerViewModel.rejectedOfferContainer(false);
                $('#active_offer_tbl').DataTable();
            }else if(brandViewModel.offerViewModel.status() == 2){
                brandViewModel.offerViewModel.activeOfferContainer(false);
                brandViewModel.offerViewModel.pendingOfferContainer(false);
                brandViewModel.offerViewModel.pastOfferContainer(false);
                brandViewModel.offerViewModel.upcomingOfferContainer(true);
                brandViewModel.offerViewModel.rejectedOfferContainer(false);
                $('#upcoming_offer_tbl').DataTable();
            }else if(brandViewModel.offerViewModel.status() == 3){
                brandViewModel.offerViewModel.activeOfferContainer(false);
                brandViewModel.offerViewModel.pendingOfferContainer(true);
                brandViewModel.offerViewModel.pastOfferContainer(false);
                brandViewModel.offerViewModel.upcomingOfferContainer(false);
                brandViewModel.offerViewModel.rejectedOfferContainer(false);
                $('#pending_offer_tbl').DataTable();
            }else if(brandViewModel.offerViewModel.status() == 5){
              
                brandViewModel.offerViewModel.activeOfferContainer(false);
                brandViewModel.offerViewModel.pendingOfferContainer(false);
                brandViewModel.offerViewModel.pastOfferContainer(false);
                brandViewModel.offerViewModel.upcomingOfferContainer(false);
                brandViewModel.offerViewModel.rejectedOfferContainer(true);
                $('#rejected_offer_tbl').DataTable();
            }else if(brandViewModel.offerViewModel.status() == 6){
                brandViewModel.offerViewModel.activeOfferContainer(false);
                brandViewModel.offerViewModel.pendingOfferContainer(false);
                brandViewModel.offerViewModel.pastOfferContainer(true);
                brandViewModel.offerViewModel.upcomingOfferContainer(false);
                brandViewModel.offerViewModel.rejectedOfferContainer(false);
                $('#past_offer_tbl').DataTable();
            }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function getBankDetails() {
    var setting = {};
    var datavalue = {};
    datavalue['bid'] = brandViewModel.current_id();
    setting.url = getBankDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.data) {
            brandViewModel.bank_name(response.data.name);
            brandViewModel.account_type(response.data.account_type);
            brandViewModel.account_number(response.data.account_number);
            brandViewModel.ifsc(response.data.ifsc);
            brandViewModel.branch_details(response.data.branch_details);
            brandViewModel.bank_detail_id(response.data.id);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getAddressList(){
    var setting = {};
    var datavalue = {
        'bid' : brandViewModel.current_id(),
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            $.each(address_list, function(i,item) {
                brandViewModel.offerViewModel.address.push(new availableAddressList(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddressList(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }
 function resetOffers(){
    $('#txtTitle').val("");
    $('#txtOfferDescription').val("");
    $('#txtValue').val("");
    $('#txtPrice').val("");
    $('#validTo').val("");
    $('#fixDate').val("");
    $('#count').val("");
    $('#publishDate').val("");
    $('#notes').val("");
    $('#txtOfferContact').val("");
    $("input[name=address]").prop("checked", false);
}
$('input[type=radio][name=txtApplicableOn]').change(function() {
    if(this.value == 1){
        brandViewModel.offerViewModel.fixDateVisibilty(true);
        brandViewModel.offerViewModel.validToVisibilty(false);
    } else {
        brandViewModel.offerViewModel.fixDateVisibilty(false);
        brandViewModel.offerViewModel.validToVisibilty(true);
    }       
});
