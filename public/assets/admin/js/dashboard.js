var viewModelName;
var statisticsViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#dashboardTab').addClass('active');
	initializeKnockout();
    getBrandStatistics();
});

function initializeKnockout(){
	statisticsViewModel = new StatisticsViewModel();
	viewModelName  = statisticsViewModel;
	ko.applyBindings(statisticsViewModel);
}

function StatisticsViewModel(){
	self = this;
    self.active_employee = ko.observable(0);
    self.active_brand = ko.observable(0);
    self.active_corporate = ko.observable(0);
    self.active_offers = ko.observable(0);
    self.pending_offers = ko.observable(0);
    self.upcoming_offers = ko.observable(0);
    self.total_sales = ko.observable(0);
    self.today_redeem = ko.observable(0);
    self.today_order = ko.observable(0);
    self.payment_request = ko.observable(0);
    self.open_tickets = ko.observable(0);
    self.closed_tickets = ko.observable(0);
   
}

function getBrandStatistics(){
	var setting   = {};
    var datavalue = {};
    setting.url    = statisticsApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        statisticsViewModel.active_employee(response.data.active_employee);
        statisticsViewModel.active_brand(response.data.active_brand);
        statisticsViewModel.active_corporate(response.data.active_corporate);
        statisticsViewModel.active_offers(response.data.active_offers);
        statisticsViewModel.pending_offers(response.data.pending_offers);
        statisticsViewModel.upcoming_offers(response.data.upcoming_offers);
        statisticsViewModel.today_redeem(response.data.today_redeem);
        statisticsViewModel.today_order(response.data.today_order);
        statisticsViewModel.total_sales(response.data.total_sales);
        statisticsViewModel.open_tickets(response.data.open_tickets);
        statisticsViewModel.closed_tickets(response.data.closed_tickets);
        statisticsViewModel.payment_request(response.data.payment_request);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
