var viewModelName;
var locationViewModel;
var countryApiPath;
var stateApiPath;
var cityApiPath;
var deleteApiPath;
var activeApiPath;
var deactiveApiPath;
$(document).ready(function(){
    initlizeKnockout();
    $('.has-submenu').removeClass('active');
    $('#locationTab').addClass('active');
    if(type=='country'){
        locationViewModel.deleteTitle("Delete Country");
        locationViewModel.deleteContent(" Are you sure to delete country ?");
        $('#locationCountryTab').addClass('active');
        $('#locationStateTab').removeClass('active');
        $('#locationCityTab').removeClass('active');
        activeApiPath = activeCountryApiPath;
        deactiveApiPath = deactiveCountryApiPath;
        locationWebPath = country;
        
    }
    else if(type=='state'){
        locationViewModel.deleteTitle("Delete State");
        locationViewModel.deleteContent(" Are you sure to delete State ?");
        $('#locationStateTab').addClass('active');
        $('#locationCountryTab').removeClass('active');
        $('#locationCityTab').removeClass('active');
        activeApiPath = activeStateApiPath;
        deactiveApiPath = deactiveStateApiPath;
        locationWebPath = state;
    }
    else if(type=='city'){
        locationViewModel.deleteTitle("Delete City");
        locationViewModel.deleteContent(" Are you sure to delete City ?");
        $('#locationCityTab').addClass('active');
        $('#locationCountryTab').removeClass('active');
        $('#locationStateTab').removeClass('active');
        activeApiPath = activeCityApiPath;
        deactiveApiPath = deactiveCityApiPath;
        locationWebPath = city;
    }
    
    getCountries();
    getStates();
    getCities(); 

    $("#form_country").validate({
        rules:{
            txtCountryName : "required",   
            txtStatus : "required",   
        },
        messages: {
            txtCountryName: "Please enter a Country name",
            txtStatus: "Please Select status",
        },
    });
    $("#form_state").validate({
        rules:{
            txtStateName : "required",   
            txtCountry : "required",   
            txtStatus : "required",   
        },
        messages: {
            txtStateName: "Please enter a state name",
            txtCountry: "Please select country",
            txtStatus: "Please Select status",
        },
    });
    $("#form_city").validate({
        rules:{
            txtCityName : "required",   
            txtState : "required",   
            txtStatus : "required",   
        },
        messages: {
            txtCityName: "Please enter a city name",
            txtState: "Please select state",
            txtStatus: "Please Select status",
        },
    });
});
function initlizeKnockout(){

    locationViewModel = new LocationViewModel();
    viewModelName = locationViewModel;
    ko.applyBindings(locationViewModel);
}
function getCountries(){
    var setting = {};
    setting.url  = countryListApiPath;
    setting.type = 'GET';
    setting.data = {};
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        locationViewModel.total(response.data.total);
        var country_list = response.data.data;
        if(country_list.length>0){
            locationViewModel.countries([]);
            $.each(country_list, function(i,item) {
                locationViewModel.countries.push(new CountryModel(item));
                locationViewModel.availbleCountries.push(new availbleCountries(item.id,item.name));
            });

            $("#country_tbl").DataTable( { responsive: true } );
            locationViewModel.countryListContainer(true);
            locationViewModel.countryAddContainer(false);
            locationViewModel.countryList(true);
            locationViewModel.countryNoData(false);

        } else {
            locationViewModel.countryListContainer(true);
            locationViewModel.countryAddContainer(false);
            locationViewModel.countryList(false);
            locationViewModel.countryNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getStates(){
    
    var setting = {};
    setting.url  = stateListApiPath;
    setting.type = 'GET';
    setting.data = {};
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        locationViewModel.total(response.data.total);
        var state_list = response.data.data;
        if(state_list.length>0){
            locationViewModel.states([]);
            $.each(state_list, function(i,item) {
                locationViewModel.states.push(new StateModel(item));
                locationViewModel.availbleStates.push(new availbleStates(item.id,item.name));
            });

            $("#state_tbl").DataTable( { responsive: true } );
            locationViewModel.stateListContainer(true);
            locationViewModel.stateAddContainer(false);
            locationViewModel.stateList(true);
            locationViewModel.stateNoData(false);

        } else {
            locationViewModel.stateListContainer(true);
            locationViewModel.stateAddContainer(false);
            locationViewModel.stateList(false);
            locationViewModel.stateNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getCities(){

    var setting = {};
    setting.url  = cityListApiPath;
    setting.type = 'GET';
    setting.data = {};
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        locationViewModel.total(response.data.total);
        var city_list = response.data.data;
        if(city_list.length>0){
            locationViewModel.cities([]);
            $.each(city_list, function(i,item) {
                locationViewModel.cities.push(new CityModel(item));
            });

            $("#city_tbl").DataTable( { responsive: true } );
            locationViewModel.cityListContainer(true);
            locationViewModel.cityAddContainer(false);
            locationViewModel.cityList(true);
            locationViewModel.cityNoData(false);

        } else {
            locationViewModel.cityListContainer(true);
            locationViewModel.cityAddContainer(false);
            locationViewModel.cityList(false);
            locationViewModel.cityNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function LocationViewModel(){
    self = this;
    self.current_id = ko.observable(""); 
    self.countries = ko.observableArray([]);
    self.states = ko.observableArray([]);
    self.cities = ko.observableArray([]);
    self.selectedStatus= ko.observable(1);
    self.availbleStatus= [{
        text: "Active",
        value: 1
      }, {
        text: "Deactive",
        value: 0
      }];
    self.total  = ko.observable(0);
    self.action = ko.observable("");
    self.deleteTitle  = ko.observable("");
    self.deleteContent  = ko.observable("");
    self.action_title = ko.observable("");
    self.countryListContainer =  ko.observable(true);
    self.countryList =  ko.observable(false);
    self.countryNoData = ko.observable(true);
    self.countryAddContainer =ko.observable(false);
    self.availbleCountries= ko.observableArray([]);
    self.selectedCountry= ko.observable();
    self.stateListContainer =  ko.observable(true);
    self.stateList =  ko.observable(false);
    self.stateNoData = ko.observable(true);
    self.stateAddContainer =ko.observable(false);
    self.availbleStates= ko.observableArray([]);
    self.selectedState= ko.observable();
    self.cityListContainer =  ko.observable(true);
    self.cityList =  ko.observable(false);
    self.cityNoData = ko.observable(true);
    self.cityAddContainer =ko.observable(false);
    self.addCountryClick = function(){
        locationViewModel.countryListContainer(false);
        locationViewModel.countryAddContainer(true);
        locationViewModel.action_title("Add");
        locationViewModel.action_name("Add Country");
        locationViewModel.action("add");
    };
    self.addStateClick = function(){
        locationViewModel.stateListContainer(false);
        locationViewModel.stateAddContainer(true);
        locationViewModel.action_title("Add");
        locationViewModel.action_name("Add State");
        locationViewModel.action("add");
    };
    self.addCityClick = function(){
        locationViewModel.cityListContainer(false);
        locationViewModel.cityAddContainer(true);
        locationViewModel.action_title("Add");
        locationViewModel.action_name("Add City");
        locationViewModel.action("add");
    };
    self.editCountryClick =function(country){
        locationViewModel.countryListContainer(false);
        locationViewModel.countryAddContainer(true);
        locationViewModel.action_title("Edit");
        locationViewModel.action_name("Edit Country");
        locationViewModel.action("edit");
        locationViewModel.current_id(country.id());
        $('#txtCountryName').val(country.name());
    };
    self.editStateClick =function(state){
        locationViewModel.stateListContainer(false);
        locationViewModel.stateAddContainer(true);
        locationViewModel.action_title("Edit");
        locationViewModel.action_name("Edit State");
        locationViewModel.action("edit");
        locationViewModel.current_id(state.id());
        $('#txtStateName').val(state.name());
        locationViewModel.selectedCountry(state.country_id());
        locationViewModel.selectedStatus(state.status());
    };
    self.editCityClick =function(city){
        locationViewModel.cityListContainer(false);
        locationViewModel.cityAddContainer(true);
        locationViewModel.action_title("Edit");
        locationViewModel.action_name("Edit city");
        locationViewModel.action("edit");
        locationViewModel.current_id(city.id());
        $('#txtCityName').val(city.name());
        locationViewModel.selectedState(city.state_id());
        locationViewModel.selectedStatus(city.status());
    };
    self.cancelCityClick = function(){
        locationViewModel.cityListContainer(true);
        locationViewModel.cityAddContainer(false);
        resetCityAll();
    }
    self.cancelCountryClick = function(){
        locationViewModel.countryListContainer(true);
        locationViewModel.countryAddContainer(false);
        resetCountryAll();
    }
    self.cancelStateClick = function(){
        locationViewModel.stateListContainer(true);
        locationViewModel.stateAddContainer(false);
        resetStateAll();
    }

    self.deleteCountryClick = function(country){
        locationViewModel.current_id(country.id());
        deleteApiPath = deleteCountryApiPath;
        $('#deleteModal').modal('show');
    };
    self.deleteStateClick = function(state){
        locationViewModel.current_id(state.id());
        deleteApiPath = deleteStateApiPath;
        $('#deleteModal').modal('show');
    };
    self.deleteCityClick = function(city){
        locationViewModel.current_id(city.id());
        deleteApiPath = deleteCityApiPath;
        $('#deleteModal').modal('show');
    };
    self.deleteConfirm = function(){
        var setting = {};
        var datavalue = {
            'id' : locationViewModel.current_id(),
        };
        setting.url  = deleteApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            $('#deleteModal').modal('hide');
            swal(response.message,"","success");
            // if(type =='country'){
            //     // getCountries();
            // } else if(type=='state'){
            //     // getStates();
            // } else if(type =='city'){
            //     // getCities();
            // }
            location.href = locationWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    };
    self.createStateClick = function(){

        if(!$("#form_state").valid()){
            return false;
        }
            
        var datavalue = {};
        var state_name = $('#txtStateName').val();
        

        if(locationViewModel.action()=="edit"){
            datavalue['id'] = locationViewModel.current_id();
            stateApiPath = updateStateApiPath;
        } else if(locationViewModel.action()=="add"){
            stateApiPath = addStateApiPath;
        }
        var setting = {};
        datavalue['txtStateName'] = state_name;
        datavalue['txtCountry'] = locationViewModel.selectedCountry();
        datavalue['status'] = locationViewModel.selectedStatus();
        setting.url  = stateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           // getStates();
           location.href = locationWebPath;
           resetStateAll();
        };
        setting.error = function(jqXHR, textStatus, error){
                if(jqXHR.status==422){
                    
                    $.each(jqXHR.responseJSON.errors,function(i,item){
                        $("#"+i ).after(function() {
                            return "<label id='"+i+"'-error' class='error' for='"+i+"'>"+item+"</label>";
                        });
                    })
                } else {
                    swal(jqXHR.responseJSON.message,"","error");
                    return false;
                }
            };
        apiAjax(setting);
    
    };
    self.createCityClick = function(){


        if(!$("#form_city").valid()){
            return false;
        }
            
        var datavalue = {};
        var city_name = $('#txtCityName').val();
        

        if(locationViewModel.action()=="edit"){
            datavalue['id'] = locationViewModel.current_id();
            cityApiPath = updateCityApiPath;
        } else if(locationViewModel.action()=="add"){
            cityApiPath = addCityApiPath;
        }
        var setting = {};
        datavalue['txtCityName'] = city_name;
        datavalue['txtState'] = locationViewModel.selectedState();
        datavalue['status'] = locationViewModel.selectedStatus();
        setting.url  = cityApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           // getCities();
           location.href = locationWebPath;
           resetStateAll();
        };
        setting.error = function(jqXHR, textStatus, error){
                if(jqXHR.status==422){
                    
                    $.each(jqXHR.responseJSON.errors,function(i,item){
                        $("#"+i ).after(function() {
                            return "<label id='"+i+"'-error' class='error' for='"+i+"'>"+item+"</label>";
                        });
                    })
                } else {
                    swal(jqXHR.responseJSON.message,"","error");
                    return false;
                }
            };
        apiAjax(setting);
    
    
    }
    self.createCountryClick = function(){
        if(!$("#form_country").valid()){
            return false;
        }
            
        var datavalue = {};
        var country_name = $('#txtCountryName').val();
        

        if(locationViewModel.action()=="edit"){
            datavalue['id'] = locationViewModel.current_id();
            countryApiPath = updateCountryApiPath;
        } else if(locationViewModel.action()=="add"){
            countryApiPath = addCountryApiPath;
        }
        var setting = {};
        datavalue['txtCountryName'] = country_name;
        datavalue['status'] = locationViewModel.selectedStatus();
        setting.url  = countryApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           // getCountries();
           location.href = locationWebPath;
           resetCountryAll();
        };
        setting.error = function(jqXHR, textStatus, error){
                if(jqXHR.status==422){
                    
                    $.each(jqXHR.responseJSON.errors,function(i,item){
                        $("#"+i ).after(function() {
                            return "<label id='"+i+"'-error' class='error' for='"+i+"'>"+item+"</label>";
                        });
                    })
                } else {
                    swal(jqXHR.responseJSON.message,"","error");
                    return false;
                }
            };
        apiAjax(setting);
    };
    self.activeClick =function(data){
        var setting = {};
        var datavalue = {
            'id' : data.id(),
        };
        setting.url  = activeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // if(type =='country'){
            //         getCountries();
            // } else if(type=='state'){
            //             getStates();
            // } else if(type =='city'){
            //         getCities();
            // }
            location.href = locationWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick =function(data){
        var setting = {};
        var datavalue = {
            'id' : data.id(),
        };
        setting.url  = deactiveApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"","success");
            // if(type =='country'){
            //         getCountries();
            // } else if(type=='state'){
            //             getStates();
            // } else if(type =='city'){
            //         getCities();
            // }
            location.href = locationWebPath;
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
}
function resetCountryAll(){
    $('#txtCountryName').val("");
}
function resetStateAll(){
    $('#txtStateName').val("");
    locationViewModel.selectedCountry("");
}
function resetCityAll(){
    $('#txtCityName').val("");
    locationViewModel.selectedState("");
}
function availbleCountries(k,v){

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function availbleStates(k,v){

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}