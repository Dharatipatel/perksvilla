$('document').ready(function () {
    updateNotification();
    getSupportMessageCount();
    setInterval(function(){
        updateNotification();
        getSupportMessageCount();
    }, notification_update_interval * 1000);
});
function  getSupportMessageCount() {
    var setting = {};
    var dataValue = {id: id};

    setting.url = message_url;
    setting.type = 'GET';
    setting.data = dataValue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.error) {
            console.log(response);
        } else {
            $('#messages_count').html(response.data);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        console.log(error);
        return false;
    };
    apiAjax(setting);
}
function updateNotification(){
    var setting = {};
    var dataValue = {id: id};

    setting.url = notification_url;
    setting.type = 'POST';
    setting.data = dataValue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.error) {
            console.log(response);
        } else {
            $('#top_header_container').html(response.data);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        console.log(error);
        return false;
    };
    apiAjax(setting);
}
function readNotification(){

    var setting = {};
    var dataValue = {user_id: id};

    setting.url = notification_read_url;
    setting.type = 'POST';
    setting.data = dataValue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.error) {
            console.log(response);
        } else {
            updateNotification();
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        console.log(error);
        return false;
    };
    apiAjax(setting);

}