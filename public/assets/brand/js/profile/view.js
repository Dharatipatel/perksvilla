var viewModelName;
var profileViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#myprofiledropdown').addClass('show');
    $('#profileTab').addClass('active');
    if(type =='edit'){
        $('#editProfileTab').addClass('active');
    } else if(type=='view'){
        $('#viewProfileTab').addClass('active');
    }
	initializeKnockout();
	getBrand();
	getCategories();
    getCities(profileViewModel.index());
    getAddress(bid);
    var rules = {
            
            txtBrandName : "required",
            txtCategory : "required",
            txtDescription : "required",
            txtEmail: {
                'required' :true,
                'email' :true,
            },
            txtContact: {
                'required' :true,
                'digits': true
            },
            txtPassword : "required",
            txtBusinessHours: {
                'required' :true,
            },
            txtPersonName : "required",
            txtAddress : "required",
            txtPincode : "required",
            txtStatus : "required",
            txtAddress0 : "required",
            txtPincode0 : "required",
            txtCity0 : "required",
        };
    var messages = {
            txtBrandName: "Please enter a brand name",
            txtCategory: "Please select  a category",
            txtDescription: "Please enter  a description",
            txtEmail: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter a valid email address'
            },
            txtContact: {
                'required' : 'Please Enter mobile',
                'digits' : 'Mobile should be only in digits'
            },
            txtPassword: "Please enter password",
            txtPersonName: "Please enter person name",
            txtAddress: "Please Enter address",
            txtPincode: "Please Enter pincode",
            txtStatus: "Please select  a Status",
            txtAddress0 : "Address is required",
            txtPincode0 : "Pincode is required",
            txtCity0 : "City is required",
        };
    $("#form_brand").validate({
        rules:rules ,
        messages: messages,
    });
});

$('#fileInput').change(function(){
   var file = $('#fileInput').prop('files')[0];
   var ImageURL;
   var FR = new FileReader();
   FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        // var blob = b64toBlob(    , contentType);
        profileViewModel.imageUri(realData);
   }, false);
   if (file) {
        FR.readAsDataURL(file);
   }
});
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function getBrand(){
	var setting = {};
	setting.url  = viewProfileApiPath.replace("id",id);
	setting.type = 'GET';
	setting.data = {};
	setting.token = window.btoa(id+"|"+token);
	setting.success = function(response){
		// if(response.data.length > 0){
		// 	console.log(response.data.id);
			profileViewModel.id(response.data.id);
			profileViewModel.uid(response.data.uid);
			profileViewModel.brand(response.data.name);
			profileViewModel.description(response.data.description);
			profileViewModel.personname(response.data.contact_person_name);
			profileViewModel.email(response.data.email);
			profileViewModel.contact(response.data.contact);
			profileViewModel.facebook_page(response.data.facebook_page);
			profileViewModel.website_url(response.data.website_url);
			profileViewModel.business_hours(response.data.business_hours);
			profileViewModel.selectedCategory(response.data.brand_category);
            if(response.data.logo==null){
                profileViewModel.profile(static_image);
            }else{
			    profileViewModel.profile(response.data.logo+"?t="+Math.random());
            }
		// }
	};
	setting.error = function(jqXHR, textStatus, error){
		swal(jqXHR.responseJSON.message,"",'error');
		return false;
	};
	apiAjax(setting);
}
function initializeKnockout(){
	profileViewModel = new ProfileViewModel();
	viewModelName  = profileViewModel;
	ko.applyBindings(profileViewModel);
}

function ProfileViewModel(){
	self = this;
	self.id =ko.observable("");
	self.uid =ko.observable("");
	self.brand =ko.observableArray("");
	self.description =ko.observable("");
	self.personname =ko.observable("");
	self.email =ko.observable("");
	self.contact =ko.observable("");
	self.facebook_page =ko.observable("");
	self.website_url =ko.observable("");
	self.address =ko.observableArray([]);
	self.business_hours =ko.observable("");
	self.category =ko.observable("");
	self.profile = ko.observable("");
    self.imagePath = ko.observable(path);
	self.selectedCategory= ko.observable("");
    self.availbleCategory= ko.observableArray([]);
    self.imageUri = ko.observable("");
    self.Cities = ko.observableArray([]);
    self.availbleCities = ko.observableArray([]);
    self.fields = ko.observableArray([
        {
            items: [
                {
                    id: ko.observable(""),
                    address: ko.observable(""),
                    pincode: ko.observable(""),
                    city: ko.observable(""),
                }
            ]
        },
    ]);
    self.index = ko.observable(0);
    self.removeProfileClick = function(){
        var datavalue = {};
        var setting = {};
        datavalue['id'] = bid
        setting.url  = removeProfileApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            window.location = editwebPath;
        };
        setting.error = function(jqXHR, textStatus, error){    
            swal(jqXHR.responseJSON.message, "", 'error');
            return false;                
        };
        apiAjax(setting);
    }
    self.addLocationClick = function () {

        profileViewModel.fields.push({items: [
                                                {
                                                    id: ko.observable(""),
                                                    address: ko.observable(""),
                                                    pincode: ko.observable(""),
                                                    city: ko.observable("")
                                                }
                                            ]
                                    });
        getCities(profileViewModel.index(profileViewModel.index()+1));
    }
    self.removeAddress = function (index) {
        profileViewModel.fields.splice(index, 1);
        profileViewModel.address.splice(index, 1);
    }
	self.updateProfileClick = function(){
        if(!$("#form_brand").valid()){
            return false
        }
        var setting = {};
        var datavalue = {};
        var address = {};
        var brand_name = profileViewModel.brand();
        var category_name = profileViewModel.selectedCategory();
        var description = profileViewModel.description();
        var email = profileViewModel.email();
        var mobile = profileViewModel.contact();
        var business_hours = profileViewModel.business_hours();
        var person_name = profileViewModel.personname();
        var website_url = profileViewModel.website_url();
        var facebook_page = profileViewModel.facebook_page();    
        
        var address_fields = $("[id^=addressDiv]").length;
        for (var i = 0; i < address_fields; i++) {
            if(profileViewModel.fields()[i].items[0].address()!="" && profileViewModel.fields()[i].items[0].pincode()!="" && profileViewModel.fields()[i].items[0].city()!="")
            { 
                address[i] = {
                    "id" :profileViewModel.fields()[i].items[0].id(),
                    "address": profileViewModel.fields()[i].items[0].address(),
                    "pincode": profileViewModel.fields()[i].items[0].pincode(),
                    "city": profileViewModel.fields()[i].items[0].city(),
                };
            }
        }
      
        datavalue['id'] = bid;
        datavalue['uid'] = profileViewModel.uid();
        datavalue['addressArray'] = address;
        datavalue['brand_name'] = brand_name;
        datavalue['category_name'] = category_name;
        datavalue['description'] = description;
        datavalue['email'] = email;
        datavalue['contact'] = mobile;
        datavalue['business_hours'] =business_hours;
        datavalue['person_name'] = person_name;
        datavalue['website_url'] = website_url;
        datavalue['facebook_page'] = facebook_page;
        datavalue['image'] = profileViewModel.imageUri();
        datavalue['status'] = 1;
        datavalue['updateaddress'] = 1;

        setting.url  = updateProfileApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
          		window.location = editwebPath;
                return false;
        };
        setting.error = function(jqXHR, textStatus, error){
                if (jqXHR.status == 422) {
                    $.each(jqXHR.responseJSON.errors, function (i, item) {
                        var fieldname;
                        tail = i.substring(1);
                        fieldName = i.substring(0, 1).toUpperCase() + tail;
                        fieldname = "txt" + fieldName;
                        $("#" + fieldname).after(function () {
                            return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                        });
                    })
                    return false;
                } else {
                    swal(jqXHR.responseJSON.message, "", 'error');
                    return false;
                }
            };
        apiAjax(setting);
    
	}

}
function getCategories(){
    var setting = {};
    setting.url  = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit':25,'type':1};
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var category_list = response.data.data;
        if(category_list.length>0){
            $.each(category_list, function(i,item) {
                profileViewModel.availbleCategory.push(new availableCategory(item.id,item.name));
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"",'error');
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k,v){

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getCities(index) {

    var setting = {};
    setting.url = cityListApiPath;
    setting.type = 'GET';
    setting.data = {'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {

        var city_list = response.data.data;
        if (city_list.length > 0) {
            profileViewModel.availbleCities()[index] = ko.observableArray([]);
            $.each(city_list, function (i, item) {
                profileViewModel.availbleCities()[index].push(new availbleCities(item.id, item.name));
            });
            profileViewModel.Cities.splice(index, 0, profileViewModel.availbleCities()[index]);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availbleCities(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getAddress(bid) {
    var setting = {};
    var datavalue = {
        'bid': bid,
    };
    setting.url = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var address_list = response.data.data;
        if (address_list.length > 0) {
            $.each(address_list, function (i, item) {
                profileViewModel.address.push(new availableAddress(item.id, item.address, item.city, item.state, item.country, item.pincode, item.city_id, item.state_id, item.country_id));
                profileViewModel.fields.push({items: [{id: ko.observable(""),address: ko.observable(""), pincode: ko.observable(""), country: ko.observable(""), state: ko.observable(""),
                            city: ko.observable("")}
                    ]
                });
            });
            profileViewModel.fields.pop();
            var address_fields = profileViewModel.fields().length;
            for (var i = 0; i < address_fields; i++) {
                getCities(i);
                profileViewModel.fields()[i].items[0].id(profileViewModel.address()[i].id());
                profileViewModel.fields()[i].items[0].address(profileViewModel.address()[i].address());
                profileViewModel.fields()[i].items[0].pincode(profileViewModel.address()[i].pincode());
                profileViewModel.fields()[i].items[0].city(profileViewModel.address()[i].city_id());
            }

        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k, address, city, state, country, pincode, city_id, state_id, country_id) {
    var self = this;
    self.value = ko.observable(k);
    self.id = ko.observable(k);
    self.address = ko.observable(address);
    self.city = ko.observable(city);
    self.state = ko.observable(state);
    self.country = ko.observable(country);
    self.city_id = ko.observable(city_id);
    self.state_id = ko.observable(state_id);
    self.country_id = ko.observable(country_id);
    self.pincode = ko.observable(pincode);
}