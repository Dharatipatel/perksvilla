var viewModelName;
var changePasswordViewModel;

$(document).ready(function(){
	$('.has-submenu').removeClass('active');
    $('#myprofiledropdown').addClass('show');
  	$('#profileTab').addClass('active');
    $('#changePasswordTab').addClass('active');
    
	initializeKnockout();
	$('#form_changePassword').validate({
		rules: {
            txtOldPassword: {
            	'required' :true,
            },
            txtNewPassword: {
            	'required' :true,
            },
            txtConfirmPassword: {
            	'required' :true,
            	'equalTo': "#txtNewPassword"
            },
        },
        messages: {
            txtOldPassword: {
            	'required' : 'Please Enter Old Password',
            },
            txtNewPassword: {
            	'required' : 'Please Enter New Password',
            },
            txtConfirmPassword: {
            	'required' : 'Please Enter Confirm Password',
            	'equalTo' : 'Confirm Password is not same'
            },
        },
	});
});

function initializeKnockout(){
	changePasswordViewModel = new ChangePasswordViewModel();
	viewModelName  = changePasswordViewModel;
	ko.applyBindings(changePasswordViewModel);
}

function ChangePasswordViewModel(){
	self = this;
	self.confirmpassword = ko.observable();
	self.newpassword = ko.observable();
	self.oldpassword = ko.observable();
	self.changePasswordClick = function(){
		var setting = {};
		self.confirmpassword($("#txtConfirmPassword").val());
		self.newpassword($("#txtNewPassword").val());
		self.oldpassword($("#txtOldPassword").val());
			
			if(!$('#form_changePassword').valid()){
				return false;
			}
			var datavalue = {
			'id' : id,
			'newpassword' : $.md5(self.newpassword()),
			'oldpassword' : $.md5(self.oldpassword()),
			};
			setting.url  = changepasswordApiPath;
			setting.type = 'POST';
			setting.data = datavalue;
			setting.token = window.btoa(id+"|"+token);
			setting.success = function(response){
				resetAll();
				swal(response.message,"",'success');
				return false;
			};
			setting.error = function(jqXHR, textStatus, error){
				if(jqXHR.status==422){
					$.each(jqXHR.responseJSON.errors,function(i,item){
						var fieldname;
                        tail = i.substring( 1 );
                        fieldName = i.substring( 0, 1 ).toUpperCase() + tail; 
                        fieldname ="txt"+fieldName;
                        $("#"+fieldname ).after(function() {
                            return "<label id='"+fieldname+"'-error' class='error' for='"+fieldname+"'>"+item+"</label>";
                        });
					})
				} else {
					$("#txtOldPassword" ).after(function() {
                            return "<label id='txtOldPassword-error' class='error' for='txtOldPassword'>"+jqXHR.responseJSON.message+"</label>";
                        });
					return false;
				}
			};
			apiAjax(setting);
	}
}

function resetAll(){
	changePasswordViewModel.oldpassword("");
	changePasswordViewModel.newpassword("");
	changePasswordViewModel.confirmpassword("");
	$("#txtConfirmPassword").val("");
	$("#txtNewPassword").val("");
	$("#txtOldPassword").val("");
}
