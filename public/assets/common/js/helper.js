function validateEmail(email) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(email)) {
        return true;
    } else {
        return false;
    }
}
function apiAjax(config) {

    var apisetting = {
        statusCode: {
            401: function () {
                logout();
            }
        },
        type: config.type || "GET",
        url: config.url,
        data: config.data,
        datatype: config.datatype || "json",
        cache: config.cache || false,
        async: config.async || true,
        success: config.success,
        error: config.error,
        complete: config.complete
    };
    if (typeof config.contentType != "undefined") {
        apisetting.contentType = config.contentType;
    }
    if (typeof config.processData != "undefined") {
        apisetting.processData = config.processData;
    }
    if (config.token) {
        apisetting.beforeSend = function (xhr) {
            xhr.setRequestHeader("Authorization", config.token);
        };
    }
    return apiajax = $.ajax(apisetting);
}
function TosterMessage(type, message) {
    toastr[type](message);
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}
function logout() {
    location.href = logoutUrl;
}
function preventGoBack() {
    history.pushState(null, null, location.href);
    window.onpopstate = function (event) {
        history.go(1);
    };
}

function ajaxRequest(url, method, type, data, callback) {
    jQuery.ajax({
        url: baseURL + url,
        type: method,
        dataType: type,
        data: data
    }).done(function (html) {
        callback(html);
    })
            .fail(function (response) {
                var response = JSON.parse(response.responseText);
                var errorString = '<ul>';
                $.each(response, function (key, value) {
                    errorString += '<li>' + value + '</li>';
                });
                errorString += '</ul>';

                showMsg("error", errorString);
            })
            .always(function () {
            });
}

function showMsg(type, msg) {
    if (type == "add") {
        swal("Added!", msg, "success");
    } else if (type == "update") {
        swal("Updated!", msg, "success");
    } else if (type == "deleted") {
        swal("Deleted!", msg, "success");
    } else if (type == "success") {

        swal({
            title: "Success!",
            text: msg,
            html: true,
            type: "success"
        });

    } else if (type == "info") {

        swal({
            title: "Information",
            text: msg,
            html: true,
            type: "info"
        });

    } else {
        swal({
            title: "Error!",
            text: msg,
            html: true,
            type: "error"
        });
    }
}


function deleteRow(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    window.location = $("#del_" + id).data('url');
                }
            });
}
