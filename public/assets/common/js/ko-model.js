function CategoryModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.user_id = ko.observable(data.user_id);
    self.description = ko.observable(data.description);
    self.user_name = ko.observable(data.user_name);
    self.userName = function(){
        if (typeof  data.user_id  === "undefined") {
            return 0;
        } else {
            return data.user_id;
        }
    }
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        } else if (data.status == 2) {
            return "Deleted By User";
        }
    }
}

function BrandModel(data) {
    self = this;

    self.id = ko.observable(data.id);
    self.uid = ko.observable(data.uid);
    self.name = ko.observable(data.name);
    self.categoryid = ko.observable(data.brand_category);
    self.category = ko.observable(data.brand_category_name);
    self.contact = ko.observable(data.contact);
    self.email = ko.observable(data.email);
    self.contact_person_name = ko.observable(data.contact_person_name);
    self.description = ko.observable(data.description);
    self.business_hours = ko.observable(data.business_hours);
    self.website_url = ko.observable(data.website_url);
    self.facebook_page = ko.observable(data.facebook_page);
    self.address = ko.observable(data.address);
    self.pincode = ko.observable(data.pincode);
    self.logo = ko.observable(data.logo);
    self.commision = ko.observable(data.commision);
    self.status = ko.observable(data.status);
    self.country = ko.observable(data.country);
    self.state = ko.observable(data.state);
    self.city = ko.observable(data.city);
    self.offer_count = ko.observable(data.offer_count);
    self.created = ko.observable(moment(data.created).format('DD-MM-YYYY' ));
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function OfferModel(data) {
    self = this;

    self.id = ko.observable(data.id);
    self.brand_id = ko.observable(data.brand_id);
    self.name = ko.observable(data.title);
    self.value = ko.observable(data.value);
    self.category = ko.observable(data.category);
    self.price = ko.observable(data.price);
    self.issued_count = ko.observable(data.issued_count);
    self.redeem_count = ko.observable(data.redeem_count);
    self.apply_on = ko.observable(data.apply_on);
    self.description = ko.observable(data.description);
    self.notes = ko.observable(data.notes);
    self.valid_to = ko.observable(moment(data.valid_to).format('DD-MM-YYYY'));
    self.created = ko.observable(moment(data.created).format('DD-MM-YYYY'));
    self.modified = ko.observable(moment(data.modified).format('DD-MM-YYYY'));
    self.modified_date = ko.observable(moment(data.modified).format('DD-MM-YYYY'));
    self.publish_date = ko.observable(moment(data.publish_date).format('DD-MM-YYYY'));
    self.publish_date_text = ko.observable(moment(data.publish_date).format('DD MMM, YY'));
    self.expiry_date_text = ko.observable(moment(data.valid_to).format('DD MMM, YY'));
    self.count = ko.observable(data.count);
    self.remaining_count = ko.observable(data.remaining_count);
    self.issued = ko.observable("");
    self.redeem = ko.observable("");
    self.status = ko.observable(data.offer_status_id);
    self.brand_name = ko.observable(data.brand_name);
    self.brand_address_id  = ko.observable(data.brand_address_id );
    self.contact  = ko.observable(data.contact);
    self.offerType = function(){
        if (data.price == 0) {
            return "Free Offer";
        } else if (data.offer_status_id == 1) {
            return "Fix Price offer";
        }
    }
    self.statusText = function () {
        if (data.offer_status_id == 0) {
            return "Deactive";
        } else if (data.offer_status_id == 1) {
            return "Active";
        } else if (data.offer_status_id == 2) {
            return "Upcoming";
        } else if (data.offer_status_id == 3) {
            return "Pending";
        } else if (data.offer_status_id == 4) {
            return "Canceled";
        } else if (data.offer_status_id == 5) {
            return "Rejected";
        } else if (data.offer_status_id == 6) {
            return "Expired";
        }

    };
    self.conversion_ratio = function(){
        return (data.redeem_count/data.count)*100;
    }
    self.applicable_date = function () {
        if (data.apply_on == 1) {
            return "Applicable on " + moment(data.valid_to).format('DD-MM-YYYY') + " Only";
        } else {
            return "Applicable UpTo " + moment(data.valid_to).format('DD-MM-YYYY');
        }
    }
    self.expiries_text = function () {
        days = (new Date(data.valid_to) - new Date()) / (1000 * 60 * 60 * 24);
        if (Math.round(days) < 0) {
            return "Expired";
        } else {
            return "Expires in " + Math.round(days) + " days";
        }
    }
    self.apply_on_text = function(){
         if (data.apply_on == 1) {
            return moment(data.valid_to).format('DD-MM-YYYY');
        } else if (data.apply_on == 2) {
            return "Week Days (Mon-Fri)";
        } else if (data.apply_on == 3) {
            return "Weekends  (Sat-Sun)";
        } else if (data.apply_on == 4) {
            return "All Days";
        }
    }
    /*brand Detail*/
    self.business_hours = ko.observable(data.business_hours);
    self.website_url = ko.observable(data.website_url);
    self.facebook_page = ko.observable(data.facebook_page);
    if(data.logo==null){
        self.logo = ko.observable("NoLogo.png");
    } else {
        self.logo = ko.observable(data.logo+"?t="+Math.random());
    }
}
function EmployeeModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.birth_date = ko.observable(moment(data.birth_date).format('DD-MM-YYYY' ));
    self.contact = ko.observable(data.contact);
    self.corporate_id = ko.observable(data.corporate_id);
    self.designation = ko.observable(data.designation);
    self.email = ko.observable(data.email);
    self.first_name = ko.observable(data.first_name);
    self.last_name = ko.observable(data.last_name);
    self.joining_date = ko.observable(moment(data.joining_date).format('DD-MM-YYYY' ));
    // self.birth_date_word = ko.observable();
    self.status = ko.observable(data.status);
    self.gender = ko.observable(data.gender);
    self.reward_count = ko.observable(data.reward_count);
    self.corporate_name = ko.observable(data.corporate_name);
    if(data.profile_pic==null){
        if(data.gender=="Male"){
            self.profile_pic = ko.observable("male.jpg");
        } else if(data.gender=="Female"){
            self.profile_pic = ko.observable("female.jpg");
        } else {
            self.profile_pic = ko.observable("male.jpg");
        }
    } else {
        self.profile_pic = ko.observable(data.profile_pic+"?t="+Math.random());
    }
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }

    }
    self.reward_word = function(){
        if(moment(data.birth_date).format('MM') == moment().format('MM')){
            return   'having birthday on '+moment(data.birth_date).format('DD MMMM' )
        } else if (moment(data.joining_date).format('MM') == moment().format('MM') && moment(data.joining_date).format('YYYY') == moment().format('YYYY')){
            return  'is a New Joinee of Current Month'
        } else if(moment(data.joining_date).format('MM')==moment().subtract(1,'months').format('MM') && moment(data.joining_date).format('YYYY') == moment().format('YYYY')){
            return  'is a New Joinee of Last Month'
        }  else if (moment(data.joining_date).format('MM') == moment().format('MM')){
            var year =' year';
            if(moment().diff(moment(data.joining_date), 'years') > 1){
                year = ' years';
            } 
            return  'Will complete '+moment().diff(moment(data.joining_date), 'years') +year+' on '+moment(data.joining_date).format('DD MMMM' )
        } 
    }
}
function CorporateModel(data) {
    self = this;
    console.log(data);

    self.id = ko.observable(data.id);
    self.uid = ko.observable(data.uid);
    self.name = ko.observable(data.name);
    self.category_id =ko.observable(data.category);
    self.category =ko.observable(data.corporate_category_name);
    self.coupen_count =ko.observable(data.coupen_count);
    self.contact = ko.observable(data.contact);
    self.email = ko.observable(data.email);
    self.contact_person_name = ko.observable(data.contact_person_name);
    self.description = ko.observable(data.description);
    self.website_url = ko.observable(data.website_url);
    self.facebook_page = ko.observable(data.facebook_page);
    self.linkedin_page = ko.observable(data.linkedin_page);
    self.address = ko.observable(data.address);
    self.logo = ko.observable(data.logo);
    self.company_size = ko.observable(data.company_size);
    self.status = ko.observable(data.status);
    self.employee_count = ko.observable(data.employee_count);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
    self.created = ko.observable(moment(data.created).format('DD-MM-YYYY' ));
}
function CountryModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function StateModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.country_id = ko.observable(data.country_id);
    self.country_name = ko.observable(data.country_name);
    self.name = ko.observable(data.name);
    self.status = ko.observable(data.status);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function CityModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.state_id = ko.observable(data.state_id);
    self.state_name = ko.observable(data.state_name);
    self.status = ko.observable(data.status);
    self.name = ko.observable(data.name);
    self.statusText = function () {
        if (data.status == 0) {
            return "Deactive";
        } else if (data.status == 1) {
            return "Active";
        }
    }
}
function brandOfferModel(data) {
    self = this;
    self.brand_id = ko.observable(data.brand_id);
    if(data.brand_logo==null){
        self.brand_logo = ko.observable(static_image);
    }else{
        self.brand_logo = ko.observable(data.brand_logo);
    }
    self.brand_name = ko.observable(data.brand_name);
    self.category = ko.observable(data.brand_category_name);
    self.freecoupen = ko.observable(data.freecoupen);
    self.fixcoupen = ko.observable(data.fixcoupen);
}
function RewardModel(data) {
    self = this;
    self.brand_name = ko.observable(data.brand_name);
    self.brand_id = ko.observable(data.brand_id);
    self.category_id = ko.observable(data.category_id);
    self.category_name = ko.observable(data.category_name);
    self.coupen_value   = ko.observable(data.coupon_value);
    self.buy_price   = ko.observable(data.buy_price);
    self.offer_name   = ko.observable(data.offer_name);
    self.first_name = ko.observable(data.first_name);
    self.last_name = ko.observable(data.last_name);
    self.issue_date  = ko.observable(moment(data.issue_date).format('DD-MM-YYYY' ));
    self.offer_id  = ko.observable(data.offer_id);
    self.created  = ko.observable(moment(data.created).format('DD-MM-YYYY hh:mm:ss A' ));
    self.redeem_date  = ko.observable((data.redeem_date) ? moment(data.redeem_date).format('DD-MM-YYYY hh:mm:ss A'):data.redeem_date);
    self.designation  = ko.observable(data.designation);
    self.bill_amount  = ko.observable(data.bill_amount);
    // self.created  = ko.observable(data.created);
    if(data.coupen){
       self.coupon  = ko.observable(data.coupen); 
    }

}
function SMSTemplateModel(data) {
    self = this;
    self.id = ko.observable(data.id);
    self.title = ko.observable(data.title);
    self.text = ko.observable(data.text);

}
function TransactionModel(data){
    self = this;
    self.id = ko.observable(data.id);
    self.brand_id = ko.observable(data.brand_id);
    self.brand_name = ko.observable(data.brand_name);
    self.availble_balance = ko.observable(data.availble_balance);
    self.commision = ko.observable(data.commision);
    self.transfer_amount = ko.observable(Math.round(data.withdraw_amount - (parseInt(data.withdraw_amount)*(data.commision/100))));
    self.created = ko.observable(moment(data.created).format('DD-MM-YYYY hh:mm:ss A' ));
    self.bank = ko.observable("");
    self.user_id = ko.observable(data.user_id);
    self.withdraw_amount = ko.observable(data.withdraw_amount);
    self.credited_to = function(){
        var acc = data.account_number;
        return data.bank_name + ' XXX' + acc.substr(acc.length - 4);
    };
    self.status = ko.observable(data.status);
    self.statusText = function(){
        if(data.status==1){
            return "Success";
        } else if(data.status==0) {
            return "Pending";
        }
    }
}
function OrderHistoryCorporateModel(data){
    self = this;
    self.employee_name = ko.observable(data.employee_name);
    self.offer_name = ko.observable(data.offer_name);
    self.offer_id = ko.observable(data.offer_id);
    self.transaction_id = ko.observable(data.transaction_id);
    self.total_amount = ko.observable(data.total_amount);
    self.brand_name = ko.observable(data.brand_name);
    self.corporate_name = ko.observable(data.corporate_name);
    self.per_coupon_price = function(){
        return (data.total_amount/data.employee_name.split(",").length);
    }
    self.purchase_date = ko.observable(moment.utc(data.purchase_date).format("DD-MM-YYYY hh:mm:ss A"));
   
}
function OrderHistoryBrandModel(data) {
    self = this;
    self.brand_name = ko.observable(data.brand_name);
    self.brand_id = ko.observable(data.brand_id);
    self.category_id = ko.observable(data.category_id);
    self.category_name = ko.observable(data.category_name);
    self.coupen_value   = ko.observable(data.coupon_value);
    self.buy_price   = ko.observable(data.buy_price);
    self.offer_name   = ko.observable(data.offer_name);
    self.first_name = ko.observable(data.first_name);
    self.last_name = ko.observable(data.last_name);
    self.issue_date  = ko.observable(moment(data.issue_date).format('DD-MM-YYYY'));
    self.offer_id  = ko.observable(data.offer_id);
    self.created  = ko.observable(moment(data.created).format('DD-MM-YYYY hh:mm:ss A' ));
    self.modified  = ko.observable(moment(data.modified).format('DD-MM-YYYY hh:mm:ss A' ));
    self.redeem_date  = ko.observable((data.redeem_date) ? moment(data.redeem_date).format('DD-MM-YYYY hh:mm:ss A' ):data.redeem_date);
    self.designation  = ko.observable(data.designation);
    self.bill_amount  = ko.observable(data.bill_amount);
    // self.created  = ko.observable(data.created);
    if(data.coupen){
       self.coupon  = ko.observable(data.coupen); 
    }

}
function CouponModel(data) {
    self = this;
    self.employee_name = ko.observable(data.first_name +" "+data.last_name);
    self.brand_name = ko.observable(data.brand_name);
    self.offer_name   = ko.observable(data.offer_name);
    self.corporate_name   = ko.observable(data.corporate_name);
    self.brand_id = ko.observable(data.brand_id);
    self.coupen_value   = ko.observable(data.coupon_value);
    self.buy_price   = ko.observable(data.buy_price);
    self.issue_date  = ko.observable(moment(data.issue_date).format('DD-MM-YYYY'));
    self.expiry_date  = ko.observable(moment(data.expiry_date).format('DD-MM-YYYY'));
    self.coupon_code  = ko.observable(data.coupen);
    self.redeem_date  = ko.observable((data.redeem_date) ? moment(data.redeem_date).format('DD-MM-YYYY hh:mm:ss A' ):data.redeem_date);
    self.bill_amount  = ko.observable(data.bill_amount);
    self.offer_id = ko.observable(data.offer_id);
}
function TotalSalesModel (data) {
    self = this;
    self.offer_name = ko.observable(data.title);
    self.offer_price = ko.observable(data.price);
   
}