var viewModelName;
var loginViewModel;

$(document).ready(function(){
	initializeKnockout();
	preventGoBack();
	$("#login_form").validate({
        rules: {
            txtEmail: {
            	'required' :true,
            	'email' :true,
            },
            txtPassword : "required",
        },
        messages: {
            txtEmail: {
            	'required' : 'Please Enter Email',
            	'email' : 'Please enter a valid email address'
            },
            txtPassword: "Please enter a password",
        },
    });
});

function initializeKnockout(){
	loginViewModel = new LoginViewModel();
	viewModelName  = loginViewModel;
	ko.applyBindings(loginViewModel);
}

function LoginViewModel(){
	self = this;
	self.email = ko.observable();
	self.password = ko.observable();
	self.isError = ko.observable(false);
	self.errorMsg = ko.observable("");
	self.loginClick = function(){
		if(!$("#login_form").valid()){		
			return false;
		}
		var setting = {};
		self.email($("#txtEmail").val());
		self.password($("#txtPassword").val());
		var datavalue = {
			'email' : self.email(),
			'password' : $.md5(self.password()),
			'role_id' : 3,
		};
		setting.url  = loginApiPath;
		setting.type = 'POST';
		setting.data = datavalue;
		setting.success = function(response){
			var sessionparams = {};
			datavalue['id'] = response.data.id;
			datavalue['cid'] = response.data.detail.cid;
			datavalue['corporate_name'] = response.data.detail.name;
			datavalue['token'] = response.data.token;
			datavalue['_token'] = _token;
			sessionparams.url = setSessionPath ;
			sessionparams.type = 'POST' ;
			sessionparams.data = datavalue;
			sessionparams.success = function(response){
				location.href=dashboardUrl;
			};
			sessionparams.error = function(jqXHR, textStatus, error){
				loginViewModel.isError(true);
				loginViewModel.errorMsg("Something Went Wrong");
				return false;
			};
			apiAjax(sessionparams);
		};
		setting.error = function(jqXHR, textStatus, error){
			loginViewModel.isError(true);
			loginViewModel.errorMsg(jqXHR.responseJSON.message);
			return false;
		};
		apiAjax(setting);
	}
	self.createAccountClick = function(){

	}
	self.forgotPasswordClick = function(){

	}
}

function resetAll(){
	$("#txtEmail").val('');
	$("#txtPassword").val('');
}
