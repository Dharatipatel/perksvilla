
$(document).ready(function(){
    if(success==1){
        getUpdateRewards();
    } else {
        location.href = historyListingWeb; 
    }
});

function getUpdateRewards(){
    var setting = {};
    var datavalue = {
        'payment_status' : success,
        'payment_id' : payment_id,
        'payment_request_id' : payment_request_id,
        'user_id' :id,
    };
    
    setting.url  = rewardUpdateApi;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        location.href = historyListingWeb; 
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        location.href = historyListingWeb;
        return false;
    };
    apiAjax(setting);


}
