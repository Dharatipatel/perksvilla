var viewModelName;
var rewardListViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#rewardDropdown').addClass('show');
    $('#rewardTab').addClass('active');
    $('#manageRewardTab').addClass('active');

    initializeKnockout();
    rewardListViewModel.status(status);
    getRewards();
    // getUsedRewards();
    // getUnUseRewards();
    getStatistics();
});
function initializeKnockout(){
    rewardListViewModel = new RewardListViewModel();
    viewModelName  = rewardListViewModel;
    ko.applyBindings(rewardListViewModel);
}
function getStatistics(){
    var setting = {};
    var datavalue = {
        'cid':cid,
    };
    
    setting.url    = statisticsApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        rewardListViewModel.used(response.data.used_reward);
        rewardListViewModel.unused(response.data.unused_reward);
        rewardListViewModel.total_save(response.data.amount_saved);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function RewardListViewModel(){
    self = this;
    self.total = ko.observable("");
    self.rewards= ko.observableArray([]);
    self.offers= ko.observableArray([]);
    self.status = ko.observable(0);
    self.payment_status = ko.observable(1);
    self.used = ko.observable(0);
    self.unused = ko.observable(0);
    self.total_save = ko.observable(0);
    self.viewOfferClick = function(reward){
        getOfferDetail(reward.offer_id(),reward);
        
    }
    /* popup */
    self.address= ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.contact = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
}
function getOfferDetail(offer_id,reward){
    var setting = {};
    var datavalue = {
        'offer_id':offer_id,
    };
    
    setting.url  = offerDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            rewardListViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                rewardListViewModel.offers.push(new OfferModel(item));
            });
            var offer = rewardListViewModel.offers()[0];
            rewardListViewModel.offer_title(offer.name());
            rewardListViewModel.offer_description(offer.description());
            rewardListViewModel.coupon_value(offer.value());
            rewardListViewModel.valid_to(offer.valid_to());
            rewardListViewModel.coupon_code("");
            rewardListViewModel.contact(offer.contact());
            rewardListViewModel.applicable_on(offer.apply_on_text());
            rewardListViewModel.address_array(offer.brand_address_id().split(','));
            rewardListViewModel.business_hours(offer.business_hours());
            rewardListViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
            rewardListViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
            rewardListViewModel.notes(offer.notes().replace(/\n/g, "<br />"));
            rewardListViewModel.brand_logo(path+""+offer.logo());
            getAddress(offer.brand_id());
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getRewards(){
    var setting = {};
    var datavalue = {
        'cid' : cid,
        'status':rewardListViewModel.status(),
        'payment_status':rewardListViewModel.payment_status(),
    };
    
    setting.url  = rewardListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var rewards_list = response.data.data;
        if ($.fn.DataTable.isDataTable("#rewardsList")) {
                $('#rewardsList').DataTable().clear().destroy();
        }
        rewardListViewModel.rewards([]);
        if(rewards_list.length>0){
            $.each(rewards_list, function(i,item) {
                rewardListViewModel.rewards.push(new RewardModel(item));
            });
            $("#rewardsList").DataTable({
                responsive: true,
                bSort : false,
                searching: false,
                pageLength:15,
                bLengthChange: false
            });
        } else {
            
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            rewardListViewModel.address([]);
            $.each(address_list, function(i,item) {
                rewardListViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            rewardListViewModel.selected_address([]);
            for (var i = 0; i < rewardListViewModel.address().length ; i++) {
                if(rewardListViewModel.address_array.indexOf(rewardListViewModel.address()[i].value().toString()) != -1){
                    rewardListViewModel.selected_address.push(rewardListViewModel.address()[i].textaddress());
                }
            }
            $('#viewModel').modal('toggle');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
}