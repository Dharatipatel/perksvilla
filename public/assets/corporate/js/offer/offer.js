var viewModelName;
var offerViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#myofferdropdown').addClass('show');
    initializeKnockout();
    $('#offerTab').addClass('active');
    if(type=='list'){
        offerViewModel.status(status);
        $('#listOfferTab').addClass('active');
        if(status==3){
            $('.offerstatus').removeClass('active');
            $('#pendingTab').addClass('active');
        } else if(status==2){
            $('.offerstatus').removeClass('active');
            $('#upcomingTab').addClass('active');
        } else if(status==1){
            $('.offerstatus').removeClass('active');
            $('#acitveTab').addClass('active');
        }
    } else if(type=='add'){
        $('#addOfferTab').addClass('active');
    } else if(type=='past'){
        offerViewModel.status(status);
        if(status==4){
            $('.offerstatus').removeClass('active');
            $('#cancelledTab').addClass('active');
        } else if(status==5){
            $('.offerstatus').removeClass('active');
            $('#rejectedTab').addClass('active');
        } else if(status==6){
            $('.offerstatus').removeClass('active');
            $('#expiredTab').addClass('active');
        }
        $('#pastOfferTab').addClass('active');
    }
    $("#form_Offer").validate({
        rules:{
            txtTitle : "required",
            txtDescription : "required",
            txtValue: {
                'required' :true,
                'digits' :true,
            },
            txtPrice: {
                'required' :true,
                'digits': true
            },
            txtApplicableOn:"required",
            fixDate : { 
                required:'#txtApplicableOn1[value=1]:checked',
            },
            validTo : { 
                required :'#txtApplicableOn2[value=2]:checked',
            },
            'address[]' :{
                  required: true,
            },
            count :{
                "required" :true,
                "digits" :true,
            },
            txtContact :{
                "required" :true,
                "digits" :true,
            },
            publishDate : "required",
            notes : "required",
        },
        messages: {
            txtTitle: "Please enter Offer title",
            txtDescription: "Please enter Offer description",
            txtValue: {
                'required' : 'Please Enter Offer Value',
                'digits' : 'Please enter Offer Value in digits'
            },
            txtPrice: {
                'required' : 'Please Enter Offer Price',
                'digits' : 'Offer Price should be only in digits'
            },
            txtApplicableOn : "Please select Applicable Place",
            fixDate : "Please select Fix date",
            validTo : "Please select Valid To date",
            'address[]': {
                required: "You must check at least 1 Address",
            },
        	count:{
                'required' : 'Please Enter Offer Count',
                'digits' : 'Offer Count should be only in digits'
            },
            txtContact:{
        		'required' : 'Please Enter Contact Number',
                'digits' : 'Contact Number should be only in digits'
        	},
        	publishDate:"Please Select Publish Date",
        	notes:"Please Enter Important Notes",

        },
    });
    $('#fixDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#fixDate').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){
                    if($("#publishDate-error").length){
                            $('#publishDate-error').css("display","none");
                    }     
                    $("#fixDate").after(function() {
                        return "<label id='fixDate'-error' class='error' for='fixDate'> Fix Date can not less than Publish Date </label>";
                    });
                    return false;        
                } else {
                    if($("#publishDate-error").length){
                        $('#publishDate-error').css("display","none");
                    }
                }
            }
        });
    $('#validTo').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            if($('#publishDate').val()!=''){
                if(!moment($('#validTo').val(), 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){     
                    $("#validTo").after(function() {
                        return "<label id='validTo'-error' class='error' for='validTo'>Expire Date can not less than Publish Date </label>";
                    });
                    return false;        
                }
            }
        });
    $('#publishDate').datepicker({startDate: '-0d',format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true}).on('changeDate', function (ev) {
            var applicableOn = $("input[name='txtApplicableOn']:checked").val()
            if(applicableOn == 1){
                applicableDate = $('#fixDate').val();
            } else {
                applicableDate = $('#validTo').val();
            }
            if(applicableDate!=''){
                if(!moment(applicableDate, 'DD-MM-YYYY').isAfter(moment($('#publishDate').val(),'DD-MM-YYYY'))){  
                    if($("#fixDate-error").length){
                            $('#fixDate-error').css("display","none");
                    } 
                    if($("#validTo-error").length){
                            $('#validTo-error').css("display","none");
                    }    
                    $("#publishDate").after(function() {
                        return "<label id='publishDate-error' class='error' for='publishDate'>Publish Date can not greater than Fix Date or Expire Date</label>";
                    });
                    return false;        
                }
            }
        });
    
    getOffers();
    getAddress();
});


function initializeKnockout(){
	offerViewModel = new OfferViewModel();
	viewModelName  = offerViewModel;
	ko.applyBindings(offerViewModel);
}

function OfferViewModel(){
	self=this;
	self.status=ko.observable(0);
	self.total=ko.observable(0);
	self.offers=ko.observableArray([]);
    self.editContainer = ko.observable(false);
    self.listContainer = ko.observable(true);
	self.offerList = ko.observable(false);
	self.offerNoData = ko.observable(false);
    self.stepTitle = ko.observable("Create Offer -> Step 1");
	self.stepSubTitle = ko.observable("");
	self.step1Container =ko.observable(true);
    self.step2Container =ko.observable(false);
    self.fixDateVisibilty =ko.observable(true);
	self.validToVisibilty =ko.observable(false);
    self.address = ko.observableArray([]);
    self.choosenAddress = ko.observableArray([]);
    self.action = ko.observable("add");
    self.action_title = ko.observable("Create Offer");
    self.offer_id = ko.observable("");
    self.isReject = ko.observable(false);
    /* popup */
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.contact = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    self.rejectOfferClick = function(data,event){
        
    }
	self.fixValueOfferCreateClick = function(){
        offerViewModel.stepTitle("Create Offer -> Step 2");
		offerViewModel.stepSubTitle("Fix Value Offer");
		offerViewModel.step1Container(false);
		offerViewModel.step2Container(true);
        $("#txtPrice").attr("disabled", false);
        $("#txtPrice").attr("placeholder", "Enter Buy price");
		resetAll();
	};
	self.freeOfferCreateClick = function(){
        offerViewModel.stepTitle("Create Offer -> Step 2");
        offerViewModel.stepSubTitle("Free Offer");
		
		offerViewModel.step1Container(false);
		offerViewModel.step2Container(true);
        $("#txtPrice").attr("disabled", true);
        $("#txtPrice").attr("placeholder", "Rs 0");
		resetAll();
	};
    self.cancelClick = function(offer){
        $('#cancelModal').modal('show');
        offerViewModel.offer_id(offer.id());
    };
    self.cancelConfirm = function(offer){

        var setting = {};
        var datavalue = {
            'id' : offerViewModel.offer_id(),
            'status': 4,
        };
        setting.url  = deactiveOfferApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            $('#cancelModal').modal('toggle');
            swal("Offer cancelled Successfully","",'success');
            getOffers();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    

    };
    self.cancelOfferClick= function(){
        if(offerViewModel.action() == "add"){
            offerViewModel.stepTitle("Create Offer -> Step 1");
            offerViewModel.step1Container(true);
            offerViewModel.step2Container(false);
        } else {
            location.href=offerList+'?pending';
        }
        resetAll();
    }
    self.offerActiveClick =function(item, event){
        location.href=offerList+'?active';
    }
    self.offerUpcomingClick = function(item, event){
        location.href=offerList+'?upcoming';
    }
    self.offerPendingClick =function(item, event){
        location.href=offerList+'?pending';
    }
    self.offerExpiredClick = function(item, event){
        location.href=offerList+'?expired';
    }
    self.offerCancelledClick = function(item, event){
        location.href=offerList+'?cancelled';
    }
    self.offerDeletedClick = function(item, event){
        location.href=offerList+'?deleted';
    }
    self.viewOfferClick = function(offer){
        offerViewModel.offer_title(offer.name());
        offerViewModel.offer_description(offer.description());
        offerViewModel.coupon_value(offer.value());
        offerViewModel.valid_to(offer.valid_to());
        offerViewModel.coupon_code("");
        offerViewModel.applicable_on(offer.apply_on_text());
        offerViewModel.address_array(offer.brand_address_id().split(','));
        offerViewModel.business_hours(offer.business_hours());
        offerViewModel.contact(offer.contact());
        offerViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
        offerViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
        offerViewModel.notes(offer.notes().replace(/\n/g, "<br />"));
        offerViewModel.brand_logo(path+""+offer.logo());
        offerViewModel.selected_address([]);
        for (var i = 0; i < offerViewModel.address().length ; i++) {
            if(offerViewModel.address_array.indexOf(offerViewModel.address()[i].value().toString()) != -1){
                offerViewModel.selected_address.push(offerViewModel.address()[i].textaddress());
            }
        }
        $('#viewModal').modal('show');
    }
    self.editOfferClick= function(offer){
        offerViewModel.listContainer(false);
        offerViewModel.editContainer(true);
        offerViewModel.action('edit');
        offerViewModel.action_title("Edit Offer");
        offerViewModel.offer_id(offer.id());
        if(offer.apply_on()==1){
            offerViewModel.fixDateVisibilty(true);
            offerViewModel.validToVisibilty(false);
            $('#fixDate').val(offer.valid_to());
        } else {
            offerViewModel.validToVisibilty(true);
            offerViewModel.fixDateVisibilty(false);
            $('#validTo').val(offer.valid_to());
        }
        $('#txtTitle').val(offer.name());
        $('#txtDescription').val(offer.description());
        $('#txtValue').val(offer.value());
        $('#txtContact').val(offer.contact());
        if(offer.price()!=0){
            $("#txtPrice").attr("disabled", false);
            $("#txtPrice").attr("placeholder", "Enter Buy price");
            $('#txtPrice').val(offer.price());
        } else {
            $("#txtPrice").attr("disabled", true);
            $("#txtPrice").attr("placeholder", "Rs 0");
        }
        $('input:radio[name="txtApplicableOn"]').filter('[value="'+offer.apply_on()+'"]').attr('checked', true);
        $('#count').val(offer.remaining_count());
        $('#publishDate').val(offer.publish_date());
        $('#notes').val(offer.notes());
        var address_array = offer.brand_address_id().split(',');
        offerViewModel.choosenAddress([]);
        $.each(address_array, function(index, value) {
            offerViewModel.choosenAddress.push(value); 
            // offerViewModel.choosenAddress.push(parseInt(value)); 
        });
    }
	self.createOfferClick = function(){
		if(!$("#form_Offer").valid()){
            return false;
        }
        var datavalue = {};
        var applicableDate = '';
        var title = $('#txtTitle').val();
        var description = $('#txtDescription').val();
        var value = $('#txtValue').val();
        var price = $('#txtPrice').val();
        var count = $('#count').val();
        var publish_date = $('#publishDate').val();
        var notes = $('#notes').val();
        var applicableOn = $("input[name='txtApplicableOn']:checked").val()
        if(applicableOn == 1){
            applicableDate = $('#fixDate').val();
        } else {
            applicableDate = $('#validTo').val();
        }
        var setting = {};
        datavalue['title'] = title;
        datavalue['description'] = description;
        datavalue['value'] = value;
        datavalue['price'] = (price==""||price==null) ? 0 : price;
        datavalue['brand_address_id'] = offerViewModel.choosenAddress();
        datavalue['apply_on'] = applicableOn;
        datavalue['valid_to'] = applicableDate;
        datavalue['count'] = count;
        datavalue['publish_date'] = publish_date;
        datavalue['notes'] = notes;
        datavalue['brand_id'] = bid;
        datavalue['contact'] = $('#txtContact').val();;
       
        if(offerViewModel.action()=="edit"){
            offerApiPath =updateOfferApiPath
            datavalue['id'] =  offerViewModel.offer_id()
        }
        else if(offerViewModel.action()=="add"){
            offerApiPath = addOfferApiPath
        }
        setting.url  = offerApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           resetAll();
           location.href=offerList+"?pending=1";
        };
        setting.error = function(jqXHR, textStatus, error){
            if(jqXHR.status==422){
                $.each(jqXHR.responseJSON.errors,function(i,item){
                    $("#publishDate" ).after(function() {
                        return "<label id='publishDate'-error' class='error' for='publishDate'>"+item+"</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            }    
        };
        apiAjax(setting);
	}
    
}

function resetAll(){
	$('#txtTitle').val("");
    $('#txtDescription').val("");
	$('#txtValue').val("");
	$('#txtPrice').val("");
   	$('#validTo').val("");
	$('#count').val("");
    $('#publishDate').val("");
    $('#notes').val("");
    $('#txtContact').val("");
}
function getOffers(){
    var setting = {};
    var datavalue = {
        'bid' : bid,
        'status':offerViewModel.status(),
    };
    setting.url  = offerListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
    	console.log(response);
        offerViewModel.total(response.data.total);
        var offer_list = response.data.data;
        if ($.fn.DataTable.isDataTable("#offerList")) {
            $('#offerList').DataTable().clear().destroy();
        } 
        offerViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                offerViewModel.offers.push(new OfferModel(item));
            });
            offerViewModel.offerList(true);
            offerViewModel.offerNoData(false);
        } else {
            offerViewModel.offerList(false);
            offerViewModel.offerNoData(true);
        }
        $("#offerList").DataTable({
                responsive: true,
                bSort : false,
                searching: false,
                pageLength:15,
                bLengthChange: false
            });
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getAddress(){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            offerViewModel.address([]);
            $.each(address_list, function(i,item) {
                offerViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
$('input[type=radio][name=txtApplicableOn]').change(function() {
    if(this.value == 1){
        offerViewModel.fixDateVisibilty(true);
        offerViewModel.validToVisibilty(false);
    } else {
        offerViewModel.fixDateVisibilty(false);
        offerViewModel.validToVisibilty(true);
    }       
});
 function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }