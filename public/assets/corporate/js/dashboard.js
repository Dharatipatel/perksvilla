var viewModelName;
var statisticsViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#dashboardTab').addClass('active');
	initializeKnockout();
    getStatistics();
    getEmployees();
    $('.employee-scroll').slimScroll({
        height: '260px'
    });
});

function initializeKnockout(){
	statisticsViewModel = new StatisticsViewModel();
	viewModelName  = statisticsViewModel;
	ko.applyBindings(statisticsViewModel);
}

function StatisticsViewModel(){
	self = this;
	self.used = ko.observable(0);
	self.unused = ko.observable(0);
	self.brands = ko.observable(0);
	self.employee = ko.observable(0);
	self.amount_saved = ko.observable(0);
	self.amount_invested = ko.observable(0);
	self.social = ko.observable(0);
    self.availble_deals = ko.observable(0);
	self.imagePathEmployee = ko.observable(path);
    self.employees = ko.observableArray([]);
    self.total = ko.observable(0);

}
function getEmployees(){

    var setting = {};
    var datavalue = {
        'cid':cid,
        'status':1
    };
    
    setting.url    = employeeListApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        statisticsViewModel.total(response.data.total);
        var employee_list = response.data.data;
        if (employee_list.length > 0) {
            statisticsViewModel.employees([]);
            $.each(employee_list, function (i, item) {
                statisticsViewModel.employees.push(new EmployeeModel(item));
            });

        } else {
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getStatistics(){
	var setting = {};
    var datavalue = {
        'cid':cid,
    };
    
    setting.url    = statisticsApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        statisticsViewModel.used(response.data.used_reward);
        statisticsViewModel.unused(response.data.unused_reward);
        statisticsViewModel.brands(response.data.brand);
        statisticsViewModel.employee(response.data.employee);
        statisticsViewModel.amount_saved(response.data.amount_saved);
        statisticsViewModel.amount_invested(response.data.amount_invested);
        statisticsViewModel.social(response.data.social);
        statisticsViewModel.availble_deals(response.data.availble_deals);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
