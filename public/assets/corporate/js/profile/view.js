var viewModelName;
var profileViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#myprofile').addClass('active');
    $('#myprofiledropdown').addClass('show');
    if(type =='edit'){
        $('#editCorporateTab').addClass('active');
    } else if(type=='view'){
        $('#viewCorporateTab').addClass('active');
    }
	initializeKnockout();
	getCorporates();
    getCategories();
    var rules = {
            
            txtCorporateName : "required",
            txtCompanySize : "required",
            txtDescription : "required",
            txtCategory : "required",
            txtEmail: {
                'required' :true,
                'email' :true,
            },
            txtContact: {
                'required' :true,
                'digits': true
            },
            txtPassword : "required",
            txtPersonName : "required",
            // txtWebsiteUrl :{
            //     "required" :true,
            //     "url" :true,
            // },
            // txtFacebookPage : "required",
            txtAddress : "required",
            txtStatus : "required",
            // txtLinkedInPage : "required",
        };
    var messages = {
            txtCorporateName: "Please enter a corporate name",
            txtCompanySize: "Please enter  a Company Size",
            txtDescription: "Please enter  a description",
            txtCategory: "Please select category",
            txtEmail: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter a valid email address'
            },
            txtContact: {
                'required' : 'Please Enter mobile',
                'digits' : 'Mobile should be only in digits'
            },
            txtPassword: "Please enter password",
            txtPersonName: "Please enter person name",
            // txtWebsiteUrl: {
            //     'required':"Please Enter url",
            //     'url':"Please Enter valid url",
            // },
            // txtFacebookPage: "Please Enter facebook page",
            txtAddress: "Please Enter address",
            // txtLinkedInPage: "Please Enter Linked in page Url",
            txtStatus: "Please select  a Status",
        };
    $("#form_corporate").validate({
        rules:rules ,
        messages: messages,
    });
});

$('#fileInput').change(function(){
   var file = $('#fileInput').prop('files')[0];
   var ImageURL;
   console.log(file);
   var FR = new FileReader();
   FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        // var blob = b64toBlob(    , contentType);
        profileViewModel.imageUri(realData);
   }, false);
   if (file) {
        FR.readAsDataURL(file);
   }
});
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function getCorporates(){
	var setting = {};
	setting.url  = viewProfileApiPath.replace("id",id);
	setting.type = 'GET';
	setting.data = {};
	setting.token = window.btoa(id+"|"+token);
	setting.success = function(response){
		// if(response.data.length > 0){
		// 	console.log(response.data.id);
			profileViewModel.id(response.data.id);
            profileViewModel.uid(response.data.uid);
			profileViewModel.company_size(response.data.company_size);
			profileViewModel.corporate(response.data.name);
			profileViewModel.description(response.data.description);
			profileViewModel.personname(response.data.contact_person_name);
			profileViewModel.email(response.data.email);
			profileViewModel.contact(response.data.contact);
			profileViewModel.facebook_page(response.data.facebook_page);
			profileViewModel.website_url(response.data.website_url);
			profileViewModel.address(response.data.address);
            if(response.data.logo==null){
                profileViewModel.profile(static_image);
            }else{
                profileViewModel.profile(response.data.logo+"?t="+Math.random());
            }
            profileViewModel.selectedCategory(response.data.category);
            profileViewModel.linkedin_page(response.data.linkedin_page);
		// }
	};
	setting.error = function(jqXHR, textStatus, error){
		swal(jqXHR.responseJSON.message,"",'error');
		return false;
	};
	apiAjax(setting);
}
function initializeKnockout(){
	profileViewModel = new ProfileViewModel();
	viewModelName  = profileViewModel;
	ko.applyBindings(profileViewModel);
}

function ProfileViewModel(){
	self = this;
	self.id =ko.observable("");
	self.uid =ko.observable("");
    self.corporate =ko.observableArray("");
	self.company_size =ko.observableArray("");
	self.description =ko.observable("");
	self.personname =ko.observable("");
	self.email =ko.observable("");
	self.contact =ko.observable("");
	self.facebook_page =ko.observable("");
    self.website_url =ko.observable("");
	self.linkedin_page =ko.observable("");
    self.selectedCategory= ko.observable("");
    self.availbleCategory= ko.observableArray([]);
	self.address =ko.observable("");
	self.pincode =ko.observable("");
	self.business_hours =ko.observable("");
	self.profile = ko.observable("");
    self.imagePath = ko.observable(path);
	
    self.imageUri = ko.observable("");
    self.removeProfileClick = function(){
        var datavalue = {};
        var setting = {};
        datavalue['id'] = profileViewModel.id();
        setting.url  = removeProfileApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            window.location = editwebPath;
        };
        setting.error = function(jqXHR, textStatus, error){    
            swal(jqXHR.responseJSON.message, "", 'error');
            return false;                
        };
        apiAjax(setting);
    }
	self.updateProfileClick = function(){
        if(!$("#form_corporate").valid()){
            return false
        }
        var datavalue = {};
        var setting = {};
        
        var corporate_name = profileViewModel.corporate();
        var company_size = profileViewModel.company_size();
        var description = profileViewModel.description();
        var email = profileViewModel.email();
        var mobile = profileViewModel.contact();
       
        var person_name = profileViewModel.personname();
        var website_url = profileViewModel.website_url();
        var facebook_page = profileViewModel.facebook_page();
        var address = profileViewModel.address();
        var category = profileViewModel.selectedCategory();
        var linkedin_page = profileViewModel.linkedin_page();
        

      
        datavalue['id'] = profileViewModel.id();
        datavalue['uid'] = profileViewModel.uid();       
        datavalue['name'] = corporate_name;
        datavalue['company_size'] = company_size;
        datavalue['description'] = description;
        datavalue['email'] = email;
        datavalue['contact'] = mobile;
        datavalue['linkedin_page'] =linkedin_page;
        datavalue['person_name'] = person_name;
        datavalue['website_url'] = website_url;
        datavalue['facebook_page'] = facebook_page;
        datavalue['address'] = address;
        datavalue['category'] = category;
        datavalue['image'] = profileViewModel.imageUri();
        datavalue['status'] = 1;

        setting.url  = updateProfileApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
          		// swal("Update Successfully","",'success');
                // return false;
                window.location = editwebPath;
        };
        setting.error = function(jqXHR, textStatus, error){
                if (jqXHR.status == 422) {
                    $.each(jqXHR.responseJSON.errors, function (i, item) {
                        var fieldname;
                        tail = i.substring(1);
                        fieldName = i.substring(0, 1).toUpperCase() + tail;
                        fieldname = "txt" + fieldName;
                        $("#" + fieldname).after(function () {
                            return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                        });
                    })
                    return false;
                } else {
                    swal(jqXHR.responseJSON.message, "", 'error');
                    return false;
                }
            };
        apiAjax(setting);
    
	}

}
function getCategories() {
    var setting = {};
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = {'limit': 1000, 'type': 2, 'status': 1};
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        var category_list = response.data.data;
        if (category_list.length > 0) {
            $.each(category_list, function (i, item) {
            profileViewModel.availbleCategory.push(new availableCategory(item.id, item.name));
            });
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function availableCategory(k, v) {

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}