var viewModelName;
var paymentViewModel;
$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#summaryTab').addClass('active');
    $('#paymentRequestTab').addClass('active');
    initlizeKnockout();
    getPaymentRequest();
});
function initlizeKnockout() {

    paymentViewModel = new PaymentViewModel();
    viewModelName = paymentViewModel;
    ko.applyBindings(paymentViewModel);
}
function PaymentViewModel() {
    self = this;
    self.brand_id = ko.observable("");
    self.limit = ko.observable(10000);
    self.paymentRequests = ko.observableArray([]);
    self.total = ko.observable(0);
    self.paymentListContainer = ko.observable(true);
    self.paymentList = ko.observable(false);
    self.paymentNoData = ko.observable(true);
    self.paymentAddContainer = ko.observable(false);
    self.viewBankDetails = function(request){
        paymentViewModel.brand_id(request.brand_id());
        getBankDetails();
        $('#viewBankDetail').modal();
    };
    self.activeClick = function(request) {
        var setting = {};
        var datavalue = {
            'id': request.id(),
            'brand_uid': request.user_id(),
            'amount' :request.withdraw_amount(),
        };
        setting.url = paymentPaidApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            window.location.reload();
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
    apiAjax(setting);
    }

    self.bankDetailsViewModel = new  BankDetailsViewModel;
}
function BankDetailsViewModel(){
    self = this;
    self.bank_name    = ko.observable("");
    self.account_type = ko.observable("");
    self.account_number    = ko.observable("");
    self.ifsc    = ko.observable("");
    self.branch_details    = ko.observable("");
}

function getPaymentRequest() {
    var setting = {};
    var datavalue = {
        'limit': paymentViewModel.limit(),
    };
    setting.url = paymentListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        paymentViewModel.total(response.data.total);
        var payment_list = response.data;
        if (payment_list.length > 0) {
            paymentViewModel.paymentRequests([]);
            $.each(payment_list, function (i, item) {
                paymentViewModel.paymentRequests.push(new TransactionModel(item));
            });

            $("#payment_tbl").DataTable( { responsive: true } );
            paymentViewModel.paymentListContainer(true);
            paymentViewModel.paymentAddContainer(false);
            paymentViewModel.paymentList(true);
            paymentViewModel.paymentNoData(false);

        } else {
            paymentViewModel.paymentListContainer(true);
            paymentViewModel.paymentAddContainer(false);
            paymentViewModel.paymentList(false);
            paymentViewModel.paymentNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}
function getBankDetails(){
    var setting = {};
    var datavalue = {};
    datavalue['bid'] = paymentViewModel.brand_id();    
    setting.url  = getBankDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        if(response.data){
            paymentViewModel.bankDetailsViewModel.bank_name(response.data.name);
            paymentViewModel.bankDetailsViewModel.account_type(response.data.account_type);
            paymentViewModel.bankDetailsViewModel.account_number(response.data.account_number);
            paymentViewModel.bankDetailsViewModel.ifsc(response.data.ifsc);
            paymentViewModel.bankDetailsViewModel.branch_details(response.data.branch_details);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);


}
