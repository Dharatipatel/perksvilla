var viewModelName;
var couponViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#couponTab').addClass('active');
    initlizeKnockout()
    if(type==0){
        $('#activeCouponTab').addClass('active');
        $('#usedCouponTab').removeClass('active');
        $('#expiredCouponTab').removeClass('active');
        couponViewModel.status(0);
    }
    else if(type==1){
        $('#usedCouponTab').addClass('active');
        $('#activeCouponTab').removeClass('active');
        $('#expiredCouponTab').removeClass('active');
        couponViewModel.status(1);   
    }
    else if(type==2){
        $('#expiredCouponTab').addClass('active');
        $('#activeCouponTab').removeClass('active');
        $('#usedCouponTab').removeClass('active');
        couponViewModel.status(2);
    }
    getCoupons();
});
function initlizeKnockout(){
    couponViewModel = new CouponViewModel();
    viewModelName  = couponViewModel;
    ko.applyBindings(couponViewModel);
}
function CouponViewModel(){
    self=this;
    self.status=ko.observable(0);
    self.total=ko.observable(0);
    self.coupons=ko.observableArray([]);
    self.couponList = ko.observable(false);
    self.couponNoData = ko.observable(false);
    self.couponListContainer = ko.observable(true);
    /*PopUp*/

    self.offer_id = ko.observable("");
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    self.offerViewClick = function(offer){
        couponViewModel.offer_id(offer.offer_id());
        if(type ==1){
            couponViewModel.coupon_code(offer.coupon_code());
            couponViewModel.isCoupon(true);
        }
        getOfferDetail();
    }
    self.offers= ko.observableArray([]);
    self.address= ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.contact = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
}
function getCoupons(){

    var setting = {};
    var datavalue = {};
    datavalue['status'] = couponViewModel.status();
    setting.url  = couponListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        couponViewModel.total(response.data.total);
        var coupon_list = response.data.data;
        if(coupon_list.length>0){
            couponViewModel.coupons([]);
            $.each(coupon_list, function(i,item) {
                couponViewModel.coupons.push(new CouponModel(item));
            });
            $("#coupon_tbl").DataTable( { responsive: true } );
            couponViewModel.couponList(true);
            couponViewModel.couponNoData(false);

        } else {
            couponViewModel.couponList(false);
            couponViewModel.couponNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getOfferDetail(){
    var setting = {};
    var datavalue = {
       'offer_id':couponViewModel.offer_id(),
    };
    
    setting.url  = offerDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            couponViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                couponViewModel.offers.push(new OfferModel(item));
            });
            var offer = couponViewModel.offers()[0];
            couponViewModel.offer_title(offer.name());
            couponViewModel.offer_description(offer.description());
            couponViewModel.coupon_value(offer.value());
            couponViewModel.valid_to(offer.valid_to());
            couponViewModel.contact(offer.contact());
            couponViewModel.applicable_on(offer.apply_on_text());
            couponViewModel.address_array(offer.brand_address_id().split(','));
            couponViewModel.business_hours(offer.business_hours());
            couponViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
            couponViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
            couponViewModel.notes(offer.notes().replace(/\n/g, "<br />"));
            couponViewModel.brand_logo(path+""+offer.logo());

            getAddress(offer.brand_id());
        }
    
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            $.each(address_list, function(i,item) {
                couponViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            couponViewModel.selected_address([]);
            for (var i = 0; i < couponViewModel.address().length ; i++) {
                if(couponViewModel.address_array.indexOf(couponViewModel.address()[i].value().toString()) != -1){
                    couponViewModel.selected_address.push(couponViewModel.address()[i].textaddress());
                }
            }
            $('#viewModel').modal('show');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
}