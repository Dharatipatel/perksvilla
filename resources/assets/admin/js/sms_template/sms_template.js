var viewModelName;
var sms_templateViewModel;
var sms_templateApiPath;
$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#sms_templateTab').addClass('active');
    initlizeKnockout();
    if(type=="list"){
        getSMSTemplates();
    } else if(type=="add") {
        sms_templateViewModel.sms_templateListContainer(false);
        sms_templateViewModel.sms_templateAddContainer(true);
        sms_templateViewModel.action_title("Add");
        sms_templateViewModel.action_name("Add SMS Template");
        sms_templateViewModel.action("add");
    }
    
    var rules = {

        txtName: "required",
        txtMessage: "required",

    };
    var messages = {
        txtName: "Please enter a Template name",
        txtMessage: "Please enter a message",
    };
    $("#form_sms_template").validate({
        rules: rules,
        messages: messages,
    });
});
function initlizeKnockout() {

    sms_templateViewModel = new sms_templateViewModel();
    viewModelName = sms_templateViewModel;
    ko.applyBindings(sms_templateViewModel);
}
function sms_templateViewModel() {
    self = this;
    self.current_id = ko.observable("");
    self.limit = ko.observable(100);
    self.sms_templates = ko.observableArray([]);
    // self.selectedCategory= ko.observable("");
    // self.availbleCategory= ko.observableArray([]);
    self.selectedStatus = ko.observable(1);
    
    self.total = ko.observable(0);
    self.sms_templateListContainer = ko.observable(true);
    self.sms_templateList = ko.observable(false);
    self.sms_templateNoData = ko.observable(true);
    self.sms_templateAddContainer = ko.observable(false);
    self.addSMSTemplateClick = function () {
        sms_templateViewModel.sms_templateListContainer(false);
        sms_templateViewModel.sms_templateAddContainer(true);
        sms_templateViewModel.action_title("Add");
        sms_templateViewModel.action_name("Add SMS Template");
        sms_templateViewModel.action("add");
    };
    self.action = ko.observable("");
    self.action_title = ko.observable("");
    self.editSMSTemplateClick = function (sms_template) {
        sms_templateViewModel.sms_templateListContainer(false);
        sms_templateViewModel.sms_templateAddContainer(true);
        sms_templateViewModel.action_title("Edit");
        sms_templateViewModel.action_name("Edit SMS Template");
        sms_templateViewModel.action("edit");
        $('#txtName').val(sms_template.title());
        $('#txtMessage').val(sms_template.text());
        sms_templateViewModel.current_id(sms_template.id());
    };
    self.cancelSMSTemplateClick = function () {
        sms_templateViewModel.sms_templateListContainer(true);
        sms_templateViewModel.sms_templateAddContainer(false);
        $('#txtName').val("");
        $('#txtMessage').val("");
        
    };
    self.deleteSMSTemplateClick = function (sms_template) {
        sms_templateViewModel.current_id(sms_template.id());
        $('#deleteModal').modal('show');
    };
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': sms_templateViewModel.current_id()
        };
        setting.url = deleteSMSTemplateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteModal').modal('hide');
            swal(response.message, "", "success");
            getSMSTemplates();

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };

    self.createSMSTemplateClick = function () {
        if (!$("#form_sms_template").valid()) {
            return false;
        }

        var datavalue = {};
        var sms_template_name = $('#txtName').val();
        var message = $('#txtMessage').val();

        if (sms_templateViewModel.action() == "edit") {
            datavalue['id'] = sms_templateViewModel.current_id();
            sms_templateApiPath = updateSMSTemplateApiPath;
        } else if (sms_templateViewModel.action() == "add") {
            sms_templateApiPath = addSMSTemplateApiPath;
        }
        var setting = {};
        datavalue['name'] = sms_template_name;
        datavalue['message'] = message;

        setting.url = sms_templateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            // getSMSTemplates();
            // resetAll();
            location.href=sms_templateListWebPath;
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {

                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message, "", "error");
                return false;
            }
        };
        apiAjax(setting);
    };
    self.activeClick = function (sms_template) {
        var setting = {};
        var datavalue = {
            'id': sms_template.id(),
        };
        setting.url = activeSMSTemplateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", "success");
            getSMSTemplates();

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function (sms_template) {
        var setting = {};
        var datavalue = {
            'id': sms_template.id(),
        };
        setting.url = deactiveSMSTemplateApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", "success");
            getSMSTemplates();

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
}
function resetAll() {
    $('#txtName').val("");
    $('#txtMessage').val("");
}
function getSMSTemplates() {
    var setting = {};
    var datavalue = {
        'limit': sms_templateViewModel.limit(),
    };
    setting.url = sms_templateListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        sms_templateViewModel.total(response.data.total);
        var sms_template_list = response.data.data;
        if (sms_template_list.length > 0) {
            sms_templateViewModel.sms_templates([]);
            $.each(sms_template_list, function (i, item) {
                sms_templateViewModel.sms_templates.push(new SMSTemplateModel(item));
            });

            // $("#sms_template_tbl").DataTable( { responsive: true } );
            sms_templateViewModel.sms_templateListContainer(true);
            sms_templateViewModel.sms_templateAddContainer(false);
            sms_templateViewModel.sms_templateList(true);
            sms_templateViewModel.sms_templateNoData(false);

        } else {
            sms_templateViewModel.sms_templateListContainer(true);
            sms_templateViewModel.sms_templateAddContainer(false);
            sms_templateViewModel.sms_templateList(false);
            sms_templateViewModel.sms_templateNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}