var viewModelName;
var rewardListViewModel;
$(document).ready(function(){
    initializeKnockout();
     $('.has-submenu').removeClass('active');
    $('#rewardTab').addClass('active');
    if(type=='general'){
        $('#rewardGeneralTab').addClass('active');
        $('#rewardUserCreatedTab').removeClass('active');
    }
    else if(type=='user'){
        $('#rewardUserCreatedTab').addClass('active');
        $('#rewardGeneralTab').removeClass('active');
    }
    getRewards();
   
});
function initializeKnockout(){
    rewardListViewModel = new RewardListViewModel();
    viewModelName  = rewardListViewModel;
    ko.applyBindings(rewardListViewModel);
}

function RewardListViewModel(){
    self = this;
    self.total = ko.observable("");
    self.rewards= ko.observableArray([]);
    self.status = ko.observable(0);
    self.used = ko.observable(0);
    self.unused = ko.observable(0);
    self.rewardList = ko.observable(true);
    self.rewardListContainer = ko.observable(true);
    self.rewardNoData = ko.observable(false);
    self.activeRewardClick = function(){
        rewardListViewModel.status(0);
        getRewards();
    }
    self.usedRewardClick = function(){
        rewardListViewModel.status(1);
        getRewards();
    }
}
function getRewards(){
    var setting = {};
    var datavalue = {
        'status':rewardListViewModel.status(),
        'type':type,
    };
    
    setting.url  = rewardListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var rewards_list = response.data.data;
            rewardListViewModel.rewards([]);
        if(rewards_list.length>0){
            $.each(rewards_list, function(i,item) {
                rewardListViewModel.rewards.push(new RewardModel(item));
            });
            rewardListViewModel.rewardNoData(false);
            rewardListViewModel.rewardList(true);
        } else {
            rewardListViewModel.rewardNoData(true);
            rewardListViewModel.rewardList(false);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
