var viewModelName;
var employeeViewModel;
var employeeApiPath;
$(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#employeeTab').addClass('active');
    initlizeKnockout();
    getEmployees();
    var rules = {

        txtEmployeeName: "required",
        txtCompany_size: {
            'required': true,
            'digits': true
        },
        txtDescription: "required",
        txtEmail: {
            'required': true,
            'email': true,
        },
        txtMobile: {
            'required': true,
            'digits': true
        },
        txtPassword: "required",
        txtBusinessHours: {
            'required': true,
        },
        txtPersonName: "required",
        txtWebsiteUrl: {
            "required": true,
            "url": true,
        },
        txtFacebookPage: "required",
        txtAddress: "required",
        txtPincode: "required",
        txtStatus: "required",
    };
    var messages = {
        txtBrandName: "Please enter a Employee name",
        txtCompany_size: {
            'required': 'Please Enter Company size',
            'digits': 'Company size should be only in digits'
        },
        txtDescription: "Please enter  a description",
        txtEmail: {
            'required': 'Please Enter Email',
            'email': 'Please enter a valid email address'
        },
        txtMobile: {
            'required': 'Please Enter mobile',
            'digits': 'Mobile should be only in digits'
        },
        txtPassword: "Please enter password",
        txtPersonName: "Please enter person name",
        txtWebsiteUrl: {
            'required': "Please Enter url",
            'url': "Please Enter valid url",
        },
        txtFacebookPage: "Please Enter facebook page",
        txtAddress: "Please Enter address",
        txtPincode: "Please Enter pincode",
        txtStatus: "Please select  a Status",
    };
    $("#form_employee").validate({
        rules: rules,
        messages: messages,
    });
});
function initlizeKnockout() {

    employeeViewModel = new employeeViewModel();
    viewModelName = employeeViewModel;
    ko.applyBindings(employeeViewModel);
}
function employeeViewModel() {
    self = this;
    self.current_id = ko.observable("");
    self.current_uid = ko.observable("");
    self.limit = ko.observable(100);
    self.employees = ko.observableArray([]);
    // self.selectedCategory= ko.observable("");
    // self.availbleCategory= ko.observableArray([]);
    self.selectedStatus = ko.observable(1);
    self.availbleStatus = [{
            text: "Active",
            value: 1
        }, {
            text: "Deactive",
            value: 0
        }];
    self.total = ko.observable(0);
    self.employeeListContainer = ko.observable(true);
    self.employeeList = ko.observable(false);
    self.employeeNoData = ko.observable(true);
    self.employeeAddContainer = ko.observable(false);
    self.addEmployeeClick = function () {
        employeeViewModel.employeeListContainer(false);
        employeeViewModel.employeeAddContainer(true);
        employeeViewModel.action_title("Add");
        employeeViewModel.action_name("Add Employee");
        employeeViewModel.action("add");
    };
    self.action = ko.observable("");
    self.action_title = ko.observable("");
    self.editEmployeeClick = function (employee) {
        employeeViewModel.employeeListContainer(false);
        employeeViewModel.employeeAddContainer(true);
        employeeViewModel.action_title("Edit");
        employeeViewModel.action_name("Edit Employee");
        employeeViewModel.action("edit");
        $('#txtEmployeeName').val(employee.name());
        $('#txtCompany_size').val(employee.company_size());
        $('#txtDescription').val(employee.description());
        $('#txtEmail').val(employee.email());
        $('#txtMobile').val(employee.contact());
        $('#txtPassword').val("");
        $('#txtPassword').attr("disabled", "disabled");
        $('#txtBusinessHours').val(employee.business_hours());
        $('#txtPersonName').val(employee.contact_person_name());
        $('#txtWebsiteUrl').val(employee.website_url());
        $('#txtFacebookPage').val(employee.facebook_page());
        $('#txtAddress').val(employee.address());
        $('#txtPincode').val(employee.pincode());
        // employeeViewModel.selectedCategory(employee.categoryid());
        employeeViewModel.selectedStatus(employee.status());
        employeeViewModel.current_id(employee.id());
        employeeViewModel.current_uid(employee.uid());
    };
    self.deleteEmployeeClick = function (employee) {
        employeeViewModel.current_id(employee.id());
        $('#deleteModal').modal('show');
    };
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': employeeViewModel.current_id()
        };
        setting.url = deleteEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteModal').modal('hide');
            swal(response.message, "", "success");
            // getEmployees();
            location.href= employeeWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            $('#deleteModal').modal('hide');
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };

    self.createEmployeeClick = function () {
        if (!$("#form_employee").valid()) {
            return false;
        }

        var datavalue = {};
        var employee_name = $('#txtEmployeeName').val();
        var description = $('#txtDescription').val();
        var company_size = $('#txtCompany_size').val();
        var email = $('#txtEmail').val();
        var mobile = $('#txtMobile').val();
        var password = $('#txtPassword').val();
        var business_hours = $('#txtBusinessHours').val();
        var person_name = $('#txtPersonName').val();
        var website_url = $('#txtWebsiteUrl').val();
        var facebook_page = $('#txtFacebookPage').val();
        var address = $('#txtAddress').val();
        var pincode = $('#txtPincode').val();


        if (employeeViewModel.action() == "edit") {
            datavalue['id'] = employeeViewModel.current_id();
            datavalue['uid'] = employeeViewModel.current_uid();
            employeeApiPath = updateEmployeeApiPath;
        } else if (employeeViewModel.action() == "add") {
            employeeApiPath = addEmployeeApiPath;
            datavalue['password'] = $.md5(password);
        }
        var setting = {};
        datavalue['name'] = employee_name;
        datavalue['company_size'] = company_size;
        datavalue['description'] = description;
        datavalue['email'] = email;
        datavalue['contact'] = mobile;
        datavalue['business_hours'] = business_hours;
        datavalue['person_name'] = person_name;
        datavalue['website_url'] = website_url;
        datavalue['facebook_page'] = facebook_page;
        datavalue['address'] = address;
        datavalue['pincode'] = pincode;
        datavalue['status'] = employeeViewModel.selectedStatus();

        setting.url = employeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            // getEmployees();
            location.href= employeeWebPath;
            resetAll();
        };
        setting.error = function (jqXHR, textStatus, error) {
            if (jqXHR.status == 422) {

                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    var fieldname;
                    tail = i.substring(1);
                    fieldName = i.substring(0, 1).toUpperCase() + tail;
                    fieldname = "txt" + fieldName;
                    $("#" + fieldname).after(function () {
                        return "<label id='" + fieldname + "'-error' class='error' for='" + fieldname + "'>" + item + "</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message, "", "error");
                return false;
            }
        };
        apiAjax(setting);
    };
    self.activeClick = function (employee) {
        var setting = {};
        var datavalue = {
            'id': employee.id(),
        };
        setting.url = activeEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", "success");
            // getEmployees();
            location.href= employeeWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function (employee) {
        var setting = {};
        var datavalue = {
            'id': employee.id(),
        };
        setting.url = deactiveEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            swal(response.message, "", "success");
            // getEmployees();
            location.href= employeeWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    }
    self.action_name = ko.observable("");
}
function resetAll() {
    $('#txtEmployeeName').val("");
    $('#txtCompany_size').val("");
    $('#txtCategory').val("");
    $('#txtDescription').val("");
    $('#txtEmail').val("");
    $('#txtMobile').val("");
    $('#txtPassword').removeAttr("disabled");
    $('#txtPassword').val("");
    $('#txtBusinessHours').val("");
    $('#txtPersonName').val("");
    $('#txtWebsiteUrl').val("");
    $('#txtFacebookPage').val("");
    $('#txtAddress').val("");
    $('#txtPincode').val("");
}
function getEmployees() {
    var setting = {};
    var datavalue = {
        'limit': employeeViewModel.limit(),
    };
    if(corporate_id != ""){
        datavalue['cid'] = corporate_id;
    }
    setting.url = employeeListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        employeeViewModel.total(response.data.total);
        var employee_list = response.data.data;
        if (employee_list.length > 0) {
            employeeViewModel.employees([]);
            $.each(employee_list, function (i, item) {
                employeeViewModel.employees.push(new EmployeeModel(item));
            });

            $("#employee_tbl").DataTable( { responsive: true } );
            employeeViewModel.employeeListContainer(true);
            employeeViewModel.employeeAddContainer(false);
            employeeViewModel.employeeList(true);
            employeeViewModel.employeeNoData(false);

        } else {
            employeeViewModel.employeeListContainer(true);
            employeeViewModel.employeeAddContainer(false);
            employeeViewModel.employeeList(false);
            employeeViewModel.employeeNoData(true);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);
}