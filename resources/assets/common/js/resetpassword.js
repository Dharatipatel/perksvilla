var viewModelName;
var forgotPasswordViewModel;
$(document).ready(function () {
    initializeKnockout();
    $("#forgot-password").validate({
        rules: {
            txtEmail: {
                'required': true,
                'email': true,
            },
            txtPassword: {
                'required': true
            },
            txtConfirmPassword: {
                'required': true
            },
        },
        messages: {
            txtEmail: {
                'required': 'Please Enter Email',
                'email': 'Please enter a valid email address'
            },
            txtPassword: {
                'required': "Please enter password"
            },
            txtConfirmPassword: {
                'required': "Please confirm password"
            },
        },
    });
});

function initializeKnockout() {
    forgotPasswordViewModel = new ForgotPasswordViewModel();
    viewModelName = forgotPasswordViewModel;
    ko.applyBindings(forgotPasswordViewModel);
}

function ForgotPasswordViewModel() {
    self = this;
    self.email = ko.observable();
    self.password = ko.observable();
    self.confirmPassword = ko.observable();
    self.sendEmailClick = function () {
        if (!$("#forgot-password").valid()) {
            return false;
        }
        var setting = {};
        self.email($("#txtEmail").val());
        self.password($("#txtPassword").val());
        self.confirmPassword($("#txtConfirmPassword").val());
        var datavalue = {
            'email': self.email(),
            '_token': _token,
            'password': self.password(),
            'password_confirmation': self.confirmPassword(),
            'reset_token': reset_token
        };
        setting.url = resetpassword;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.success = function (response) {
            if (response.error) {
                swal("Error", response.message, 'error');
            } else {
                swal("Success", response.message, 'success');
                window.location.href = response.data.url;
            }
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal("Error", jqXHR.responseJSON.message, 'error');
            return false;
        };
        apiAjax(setting);
    };
}

function resetAll() {
    $("#txtEmail").val('');
}
