var viewModelName;
var couponViewModel;
var isBefore;
var isAfter;
var isAftermessage;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#couponTab').addClass('active');
	initializeKnockout()
    $("#coupen_form").validate({
        rules: {
            txtCoupenCode : "required",
            txtEmailPhone: "required",
        },
        messages: {
            txtCoupenCode: "Please enter a Coupon Code",
            txtEmailPhone: "Please enter an Email Id or Mobile Number",
        },
    });
     $("#redeem_form").validate({
        rules: {
            txtMember: {
                
                required: true,
                min: 1
            },
            txtBillAmount: {
                
                required: true,
                min: 1
            },
            txtRedeemCoupenCode: "required",
            txtOtp:"required"
        },
        messages: {
            txtBillAmount :{
              'required'  : "Please enter a Bill Amount",
              'min'  : "It Should be grater than 0",
            },
            txtMember :{
              'required'  : "Please enter a Member",
              'min'  : "It Should be Minimum 1",
            },
            txtRedeemCoupenCode: "Please enter Coupen Code",
            txtOtp : "Please Enter OTP"
        },
    });
});

function initializeKnockout(){
	couponViewModel = new CouponViewModel();
	viewModelName  = couponViewModel;
	ko.applyBindings(couponViewModel);
}

function CouponViewModel(){
    self = this;
    self.coupenCode = ko.observable();
    self.errorText = ko.observable();
    self.emailPhone = ko.observable();
    self.isNoCouponAvailble = ko.observable(false);
    self.isCouponAvailble = ko.observable(false);
    self.issued_by = ko.observable();
    self.email = ko.observable();
    self.contact = ko.observable();
    self.employee_id = ko.observable();
    self.issued_by = ko.observable();
    self.issued_for = ko.observable();
    self.issued_date = ko.observable();
    self.expire_date = ko.observable();
    self.issued_date_show = ko.observable();
    self.expire_date_show = ko.observable();
    self.otp = ko.observable();
    self.coupon_value = ko.observable(0);
    self.coupon_value_show = ko.observable('Rs 0');
    self.billAmount = ko.observable(0);
    self.member = ko.observable("");
    self.customerPayble = ko.observable(0);
    self.reward_id = ko.observable("");
    self.apply_on= ko.observable();
    self.redeemVisibility = ko.observable(false);
    self.sendOtpVisibility = ko.observable(true);
    self.searchCoupenDetails = function(){
        if(!$("#coupen_form").valid()){
            return false;
        }
        getCoupenDetails();        
    }
    self.billAmountChange = function(){
      if((couponViewModel.billAmount()-couponViewModel.coupon_value())>0){

        couponViewModel.customerPayble(couponViewModel.billAmount()-couponViewModel.coupon_value());
      }else{
        couponViewModel.customerPayble(0);
      }
    }
    self.redeemCoupen = function(){
        if(!$("#redeem_form").valid()){
            return false;
        }
        redeemCoupon();
    };
    self.sendOtp = function(){
        sendOTP();
    }
     /* popup */
    self.brand_id = ko.observable();
    self.address = ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.contact = ko.observable("");
    self.expiry_label = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    self.viewCoupenDetails = function(){
        $('#viewModal').modal('show');
    }
}
function sendOTP () {

    var setting = {};
    couponViewModel.emailPhone($("#txtEmailPhone").val());
    var datavalue = {
        'employee_name' : couponViewModel.issued_for(),
        'employeeId' :couponViewModel.employee_id(),
        'txtPhone' : couponViewModel.contact(),
        'txtEmail' : couponViewModel.email(),
        'txtCoupon' : couponViewModel.coupenCode(),
    };
    setting.url  = sendOtpApiPath;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
          swal("OTP send Succeessfully","","success");
          couponViewModel.redeemVisibility(true);                                                                                                                                                                                                                                                                                                                          
          couponViewModel.sendOtpVisibility(false);                                                                                                                                                                                                                                                                                                                          
    };
    setting.error = function(jqXHR, textStatus, error){
          swal("Otp is not sent, Try again","","error");
          // couponViewModel.redeemVisibility(true);                                                                                                                                                                                                                                                                                                                          
          // couponViewModel.sendOtpVisibility(false); 
    };
    apiAjax(setting);

}
function getCoupenDetails(){
    var setting = {};
    couponViewModel.coupenCode($("#txtCoupenCode").val());
    couponViewModel.emailPhone($("#txtEmailPhone").val());
    var datavalue = {
        'txtCoupenCode' : couponViewModel.coupenCode(),
        'txtEmailPhone' : couponViewModel.emailPhone(),
        // 'brand_id' : bid
    };
    setting.url  = couponDetailApiPath;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var coupen_detail = response.data;
        if(coupen_detail){
               couponViewModel.isCouponAvailble(true);
               couponViewModel.isNoCouponAvailble(false);
               couponViewModel.issued_by(coupen_detail.company_name);
               couponViewModel.email(coupen_detail.email);
               couponViewModel.employee_id(coupen_detail.employee_id);
               couponViewModel.contact(coupen_detail.contact);
               couponViewModel.apply_on(coupen_detail.apply_on);
               couponViewModel.issued_for(coupen_detail.first_name +" "+coupen_detail.last_name);
               couponViewModel.issued_date(moment(coupen_detail.issue_date));
               couponViewModel.expire_date(moment(coupen_detail.expiry_date));
               couponViewModel.issued_date_show(moment(coupen_detail.issue_date).format('DD MMMM, YYYY'));
               couponViewModel.expire_date_show(moment(coupen_detail.expiry_date).format('DD MMMM, YYYY'));
               couponViewModel.coupon_value_show('Rs '+coupen_detail.coupen_value);
               couponViewModel.coupon_value(coupen_detail.coupen_value);
               couponViewModel.reward_id(coupen_detail.reward_id);
               if(coupen_detail.apply_on == 1){
                  couponViewModel.expiry_label("Applicable on");
               } else{
                  couponViewModel.expiry_label("Expiry Date");
               }
               /* Pop Up*/
               couponViewModel.coupon_value(coupen_detail.coupen_value);
               couponViewModel.brand_id(coupen_detail.brand_id);
               couponViewModel.offer_title(coupen_detail.title);
               couponViewModel.offer_description(coupen_detail.description);
               couponViewModel.contact(coupen_detail.offer_contact);
               couponViewModel.applicable_on(coupen_detail.coupen_value);
               couponViewModel.brand_logo(path+""+coupen_detail.brand_logo);
               couponViewModel.business_hours(coupen_detail.business_hours);
               couponViewModel.website_url(coupen_detail.website_url);
               couponViewModel.address_array(coupen_detail.brand_address_id.split(','));
               couponViewModel.facebook_page(coupen_detail.facebook_page);
               couponViewModel.notes(coupen_detail.notes.replace(/\n/g, "<br />"));
               couponViewModel.valid_to(moment(coupen_detail.expiry_date).format('DD-MM-YYYY'));
               getAddress(coupen_detail.brand_id);
                  if (coupen_detail.apply_on == 1) {
                      couponViewModel.applicable_on(moment(coupen_detail.valid_to).format('DD-MM-YYYY'));
                  } else if (coupen_detail.apply_on == 2) {
                      couponViewModel.applicable_on("Week Days (Mon-Fri)");
                  } else if (coupen_detail.apply_on == 3) {
                      couponViewModel.applicable_on("Weekends  (Sat-Sun)");
                  } else if (coupen_detail.apply_on == 4) {
                      couponViewModel.applicable_on("All Days");
                  }
        
              //  if(!moment(coupen_detail.issue_date, 'YYYY-MM-DDTHH:mm:ssZ').isBefore(moment())){
              //       isBefore = true;
              //  }else{
              //       isBefore = false;
              //  }

              //  if(moment(couponViewModel.expire_date(), 'YYYY-MM-DDTHH:mm:ssZ').isBefore(moment(),'day')){
              //      isAfter = true;
              //      isAftermessage = "Coupen is Expired";
              //  }else{
              //      if(couponViewModel.apply_on()==1){
              //          if(!moment(couponViewModel.expire_date(), 'YYYY-MM-DDTHH:mm:ssZ').isSame(moment(),'day')){
              //              isAfter = true;       
              //              isAftermessage = "Coupen is availble on " + couponViewModel.expire_date().format('DD-MM-YYYY');            
              //         } else {
              //              isAfter =false;
              //         }
              //     } else if(couponViewModel.apply_on()==2){
              //         if(moment().format('ddd') == "Sat" || moment().format('ddd') == "Sun"){
              //              isAfter = true;       
              //              isAftermessage = "Coupon is availble Only in Week Days";            
              //         } else {
              //              isAfter =false;
              //         } 
              //     } else if(couponViewModel.apply_on()==3){
              //         if(moment().format('ddd') != "Sat" || moment().format('ddd') != "Sun"){
              //              isAfter = true;       
              //              isAftermessage = "Coupon is availble Only in WeekEnds";
              //         } else {
              //              isAfter =false;
              //         }
              //     } else if(couponViewModel.apply_on()==4){
              //         isAfter =false;
              //     }
              // }
        } else {
            if(response.message){
                couponViewModel.errorText(response.message);     
            } 
            else{
                couponViewModel.errorText("Coupon Code Or Email Id may Wrong"); 
            }
            couponViewModel.isCouponAvailble(false);
            couponViewModel.isNoCouponAvailble(true);
            
            resetall();
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        couponViewModel.isCouponAvailble(false);
        couponViewModel.isNoCouponAvailble(true);
        couponViewModel.errorText(jqXHR.responseJSON.message); 
        
    };
    apiAjax(setting);
}
function redeemCoupon(){
    var setting = {};
    // if(isBefore){
    //     alert("You Can not Issue Today");
    //     return false;
    // }
    // if(isAfter){
    //     alert(isAftermessage);
    //     return false; 
    // }
    
    couponViewModel.coupenCode($("#txtCoupenCode").val());
    couponViewModel.emailPhone($("#txtEmailPhone").val());
    var datavalue = {
        'txtRedeemCoupenCode' : couponViewModel.coupenCode(),
        'txtBillAmount' : couponViewModel.billAmount(),
        'txtMember' : couponViewModel.member(),
        'reward_id' : couponViewModel.reward_id(),
        'txtOtp' :couponViewModel.otp(),
        'txtEmail' :couponViewModel.email(),
        'txtContact' :couponViewModel.contact()
    };
    setting.url  = redeemCouponApiPath;
    setting.type = 'POST';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        swal("Redeemed Coupon SuccessFully","","success");
        location.href = orderWebPath;    
    };
    setting.error = function(jqXHR, textStatus, error){
       if (jqXHR.status == 422) {
                $.each(jqXHR.responseJSON.errors, function (i, item) {
                    $("#" + i).after(function () {
                        return "<label id='" + i + "'-error' class='error' for='" + i + "'>" + item + "</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message, "", "error");
                return false;
            }
    };
    apiAjax(setting);
}
function resetall(){
    couponViewModel.coupenCode("");
    couponViewModel.emailPhone("");
    couponViewModel.issued_by("");
    couponViewModel.issued_for("");
    couponViewModel.issued_date("");
    couponViewModel.expire_date("");
    couponViewModel.coupon_value("");
    couponViewModel.billAmount("");
    couponViewModel.member("");
    couponViewModel.email("");
    couponViewModel.contact("");
    couponViewModel.employee_id("");
}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            couponViewModel.address([]);
            $.each(address_list, function(i,item) {
                couponViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            couponViewModel.selected_address([]);
            for (var i = 0; i < couponViewModel.address().length ; i++) {
                if(couponViewModel.address_array.indexOf(couponViewModel.address()[i].value().toString()) != -1){
                    couponViewModel.selected_address.push(couponViewModel.address()[i].textaddress());
                }
            }
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }