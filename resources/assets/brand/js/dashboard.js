var viewModelName;
var statisticsViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#dashboardTab').addClass('active');
	initializeKnockout();
    getStatistics();
});

function initializeKnockout(){
	statisticsViewModel = new StatisticsViewModel();
	viewModelName  = statisticsViewModel;
	ko.applyBindings(statisticsViewModel);
}

function StatisticsViewModel(){
	self = this;
    self.active_offer_count = ko.observable("");
    self.corporate_count = ko.observable("");
    self.brand_count = ko.observable("");
    self.employee_count = ko.observable("");
    self.expected_visit = ko.observable("");
    self.monthly_sales = ko.observable("");
    self.new_customer = ko.observable("");
    self.total_balance = ko.observable("");
    self.total_reach = ko.observable("");
    self.total_sales = ko.observable("");
}

function getStatistics(){
	var setting   = {};
    var datavalue = {};
    setting.url    = brandStatisticsApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        statisticsViewModel.active_offer_count(response.data.active_offer_count);
        statisticsViewModel.corporate_count(response.data.corporate_count);
        statisticsViewModel.brand_count(response.data.brand_count);
        statisticsViewModel.employee_count(response.data.employee_count);
        statisticsViewModel.expected_visit(response.data.expected_visit);
        statisticsViewModel.new_customer(response.data.new_customer);
        statisticsViewModel.monthly_sales(response.data.monthly_sales);
        statisticsViewModel.total_balance(response.data.total_balance);
        statisticsViewModel.total_reach(response.data.total_reach);
        statisticsViewModel.total_sales(response.data.total_sales);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
