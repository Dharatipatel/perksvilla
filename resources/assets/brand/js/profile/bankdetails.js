var viewModelName;
var bankDetailsViewModel;

$(document).ready(function(){
	initializeKnockout();
    $('.has-submenu').removeClass('active');
    $('#myprofiledropdown').addClass('show');
    $('#profileTab').addClass('active');
    $('#bankDetailTab').addClass('active');
    getBankDetails();
   
});



function initializeKnockout(){
	bankDetailsViewModel = new BankDetailsViewModel();
	viewModelName  = bankDetailsViewModel;
	ko.applyBindings(bankDetailsViewModel);
}

function BankDetailsViewModel(){
	self = this;
    self.bank_name    = ko.observable("");
    self.account_type = ko.observable("");
    self.account_number    = ko.observable("");
    self.ifsc    = ko.observable("");
    self.branch_details    = ko.observable("");
	

}
function getBankDetails(){
    var setting = {};
    var datavalue = {};
    datavalue['bid'] = bid;    
    setting.url  = getBankDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        if(response.data){
            bankDetailsViewModel.bank_name(response.data.name);
            bankDetailsViewModel.account_type(response.data.account_type);
            bankDetailsViewModel.account_number(response.data.account_number);
            bankDetailsViewModel.ifsc(response.data.ifsc);
            bankDetailsViewModel.branch_details(response.data.branch_details);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);


}

