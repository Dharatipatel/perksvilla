var viewModelName;
var walletViewModel;

$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#walletTab').addClass('active');
	initializeKnockout();
    getWalletTransaction();
    getWalletAmount();
    var rules = {
            txtAmount: {
                'required' :true,
                'digits': true
            },
        };
    var messages = {
            txtAmount: {
                'required' : 'Please Enter Amount',
                'digits' : 'Amount should be only in digits'
            },
        };
    $("#form_withdraw").validate({
        rules:rules ,
        messages: messages,
    });
});



function initializeKnockout(){
	walletViewModel = new WalletViewModel();
	viewModelName  = walletViewModel;
	ko.applyBindings(walletViewModel);
}

function WalletViewModel(){
	self = this;
    self.amount = ko.observable('');
    self.availble_balance = ko.observable(0);
    self.credit_amount = ko.observable(0);
    self.commision = ko.observable(0);
    self.isCommision = ko.observable(false);
    self.transactionList = ko.observable(false);
    self.transactionNoData = ko.observable(false);
    self.transactions = ko.observableArray([]);
    self.amountUpdate = function(){ 
        if(walletViewModel.amount() != ''){
            if(parseInt(walletViewModel.amount())>parseInt(walletViewModel.availble_balance())){
                if($("#txtAmount-error").length){
                    $('#txtAmount-error').css("display","block");
                } else {
                    $('#txtAmount').addClass('error');
                    $('#txtAmount').parent().append('<label id="txtAmount-error" class="error" for="txtAmount">You have not sufficient Balance</label>');    
                }   
                walletViewModel.isCommision(false);
                walletViewModel.credit_amount(0);
            }else {
                $('#txtAmount-error').css("display","none");
                var amount = walletViewModel.amount() - (parseInt(walletViewModel.amount())*(walletViewModel.commision()/100))
                walletViewModel.credit_amount(Math.round(amount));
                walletViewModel.isCommision(true);
            }
        } else {
            walletViewModel.credit_amount(0);
            $('#txtAmount-error').css("display","none");
        }
        
    }
	self .withDrawButton = function(){
        if(!$('#form_withdraw').valid()){
            return false;
        }
        
        var setting = {};
        var datavalue = {
            'user_id' : id,
            'brand_id' : bid,
            'txtAmount' : walletViewModel.amount()
        };
        setting.url  = withDrawAmountApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            walletViewModel.amount("");
            getWalletTransaction();
            getWalletAmount();
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }

}

function getWalletTransaction(){
    var setting = {};
    var datavalue = {
        'user_id' : id,
        'brand_id' : bid,
    };
    setting.url  = withDrawTranscationApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var transaction_list = response.data;
        if(transaction_list.length>0){
            walletViewModel.transactions([]);
            $.each(transaction_list, function(i,item) {
                walletViewModel.transactions.push(new TransactionModel(item));
            });

            walletViewModel.transactionList(true);
            walletViewModel.transactionNoData(false);
            $('#transactionList').DataTable({
                responsive: true,
                bSort : false,
                searching: false,
                pageLength:15,
                bLengthChange: false
            });
        } else {
            walletViewModel.transactionList(false);
            walletViewModel.transactionNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}

function getWalletAmount(){
    var setting = {};
    var datavalue = {
        'user_id' : id,
    };
    setting.url  = brandBalanceApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        
        var amount_list = response.data;
        if(amount_list){
            walletViewModel.availble_balance(amount_list.balance);
            walletViewModel.commision(amount_list.commision);
        } else {
                walletViewModel.availble_balance(0);
                walletViewModel.commision(0);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}