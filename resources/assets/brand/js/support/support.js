$('document').ready(function () {
    $('.has-submenu').removeClass('active');
    $('#supportTab').addClass('active');
    $('#messageDropdown').addClass('show');
    if(type=='list'){
        $('#listMessageTab').addClass('active');
    } 
    if (user_support_lists !== undefined) {
        load_table();
    }
    function load_table() {
        if (datatable != "") {
            datatable.destroy();
        }
        datatable = $('#pending_investment_tbl').DataTable({
            processing: true,
            serverSide: true,
            bSort : false,
            order: [2, "desc"],
            searching: false,
            bLengthChange: false,
            ajax: user_support_lists,
            columns: [
                {data: 'title', name: 'title'},
                {data: 'category_name', name: 'category_name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', 'visible': true, bSearchable: false, sortable: false}
            ]
        });
    }
});