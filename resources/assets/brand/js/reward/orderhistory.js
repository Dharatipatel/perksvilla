var viewModelName;
var orderHistoryViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#orderHistoryTab').addClass('active');
    initializeKnockout();
    getOrderHistory();
    getStatistics();
});
function initializeKnockout(){
    orderHistoryViewModel = new OrderHistoryViewModel();
    viewModelName  = orderHistoryViewModel;
    ko.applyBindings(orderHistoryViewModel);
}

function OrderHistoryViewModel(){
    self = this;
    self.total = ko.observable("");
    self.orderhistory = ko.observableArray([]);
    self.reward_issued= ko.observable(0);
    self.total_save = ko.observable(0);
    self.total_invest = ko.observable(0);    
    self.new_customer = ko.observable("");
    self.total_balance = ko.observable("");
    self.total_sales = ko.observable("");
    /* POP UP */
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.facebook_page = ko.observable("");
    self.contact = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.offers  = ko.observableArray([]);
    self.address  = ko.observableArray([]);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    
    self.viewOfferClick = function(order){
        getOfferDetail(order.offer_id());
    }
}
function getOfferDetail(offer_id){
    var setting = {};
    var datavalue = {
        'offer_id':offer_id,
    };
    
    setting.url  = offerDetailApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            orderHistoryViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                orderHistoryViewModel.offers.push(new OfferModel(item));
            });
            var offer = orderHistoryViewModel.offers()[0];
            orderHistoryViewModel.offer_title(offer.name());
            orderHistoryViewModel.offer_description(offer.description());
            orderHistoryViewModel.coupon_value(offer.value());
            orderHistoryViewModel.valid_to(offer.valid_to());
            orderHistoryViewModel.coupon_code("");
            orderHistoryViewModel.contact(offer.contact());
            orderHistoryViewModel.applicable_on(offer.apply_on_text());
            orderHistoryViewModel.address_array(offer.brand_address_id().split(','));
            orderHistoryViewModel.business_hours(offer.business_hours());
            orderHistoryViewModel.website_url(offer.website_url() != null ? offer.website_url() :"" );
            orderHistoryViewModel.facebook_page(offer.facebook_page() != null ? offer.facebook_page() :"" );
            orderHistoryViewModel.notes(offer.notes().replace(/\n/g, "<br />"));
            orderHistoryViewModel.brand_logo(path+""+offer.logo());
            getAddress(offer.brand_id());
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getOrderHistory(){
    var setting = {};
    var datavalue = {
       'redeem':1,
    };
    
    setting.url  = brandOrderHistoryApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data.data;
        if ($.fn.DataTable.isDataTable("#orderHistoryList")) {
                $('#orderHistoryList').DataTable().clear().destroy();
        }
        orderHistoryViewModel.orderhistory([]);
        if(order_list.length>0){
            $.each(order_list, function(i,item) {
                orderHistoryViewModel.orderhistory.push(new OrderHistoryBrandModel(item));
            });
            $("#orderHistoryList").DataTable({
                responsive: true,
                bSort : false,
                searching: false,
                pageLength:15,
                bLengthChange: false
            });
        } else {
            
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getStatistics(){
    var setting   = {};
    var datavalue = {};
    setting.url    = brandStatisticsApi;
    setting.type   = 'GET';
    setting.data   = datavalue;
    setting.token  = window.btoa(id+"|"+token);
    setting.success = function(response){
        orderHistoryViewModel.new_customer(response.data.new_customer);
        orderHistoryViewModel.total_balance(response.data.total_balance);
        orderHistoryViewModel.total_sales(response.data.total_sales);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getOrderHistoryStatistic(){
    var setting = {};
    var datavalue = {
       'cid':cid,
    };
    
    setting.url  = orderHistoryStatisticApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var order_list = response.data;
        if(order_list){
           orderHistoryViewModel.reward_issued(order_list.total_reward);
           orderHistoryViewModel.total_save(order_list.total_saved);
           orderHistoryViewModel.total_invest(order_list.total_invested);
        } 
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            orderHistoryViewModel.address([]);
            $.each(address_list, function(i,item) {
                orderHistoryViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            orderHistoryViewModel.selected_address([]);
            for (var i = 0; i < orderHistoryViewModel.address().length ; i++) {
                if(orderHistoryViewModel.address_array.indexOf(orderHistoryViewModel.address()[i].value().toString()) != -1){
                    orderHistoryViewModel.selected_address.push(orderHistoryViewModel.address()[i].textaddress());
                }
            }
            $('#viewOfferModal').modal('show');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;orderHistoryViewModel
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
}