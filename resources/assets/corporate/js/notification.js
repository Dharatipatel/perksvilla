$('document').ready(function () {
    updateNotification();
    setInterval(function(){
        updateNotification();
    }, notification_update_interval * 1000);
});

function updateNotification(){
    var setting = {};
    var dataValue = {id: id};

    setting.url = notification_url;
    setting.type = 'POST';
    setting.data = dataValue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.error) {
            console.log(response);
        } else {
            $('#top_header_container').html(response.data);
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        console.log(error);
        return false;
    };
    apiAjax(setting);
}

function readNotification(notification_id){
    
    var setting = {};
    var dataValue = {id: notification_id};

    setting.url = notification_read_url;
    setting.type = 'POST';
    setting.data = dataValue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        if (response.error) {
            console.log(response);
        } else {
            updateNotification();
        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        console.log(error);
        return false;
    };
    apiAjax(setting);

}