var viewModelName;
var rewardViewModel;
$(document).ready(function(){
    $('.has-submenu').removeClass('active');
    $('#perksvillaTab').addClass('active');
    initializeKnockout();
    getCorporateCategories()
    getBrandOffers();
    $("#form_category").validate({
        rules: {
            name: "required",
            txtDescription : {
                required :true,
                maxlength :65,
            },
        },
        messages: {
            name: "Please enter Category Name",
            txtDescription : {
                required :"Please Enter Description",
                maxlength :" Description should not be grater than 65 letters",
            },
        },
    });
    
});
function initializeKnockout(){
    rewardViewModel = new RewardViewModel();
    viewModelName  = rewardViewModel;
    ko.applyBindings(rewardViewModel);
}

function RewardViewModel(){
    self = this;
    self.total = ko.observable("");
    self.searchValue = ko.observable("");
    self.step1 = ko.observable(true);
    self.step2 = ko.observable(false);
    self.step3 = ko.observable(false);
    self.step4 = ko.observable(false);
    self.rewards = ko.observableArray([]);
    self.employees = ko.observableArray([]);
    self.offers = ko.observableArray([]);
    self.selectEmployeeDetail = ko.observableArray([]);
    self.brandOffers = ko.observableArray([]);
    self.choosenEmployee = ko.observableArray([]);
    self.selectedOffer   = ko.observableArray([]);
    self.selectedOfferDetail   = ko.observableArray([]);
    self.selectedEmployeeDetail   = ko.observableArray([]);
    self.availbleBrandCategory = ko.observableArray([]);
    self.coupenType = ko.observableArray([]);
    self.priceRange = ko.observableArray([]);
    self.offerList = ko.observable(false);
    self.brandList = ko.observable(false);
    self.selectedBrand = ko.observable();
    self.offerNoData = ko.observable(false);
    self.type = ko.observable(1);
    self.selectedBrandCategory = ko.observable();
    self.stepDescription = ko.observable("");
    self.deleteId = ko.observable("");
    self.imagePath = ko.observable(path);
    self.seletedCategory= ko.observable();
    self.brandDetailName = ko.observable("");
    self.brandDetailExpiry = ko.observable("");
    self.brandDetailOffer = ko.observable("");
    self.brandDetailPrice = ko.observable("");
    self.brandDetailPublish = ko.observable("");
    self.brandDetailValue = ko.observable("");
    self.brandDetailApplicable = ko.observable("");
    self.offerType = ko.observable("");
    self.imagePathEmployee = ko.observable(employee_path);
     /* popup */
    self.address = ko.observableArray([]);
    self.offer_title = ko.observable("");
    self.offer_description = ko.observable("");
    self.coupon_value = ko.observable("");
    self.valid_to = ko.observable("");
    self.coupon_code = ko.observable("");
    self.applicable_on = ko.observable("");
    self.brand_logo = ko.observable("");
    self.business_hours = ko.observable("");
    self.website_url = ko.observable("");
    self.contact = ko.observable("");
    self.facebook_page = ko.observable("");
    self.notes = ko.observable("");
    self.isCoupon = ko.observable(false);
    self.address_array = ko.observableArray([]);
    self.selected_address = ko.observableArray([]);
    self.brandDetailClick  = function(brandoffer){
        
        $('#viewBrandDetail').modal();
        rewardViewModel.offer_title(brandoffer.name());
        rewardViewModel.offer_description(brandoffer.description());
        rewardViewModel.coupon_value(brandoffer.value());
        rewardViewModel.valid_to(brandoffer.valid_to());
        rewardViewModel.coupon_code("");
        rewardViewModel.isCoupon(false);
        rewardViewModel.applicable_on(brandoffer.apply_on_text());
        rewardViewModel.contact(brandoffer.contact());
        rewardViewModel.address_array(brandoffer.brand_address_id().split(','));
        rewardViewModel.business_hours(brandoffer.business_hours());
        rewardViewModel.website_url(brandoffer.website_url() != null ? brandoffer.website_url() :"" );
        rewardViewModel.facebook_page(brandoffer.facebook_page() != null ? brandoffer.facebook_page() :"" );
        rewardViewModel.notes(brandoffer.notes().replace(/\n/g, "<br />"));
        rewardViewModel.brand_logo(path+""+brandoffer.logo());
        getAddress(brandoffer.brand_id());

    }
    self.previousStep4Click  = function(){
        rewardViewModel.selectedOfferDetail([]);
        rewardViewModel.step1(true);
        rewardViewModel.step2(false);
        rewardViewModel.step3(false);
        rewardViewModel.step4(false);
        rewardViewModel.type(1);
        getCorporateCategories();
    }
    self.createRewardClick = function(data,event){
        rewardViewModel.seletedCategory(event.target.getAttribute('data-id'));
        rewardViewModel.step1(false);
        rewardViewModel.step2(true);
        rewardViewModel.step3(false);
        rewardViewModel.step4(false);
        rewardViewModel.stepDescription("Pick the employees you would like to reward");
        rewardViewModel.choosenEmployee([]);
        getEmployees();
    }
    self.rewardStep2Click = function(offer){
        rewardViewModel.selectedOffer(offer.id());
        rewardViewModel.selectedOfferDetail.push(offer);
        if(rewardViewModel.selectedOffer()==""){
            alert("Please Select Offer");
            return false;
        }
        rewardViewModel.type(3);
        getCorporateCategories();
        rewardViewModel.step1(false);
        rewardViewModel.step4(true);
        rewardViewModel.step2(false);
        rewardViewModel.step3(false);
    } ;
    self.payNowClick = function(){
        if(rewardViewModel.choosenEmployee().length <= 0 ){
            alert("Please Select Employee");
            return false;
        } 
        rewardViewModel.stepDescription("Order Summary");
        rewardViewModel.step1(false);
        rewardViewModel.step2(false);
        rewardViewModel.step3(true);
        getEmployees();
    }
    self.previousStep2Click = function(){
        rewardViewModel.step1(false);
        rewardViewModel.step2(false);
        rewardViewModel.step3(false);
        rewardViewModel.step4(true);
    };
    self.previousStep3Click = function(){
        rewardViewModel.step1(false);
        rewardViewModel.step2(true);
        rewardViewModel.step3(false);
        rewardViewModel.stepDescription("Pick the employees you would like to reward");
        rewardViewModel.selectedEmployeeDetail([]);
        getEmployees();
    };
    self.createRewardModalClick =function(){
        if(!$("#form_category").valid()){
            return false;
        }
        var datavalue = {
            'type' : 3,
            'name' : $('#txtCategoryName').val(),
            'description' : $('#txtDescription').val(),
            'status' : 1,
            'user_id' : id,
        };


        var setting = {};
        setting.url  = addCategoryPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            rewardViewModel.seletedCategory(response.data.id);
            $('#createReward').modal('toggle'); 
            // rewardViewModel.step1(false);
            // rewardViewModel.step2(true);
            // rewardViewModel.step3(false);
            // rewardViewModel.step4(false);
            // rewardViewModel.stepDescription("Pick the employees you would like to reward");
            // rewardViewModel.choosenEmployee([]);
            // getEmployees();
            getCorporateCategories();
        };
        setting.error = function(jqXHR, textStatus, error){
                swal(jqXHR.responseJSON.message,"","error");
                return false;
        };
        apiAjax(setting);

    }
    self.createRewardCloseClick = function(){
        $('#createReward').modal('toggle'); 
    }
    self.createOwnRewardModelClick = function(){
        $("#createReward").modal();
    }
    self.searchEmployeeClick = function(){
        getEmployees();
    };
    self.offerSearchClick = function(){
        rewardViewModel.selectedBrand("");
        getOffers();
    };
    self.deleteCustomRewardPopUp = function (category) {
        rewardViewModel.deleteId(category.id());
        $('#deleteModal').modal('show');
    }
    
    self.deleteCustomRewardClick= function(){

        var setting = {};
        var datavalue = {
            'id' : rewardViewModel.deleteId(),
            'custom' :true,
        };
        setting.url  = deleteCategoriesApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           
            $('#deleteModal').modal('hide');
            getCorporateCategories();

        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    
    }
    self.selectOfferDetail = function(offer){
        rewardViewModel.selectedOfferDetail.push(offer);    
    }
    self.offerSearchBrandClick =function(brand){
        rewardViewModel.selectedBrand(brand.brand_id());
        getOffers();
    };
    self.removeEmployee = function(Employee){
        rewardViewModel.selectedEmployeeDetail.remove(function(employee) {
            rewardViewModel.choosenEmployee.remove(Employee.id());
            return employee.id() == Employee.id();
        });
        
    }
    self.payNowSubmit = function(){
        if(rewardViewModel.selectedEmployeeDetail().length > rewardViewModel.selectedOfferDetail()[0].remaining_count()){
            alert(rewardViewModel.selectedEmployeeDetail().length +" Offers Are not Availble Please remove Employee");
            return false;
        }
        var employee = {};
        var datavalue = {};
        var issueDateCount = $("[id^=issueDate]").length;
        for(var i=0; i<issueDateCount;i++){
            if($('#'+'issueDate'+i).val() == ""){
                $('#'+'issueDate'+i).focus();
                return false;
            } else{
                var issueDate = moment($('#'+'issueDate'+i).val(), 'DD-MM-YYYY');
                var validTo =  moment(rewardViewModel.selectedOfferDetail()[0].valid_to(), 'DD-MM-YYYY');
                var coupon_value =  rewardViewModel.selectedOfferDetail()[0].value();
                var coupon_price =  rewardViewModel.selectedOfferDetail()[0].price();
                // if( validTo.isBefore(issueDate)){
                //     $('#'+'issueDate'+i).focus();
                //     alert("Issue date is grater than valid date Please select anothor date");
                //     return false;
                // }else{
                    employee[$('#'+'employee_id'+i).val()] = $('#'+'issueDate'+i).val();
                // }
            }
        }
        datavalue['employee'] = employee;
        datavalue['category_id'] = rewardViewModel.seletedCategory();
        datavalue['offer_id'] = rewardViewModel.selectedOffer();
        datavalue['coupon_value'] = coupon_value;
        datavalue['coupon_price'] = coupon_price;
        datavalue['user_id'] = id;
        var setting = {};
        setting.url  = addRewardApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            if(coupon_price>0){
                var n = $(response.form);
                $("body").append(n);
                n.submit();
            } else {
                location.href = historyListingWeb;
            }
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
}

function getEmployees(){
    var setting = {};
    var datavalue = {
        'cid' : cid,
        'status':1,
    };
    if(rewardViewModel.searchValue()!=""){
        datavalue['q'] = rewardViewModel.searchValue();
    }
    if(rewardViewModel.step3()){
        if(rewardViewModel.choosenEmployee().length > 0){
            datavalue['eid'] = rewardViewModel.choosenEmployee();
        }
    }
    setting.url  = employeeListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var employee_list = response.data.data;
            rewardViewModel.employees([]);
        if(employee_list.length>0){
            $.each(employee_list, function(i,item) {
                rewardViewModel.employees.push(new EmployeeModel(item));
                if(rewardViewModel.step3()){
                    if(rewardViewModel.choosenEmployee().length > 0){
                        rewardViewModel.selectedEmployeeDetail.push(new EmployeeModel(item));
                        $('#issueDate' +i).datepicker({startDate: '-0d',endDate:rewardViewModel.selectedOfferDetail()[0].valid_to(),format: 'dd-mm-yyyy', autoclose: true, todayHighlight: true}); 
                            
                    }
                }
            });
        } else {
            
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);

}
function getCorporateCategories(){
    var setting = {};
    var datavalue = {
        'type' :rewardViewModel.type(),
        'status' :1,
        'user_id':id
    };
    setting.url = getCategoriesApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id + "|" + token);
    setting.success = function (response) {
        rewardViewModel.total(response.data.total);
        var reward_list = response.data.data;
        if (reward_list.length > 0) {
            rewardViewModel.rewards([]);
            rewardViewModel.availbleBrandCategory([]);
            $.each(reward_list, function (i, item) {
                rewardViewModel.availbleBrandCategory.push(new availbleBrandCategory(item.id,item.name));
                rewardViewModel.rewards.push(new CategoryModel(item));
            });
        } else {

        }
    };
    setting.error = function (jqXHR, textStatus, error) {
        swal(jqXHR.responseJSON.message, "", "error");
        return false;
    };
    apiAjax(setting);

}
function availbleBrandCategory(k,v){

    var self = this;
    self.value = ko.observable(k);
    self.text = ko.observable(v);

}
function getBrandOffers(){
    var setting = {};
    
    setting.url  = brandofferListApi;
    setting.type = 'GET';
    setting.data = {};
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var offer_list = response.data.data;
            rewardViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                rewardViewModel.brandOffers.push(new brandOfferModel(item));
            });
            rewardViewModel.offerNoData(false);
        } else {
            rewardViewModel.offerNoData(true);
        }
        rewardViewModel.brandList(true);
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}

function getOffers(){
    
    var setting = {};
    var datavalue = {
        'status':1,
        'count' :true,
    };
    if(typeof rewardViewModel.selectedBrandCategory() != 'undefined'){
        datavalue['brand_category'] = rewardViewModel.selectedBrandCategory();
    }
    if(rewardViewModel.coupenType().length > 0){
        datavalue['coupentype'] = rewardViewModel.coupenType();
    }
    if(rewardViewModel.priceRange().length > 0){
        datavalue['pricerange'] = rewardViewModel.priceRange();
    }
    if(rewardViewModel.selectedBrand() != 'undefined' || rewardViewModel.selectedBrand() != ""){
        datavalue['bid'] = rewardViewModel.selectedBrand();
    }
    setting.url  = offerListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        rewardViewModel.offerList(true);
        rewardViewModel.brandList(false);
        var offer_list = response.data.data;
            rewardViewModel.offers([]);
        if(offer_list.length>0){
            $.each(offer_list, function(i,item) {
                rewardViewModel.offers.push(new OfferModel(item));
            });
            rewardViewModel.offerNoData(false);
        } else {
            rewardViewModel.offerNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}

function getAddress(bid){
    var setting = {};
    var datavalue = {
        'bid' : bid,
    };
    setting.url  = addressListApiPath;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        var address_list = response.data.data;
        if(address_list.length>0){
            rewardViewModel.address([]);
            $.each(address_list, function(i,item) {
                rewardViewModel.address.push(new availableAddress(item.id,item.address,item.city,item.state,item.country,item.pincode));
            });
            rewardViewModel.selected_address([]);
            for (var i = 0; i < rewardViewModel.address().length ; i++) {
                if(rewardViewModel.address_array.indexOf(rewardViewModel.address()[i].value().toString()) != -1){
                    rewardViewModel.selected_address.push(rewardViewModel.address()[i].textaddress());
                }
            }
            $('#viewModel').modal('toggle');
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
function availableAddress(k,address,city,state,country,pincode){
    var self = this;
    self.value = ko.observable(k);
    self.textaddress  = ko.observable(address+", "+city+" ,"+state+" ,"+country+" ,"+pincode);
 }