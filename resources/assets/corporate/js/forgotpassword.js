var viewModelName;
var forgotPasswordViewModel;

$(document).ready(function () {
    initializeKnockout();
    $("#forgot-password").validate({
        rules: {
            txtEmail: {
                'required': true,
                'email': true,
            },
        },
        messages: {
            txtEmail: {
                'required': 'Please Enter Email',
                'email': 'Please enter a valid email address'
            },
        },
    });
});

function initializeKnockout() {
    forgotPasswordViewModel = new ForgotPasswordViewModel();
    viewModelName = forgotPasswordViewModel;
    ko.applyBindings(forgotPasswordViewModel);
}

function ForgotPasswordViewModel() {
    self = this;
    self.email = ko.observable();
    self.sendEmailClick = function () {
        if (!$("#forgot-password").valid()) {
            return false;
        }
        var setting = {};
        self.email($("#txtEmail").val());
        var datavalue = {
            'email': self.email(),
            '_token': _token,
            'resetpassword': resetpassword,
            'role_id': 3,
        };
        setting.url = forgotPasswordPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.success = function (response) {
            if (response.error) {
                swal("Error", response.message, 'error');
            } else {
                swal("Success", response.message, 'success');
            }
        };
        setting.error = function (jqXHR, textStatus, error) {
            swal("Error", jqXHR.responseJSON.message, 'error');
            return false;
        };
        apiAjax(setting);
    };
}

function resetAll() {
    $("#txtEmail").val('');
}
