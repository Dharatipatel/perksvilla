var viewModelName;
var employeeViewModel;

$(document).ready(function(){
	initializeKnockout();
    $('.has-submenu').removeClass('active');
    $('#employeeDropdown').addClass('show');
    $('#employeeTab').addClass('active');
    if(type=='add'){
        employeeViewModel.employeeListContainer(false);
        employeeViewModel.employeeAddContainer(true);
        employeeViewModel.formCsvVisibility(true);
        $('#addEmployeeTab').addClass('active');
    } else if(type=='list'){
        employeeViewModel.employeeListContainer(true);
        employeeViewModel.employeeAddContainer(false);
        $('#listEmployeeTab').addClass('active');
    }
    $("#form_employee").validate({
        rules:{
            txtFirstName : "required",
            txtLastName : "required",
            gender : "required",
            txtDesignation : "required",
            txtEmail: {
                'required' :true,
                'email' :true,
            },
            txtContact: {
                'required' :true,
                'digits': true
            },
            dateBirth : { 
                required:true,
            },
            dateJoin : { 
                required :true,
            },
        },
        messages: {
            txtFirstName: "Please enter First name",
            txtLastName: "Please enter Last name",
            gender: "Please select gender ",
            txtDesignation: "Please enter Designation",
            txtEmail: {
                'required' : 'Please Enter Email',
                'email' : 'Please enter valid Email'
            },
            txtContact: {
                'required' : 'Please Enter Mobile Number',
                'digits' : 'Mobile Number should be only in digits'
            },
            dateBirth : "Please select Date of Birth",
            dateJoin : "Please select Date of join",
        },
    });
    $("#form_import").validate({
        rules:{
            fileImport: 'required',
        },
        message:{
            fileImport: 'Csv File is required',
        }
    });
    employeeViewModel.status(1);
    getEmployees();
    $('#dateBirth').datepicker({format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true});
    $('#dateJoin').datepicker({format: 'dd-mm-yyyy', autoclose: true, 
        todayHighlight: true});
});


function initializeKnockout(){
	employeeViewModel = new EmployeeViewModel();
	viewModelName  = employeeViewModel;
	ko.applyBindings(employeeViewModel);
}

function EmployeeViewModel(){
	self=this;
    self.imagePathEmployee = ko.observable(path);
    self.profile = ko.observable("");
    self.current_id = ko.observable("");
    self.current_id = ko.observable(); 
	self.status=ko.observable(0);
    self.total=ko.observable(0);
    self.searchValue = ko.observable("");
    self.selectedStatus = ko.observable(0);
    self.statusText = ko.observable("Active");
    self.availbleStatus = [{
            text: "Active",
            value: 1
        }, {
            text: "Deactive",
            value: 0
        }];
    self.searchClick = function(){
        getEmployees();
    }
    self.searchChange = function(){
        if(employeeViewModel.searchValue()==""){
            getEmployees();
        }
    };
    self.deleteEmployeeClick = function (employee) {
        employeeViewModel.current_id(employee.id());
        $('#deleteModal').modal('show');
    };
    self.deleteConfirm = function () {
        var setting = {};
        var datavalue = {
            'id': employeeViewModel.current_id()
        };
        setting.url = deleteEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id + "|" + token);
        setting.success = function (response) {
            $('#deleteModal').modal('hide');
            swal(response.message, "", "success");
            getEmployees();
            // location.href= employeeWebPath;

        };
        setting.error = function (jqXHR, textStatus, error) {
            $('#deleteModal').modal('hide');
            swal(jqXHR.responseJSON.message, "", "error");
            return false;
        };
        apiAjax(setting);
    };
	self.action=ko.observable("add");
    self.title =ko.observable("Add Employee");
    self.selectedGender =ko.observable("Male");
	self.employee=ko.observableArray([]);
    self.button_title = ko.observable("ADD");
    self.imageUri = ko.observable("");
	self.employeeList = ko.observable(false);
	self.employeeNoData = ko.observable(false);
    self.employeeListContainer = ko.observable(true);
    self.employeeAddContainer = ko.observable(false);
	self.formCsvVisibility = ko.observable(false);
    self.getOfferActiveClick =function(){
        employeeViewModel.statusText("Active");
        employeeViewModel.status(1);
        getEmployees();
    }
    self.getOfferDeactiveClick =function(){
        employeeViewModel.statusText("Inactive");
        employeeViewModel.status(0);
        getEmployees();
    }
    self.activeClick = function(employee){
        var setting = {};
        var datavalue = {
            'id' : employee.id(),
        };
        setting.url  = activeEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            getEmployees();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.deactiveClick = function(employee){
        var setting = {};
        var datavalue = {
            'id' : employee.id(),
        };
        setting.url  = deactiveEmployeeApiPath;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
            swal(response.message,"",'success');
            getEmployees();
            
        };
        setting.error = function(jqXHR, textStatus, error){
            swal(jqXHR.responseJSON.message,"","error");
            return false;
        };
        apiAjax(setting);
    }
    self.importEmployeeClick = function(){
        if(!$("#form_import").valid()){
            return false;
        }
        var fileUpload = $('#fileImport').val();
        if (fileUpload != null) {
            var uploadFile = new FormData();
            var files = $("#fileImport").get(0).files;
            // Add the uploaded file content to the form data collection
            if (files.length > 0) {
                var setting = {};
                uploadFile.append("employeecsv", files[0]);
                setting.url  = employeeImportApi;
                setting.type = 'POST';
                setting.data = uploadFile;
                setting.processData = false;
                setting.contentType = false;
                setting.token = window.btoa(id+"|"+token);
                setting.success = function(response){
                   resetAll();
                   employeeViewModel.employeeListContainer(true);
                   employeeViewModel.employeeAddContainer(false);
                   employeeViewModel.status(0);
                   employeeViewModel.statusText("Inactive");
                   getEmployees();
                };
                setting.error = function(jqXHR, textStatus, error){
                    $('#fileImport').val("");
                    if(jqXHR.status==422){
                        var error_msg="Following email and contact are already exist";
                        $.each(jqXHR.responseJSON.errors,function(i,item){                
                            if(item.email){
                                error_msg += item.email +" ";
                            }
                            if(item.contact){
                                error_msg += item.contact +" ";
                            }
                        })
                            swal(error_msg,"","error");
                    } else {
                        swal(jqXHR.responseJSON.message,"test","error");
                        return false;
                    }
                };
                apiAjax(setting);
            }
    }

    }
	self.createEmployeeClick = function(){

		if(!$("#form_employee").valid()){
            return false;
        }
        var datavalue = {};
        var first_name = $('#txtFirstName').val();
        var last_name = $('#txtLastName').val();
        var designation = $('#txtDesignation').val();
        var contact = $('#txtContact').val();
        var email = $('#txtEmail').val();
        var joining_date = $('#dateJoin').val();
        var birth_date = $('#dateBirth').val();
        
        var setting = {};
        datavalue['first_name'] = first_name;
        datavalue['last_name'] = last_name;
        datavalue['gender'] = employeeViewModel.selectedGender();
        datavalue['status'] = employeeViewModel.selectedStatus();
        datavalue['designation'] = designation;
        datavalue['contact'] = contact;
        datavalue['email'] = email;
        datavalue['joining_date'] = joining_date;
        datavalue['birth_date'] = birth_date;
        datavalue['profile_pic'] = employeeViewModel.imageUri();
        datavalue['corporate_id'] = cid;
        if(employeeViewModel.action()=='add'){
            employeeApi = employeeAddApi
        } else if(employeeViewModel.action()=='update'){
            employeeApi = employeeUpdateApi
            datavalue['id'] = employeeViewModel.current_id();
        }

        setting.url  = employeeApi;
        setting.type = 'POST';
        setting.data = datavalue;
        setting.token = window.btoa(id+"|"+token);
        setting.success = function(response){
           resetAll();
           employeeViewModel.employeeListContainer(true);
           employeeViewModel.employeeAddContainer(false);
           employeeViewModel.status(employeeViewModel.selectedStatus());
           if(employeeViewModel.selectedStatus()==0){
                employeeViewModel.statusText("Inactive");
           } else if(employeeViewModel.selectedStatus()==1){
                employeeViewModel.statusText("Active");
           }
           getEmployees();
        };
        setting.error = function(jqXHR, textStatus, error){
            if(jqXHR.status==422){
                $.each(jqXHR.responseJSON.errors,function(i,item){
                    var fieldname;
                    tail = i.substring( 1 );
                    fieldName = i.substring( 0, 1 ).toUpperCase() + tail; 
                    fieldname ="txt"+fieldName;
                    $("#"+fieldname ).after(function() {
                        return "<label id='"+fieldname+"'-error' class='error' for='"+fieldname+"'>"+item+"</label>";
                    });
                })
            } else {
                swal(jqXHR.responseJSON.message,"","error");
                return false;
            }


            };
        apiAjax(setting);
	}
    self.cancleEmployeeClick = function(){
        resetAll();
        employeeViewModel.employeeListContainer(true);
        employeeViewModel.employeeAddContainer(false);
        employeeViewModel.status(1);
        getEmployees();
    }
    self.editEmployeeClick   = function(employee){
    
        employeeViewModel.employeeAddContainer(true);
        employeeViewModel.formCsvVisibility(false);
        employeeViewModel.employeeListContainer(false);
        employeeViewModel.title("Edit Employee");
        $('#txtFirstName').val(employee.first_name());
        $('#txtLastName').val(employee.last_name());
        $('#txtDesignation').val(employee.designation());
        $('#txtContact').val(employee.contact());
        $('#txtEmail').val(employee.email());
        $('#dateJoin').val(employee.joining_date());
        $('#dateBirth').val(employee.birth_date());
        if(employee.profile_pic()==null){
            if(employee.gender()=="Male"){    
                employeeViewModel.profile(static_image_male);
            } else if(employee.gender()=="Female"){
                employeeViewModel.profile(static_image_female);
            }
        }else{
            employeeViewModel.profile(employee.profile_pic()+"?t="+Math.random());
        }
        employeeViewModel.selectedStatus(employee.status());
        employeeViewModel.profile(employee.profile_pic());
        employeeViewModel.current_id(employee.id());
        employeeViewModel.action("update");
        employeeViewModel.button_title("UPDATE");
        employeeViewModel.selectedGender(employee.gender());
    }
    
}

function resetAll(){
	$('#txtFirstName').val("");
    $('#txtLastName').val("");
	$('#txtDesignation').val("");
	$('#txtContact').val("");
   	$('#txtEmail').val("");
	$('#dateJoin').val("");
    $('#dateBirth').val("");
    $('#fileImport').val("");
    employeeViewModel.title("Add Employee");

}
function getEmployees(){
    var setting = {};
    var datavalue = {
        'cid' : cid,
        'status':employeeViewModel.status(),
    };
    if(employeeViewModel.searchValue()!=""){
       datavalue['q'] = employeeViewModel.searchValue();
    }
    setting.url  = employeeListApi;
    setting.type = 'GET';
    setting.data = datavalue;
    setting.token = window.btoa(id+"|"+token);
    setting.success = function(response){
        employeeViewModel.total(response.data.total);
        var employee_list = response.data.data;
        if(employee_list.length>0){
            if ($.fn.DataTable.isDataTable("#employeeList")) {
                $('#employeeList').DataTable().clear().destroy();
            } 
            employeeViewModel.employee([]);
            $.each(employee_list, function(i,item) {
                employeeViewModel.employee.push(new EmployeeModel(item));
            });
            $("#employeeList").DataTable({
                responsive: true,
                bSort : false,
                searching: false,
                pageLength:15,
                bLengthChange: false
            });
            employeeViewModel.employeeList(true);
            employeeViewModel.employeeNoData(false);
           
        } else {
            employeeViewModel.employeeList(false);
            employeeViewModel.employeeNoData(true);
        }
    };
    setting.error = function(jqXHR, textStatus, error){
        swal(jqXHR.responseJSON.message,"","error");
        return false;
    };
    apiAjax(setting);
}
$('#fileInput').change(function(){
   var file = $('#fileInput').prop('files')[0];
   var ImageURL;
   var FR = new FileReader();
   FR.addEventListener("load", function () {
        ImageURL = FR.result;
        var block = ImageURL.split(";");
        // Get the content type of the image
        var contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        var realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        // Convert it to a blob to upload
        // var blob = b64toBlob(    , contentType);
        employeeViewModel.imageUri(realData);
   }, false);
   if (file) {
        FR.readAsDataURL(file);
   }
});
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}