<?php

$status = $data["status"];
$firstname = $data["firstname"];
$amount = $data["amount"];
$txnid = $data["txnid"];

$posted_hash = $data["hash"];
$key = $data["key"];
$productinfo = $data["productinfo"];
$email = $data["email"];
$salt = "";

// Salt should be same Post Request 

If (isset($data["additionalCharges"])) {
    $additionalCharges = $data["additionalCharges"];
    $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
} else {
    $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
}
$hash = hash("sha512", $retHashSeq);

if ($hash != $posted_hash) {
    echo "Invalid Transaction. Please try again";
} else {
    echo "<h3>Your order status is " . $status . ".</h3>";
    echo "<h4>Your transaction id for this transaction is " . $txnid . ". You may try making the payment by clicking the link below.</h4>";
}
?>
