<li class="nav-item dropdown"> 
    <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
        <i class="fa fa-bell-o"></i>
        @if(count($notification)>0)
        <span class="badge bg-red badge-corner">{{count($notification)}}</span>
        @endif
    2</a>
  <ul aria-labelledby="notifications" class="dropdown-menu col-xs-12">
    @if(count($notification))
        @foreach($notification as $n)
            <li>
                <a rel="nofollow" href="javascript:void(0)" onclick="readNotification({!!$n->id!!})" class="dropdown-item">
                    <div class="notification">
                        <div class="notification-content">
                            <i class="fa fa-envelope bg-green"></i>
                            <span>{!! $n->message !!}</span>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach
    @else
        <li>
            <a rel="nofollow" href="#" class="dropdown-item">
                <div class="notification">
                    <div class="notification-content">
                        <i class="fa fa-envelope bg-green"></i>
                        No new notifications
                    </div>
                </div>
            </a>
        </li>
    @endif           
  </ul>
</li>