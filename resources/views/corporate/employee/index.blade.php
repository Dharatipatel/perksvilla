@extends('corporate.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
  <style type="text/css">
  #employeeList td ,#employeeList th{
        vertical-align: middle;
  }
  </style>
@endsection
@section("content")
    <header class="page-header">
      <div class="container-fluid">
          <h2 class="no-margin-bottom">Employees</h2>
      </div>
    </header>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Delete Employee</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Are you sure to delete Employee ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
    <section class="tables" data-bind="visible:employeeListContainer">   
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4 col-lg-9"><span data-bind="text:statusText"></span> Employee List</h3>
                            <div class="input-group col-lg-3">
                                <input type="text" placeholder="Search" data-bind="event:{keyup:searchClick},value:searchValue,valueUpdate: 'afterkeydown'"  class="form-control">
                                <!-- <div class="input-group-append">
                                    <button type="button" data-bind="click:searchClick" class="btn btn-primary">Search</button>
                                </div> -->
                            </div>
                        </div>
                        <div class="card-body" >
                            <div style="float:right;">
                                <a href="#" data-bind="click:getOfferActiveClick">Active</a> |  
                                <a href="#" data-bind="click:getOfferDeactiveClick">Inactive</a>
                            </div>
                            <div class="table-responsive" data-bind="visible:employeeList">
                                <table class="table" id ="employeeList">
                                    <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Name</th>
                                          <th>Designation</th>
                                          <th>Rewards</th>
                                          <th>Mobile</th>
                                          <th>Status</th>
                                          <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <!-- ko foreach:employee -->
                                        <tr>
                                            <th scope="row"><span data-bind="text:($index()+1)"></span></th>
                                            <td>
                                              <img style="border-radius: 50%;" data-bind="attr: { src: employeeViewModel.imagePathEmployee() + profile_pic() }" height="50" width="50">
                                            <span data-bind="text:first_name()+' '+last_name()"></span></td>
                                            <td><span data-bind="text:designation"></span></td>
                                            <td><span data-bind="text:reward_count()"></span></td>
                                            <td><span data-bind="text:contact"></span></td>
                                            <td>
                                                <!-- ko if: statusText() == "Deactive" -->
                                                <a href="#" data-bind="click:$root.activeClick" class="on-default remove-row">
                                                <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                                </a>
                                                <!-- /ko -->
                                                <!-- ko if: statusText() == "Active" -->
                                                <a href="#" data-bind="click:$root.deactiveClick" class="on-default remove-row">
                                                  <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                                </a>
                                                <!-- /ko -->
                                            </td>
                                            <td>
                                                <a href="#" data-bind="click:$root.editEmployeeClick">Edit</a> |
                                                <a href="" data-bind="click:$root.deleteEmployeeClick">Delete</a>  
                                            </td>
                                        </tr>
                                      <!-- /ko -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-body" data-bind="visible:employeeNoData">
                          <p>No Data Availble</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="forms" data-bind="visible:employeeAddContainer"> 
        <div class="container-fluid">
            <div class="row">
                <!-- Form Elements -->
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                          <h3 class="h4" data-bind="text:title"></h3>
                        </div>
                      <div class="card-body">
                        <form class="form-horizontal" action="" name="form_employee" id="form_employee">
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">First Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="txtFirstName" id="txtFirstName" placeholder="Enter First Name" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Last Name</label>
                                  <div class="col-sm-8">
                                      <input type="text" name="txtLastName" id="txtLastName" placeholder="Enter Last Name" class="mr-3 form-control">
                                  </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Gender</label>
                                <div class="col-sm-8">
                                    <label class="checkbox-inline">
                                        <input id="radioMale" type="radio" value="Male" name="gender" class="radio-template" data-bind="checked: selectedGender"> Male 
                                    </label> 
                                    <label class="checkbox-inline">
                                        <input id="radioFemale" type="radio" value="Female" name="gender" class="radio-template" data-bind="checked: selectedGender"> Female
                                    </label>
          
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Designation</label>
                                <div class="col-sm-8">
                                    <input type="text" name="txtDesignation" id="txtDesignation" placeholder="Enter Designation" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Email Address</label>
                                <div class="col-sm-8">
                                  <input type="text" name="txtEmail" id="txtEmail" placeholder="Enter Email Address" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Mobile Number</label>
                                <div class="col-sm-8">
                                    <input type="text" name="txtContact" id="txtContact" placeholder="Enter Mobile Number" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Birth Date</label>
                                <div class="col-sm-8">
                                    <input type="text" name="dateBirth" id="dateBirth" placeholder="Select Birth date" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Date of Joining</label>
                                <div class="col-sm-8">
                                    <input type="text" name="dateJoin" id="dateJoin" placeholder="Select Joining Date" class="mr-3 form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Status</label>
                                <div class="col-sm-8">
                                      <select class="form-control" name="txtStatus" data-bind="
                                        value: selectedStatus,
                                        options: availbleStatus,
                                        optionsText: 'text',
                                        optionsValue: 'value',
                                        valueAllowUnset: true,
                                        optionsCaption: 'Select Status'
                                      ">
                                      </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Upload Pic</label>
                                <div class="col-sm-8">
                                    <input id="fileInput"  name="fileInput" type="file" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                                    <!-- ko if :employeeViewModel.action() =="update" -->
                                        <img style="border-radius: 50%;" data-bind="attr: { src: employeeViewModel.imagePathEmployee() + employeeViewModel.profile() }" height="100" width="100">
                                    <!-- /ko -->
                                </div>
                            </div>
                            <div class="form-group row">
                              <div class="col-sm-8 offset-sm-4">
                                  <button type="button" data-bind="click:createEmployeeClick" class="btn btn-primary"><span data-bind="text:button_title"></span></button>
                                  <button type="button" data-bind="click:cancleEmployeeClick" class="btn btn-danger text-uppercase"><span data-bind="text:'Cancel'"></span></button>
                              </div>
                            </div>
                        </form>
                      </div>
                    </div>
                </div>
            <!-- CSV Upload -->
            <div class="col-lg-6" data-bind="visible:formCsvVisibility">
                <div class="card">    
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">CSV Import</h3>
                    </div>
                    <div class="card-body">
                        <p>
                          To add your employees, you can also use our CSV import functionality. Using CSV import you can add all your employees at a time.<br>
                          <br>
                          For this you just have to create a CSV file of your employees with required fields and you are good to go. <a href="{{URL::to('/')}}/uploads/csv/sample.csv" download>Click here</a> to download the sample CSV import file.<br>
                          <br>
                        </p>
                        <form class="form-horizontal" id="form_import" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label class="col-sm-4 form-control-label">Upload CSV File</label>
                                <div class="col-sm-8">
                                    <input id="fileImport" name="fileImport" accept=".csv" type="file" class="form-control-file">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 offset-sm-4">
                                    <button type="button" data-bind="click:importEmployeeClick" class="btn btn-primary">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var type = "{{ $type }}";
    var employeeListApi = "{{config('corporate_constant.api.employee.list')}}";
    var employeeAddApi  = "{{config('corporate_constant.api.employee.add')}}";
    var employeeUpdateApi  = "{{config('corporate_constant.api.employee.update')}}";
    var activeEmployeeApiPath  = "{{config('corporate_constant.api.employee.active')}}";
    var deactiveEmployeeApiPath  = "{{config('corporate_constant.api.employee.deactive')}}";
    var deleteEmployeeApiPath  = "{{config('corporate_constant.api.employee.delete')}}";
    var employeeImportApi  = "{{config('corporate_constant.api.employee.import')}}";
    var path ="{{URL::to('/')}}"+"/uploads/employee/";
    var static_image_male="male.jpg";
    var static_image_female ="female.jpg";
</script>

<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('corporate_constant.script.employee.employee')}}"></script>
@endsection