@extends('corporate.master')
  @section('css')
    <style type="text/css">

    </style>
  @endsection
@section("content")
<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Dashboard</h2>
            </div>
          </header>
          <!-- Dashboard Counts Section-->
          <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-check"></i></div>
                    <div class="title"><span>Rewards<br>Issued</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:unused()+used()"></strong></div>
                  </div>
                </div>
        <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Rewards<br>Used</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:used()"></strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="icon-user"></i></div>
                    <div class="title"><span>Social<br>Reach</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                    <div class="number"><strong>100%</strong></div>
                  </div>
                </div>
                <!-- Item -->
                <div class="col-xl-3 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-green"><i class="icon-bill"></i></div>
                    <div class="title"><span>Amount<br>Saved</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 40%; height: 4px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-green"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:amount_saved()"></strong></div>
                  </div>
                </div>
                
              </div>
            </div>
          </section>
          <!-- Dashboard Header Section    -->
          <section class="dashboard-header">
            <div class="container-fluid">
              <div class="row">
                <!-- Statistics -->
                <div class="statistics col-lg-3 col-12">
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-red"><i class="fa fa-tasks"></i></div>
                    <div class="text"><strong data-bind="text:employee()"></strong><br><small>Active Employees</small></div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-calendar-o"></i></div>
                    <div class="text"><strong data-bind="text:availble_deals()"></strong><br><small>Available Deals</small></div>
                  </div>
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-orange"><i class="fa fa-paper-plane-o"></i></div>
                    <div class="text"><strong data-bind="text:brands()"></strong><br><small>Brands</small></div>
                  </div>
                </div>
                <!-- Line Chart            -->
                <!--<div class="chart col-lg-6 col-12">
                  <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
                    <canvas id="lineCahrt"></canvas>
                  </div>
                </div>-->
                
                    <div class="col-lg-6">
                        <div class="articles card">
                            <div class="card-close"></div>
                            <div class="card-header d-flex align-items-center">
                                <h2 class="h3">What's New this Month</h2>
                                <div class="badge badge-rounded bg-green" data-bind="text: (total())+' new'"></div>
                            </div>
                            <div class="card-body no-padding employee-scroll">
                              
                                <!-- ko foreach : employees() -->

                                <div class="item d-flex align-items-center" style=" justify-content: flex-end;">
                                    <div class="image">
                                        <img  data-bind="attr: { src: $root.imagePathEmployee() + profile_pic() }" height="50" width="50" alt="..." class="rounded-circle" />
                                    </div>
                                    <div class="text" style="width: 80%">
                                        <a href="#">
                                            <h3 class="h5" data-bind="text:first_name() +' '+ last_name() "></h3>
                                        </a>
                                        <small data-bind="text:reward_word() "></small>
                                    </div>
                                    <a href="{{route('perksvilla')}}" class="btn btn-primary" style="float:right;">
                                        <font color="#FFFFFF">Create Reward</font>
                                    </a>
                                </div>
                                <!-- /ko -->

                            </div>
                        </div>
                    </div>
                <div class="chart col-lg-3 col-12">
                  
                  <!-- Numbers-->
                  <div class="statistic d-flex align-items-center bg-white has-shadow">
                    <div class="icon bg-green"><i class="fa fa-line-chart"></i></div>
                    <div class="text"><strong data-bind="text:'Rs '+amount_invested()"></strong><br><small>Amount Invested</small></div>
                  </div>
                <!-- Bar Chart   -->
                  <div class="bar-chart has-shadow bg-white">
                    <div class="title"><strong class="text-violet">100%</strong><br><small>Increase in the productivity</small></div>
                    <canvas id="barChartHome"></canvas>
                  </div>
        </div>
              </div>
            </div>
          </section>
          @section('script')
              <script type="text/javascript">
                  var rewardStatisticsApi = "{{config('constant.api.reward.statistics')}}";
                  var statisticsApi = "{{config('corporate_constant.api.profile.statistics')}}";
                  var employeeListApi = "{{config('constant.api.employee.dashboard')}}";
                  var path ="{{URL::to('/')}}"+"/uploads/employee/";
                  var static_image_male="male.jpg";
                  var static_image_female ="female.jpg";
              </script>
              <script type="text/javascript" src="{{config('corporate_constant.script.dashboard')}}"></script>
              <script type="text/javascript" src="{{config('constant.script.common.slimscroll')}}"></script>
@endsection
@endsection