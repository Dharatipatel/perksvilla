<style type="text/css">
  nav.side-navbar a i{
    width: 25px;
  }

</style>
<nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar">
            @if(file_exists("uploads/corporate/".Session::get('cid').".png"))
              <img src="{{URL::to('/')}}/uploads/corporate/{{ Session::get('cid')}}.png?t={{ time() }}" alt="..." class="img-fluid rounded-circle">
            @else
              <img src="{{URL::to('/')}}/uploads/corporate/NoLogo.png" alt="Perksvilla" class="img-fluid rounded-circle">
            @endif
            </div>
            <div class="title">
              <h1 class="h4">{{ Session::get('corporate_name')}}</h1>
            </div>
          </div>
          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
              <li class=" has-submenu" id="dashboardTab">
                  <a href="{{route('corporatedashboard')}}">
                      <i class="icon-home"></i>Dashboard
                  </a>
              </li>
              <li class=" has-submenu" id="employeeTab">
                  <a href="#employeeDropdown" aria-expanded="false" data-toggle="collapse">
                      <i class="fa fa-users"></i>Employees
                  </a>
                  <ul id="employeeDropdown" class="collapse list-unstyled ">
                      <li id="addEmployeeTab"><a href="{{route('addemployee')}}">Add Employee</a></li>
                      <li id="listEmployeeTab"><a href="{{route('employee')}}">Employee List</a></li>
                  </ul>
              </li>
          <li class=" has-submenu" id="rewardTab">
              <a href="#rewardDropdown" aria-expanded="false" data-toggle="collapse">
                  <i class="fa fa-trophy"></i>Rewards
              </a>
              <ul id="rewardDropdown" class="collapse list-unstyled ">
                  <li id="createRewardTab"><a href="{{route('addreward')}}">Create Rewards</a></li>
                  <li id="manageRewardTab"><a href="{{route('reward').'?acitve'}}">Manage Rewards</a></li>
              </ul>
          </li>
          <li class=" has-submenu" id="perksvillaTab">
              <a href="{{route('perksvilla')}}">
                  <i class="icon-grid"></i>PerksVilla
              </a>
          </li> 
          <li class=" has-submenu" id="orderHistoryTab">
              <a href="{{route('corporateorderhistory')}}"> 
                  <i class="fa fa-bar-chart"></i>Order History
              </a>
          </li>
           <!--<li><a href="my-wallet.html"> <i class="icon-interface-windows"></i>My Wallet</a></li>-->
                   <li class=" has-submenu" id="myprofile"><a href="#myprofiledropdown" aria-expanded="false" data-toggle="collapse"> <i class="fa fa-user"></i>My Profile</a>
                      <ul id="myprofiledropdown" class="collapse list-unstyled ">
                        <li id="viewCorporateTab"><a href="{{route('corporateviewprofile')}}">View Profile</a></li>
                        <li id="editCorporateTab"><a href="{{route('corporateeditprofile')}}">Edit Profile</a></li>
                        <li id="changePasswordTab"><a href="{{route('corporatechangepassword')}}">Change Password</a></li>
                      </ul>
            </li>
            <li class="has-submenu" id="supportTab">  
                <a href="#messageDropdown" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-life-saver"></i>Support
            </a>
            <ul id="messageDropdown" class="collapse list-unstyled ">
                <li id="addMessageTab">
                    <a href="{{route('corporate.support.manage')}}">Create Message</a>
                </li>
                <li id="listMessageTab">
                    <a href="{{route('corporate.support')}}">Message List</a>
                </li>
            </ul>
            </li>
          </ul>
        </nav>