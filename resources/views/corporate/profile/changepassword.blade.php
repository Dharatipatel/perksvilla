@extends('corporate.master')
@section('content')
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">My Profile</h2>
        </div>
    </header>
		<section class="forms"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Change Password</h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" id="form_changePassword">
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Old Password</label>
                                    <div class="col-md-4">
                                        <input type="password" name="txtOldPassword" id="txtOldPassword" class="form-control" placeholder="Enter Old Password">
                                    </div>
                                </div>
						                    <div class="line"></div>
						                    <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">New Password</label>
                                    <div class="col-md-4">
                                        <input type="password" name="txtNewPassword" id="txtNewPassword" placeholder="Enter New Password" class="form-control">
                                    </div>
                                </div>
						                    <div class="line"></div>
                    						<div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Confirm Password</label>
                                    <div class="col-md-4">
                                        <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" placeholder="Enter New Password Again" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 offset-sm-3">
                                        <button type="button" class="btn btn-primary" data-bind="click:$root.changePasswordClick">Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>         
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var token = "{{ Session::get('token')}}";
          var changepasswordApiPath = "{{ config('brand_constant.api.profile.changepassword')}}";
        </script>
        <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
        <script type="text/javascript" src="{{config('corporate_constant.script.profile.changepassword')}}"></script>
    @endsection  
@endsection