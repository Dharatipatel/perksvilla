@extends('corporate.master')
@section('content')
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">My Profile</h2>
        </div>
    </header>
    <!-- Form Start-->
    <section class="forms"> 
        <div class="container-fluid">
            <div class="row">
            <!-- Form Elements -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Edit Profile</h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" >
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Corporate Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:corporate" disabled="disabled" placeholder="Enter Corporate Name" >
                                    </div>
                                    <div class="col-md-4">
                                        <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                                    </div>
                                </div>
                                <div class="line"></div>
                               
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Category</label>
                                    <div class="col-sm-4">
                                        <select class="form-control" disabled="disabled" name="txtCategory" id="txtCategory" data-bind="
                                          value: selectedCategory,
                                          options: availbleCategory,
                                          optionsText: 'text',
                                          optionsValue: 'value',
                                          valueAllowUnset: true,
                                          optionsCaption: 'Select Category'
                                        ">
                                        </select>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Company Size</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name ="txtCompanySize" data-bind="value:company_size"  placeholder="Enter Company Size Name" disabled="disabled" >
                                    </div>
                                </div>
                                <div class="line"></div>
                               
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Corporate Description</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" data-bind="value:description" disabled="disabled" placeholder="Enter Corporate Description"></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Contact Person</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:personname" disabled="disabled" placeholder="Enter Contact Person" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Email Address</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:email" disabled="disabled" placeholder="Enter Email Address" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Phone Number</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:contact" disabled="disabled" placeholder="Enter Phone Number" class="form-control">
                                    </div>
                                </div>
                
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Website URL</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:website_url" disabled="disabled" placeholder="Enter Website URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Facebook Page</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:facebook_page" disabled="disabled" placeholder="Enter Facebook URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">LinkedIn Page</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" placeholder="Linked In Page" name="txtLinkedInPage" data-bind="value:linkedin_page" id="txtLinkedInPage" disabled="disabled" type="text" value="">
                                    </div>
                                </div> 
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Business Address</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" data-bind="value:address" placeholder="Address" name="txtAddress" id="txtAddress" disabled="disabled" type="text" value=""></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var token = "{{ Session::get('token')}}";
          var type = "{{$type}}";
          var viewProfileApiPath = "{{ config('corporate_constant.api.profile.view')}}";
          var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
          var path ="{{URL::to('/')}}"+"/uploads/corporate/";
        </script>
        <script type="text/javascript" src="{{config('corporate_constant.script.profile.view')}}"></script>
    @endsection
@endsection