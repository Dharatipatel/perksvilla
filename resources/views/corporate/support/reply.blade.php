@extends('corporate.master')
@section("css")
<link rel="stylesheet" href="{{config('constant.css.common.wysihtml5')}}" type="text/css">
<link rel="stylesheet" href="{{config('constant.css.common.wysihtml5_color')}}" type="text/css">
@endsection
@section("content")		
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">{{@$title}}</h2>
    </div>
</header>
<section class="tables">   
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card m-t-20">
                    <div class="card-header"> 
                        <h3 class="panel-title">{{$support->title}}</h3> 
                    </div>
                    
                    <div class="card-body p-t-10 p-b-10">
                        <div class="media">
                            <div class="media-body">
                                @foreach($support_reply as $s)
                                <div class="card-body">
                                    <div class="media m-b-30 ">
                                        <div class="media-body">
                                            <span class="media-meta pull-right">{{$s->created_at->format('d-m-Y g:i:s A')}}</span>

                                             @if($s->role_id==1)
                                                <h4 class="text-primary m-0">Admin</h4>
                                            @else
                                                <h4 class="text-primary m-0">{{ $s->corporate_name }}</h4>
                                            @endif
                                        </div>
                                    </div> <!-- media -->

                                    {!! html_entity_decode($s->comment) !!}
                                    <hr/>
                                </div> <!-- panel-body -->
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div> <!-- End panel -->
            </div> <!-- End Rightsidebar -->
        </div>
        <div class="card">
            @if($support->status==1)
            {!! Form::open(["url"=>route("corporate.support.reply.save"),"method"=>"POST", 'id'=>'form', 'class'=>"form-horizontal","role"=>"form"]) !!}
            {!! Form::hidden("id", $support->id)    !!}
            <div class="card-body p-t-10 p-b-10">
                <div class="media">
                    <div class="media-body">
                        {!! Form::textarea("comment", '', ["class"=>"form-control wysihtml5", "placeholder"=>"Reply here..."])!!}
                        @if($errors->has("comment"))
                        <span class="text-danger flash">{{ $errors->first("comment") }}</span>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            <div class="card-body">
                <div class="form-group">
                    <br/>
                    @if($support->status==1)
                    <button class="btn btn-purple"> <span>Reply</span> <i class="fa fa-send m-l-10"></i> </button>
                    @endif
                    <a  href="{{route("corporate.support")}}" class="btn btn-primary"> <span>Back</span></a>
                </div>
            </div>
            {!! Form::close()   !!}
        </div>
    </div>
</section>

@endsection
@section('script')

<script>
    jQuery(document).ready(function () {
        $('.has-submenu').removeClass('active');
        $('#supportTab').addClass('active');
        $('#messageDropdown').addClass('show');
        $('#listMessageTab').addClass('active');
    });
    @if (session() -> has("success"))
            showMsg("success", "{{session()->get('success')}}");
    @endif
</script>
@endsection