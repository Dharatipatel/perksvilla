@extends('corporate.master')
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
<style type="text/css">
    .dataTables_wrapper  .row{
        width: 100%
    }
    div.dataTables_info {
        padding-top: 0px; 
    }
    div.dataTables_paginate{
        float: right;
    }
    .pagination>li:first-child>a, .pagination>li:first-child>span {
        margin-left: 0;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }
    .pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
        color: #777;
        cursor: not-allowed;
        background-color: #fff;
        border-color: #ddd;
    }
    .pagination>li>a, .pagination>li>span {
        color: #373e4a;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #fff;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
    .pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus {
        background-color: #1c2b36;
        border-color: #1c2b36;
    }   
</style>
@endsection
@section("content")		
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">{{@$title}}</h2>
    </div>
</header>

<!-- Dashboard Header Section    -->

<section class="tables">   
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">     
                        <a href="{{route("corporate.support.manage")}}" class="btn btn-primary m-b-5">Add {{@$title}}</a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table id="pending_investment_tbl" class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Date Time</th>
                                        <th>Action</th> 
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
@endsection

@section('script')
<script type="text/javascript">
    var datatable = "";
    var user_support_lists = '{{ route("corporate.support.lists") }}';
    @if (session() -> has("success"))
            showMsg("success", "{{session()->get('success')}}");
    @endif
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('corporate_constant.script.support.support')}}"></script>
@endsection