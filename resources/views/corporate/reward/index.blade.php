@extends('corporate.master')
@section("title")
	{{ @$title. ' - '}}
@stop
@section("css")
  <style type="text/css">
      .table thead th{
          vertical-align: top;
      }
  </style>
  <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")

	<header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">Rewards</h2>
            </div>
          </header>
		  <!-- Table Start-->
		  <div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="createRewardCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                          @include('popup')
                      </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
  
		  <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
               
			    <!-- Item -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-red"><i class="icon-user"></i></div>
                    <div class="title"><span>Rewards<br>Issued</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:unused()+used()"></strong></div>
                  </div>
                </div>
			    <!-- Item -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-orange"><i class="icon-check"></i></div>
                    <div class="title"><span>Rewards<br>Used</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:used()"></strong></div>
                  </div>
                </div>
				<!-- Item -->
                <div class="col-xl-4 col-sm-6">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Total<br>Saving</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:'Rs '+total_save()"></strong></div>
                  </div>
                </div>
               
                <!-- Item -->
                
                
              </div>
            </div>
          </section>
         
		  <section class="tables">   
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      <div>
                      	<a href="{{route('reward').'?acitve'}}" @if($status ==0)style="color:#ff7676 ;"@endif>Active</a> |  
                      	<a href="{{route('reward').'?used'}}" @if($status ==1)style="color:#ff7676 ;"@endif>Used</a></div>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Manage Rewards</h3>
                    </div>
					
                    <div class="card-body">
					
                      <div class="table-responsive">
                        <table class="table" id ="rewardsList">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Employee Name</th>
                              <th>Designation</th>
              							  <th>Reward Type</th>
                              <th>Rewards Name</th>
                              <!-- ko if: status() == 0 -->
                              <th>Coupon Value</th>
                              <!-- /ko -->
                              <th>Purchase Price</th>
                              <th>Purchase Date</th>
              							  <th>Issue Date</th>
              							  <!-- ko if: status() == 1 -->
              							  <th>Redeem Date</th>
              							  <!-- /ko -->
                            </tr>
                          </thead>
                          <tbody>
                          	<!--ko foreach:rewards()-->
                            <tr>
                                <td scope="row">
                                    <span data-bind="text:$index()+1"></span>
                                </td>
                                <td>
                                    <span data-bind="text:first_name() +' '+ last_name()"></span>
                                </td>
                                <td>
                                    <span data-bind="text:designation()"></span>
                                </td>
							                  <td>
                                    <span data-bind="text:category_name()"></span>
                                </td>
							                  <td>
                                    <a href="" data-bind="click:$root.viewOfferClick">
                                        <span data-bind="text:offer_name()"></span>
                                    </a>
                                </td>
                                <!-- ko if: $root.status() == 0 -->
                                    <td>
                                        <span data-bind="text:'Rs '+coupen_value()"></span>
                                    </td>
                                <!-- /ko -->
                                <td>
                                    <span data-bind="text:'Rs '+buy_price()"></span>
                                </td>
                                 <td>
                                    <span data-bind="text:created()"></span>
                                </td>
                                <td>
                                    <span data-bind="text:issue_date()"></span>
                                </td>
                                <!-- ko if: $root.status() == 1 -->
                              	    <td>
                                        <span data-bind="text:redeem_date()"></span>
                                    </td>
                                <!-- /ko -->
                               
                            </tr>
                             <!-- /ko-->
							          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </section>
@endsection
@section('script')
<script type="text/javascript">
    var type = "{{ $type }}";
    var status = "{{ $status }}";
    var rewardListApi = "{{config('constant.api.reward.list')}}";
    var rewardStatisticsApi = "{{config('constant.api.reward.statistics')}}";
    var path ="{{URL::to('/')}}"+"/uploads/brand/";
    var offerDetailApi = "{{config('constant.api.offer.list')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
    var statisticsApi = "{{config('corporate_constant.api.profile.statistics')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('corporate_constant.script.reward.list')}}"></script>
@endsection