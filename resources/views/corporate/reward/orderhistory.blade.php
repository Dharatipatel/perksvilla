@extends('corporate.master')
@section("title")
	{{ @$title. ' - '}}
@stop
@section("css")
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
    <style type="text/css">
      #rewardsList td ,#rewardsList th{
        text-align: center;
      }
    </style>
@endsection
@section("content")
<header class="page-header">
    <div class="container-fluid">
      	<h2 class="no-margin-bottom">Order History</h2>
    </div>
</header>
<div class="modal fade" id="viewModel" tabindex="-1" role="dialog" aria-labelledby="createRewardCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="container-fluid">
                    @include('popup')
                </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
          </div>
      </div>
  </div>
<section class="dashboard-counts" style="padding-bottom: 25px;">
    <div class="container-fluid">
      	<div class="row bg-white has-shadow">
            <div class="col-xl-4 col-sm-6">
              	<div class="item d-flex align-items-center">
                    <div class="icon bg-red">
                    	<i class="icon-user"></i>
                    </div>
                    <div class="title">
                    	<span>Rewards<br>Issued</span>
                      	<div class="progress">
                        	<div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red">
                        	</div>
                      </div>
                    </div>
                    <div class="number">
                    	<strong data-bind="text:reward_issued()"></strong>
                    </div>
              	</div>
            </div>
            <div class="col-xl-4 col-sm-6">
              	<div class="item d-flex align-items-center">
                    <div class="icon bg-orange">
                    	<i class="icon-check"></i>
                    </div>
                    <div class="title">
                    	<span>Total<br>Saving</span>
                      	<div class="progress">
                        	<div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange">
                        	</div>
                      	</div>
                    </div>
                    <div class="number">
                    	<strong data-bind="text:'Rs '+total_save()"></strong>
                    </div>
              	</div>
            </div>
        	<div class="col-xl-4 col-sm-6">
          		<div class="item d-flex align-items-center">
            		<div class="icon bg-violet">
            			<i class="icon-padnote"></i>
            		</div>
            		<div class="title">
            			<span>Your<br>Investment</span>
              			<div class="progress">
	                    	<div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet">			
	                    	</div>
              			</div>
           			</div>
            		<div class="number">
            			<strong data-bind="text:'Rs '+total_invest()"></strong>
            		</div>
          		</div>
        	</div>
      	</div>
    </div>
</section>
@if (Session::has('payment_success'))
    <section class="tables" style="padding: 0;">
      <div class="container-fluid">
        <div class="alert alert-success">
            {{ Session::get('payment_success') }}
            {{ Session::forget('payment_success') }}
        </div>
      <div class="container-fluid">
    </section>
@endif
@if (Session::has('payment_fail'))
    <section class="tables"  style="padding: 0;">
        <div class="container-fluid">  
            <div class="alert alert-danger">
              {{ Session::get('payment_fail') }}
            </div>
        </div>
    </section>
@endif
<section class="tables" style="padding-top: 0px;">   
	<div class="container-fluid">
      	<div class="row">
            <div class="col-lg-12">
              	<div class="card">
                    <div class="card-close">
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Transaction Summary</h3>
                    </div>
                    <div class="card-body">
                      	<div class="table-responsive">
	                        <table class="table" id="rewardsList">
		                        <thead>
		                            <tr>
		                              <td><b>#</b></td>
		                              <td><b>Employee Name</b></td>
		                              <td><b>Reward Name</b></td>
                                  <td><b>Per Coupon Price</b></td>
              									  <td><b>Total Amount</b></td>
              									  <td><b>Purchase Date</b></td>
		                            </tr>
		                        </thead>
		                        <tbody>
		                        	<!-- ko foreach:orderhistory() -->  
			                            <tr>
				                          	<td scope="row" data-bind="text:($index() + 1)"></td>
				                          	<td data-bind="text:employee_name()"></td>
				                          	<td>
				                          		<a href="" data-bind="click:$root.viewOfferClick">
                                        <span data-bind="text:offer_name()"></span>
                                      </a>
				                          	</td>
                                    <td style="text-align: center;" data-bind="text:'Rs '+per_coupon_price()"></td>
				                          	<td style="text-align: center;" data-bind="text:'Rs '+total_amount()"></td>
				                          	<td data-bind="text:purchase_date()"></td>
			                            </tr>
		                            <!-- /ko -->  
		                        </tbody>
	                        </table>
                      	</div>
                    </div>
              	</div>
            </div>
      	</div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
    var type = "{{ $type }}";
    var orderHistoryListApi = "{{config('constant.api.reward.orderhistory')}}";
    var orderHistoryStatisticApi = "{{config('constant.api.reward.orderhistorystatistic')}}";
    var offerDetailApi = "{{config('constant.api.offer.list')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
    var path ="{{URL::to('/')}}"+"/uploads/brand/";

</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('corporate_constant.script.reward.orderhistory')}}"></script>
@endsection