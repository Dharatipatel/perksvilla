@extends('corporate.master')
@section("title")
	{{ @$title. ' - '}}
@stop
@section("css")
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
 <style type="text/css">
  #employeeList td ,#employeeList th{
        vertical-align: middle;
  }
  </style>
@endsection
@section("content")
	<header class="page-header">
		<div class="container-fluid">
			<h2 class="no-margin-bottom">Rewards</h2>
		</div>
	</header>
	<div class="modal fade" id="viewBrandDetail" role="dialog">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h4 class="modal-title">Offer Detail</h4>
	                <button type="button" class="close" data-dismiss="modal">&times;</button>
	            </div>
	            <div class="modal-body">
	            	@include('popup')
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure to delete Category ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-bind ="click:deleteCustomRewardClick" class="btn btn-primary">Yes</button>
              </div>
            </div>
          </div>
        </div>
	<div class="modal fade" id="createReward" tabindex="-1" role="dialog" aria-labelledby="createRewardCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Create Own Reward Category</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    	<form id="form_category">
                        <div class="form-group">
			                <label class="col-sm-3 control-label">Name</label>
			                <div class="col-sm-8">
			                    <input class="form-control" placeholder="Category Name" name="name" id="txtCategoryName" type="text" value="">
			                </div>
            			</div>
			            <div class="form-group">
			                <label class="col-sm-3 control-label">Description</label>
			                <div class="col-sm-8">
			                    <input class="form-control" placeholder="Category Description" name="txtDescription" id="txtDescription" type="text" value="">
			                </div>
			            </div>  
			            </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bind ="click:createRewardCloseClick">Close</button>
                        <button type="button" data-bind ="click:createRewardModalClick" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </div>
        </div>
	<section class="forms">
        <div class="container-fluid">
            <div class="row">
			  	<div class="col-lg-12">
	                <div class="card">
		                <div class="card-body text-left">
							<b>Create Reward</b> - <span data-bind="text:stepDescription"></span>
		                </div>
		                <div data-bind="visible: step2" class="card-close">
                     		<span data-bind="text: choosenEmployee().length"></span> Employee Selected
                    	</div>
	                </div>
              	</div>
              	<div class="col-lg-12" data-bind="visible:step1">
              		<div class="card">
              			<div class="card-body text-left">
              				<div class="row">
			 				<div class="col-lg-3">
								<p><b>Category</b></p>
                        		<select class="form-control country" data-bind="
                        		  value: selectedBrandCategory,
	                              options: $root.availbleBrandCategory,
	                              optionsText: 'text',
	                              optionsValue: 'value',
	                              valueAllowUnset: true,
	                              optionsCaption: 'Select Category',
	                              attr: { id:'brandCategory' ,name: 'brandCategory'}
	                            ">
                        		</select>
							</div>
							<div class="col-lg-3">			
								<p><b>Coupon Type</b></p>
								<input id="coupenFree" type="checkbox" value="0"  data-bind="checked: coupenType"> - Free <br>
								<input id="coupenFix" type="checkbox" value="1"  data-bind="checked: coupenType"> - Fix Price 
								<br>
	
							</div>
							
							<div class="col-lg-3">
								<p><b>Price Range</b></p>
								<input id="priceRange1" type="checkbox" value="0" data-bind="checked: priceRange"> - Below Rs 500<br>
								<input id="priceRange2" type="checkbox" value="1" data-bind="checked: priceRange"> - Rs 500 to Rs 2000 <br>
								<input id="priceRange3" type="checkbox" value="2" data-bind="checked: priceRange"> - Rs 2000 to Rs 5000 <br>
								<input id="priceRange4" type="checkbox" value="3" data-bind="checked: priceRange"> - Above Rs 5000 
							</div>
							<div class="col-lg-3">
                				<a href="#" data-bind="click:offerSearchClick" class="btn btn-primary"> Search</a>
							</div>
							</div>
                  		<!-- Modal-->
              			</div>  
              		</div>
              		<div class="row" data-bind="visible:brandList">
						<!--ko foreach : brandOffers--> 
							<div class="col-lg-3">
		                  		<div class="card">
		                   			<div class="card-header text-center">
		                    			<h3 class="h4" data-bind="text:brand_name"></h3>
		                    		</div>
		                    		<div class="card-body text-center">
										<p>
											<a href="#" data-bind="click:$root.offerSearchBrandClick">
												<img  style="cursor:pointer" data-bind="attr: { src: $root.imagePath() + brand_logo() }" height="150" width="150">
											</a> 
											<br><br>
							 			<span data-bind ="text:fixcoupen() + ' Fix Price Offers' "></span><br>
							  			<span data-bind ="text:freecoupen() + ' Free Offers' "></span> <br><br>
							  			<span data-bind ="text:'Category - '+category() "></span>
										</p>
		                    		<a href="#" data-bind="click:$root.offerSearchBrandClick" class="btn btn-primary">Select</a>
							 		
		                    		</div>
		                  		</div>
		                	</div>
		                <!--/ko-->

		                <div class="card" data-bind="visible:offerNoData" style="margin:15px;width:100%">
		                	<div class="card-body" > 
	            				<p>No offers available, please check back later</p>
	            			</div>
	            	    </div>
	                </div>	               
	               	<section class="projects no-padding-top" data-bind="visible:offerList">
            			<div class="table-responsive">
	                        <table id="table1" class="table" style="font-size: 0.8em">
	                            <thead>
		                            <tr class="bg-white has-shadow">
		                              <th>Offer Name</th>
		                              <th class="text-center">Category</th>
		                              <th class="text-center">Issue Date</th>
		                              <th class="text-center">Expire Date</th>
									  <!-- <th class="text-center">Total Coupons</th> -->
									  <th class="text-center">Coupons Left</th>
									  <th class="text-center">Coupon Value</th>
									  <th class="text-center">Purchase Price</th>
									  <th class="text-center">&nbsp;</th>
		                            </tr>
	                          	</thead>
	                          	<tbody class="project">
	                          		<!-- ko foreach : offers-->
			                            <tr class="bg-white has-shadow">
				                            <td scope="row">
				                            	<h3 class="h6">
					                            	<a href="#" data-bind="click:$root.brandDetailClick,text:name"></a>
					                            </h3>
					                            <small data-bind="text:'By '+brand_name()"></small>
				                            </td>
				                            <td data-bind="text:category()" class="text-center" style="vertical-align: middle;"></td>  
				                            <td data-bind="text:publish_date_text" class="text-center" style="vertical-align: middle;"></td>
											<td data-bind="text:expiry_date_text" class="text-center" style="vertical-align: middle;"></td>
				                            <!-- <td data-bind="text:count" class="text-center" style="vertical-align: middle;"></td>   -->
				                            <td data-bind="text:remaining_count" class="text-center" style="vertical-align: middle;"></td>
				                            <td data-bind="text:'Rs '+value()" class="text-center" style="vertical-align: middle;"></td>
				                            <td data-bind="text:'Rs '+price()" class="text-center" style="vertical-align: middle;"></td>
				                            <td class="text-center">
				                            	<a href="#" class="btn btn-primary" data-bind="click:$root.rewardStep2Click"> Buy</a>
				                            </td>
			                            </tr>
			                        <!--/ko-->
	                          </tbody>
	                        </table>
                        </div>	
    					<div data-bind="visible:offerNoData" class="card"> 
    						<div class="card-body" > 
	            				<p>No offers available, please check back later</p>
	            			</div>
    					</div>
          
          			</section>
            	</div>
            	<div class="col-lg-12" data-bind="visible:step4">
              		<div class="row">
		               	<!-- ko foreach:rewards-->
			                <div class="col-lg-4">
			                  	<div class="card">
				                    <div class="card-header text-center">
				                      <h3 class="h4" data-bind="text:name"></h3>
				                    </div>
				                    <div class="card-body text-center"><br>
										<p data-bind="text:description"></p><br>
					                    <a href="#"  data-bind="attr:{'data-id':id},click:$root.createRewardClick" class="btn btn-primary"> Create Reward</a>
										<!-- ko if:user_id()!=null-->
											<a href="#"  data-bind="attr:{'data-id':id},click:$root.deleteCustomRewardPopUp" class="btn btn-danger"><i class="fa fa-remove" area-hidden ="true"></i></a>
										<!-- /ko -->
				                    </div>
			                  	</div>
			                </div>
		                <!-- /ko -->
		                <div class="col-lg-4">
		                  	<div class="card">
			                    <div class="card-header text-center">
			                      <h3 class="h4">Create Your Own</h3>
			                    </div>
			                    <div class="card-body text-center"><br>
									<p>Some occassion are very special for that create your own reward</p><br>
				                    <a href="#" data-bind="click:$root.createOwnRewardModelClick" class="btn btn-primary">Add New</a>
			                    </div>
		                  	</div>
		                </div>
		            </div>
            	</div>
            	<div class="col-lg-12" data-bind="visible:step2">
	                <div class="card">
	                    <div class="card-close">
	                    </div>
	                    <div class="card-header d-flex align-items-center">
	                      	<h3 class="h4">Employee List</h3>
						</div>
	                    <div class="card-body">
							<div style="float:right; margin-bottom:15px; margin-right:20px;"> 
		                    	<!-- <form class="form-inline" id="form-searchEmployee"> -->
		                        	<div class="form-group">
		                          		<label for="txtSearch" class="sr-only">Search by Name</label>
		                          		<input id="txtSearch" data-bind="event:{keyup:searchEmployeeClick},value: searchValue,valueUpdate: 'afterkeydown'" name="txtSearch" type="text" placeholder="Search by Name" class="mr-3 form-control">
		                        	</div>
		                  
		                        <!-- </form> -->
		                    </div>
	                      	<div class="table-responsive">
	                        	<table class="table" id="employeeList">
	                          		<thead>
	                            		<tr>
	                              			<th>#</th>
	                          				<th>Employee Name</th>
	                              			<th>Designation</th>
	                              			<th>Email Address</th>
								  			<th>Mobile Number</th>
								  			<th>Select</th>
	                            		</tr>
	                          		</thead>
	                          		<tbody>
	                          			<!-- ko foreach:employees-->
		                            		<tr>
		                              			<th scope="row">
		                              				<span data-bind="text:($index()+1)"></span>
		                              			</th>
		                              			<td>
		                              				<img style="border-radius: 50%;" data-bind="attr: { src: $root.imagePathEmployee() + profile_pic() }" height="50" width="50">
		                              				<span data-bind="text:first_name()+' '+last_name()"></span>
		                              			</td>
		                              			<td>
		                              				<span data-bind="text:designation"></span>
		                              			</td>
									  			<td>
									  				<span data-bind="text:email">
									  			</td>
		                              			<td>
		                              				<span data-bind="text:contact">
		                              			</td>
		                             			<td>
		                             				<input type="checkbox" data-bind="attr: { 'id': 'employee' + $index() } ,checkedValue: id, checked: $root.choosenEmployee" name="employee[]" />
		                             			</td>
		                            		</tr>
	                            		<!-- /ko -->
	                          		</tbody>
	                    		</table>
	                      	</div>
	                    </div>
	                </div>
            	</div>
            	<div class="col-lg-12" data-bind="visible:step3">
                  	<div class="card">
                    	<div class="card-body">
                      		<div class="table-responsive">
                        		<table class="table">
                          			<thead>
			                            <tr>
			                              <th>#</th>
			                              <th>Employee Name</th>
			                              <th>Coupon</th>
			                              <th>Issue Date</th>
										  <th>Amount</th>
										  <th>Action</th> 
			                            </tr>
                          			</thead>
                          			<tbody>
	                          			<!-- ko foreach:selectedEmployeeDetail-->
				                            <tr>
				                            	<th scope="row">
				                            		<span data-bind="text:($index()+1)"></span>
				                            		<input type="hidden" data-bind="attr:{name:'employee_id'+$index(),id:'employee_id'+$index()},value:id" placeholder="Select Issue Date" class="form-control">
				                            	</th>
				                              	<td>
				                              		<span data-bind="text:first_name()+' '+last_name()"></span>
				                              	</td>
				                              	<td><a href="#" data-bind="click:$root.brandDetailClick(rewardViewModel.selectedOfferDetail()[0])"><span data-bind="text:rewardViewModel.selectedOfferDetail()[0].name()"></span></a></td>
				                              	<td>
				                              		<input type="text" data-bind="attr:{name:'issueDate'+$index(),id:'issueDate'+$index()}" placeholder="Select Issue Date" class="form-control">
				                              	</td>
											   	<td><span data-bind="text:'Rs '+rewardViewModel.selectedOfferDetail()[0].price()"></td>
											   	<td><a href="#" data-bind="click:$root.removeEmployee">Remove</a></td>
				                            </tr>
				                        <!-- /ko-->
	                            		<!-- ko if:selectedEmployeeDetail().length > 0-->
				                            <tr>
				                              	<th scope="row"></th>
				                              	<td></td>
				                             	<td></td> 
				                              	<td><b>Total Amount</b></td>
											   	<td><b><span data-bind="text:'Rs '+selectedOfferDetail()[0].price() * selectedEmployeeDetail().length"></b></td>
				                              	<td>
				                   		            <button type="button" data-bind="click:payNowSubmit" class="btn btn-primary">Pay Now</button>
				                              	</td>	  
				                            </tr>
			                            <!-- /ko-->
                       				</tbody>
                        		</table>
                      		</div>
                    	</div>
           			</div>
        		</div>
                <div data-bind="visible: step1" class="col-lg-12">
                	<!-- <button type="button" class="btn btn-primary" data-bind="click:rewardStep2Click,visible:offerList" style="float: right;">
                		<span>Next</span>
                		<i class="fa fa-angle-double-right"></i>
                	</button> -->
                </div>
                <div data-bind="visible: step4" class="col-lg-12">
                	<button type="button" class="btn btn-primary" data-bind="click:previousStep4Click,visible:step4">
                		<i class="fa fa-angle-double-left"></i>
                		<span>Previous</span>
                	</button>
                </div>
                <div data-bind="visible: step2" class="col-lg-12">
                	<button type="button" class="btn btn-primary" data-bind="click:previousStep2Click">
                		<i class="fa fa-angle-double-left"></i>
                		<span>Previous</span>
                	</button>
                	<button type="button" class="btn btn-primary" data-bind="click:payNowClick" style="float: right;">
                		<span>Pay Now</span>
                		<i class="fa fa-angle-double-right"></i>
                	</button>
            	</div>
            	<div data-bind="visible: step3" class="col-lg-12">
                	<button type="button" class="btn btn-primary" data-bind="click:previousStep3Click">
                		<i class="fa fa-angle-double-left"></i>
                		<span>Previous</span>
                	</button>
            	</div>
        	</div>
        </div>
    </section>
          
@endsection
@section('script')
<script type="text/javascript">
    var type = "{{ $type }}";
    var employeeListApi = "{{config('corporate_constant.api.employee.list')}}";
    var rewardUpdateApi  = "{{config('corporate_constant.api.reward.update')}}";
    var activeRewardApiPath  = "{{config('corporate_constant.api.reward.active')}}";
    var deactiveRewardApiPath  = "{{config('corporate_constant.api.reward.deactive')}}";
    var rewardImportApi  = "{{config('corporate_constant.api.reward.import')}}";
    var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
    var offerListApi = "{{config('constant.api.offer.list')}}";
    var brandofferListApi = "{{config('constant.api.brand.brandoffer')}}";
    var addRewardApiPath = "{{config('constant.api.reward.add')}}";
    var path ="{{URL::to('/')}}"+"/uploads/brand/";
    var employee_path ="{{URL::to('/')}}"+"/uploads/employee/";
    var historyListingWeb = "{{route('corporateorderhistory')}}";
    var rewardListingWeb = "{{route('reward')}}";
    var addCategoryPath = "{{config('constant.api.category.add')}}";
    var deleteCategoriesApiPath = "{{config('constant.api.category.delete')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('corporate_constant.script.reward.perksvilla')}}"></script>
@endsection