@extends('corporate.master')
@section("title")
	{{ @$title. ' - '}}
@stop
@section("css")

@endsection
@section("content")
	
@endsection
@section('script')
<script type="text/javascript">
    var success = "{{ $success }}";
    var payment_request_id = "{{ $payment_request_id }}";
    var payment_id = "{{ $payment_id }}";
    var rewardUpdateApi  = "{{config('constant.api.reward.update')}}";
    var rewardListingWeb = "{{route('reward')}}";
    var historyListingWeb = "{{route('corporateorderhistory')}}";

</script>
<script type="text/javascript" src="{{config('corporate_constant.script.reward.success')}}"></script>
@endsection