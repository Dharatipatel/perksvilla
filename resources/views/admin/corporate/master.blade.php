@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .highlightheader{
                padding: 30px 15px;
                margin: 20px 0;
        }
            .has-shadow {
            -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1), -1px 0 2px rgba(0, 0, 0, 0.05);
            box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1), -1px 0 2px rgba(0, 0, 0, 0.05);
        }
        .bg-white {
            background: #fff !important;
        }
        .item{
            border-right: 1px solid #eee;
            padding: 15px 0;
        }
        .title {
            font-size: 1.1em;
            font-weight: 300;
            color: #777;
            margin: 0 20px;
            vertical-align: middle;
            display: inline-block;
        }
        .number {
            font-size: 1.8em;
            font-weight: 300;
            vertical-align: middle;
            display: inline-block;
        }
        .employeelist.active{
            color: #3bc0c3;
        }
    </style>
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.corporate.index')
        
        @include('admin.corporate.manage')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Delete Corporate</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Are you sure to delete Corporate ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="deleteEmployeeModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Delete Employee</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">Are you sure to delete Employee ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-bind ="click:employeeViewModel.deleteConfirm" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Send Email</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {!! Form::open(['id'=>'form_email_send', 'class'=>"form-horizontal","role"=>"form"]) !!}
                    {!! Form::hidden("corporate_id", "", ["id"=>"hiddenCorporateId"]) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email:</label>
                            <label class="col-sm-8" id="labelEmail"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password:</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Password" name="txtSendPassword" id="txtSendPassword" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-bind="click:$root.emailSendClick" class="btn btn-primary">Send</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var type = "{{$type}}";
    var cid ="{{Request::get('cid')}}";
    var corporateListApiPath ="{{config('constant.api.corporate.list')}}";
    var addCorporateApiPath ="{{config('constant.api.corporate.add')}}";
    var updateCorporateApiPath ="{{config('constant.api.corporate.update')}}";
    var deleteCorporateApiPath ="{{config('constant.api.corporate.delete')}}";
    var activeCorporateApiPath ="{{config('constant.api.corporate.active')}}";
    var deactiveCorporateApiPath ="{{config('constant.api.corporate.deactive')}}";
    var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
    var employeeWebList = "{{route('employee')}}";
    var corporateWebList = "{{route('corporatelist')}}";
    var path ="{{URL::to('/')}}"+"/uploads/corporate/";
    var pathEmployee ="{{URL::to('/')}}"+"/uploads/employee/";
    var pathBrand ="{{URL::to('/')}}"+"/uploads/brand/";
    var employeeListApiPath = "{{config('corporate_constant.api.employee.list')}}";
    var employeeAddApi  = "{{config('corporate_constant.api.employee.add')}}";
    var employeeUpdateApi  = "{{config('corporate_constant.api.employee.update')}}";
    var employeeImportApi  = "{{config('corporate_constant.api.employee.import')}}";
    var emailSendApiPath = "{{config('constant.api.corporate.email.send')}}";
    var orderHistoryListApi = "{{config('constant.api.reward.orderhistory')}}";
    var static_image ="NoLogo.png";
    var static_image_male="male.jpg";
    var static_image_female ="female.jpg";
    var offerDetailApi = "{{config('constant.api.offer.list')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
    var deleteEmployeeApiPath ="{{config('constant.api.employee.delete')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('constant.script.corporate')}}"></script>
@endsection