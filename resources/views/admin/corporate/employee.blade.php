<div class="panel-body"  data-bind="visible:employeeViewModel.employeeAction">
 <a href="" class="btn btn-primary m-b-5" data-bind="click:employeeViewModel.addEmployeeClick">Add Employee</a>
 <a href="" class="btn btn-primary m-b-5" data-bind="click:employeeViewModel.importCsvClick">Import CSV Employee</a>
 <a href="" class="btn btn-danger m-b-5" data-bind="click:cancelCorporateClick">Cancel</a>
 <div style="float:right">
 <a href="" id="activeEmployee" class="active employeelist" data-bind="click:employeeViewModel.activeEmployeeClick">Active </a> |
 <a href="" id="deactiveEmployee" class="employeelist" data-bind="click:employeeViewModel.deactiveEmployeeClick"> Deactive</a>
</div>
</div> 

<div class="panel-body"  data-bind="visible:employeeViewModel.employeeList">
    <table class="table table-striped table-bordered" id="employeeList">
        <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Designation</th>
              <th>Mobile</th>
              <th>Action</th>
            </tr>
        </thead>
        <tbody>
          <!-- ko foreach:employeeViewModel.employees -->
            <tr>
                <th scope="row"><span data-bind="text:($index()+1)"></span></th>
                <td><a href=""><span data-bind="text:first_name()+' '+last_name()"></span></a></td>
                <td><span data-bind="text:designation"></span></td>
                <td><span data-bind="text:contact"></span></td>
                <td>
                    <a href="#" data-bind="click:$root.employeeViewModel.editEmployeeClick"><i class="text-success fa fa-pencil"></i></a>
                    <a href="#" data-bind="click:$root.employeeViewModel.deleteEmployeeClick"><i class="text-danger fa fa-trash-o"></i></a>
                </td>
            </tr>
          <!-- /ko -->
        </tbody>
    </table>
</div>
<div class="col-xs-12" data-bind="visible:employeeViewModel.employeeNoData">
    <p>No Data Availble</p>
</div>
<div class="panel-body" data-bind="visible:employeeViewModel.addEmployeeContainer">
    <form class="form-horizontal" action="" name="form_employee" id="form_employee">
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">First Name</label>
              <div class="col-sm-8">
                  <input type="text" name="txtFirstName" id="txtFirstName" placeholder="Enter First Name" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Last Name</label>
                <div class="col-sm-8">
                    <input type="text" name="txtLastName" id="txtLastName" placeholder="Enter Last Name" class="mr-3 form-control">
                </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Gender</label>
              <div class="col-sm-8">
                  <label class="checkbox-inline">
                      <input id="radioMale" type="radio" value="Male" name="gender" class="radio-template" data-bind="checked: employeeViewModel.selectedGender"> Male 
                  </label> 
                  <label class="checkbox-inline">
                      <input id="radioFemale" type="radio" value="Female" name="gender" class="radio-template" data-bind="checked: employeeViewModel.selectedGender"> Female
                  </label>

              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Designation</label>
              <div class="col-sm-8">
                  <input type="text" name="txtDesignation" id="txtDesignation" placeholder="Enter Designation" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Email Address</label>
              <div class="col-sm-8">
                <input type="text" name="txtEmailEmployee" id="txtEmailEmployee" placeholder="Enter Email Address" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Mobile Number</label>
              <div class="col-sm-8">
                  <input type="text" name="txtContactEmployee" id="txtContactEmployee" placeholder="Enter Mobile Number" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Birth Date</label>
              <div class="col-sm-8">
                  <input type="text" name="dateBirth" id="dateBirth" placeholder="Select Birth date" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Date of Joining</label>
              <div class="col-sm-8">
                  <input type="text" name="dateJoin" id="dateJoin" placeholder="Select Joining Date" class="mr-3 form-control">
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Status</label>
              <div class="col-sm-8">
                  <select class="form-control" name="txtStatus" data-bind="
                    value: employeeViewModel.selectedStatus,
                    options: employeeViewModel.availbleStatus,
                    optionsText: 'text',
                    optionsValue: 'value',
                    valueAllowUnset: true,
                    optionsCaption: 'Select Status'
                  ">
                  </select>
              </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 form-control-label">Upload Pic</label>
              <div class="col-sm-8">
                  <input id="fileInputEmployee" name="fileInputEmployee" type="file" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                    <!-- ko if :employeeViewModel.action() =="edit" -->
                        <img class="img-circle" data-bind="attr: { src: employeeViewModel.imagePathEmployee() + employeeViewModel.profile() }" height="100" width="100">

                    <!-- /ko -->
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4 offset-sm-4">
                <button type="button" data-bind="click:employeeViewModel.createEmployeeClick" class="btn btn-primary"><span data-bind="text:employeeViewModel.button_title"></span></button>
                    <button  type="button" data-bind="click:employeeViewModel.cancelEmployeeClick,text:'cancel'" class="btn btn-danger "></button>
            </div>
          </div>
    </form>
</div>
<div class="panel-body" data-bind="visible:employeeViewModel.importEmployeeContainer">
    <p>
        To add your employees, you can also use our CSV import functionality. Using CSV import you can add all your employees at a time.<br>
        <br>
        For this you just have to create a CSV file of your employees with required fields and you are good to go. <a href="{{URL::to('/')}}/uploads/csv/sample.csv" download>Click here</a> to download the sample CSV import file.<br>
        <br>  
    </p>
    <h3 class="h4">
        CSV Import
    </h3> 
    <form class="form-horizontal" id="form_import" enctype="multipart/form-data">
        <div class="form-group row">
            <label class="col-sm-4 form-control-label">Upload CSV File</label>
            <div class="col-sm-8">
                <input id="fileImport" name="fileImport" accept=".csv" type="file" class="form-control-file">
            </div>
        </div>
        <div class="form-group row">
              <div class="col-sm-4 offset-sm-4">
                  <button type="button" data-bind="click:employeeViewModel.importEmployeeClick" class="btn btn-primary">Import</button>
                  <button  type="button" data-bind="click:employeeViewModel.cancelEmployeeClick,text:'cancel'" class="btn btn-danger "></button>
              </div>
        </div>
    </form>
</div>