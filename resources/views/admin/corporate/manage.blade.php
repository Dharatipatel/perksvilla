 <!-- Corporate Add/update -->
<div class="col-lg-12" id="divCorporateAdd" data-bind="visible:corporateAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id="corporate_detailstab" href="#corporate_details">Corporate Detail</a></li>
                <li><a  id="employeetab" href="#employee">Employee</a></li>
                <li><a  id="order_historytab" href="#order_history">Order History</a></li>
            </ul>
            <div class="tab-content">
                <div id="corporate_details" class="tab-pane fade in active">
                    {!! Form::open(['id'=>'form_corporate', 'class'=>"form-horizontal","role"=>"form"]) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-4">
                            <input class="form-control" placeholder="Corporate Name" name="txtCorporateName" id="txtCorporateName" type="text" value="">
                        </div>
                        <!-- ko if :$root.action() =="edit" -->
                        <div class="col-sm-4">
                            <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                        </div>
                         <!-- /ko -->
                    </div>
                    <div class="form-group">
                        <label for="fileInput" class="col-sm-3 control-label">Logo</label>
                        <div class="col-sm-9">
                            <input id="fileInput" type="file" name="fileInput" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                        </div>
                    </div>        
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="txtCategory" id="txtCategory" data-bind="
                              value: selectedCategory,
                              options: availbleCategory,
                              optionsText: 'text',
                              optionsValue: 'value',
                              valueAllowUnset: true,
                              optionsCaption: 'Select Category'
                            ">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Company Size</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Company Size" name="txtCompany_size" id="txtCompany_size" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" rows="5" placeholder="Corporate Description" name="txtDescription" id="txtDescription" type="text" value=""></textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label">Contact Person</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Contact Person Name" name="txtPersonName" id="txtPersonName" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email Address</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Email" name="txtEmail" id="txtEmail" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Phone Number</label>
                        <div class="col-sm-8">
                            <input class="form-control number-only" placeholder="Mobile" name="txtContact" id="txtContact" type="text" value="" maxlength="10">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Password" name="txtPassword" id="txtPassword" type="password" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Website Url</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Website Url" name="txtWebsiteUrl" id="txtWebsiteUrl" type="text" value="">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Facebook Page</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Facebook Page" name="txtFacebookPage" id="txtFacebookPage" type="text" value="">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">LinkedIn Page</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Linked In Page" name="txtLinkedInPage" id="txtLinkedInPage" type="text" value="">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Company Address</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Address" name="txtAddress" id="txtAddress" type="text" value="">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Status</label>
                        <div class="col-sm-8">
                            <select class="form-control" data-bind="
                              value: selectedStatus,
                              options: availbleStatus,
                              optionsText: 'text',
                              optionsValue: 'value',
                              valueAllowUnset: true,
                              optionsCaption: 'Select Status'
                            ">
                            </select>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button id="" type="button" data-bind="click:createCorporateClick,text:action_name" class="btn btn-purple"></button>
                            <!--ko if: action() != "add"-->
                                <button  type="button" data-bind="click:cancelCorporateClick,text:'cancel'" class="btn btn-danger "></button>
                            <!--/ko-->
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div id="employee" class="tab-pane fade">
                    @include('admin.corporate.employee')
                </div>
                <div id="order_history" class="tab-pane fade">
                    @include('admin.corporate.orderhistorytab')
                </div>
            </div>
        </div>
    </div>
</div>