@extends('admin.master')
@section("title")
	{{ @$title. ' - '}}
@stop
@section("css")
@endsection
@section("content")
<div class="modal fade" id="viewModel" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Offer Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                          @include('popup')
                      </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divPaymentRequestList" data-bind="visible:orderHistoryListContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            <table id="payment_tbl" class="table table-striped table-bordered" data-bind="visible:orderHistoryList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Brand Name</th>
                        <th>Corporate Name</th>
                        <th>Employee Name</th>
                        <th>Offer Name</th>
                        <th>Paid Amount</th>
                        <th>Order Date</th>
                        <th>Trasaction ID</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: orderhistory() -->
                    <tr>
                        <td><span data-bind="text:($index()+1)"></span></td>
                        <td><span data-bind="text:$data.brand_name"></span></td>
                        <td><span data-bind="text:$data.corporate_name"></span></td>
                        <td><span data-bind="text:$data.employee_name"></span></td>
                        <td><a href="#" style="text-decoration:underline" data-bind="attr:{'data-id':$data.offer_id},click:$root.offerDetailClick,text:$data.offer_name"></a></td>
                        <td><span data-bind="text:$data.total_amount"></span></td>
                        <td><span data-bind="text:$data.purchase_date"></span></td>
                        <td><span data-bind="text:$data.transaction_id"></span></td>
                    </tr>
                    <!-- /ko -->
                </tbody>
            </table>
            <div data-bind="visible:orderHistoryNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var type = "{{ $type }}";
        var id = "{{ Session::get('id') }}";
        var token = "{{ Session::get('token') }}";
        var orderHistoryListApi = "{{config('constant.api.reward.orderhistory')}}";
        var offerDetailApi = "{{config('constant.api.offer.list')}}";
        var addressListApiPath ="{{config('constant.api.country.address')}}";
        var pathBrand ="{{URL::to('/')}}"+"/uploads/brand/";
    </script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.orderhistory')}}"></script>
@endsection