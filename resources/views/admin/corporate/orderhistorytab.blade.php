<div class="modal fade" id="viewModel" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Offer Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                          @include('popup')
                      </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divPaymentRequestList" data-bind="visible:orderHistoryListContainer">
            <table id="history_tbl" class="table table-striped table-bordered" data-bind="visible:orderHistoryList">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Brand Name</th>
                        <th>Corporate Name</th>
                        <th>Employee Name</th>
                        <th>Offer Name</th>
                        <th>Paid Amount</th>
                        <th>Order Date</th>
                        <th>Trasaction ID</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: orderhistory() -->
                    <tr>
                        <td><span data-bind="text:($index()+1)"></span></td>
                        <td><span data-bind="text:$data.brand_name"></span></td>
                        <td><span data-bind="text:$data.corporate_name"></span></td>
                        <td><span data-bind="text:$data.employee_name"></span></td>
                        <td><a href="#" style="text-decoration:underline" data-bind="attr:{'data-id':$data.offer_id},click:$root.offerDetailClick,text:$data.offer_name"></a></td>
                        <td><span data-bind="text:$data.total_amount"></span></td>
                        <td><span data-bind="text:$data.purchase_date"></span></td>
                        <td><span data-bind="text:$data.transaction_id"></span></td>
                    </tr>
                    <!-- /ko -->
                </tbody>
            </table>
            <div data-bind="visible:orderHistoryNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>
