<!-- Brand Listing -->

        <div class="col-lg-12" id="divBrandList" data-bind="visible:rewardListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                     <div>
                        <a href="#"  data-bind="click:activeRewardClick">Active</a> |  
                        <a href="#"  data-bind="click:usedRewardClick">Used</a></div>
                    </div>
                    <table id="reward_tbl" class="table table-striped table-bordered" data-bind="visible:rewardList">
                        <thead>
                            <tr>
                                  <th>#</th>
                                  <th>Employee Name</th>
                                  <th>Designation</th>
                                  <th>Reward Type</th>
                                  <th>Rewards Name</th>
                                  <th>Issue Date</th>
                                  <!-- ko if: status() == 1 -->
                                  <th>Redeem Date</th>
                                  <!-- /ko -->
                                  <!-- ko if: status() == 0 -->
                                  <th>Coupon Value</th>
                                  <!-- /ko -->
                            </tr>
                        </thead>
                        <tbody>
                            <!--ko foreach:rewards()-->
                            <tr>
                              <th scope="row"><span data-bind="text:($index()+1)"></span></th>
                              <td><span data-bind="text:first_name() +' '+ last_name()"></span></td>
                              <td><span data-bind="text:designation()"></span></td>
                              <td><span data-bind="text:category_name()"></span></td>
                              <td><a href=""><span data-bind="text:brand_name()"></span></a></td>
                              <td><span data-bind="text:issue_date()"></span></td>
                              <!-- ko if: $root.status() == 1 -->
                                <td><span data-bind="text:redeem_date()"></span></td>
                              <!-- /ko -->
                              <!-- ko if: $root.status() == 0 -->
                                <td><span data-bind="text:coupen_value()"></span></td>
                              <!-- /ko -->
                             </tr>
                             <!-- /ko-->
                            </tbody>
                    </table>
                    <div data-bind="visible:rewardNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>