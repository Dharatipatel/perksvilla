@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <!-- corporate Listing -->
        <div class="col-lg-12" id="divMailTemplateList" data-bind="visible:mailTemplateListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ @$title }}</h3> 
                </div> 
                <div class="panel-body">
                    <a href="{{ route('admin.mailtemplates.create')}}" class="btn btn-primary m-b-5" data-bind="click:$root.addMailTemplateClick">Add Mail Template</a>
                    <table id="Mailtemplate_list" class="table table-striped table-bordered" data-bind="visible:mailTemplateList">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Subject</th>
                                <th class="col-md-2">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>



    </div>
    @endsection
    @section('script')

    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript">
$('document').ready(function () {
    $('#Mailtemplate_list').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ route("admin.mailtemplates.list") }}',
        columns: [
            {data: 'title', name: 'title'},
            {data: 'subject', name: 'subject'},
            {data: 'action', name: 'action', "bSearchable": false},
        ],
    });
});

$(".has-submenu").removeClass("active");
$("#MailTemplateTab").addClass("active")
    </script>
    @endsection