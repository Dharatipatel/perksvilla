@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divMailTemplateAdd" data-bind="visible:mailTemplateAddContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title"><span data-bind="text:action_title"></span>{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    @if(isset($mail_template->id))
                    {!! Form::open(["url"=>route('admin.mailtemplates.update',['id'=>$mail_template->id]),"method"=>"POST", 'class'=>"form-horizontal","role"=>"form", 'id'=>'mailtemplate_form']) !!}
                    @else
                    {!! Form::open(["url"=>route('admin.mailtemplates.save'),"method"=>"POST", 'class'=>"form-horizontal","role"=>"form", 'id'=>'mailtemplate_form']) !!}
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Title" name="title" id="txtTitle" type="text" value="{{@$mail_template->title}}">
                            @if($errors->has('title'))
                            <label class="text-danger">{{ $errors->first('title') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Subject</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Subject" name="subject" id="txtSubject" type="text" value="{{@$mail_template->subject}}">
                            @if($errors->has('subject'))
                            <label class="text-danger">{{ $errors->first('subject') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Mail template Text</label>
                        <div class="col-sm-8">
                            {{ Form::textarea('text',@$mail_template->text,['class'=>'form-control','id'=> 'text', 'placeholder'=>'Mail template Text']) }} 
                            @if($errors->has('text'))
                            <label class="text-danger">{{ $errors->first('text') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tags</label>
                        <div class="col-sm-8">
                            <input class="form-control" placeholder="Tags" name="tags" id="txtTags" type="text" value="{{@$mail_template->tags}}">
                            @if($errors->has('tags'))
                            <label class="text-danger">{{ $errors->first('tags') }}</label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-b-0">
                        <div class="col-sm-offset-3 col-sm-8">
                            <button id="" type="submit" data-bind="click:createMailTemplateClick,text:action_name" class="btn btn-purple">Save</button>
                            <a href="{{ route('admin.mailtemplates.index') }}" class="btn btn-danger">Cancel</a>
                            @if(isset($mail_template->id))
                            <a href="{{ route('admin.mailtemplates.preview', $mail_template->id) }}" target="_blank" class="btn btn-primary">Preview</a>
                            @endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
    @endsection
    @section('script')
    <script type="text/javascript" src="{{config('constant.script.tinymce.js')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.validate.js')}}"></script>
    <script>
tinymce.init({
    selector: 'textarea',

    height: 300,
    theme: 'modern',
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',
    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample code',
    relative_urls: false,
    remove_script_host: false,
    convert_urls: true,
    content_css: [
        '//www.tinymce.com/css/codepen.min.css'
    ]
});

$("#mailtemplate_form").validate({
    rules: {
        title: {
            required: true,
        },
        subject: {
            required: true,
        },
        text: {
            required: true,
        },
        tags: {
            required: true,
        },
    },
    messages: {
        title: {
            required: "Please enter title",
        },
        subject: {
            required: "Please enter subject",
        },
        text: {
            required: "Please enter subject",
        },
        tags: {
            required: "Please enter subject",
        },
    }
});

$(".has-submenu").removeClass("active");
$("#MailTemplateTab").addClass("active")
    </script>
    @stop