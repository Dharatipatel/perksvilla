<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="title" content="{{env("PROJECT_NAME")}}">
        <meta name="author" content="{{env("PROJECT_NAME")}}">
        <title>@yield("title"){{env("PROJECT_NAME")}}</title>
        
        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>

        <!-- Bootstrap core CSS -->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.bootstrap')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.bootstrap-reset')}}" />

        <!--Animation css-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.animate')}}" />

        <!--Icon-fonts css-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.ionicons')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.fontawesome')}}" />

        <!-- Custom styles for this template -->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.style')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.styleresponsive')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.helper')}}" />

        <!-- Tostr Notification-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.toastr')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.sweetalert')}}" />
    </head>
    <body  class="login-bg">
        <div class="wrapper-page animated fadeInDown" id="divLogin">
            <div class="panel panel-color panel-primary">
            <div class="panel-heading"> 
               <h3 class="text-center m-t-10"> Sign In to <strong>PerksVilla</strong> </h3>
            </div> 
            <form id="login_form" class="form-horizontal m-t-40">
                <div class="form-group" >
                    <div class="col-xs-12" data-bind="visible:isError">
                        <h3 class="error" data-bind="text:errorMsg"></h3>
                    </div>
                </div>        
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" name="txtEmail" id="txtEmail" type="text" placeholder="Username">
                    </div>
                </div>
                <div class="form-group ">
                    
                    <div class="col-xs-12">
                        <input class="form-control" name="txtPassword" id="txtPassword" type="password" placeholder="Password">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label class="cr-styled">
                            <input type="checkbox" name="rememberme" value="1" id="txtRemember" checked="checked">
                            <i class="fa"></i> 
                            Remember me
                        </label>
                    </div>
                </div>
                
                <div class="form-group text-right">
                    <div class="col-xs-12">
                        <button class="btn btn-purple w-md" id="btnLogin" data-bind = "click: loginClick" type="button">Log In</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        <script>
            var baseURL         = "{{url('/').'/'}}";
            var _token          = "{{csrf_token()}}";
            var loginApiPath    = "{{config('constant.api.login')}}";
            var setSessionPath  = "{{config('constant.web.setSession')}}";
            var landingPage    = " {{route('admindashboard')}}";
        </script>
        <script type="text/javascript" src="{{config('admin_constant.script.jquery')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.pace')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.wow')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalert')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalertinit')}}"></script> 
        <script type="text/javascript" src="{{config('admin_constant.script.nicescroll')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.app')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.toastr')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.validate')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.login')}}"></script>
    </body>
</html>