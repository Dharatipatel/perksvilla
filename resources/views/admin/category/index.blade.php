<div class="col-lg-12" id="divCategoryList" data-bind="visible:categoryListContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            @if($type==3)
            <ul class="nav nav-tabs">
                <li class="active">
                    <a data-toggle="tab" id="generaltab" href="#general_category">
                        General
                    </a>
                </li>
                <li><a data-toggle="tab" id="user_createdtab" href="#user_created">User Created</a></li>
            </ul>
            @endif
            
            <div class="tab-content">
                <div id="general_category" class="tab-pane fade in active">
                    <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addCategoryClick">Add Category</a>
                    <table id="category_tbl" class="table table-striped table-bordered" data-bind="visible:categoryList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: categories() -->
                            <!-- ko if: $data.userName() == null -->
                              <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                <td><span data-bind="text: $data.statusText()"></span></td>
                                <td><span data-bind="text: $data.description()"></span></td>
                                <td class="text-center">
                                    <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">    <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">      <i class="fa fa-toggle-on" aria-hidden="true"></i>   
                                    </a>
                                     <!-- /ko -->
                                </td>
                                <td class="text-center">
                                    <a href="#" data-bind="click:$root.editCategoryClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteCategoryClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                    
                                </td>
                                
                              </tr>
                            <!-- /ko -->
                            <!-- /ko -->
                            </tbody>
                    </table>
                </div>
                @if($type==3)
                <div id="user_created" class="tab-pane fade">
                    <table id="user_created_tbl" class="table table-striped table-bordered" data-bind="visible:categoryList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Created By</th>
                                <th>Status</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: categories() -->
                            <!-- ko if: $data.userName() != null -->
                              <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                <td><span data-bind="text: $data.user_name"></span></td>
                                <td><span data-bind="text: $data.statusText()"></span></td>
                                <td><span data-bind="text: $data.description()"></span></td>
                                <td class="text-center">
                                    <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">    <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">      <i class="fa fa-toggle-on" aria-hidden="true"></i>   
                                    </a>
                                     <!-- /ko -->
                                </td>
                                <td class="text-center">
                                    <a href="#" data-bind="click:$root.editCategoryClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteCategoryClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                    
                                </td>
                                
                              </tr>
                              <!-- /ko -->
                            <!-- /ko -->
                            </tbody>
                    </table>
                </div>
                @endif
            </div>
            <div data-bind="visible:categoryNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>