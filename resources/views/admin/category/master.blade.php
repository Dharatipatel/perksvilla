@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.category.index')
        @include('admin.category.manage')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                Are you sure to delete Category ?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        var id = "{{ Session::get('id')}}";
        var token = "{{ Session::get('token')}}";
        var type = "{{ $type }}";
        var brandcategoryPath = "{{config('constant.web.category.brand')}}";
        var employeecategoryPath = "{{config('constant.web.category.corporate')}}";
        var rewardcategoryPath = "{{config('constant.web.category.reward')}}";
        var addCategoryPath = "{{config('constant.api.category.add')}}";
        var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
        var deleteCategoriesApiPath = "{{config('constant.api.category.delete')}}";
        var updateCategoryPath = "{{config('constant.api.category.update')}}";
        var activeCategoryApiPath = "{{config('constant.api.category.active')}}";
        var deactiveCategoryApiPath = "{{config('constant.api.category.deactive')}}";
        
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.validate')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.category')}}"></script>
@endsection