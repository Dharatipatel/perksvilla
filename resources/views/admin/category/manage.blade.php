<div class="col-lg-12" id="divCategoryAdd" data-bind="visible:categoryAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>  {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_category', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="Category Name" name="name" id="txtCategoryName" type="text" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="Category Description" name="txtDescription" id="txtDescription" type="text" value="">
                </div>
            </div>  
            <div class="form-group">
                <label class="col-sm-3 control-label">Status</label>
                <div class="col-sm-8">
                    <select class="form-control" name="status" data-bind="
                      value: selectedStatus,
                      options: availbleStatus,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select Status'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="payment_button" type="button" data-bind="click:createCategoryClick,text:action_name" class="btn btn-purple"></button>
                    <!--ko if: action() != "add"-->
                    <button id="payment_button" type="button" data-bind="click:cancelCategoryClick,text:'Cancel'" class="btn btn-danger"></button>
                    <!--/ko-->
                </div>
            </div>
            {!! Form::close() !!}
        </div> <!-- panel-body -->
    </div>
</div>
