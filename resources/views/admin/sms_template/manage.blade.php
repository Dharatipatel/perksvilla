 <!-- SMSTemplate Add/update -->
<div class="col-lg-12" id="divSMSTemplateAdd" data-bind="visible:sms_templateAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_sms_template', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="SMS Template Name" name="txtName" id="txtName" type="text" value="">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-3 control-label">Message</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="5" placeholder="Message" name="txtMessage" id="txtMessage" ></textarea>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="" type="button" data-bind="click:createSMSTemplateClick,text:action_name" class="btn btn-purple"></button>
                    <!--ko if: action() != "add"-->
                    <button id="" type="button" data-bind="click:cancelSMSTemplateClick,text:'cancel'" class="btn btn-danger"></button>
                    <!--/ko-->
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>