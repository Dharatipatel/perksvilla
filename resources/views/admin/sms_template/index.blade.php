<!-- SMSTemplate Listing -->
<div class="col-lg-12" id="divSMSTemplateList" data-bind="visible:sms_templateListContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            <!-- <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addSMSTemplateClick">Add SMS Template</a> -->
            <table id="sms_template_tbl" class="table table-striped table-bordered" data-bind="visible:sms_templateList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Template</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: sms_templates() -->
                    <tr>
                        <td><span data-bind="text: ($index()+1)"></span></td>
                        <td><span data-bind="text: $data.title"></span></td>
                        <td><span data-bind="text: $data.text"></span></td>
                        <td>
                            <a href="#" data-bind="click:$root.editSMSTemplateClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                            <a href="#" data-bind="click:$root.deleteSMSTemplateClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    <!-- /ko -->
                </tbody>
            </table>
            <div data-bind="visible:sms_templateNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>