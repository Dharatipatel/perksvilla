@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.sms_template.index')
        
        @include('admin.sms_template.manage')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Delete SMS Template</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Are you sure to delete SMS Template ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var type = "{{ $type }}";
    var sms_templateListApiPath ="{{config('constant.api.sms_template.list')}}";
    var sms_templateListWebPath ="{{route('sms.templates')}}";
    var addSMSTemplateApiPath ="{{config('constant.api.sms_template.add')}}";
    var updateSMSTemplateApiPath ="{{config('constant.api.sms_template.update')}}";
    var deleteSMSTemplateApiPath ="{{config('constant.api.sms_template.delete')}}";
    var activeSMSTemplateApiPath ="{{config('constant.api.sms_template.active')}}";
    var deactiveSMSTemplateApiPath ="{{config('constant.api.sms_template.deactive')}}";
    
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="{{config('constant.script.sms_template')}}"></script>
@endsection