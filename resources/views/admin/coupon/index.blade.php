<!-- Brand Listing -->
        <div class="modal fade" id="viewModel" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Offer Detail</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                          @include('popup')
                      </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
        <div class="col-lg-12" id="divCouponList" data-bind="visible:couponListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <table id="coupon_tbl" class="table table-striped table-bordered" data-bind="visible:couponList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Employee Name</th>
                                <th>Offer Name</th>
                                <th>Brand Name</th>
                                <th>Corporate Name</th>
                                <!--ko if:couponViewModel.status() != 1  -->
                                    <th>Coupon Value</th>
                                <!-- /ko -->
                                <th>Purchase Price</th>
                                <!--ko if:couponViewModel.status() != 1  -->
                                    <th>Issue Date</th>
                                <!-- /ko -->
                                <!--ko if:couponViewModel.status() == 0  -->
                                    <th>Expire Date</th>
                                <!-- /ko -->
                                <!--ko if:couponViewModel.status() == 2  -->
                                    <th>Expired On</th>
                                <!-- /ko -->
                                <!--ko if:couponViewModel.status() == 1  -->
                                    <th>Redeem Date</th>
                                    <th>Coupon Code</th>
                                    <th>Final Bill</th>
                                <!-- /ko -->
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: coupons() -->
                              <tr>
                              <td><span data-bind="text: ($index()+1)"></span></td>
                              <td><span data-bind="text: $data.employee_name"></td>
                              <td><a href="" data-bind="click:$root.offerViewClick,text: $data.offer_name" style="text-decoration: underline;"></a></td>
                              <td><span data-bind="text: $data.brand_name"></td>
                              <td><span data-bind="text: $data.corporate_name"></td>
                              <!--ko if:couponViewModel.status() != 1  -->
                                  <td><span data-bind="text: 'Rs ' +$data.coupen_value()"></td>
                              <!-- /ko -->
                              <td><span data-bind="text: 'Rs ' +$data.buy_price()"></td>
                              <!--ko if:couponViewModel.status() != 1  -->
                                  <td><span data-bind="text: $data.issue_date"></td>
                                  <td><span data-bind="text: $data.expiry_date"></td>
                              <!-- /ko -->
                              <!--ko if:couponViewModel.status() == 1  -->
                                  <td><span data-bind="text: $data.redeem_date"></td>
                                  <td><span data-bind="text: $data.coupon_code"></td>
                                  <td><span data-bind="text:'Rs ' +$data.bill_amount()"></td>
                              <!-- /ko -->
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:couponNoData" class="text-center"><h1>No Data Available</h1></div>
                    
                </div>
            </div>
        </div>
