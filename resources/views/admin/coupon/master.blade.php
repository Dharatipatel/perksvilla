@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
    	
        @include('admin.coupon.index')
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var type = "{{ $type }}";
    var path ="{{URL::to('/')}}"+"/uploads/brand/";
    var couponListApiPath ="{{config('constant.api.coupon.list')}}";
    var offerDetailApi = "{{config('constant.api.offer.list')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.coupon')}}"></script>
@endsection