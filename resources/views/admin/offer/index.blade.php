<!-- Brand Listing -->
        <div class="col-lg-12" id="divOfferList" data-bind="visible:offerListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <!-- <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addBrandClick">Add Brand</a> -->
                    <table id="offer_tbl" class="table table-striped table-bordered" data-bind="visible:offerList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Offer Name</th>
                                <th>Contact</th>
                                <th>Offer Value</th>
                                <th>Buy Price</th>
                                <th>Publish Date</th>
                                <th>End Date</th>
                                <th>Total</th>
                                <th>Brand Name</th>
                                <!--ko if:offerViewModel.status() ==1 || offerViewModel.status() ==4 || offerViewModel.status() == 6  -->
                                  <th>Issued</th>
                                  <th>Redeem</th>
                                <!--/ko-->
                                <!--ko if:offerViewModel.status() ==1 || offerViewModel.status() ==2 || offerViewModel.status() == 3  -->
                                  <th>status</th>
                                 <!--/ko-->
                                <!--ko if:offerViewModel.status() ==3  -->
                                <th>Action</th>
                                <!--/ko-->
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: offers() -->
                              <tr>
                              <td><span data-bind="text: ($index()+1)"></span></td>
                              <td><span data-bind="text: $data.name"></td>
                              <td><span data-bind="text: $data.contact"></td>
                              <td><span data-bind="text: $data.value"></td>
                              <td><span data-bind="text: $data.price"></td>
                              <td><span data-bind="text: $data.publish_date"></td>
                              <td><span data-bind="text: $data.valid_to"></td>
                              <td><span data-bind="text: $data.count"></td>
                              <td><span data-bind="text: $data.brand_name"></td>
                              <!--ko if:offerViewModel.status() ==1 || offerViewModel.status() ==4 || offerViewModel.status() == 6  -->
                                <td><span data-bind="text: $data.issued_count"></td>
                                <td><span data-bind="text: $data.redeem_count"></td>
                              <!--/ko-->
                              <!--ko if:offerViewModel.status() ==1 || offerViewModel.status() ==2 || offerViewModel.status() == 3  -->
                              <td>
                                  <!-- ko if: $data.status() == 3 -->
                                  <a href="" data-bind="click:$root.approveClick" class="on-default remove-row">
                                    <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                  </a>
                                  <!-- /ko -->
                                  <!-- ko if: $data.status() == 1 || $data.status() == 2 -->
                                  <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">
                                    <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                  </a>
                                  <!-- /ko -->
                              </td>
                              <!--/ko-->
                              <!--ko if:offerViewModel.status() ==3  -->
                              <td>
                                <a href="#" data-bind="click:$root.editOfferClick"><i class="text-danger fa fa-pencil"></i></a>
                              </td>
                              <!--/ko-->
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:offerNoData" class="text-center"><h1>No Data Available</h1></div>
                    
                </div>
            </div>
        </div>
        <div class="forms" class="col-lg-12" data-bind="visible:editContainer">
          <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Edit Offer</h3> 
                </div> 
            <div class="panel-body"> 
                @include('brand.offer.step2')
            </div>
          </div>
        </div> 