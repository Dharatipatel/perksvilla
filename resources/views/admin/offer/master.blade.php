@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <style type="text/css">
        .address_checkbox{
            float: left;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.offer.index')
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var brand_id = "{{ $brand }}";
    var type = "{{ $type }}";
    var offerListApiPath ="{{config('constant.api.offer.list')}}";
    var addressListApiPath ="{{config('constant.api.country.address')}}";
    var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
    var offerPendingWebPath ="{{route('offerpending')}}";
    var offerActiveWebPath ="{{route('offeractive')}}";
    var offerRejectWebPath ="{{route('offerreject')}}";
    var offerUpcomingWebPath ="{{route('offerupcoming')}}";
    var offerPastWebPath ="{{route('offerpast')}}";
    var offerApproveApiPath ="{{config('constant.api.offer.approve')}}";
    var offerDeactiveApiPath ="{{config('constant.api.offer.deactive')}}";
    var offerRejectApiPath ="{{config('constant.api.offer.reject')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{config('constant.script.offer')}}"></script>
@endsection