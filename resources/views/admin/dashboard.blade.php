@extends('admin.master')
@section('content')
	<div class="wraper container-fluid">
		<div class="row">
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-pink"><i class="fa fa-tag"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('offerpending')}}"><span class="" data-bind="text:pending_offers"></span></a>
                        <span style="font-size: 19px;">Pending Offers</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-success"><i class="fa fa-tag"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('offeractive')}}"><span class="" data-bind="text:active_offers"></span></a>
                         <span style="font-size: 19px;">Active Offers</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-warning"><i class="fa fa-tag"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('offerupcoming')}}"><span class="" data-bind="text:upcoming_offers"></span></a>
                         <span style="font-size: 19px;">Upcoming Offers</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-success"><i class="fa fa-shopping-cart"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('orderhistorycorporate')}}"><span class="" data-bind="text:today_order"></span></a>
                        <span style="font-size: 19px;">Today's Order</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-warning"><i class="fa fa-gift"></i></span>
                    <div class="mini-stat-info text-right">
                        <span class="" data-bind="text:today_redeem"></span>
                        <span style="font-size: 19px;">Today's Redeem</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-pink"><i class="fa fa-usd"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('orderhistorycorporate')}}"><span class="" data-bind="text:total_sales"></span></a>
                        <span style="font-size: 19px;">Total Sales</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-success"><i class="fa fa-credit-card"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('brandpayment')}}"><span class="" data-bind="text:payment_request"></span></a>
                        <span style="font-size: 19px;">Payment Request</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-pink"><i class="fa fa-ticket"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('support.open')}}"><span class=""  data-bind="text:open_tickets"></span></a>
                        <span style="font-size: 19px;">Open Tickets</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-success"><i class="fa fa-ticket"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('support.closed')}}"><span class="" data-bind="text:closed_tickets"></span></a>
                        <span style="font-size: 19px;">Closed Tickets</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-info"><i class="ion-flask"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('brandlist')}}"><span class="" data-bind="text:active_brand"></span></a>
                        <span style="font-size: 19px;">Active Brands</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-warning"><i class="fa fa-building-o"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('corporatelist')}}"><span class="" data-bind="text:active_corporate"></span></a>
                        <span style="font-size: 19px;">Active Corporates</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="mini-stat clearfix">
                    <span class="mini-stat-icon bg-pink"><i class="ion-person"></i></span>
                    <div class="mini-stat-info text-right">
                        <a href="{{route('employeeadmin')}}"><span class="" data-bind="text:active_employee"></span></a>
                        <span style="font-size: 19px;">Active Employees</span>
                    </div>
                </div>
            </div>
        </div>
	</div>
	@section('script')
		<script type="text/javascript">
			var id = "{{ Session::get('id')}}";
			var token = "{{ Session::get('token')}}";
            var statisticsApi = "{{config('constant.api.admin.statistics')}}";
        </script>
		<script type="text/javascript" src="{{config('constant.script.dashboard')}}"></script>
	@endsection
@endsection