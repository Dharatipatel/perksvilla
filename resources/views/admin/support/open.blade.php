@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <!-- corporate Listing -->
        <div class="col-lg-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ @$title }}</h3> 
                </div> 
                <div class="panel-body">
                    <table id="list" class="table table-striped table-bordered">
                        <thead>
                            <th>Comapany</th>
                            <th>Type</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Date Time</th>
                            <th>Action</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>



    </div>
    @endsection
    @section('script')

    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript">
$('document').ready(function () {
    $('#list').DataTable({
        processing: true,
        serverSide: true,
        order: [4, "desc"],
        ajax: '{{ route("support.lists",["1"]) }}',
        columns: [
            {data: 'company_name', name: 'company_name'},
            {data: 'type', name: 'type'},
            {data: 'title', name: 'support.title'},
            {data: 'category_name', name: 'support_category.name'},
            {data: 'status_name', name: 'support_status.name'},
            {data: 'created_at', name: 'support.created_at'},
            {data: 'action', name: 'action', "bSearchable": false, sortable: false}
        ]
    });
});

$(".has-submenu").removeClass("active");
$("#supportTab").addClass("active");
$("#openSupportTab").addClass("active");
    </script>
    @endsection