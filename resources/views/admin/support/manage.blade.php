@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <style type="text/css">
        .mailbox-read-message {
            padding: 10px;
            border: 1px solid #3bc0c3;
            border-radius: 4px;
            margin: 5px;
        }
    </style>
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="box box-primary">

                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <div class="mailbox-read-info">
                                    <h3><b><u>{{@$support->title}}</b></u></h3>
                                    <h5>From: {{@$support->email}}
                                        <span class="mailbox-read-time pull-right">{{@$support->created_at->format('d-m-Y g:i:s A')}}</span></h5>
                                </div>
                                <!-- /.mailbox-read-info -->

                                <!-- /.mailbox-controls -->
                                @foreach(@$support_reply as $s)
                                <div class="mailbox-read-message">
                                    @if($s->role_id == 2)
                                        <h3>{{$s->brand_name}}</h3>                                    
                                    @elseif($s->role_id == 3)
                                        <h3>{{$s->corporate_name}}</h3>
                                    @else
                                        <h3>Admin <span style="font-size: 14px;">({{$s->email}})</span></h3>                                                                        
                                    @endif
                                    <h5><span class="mailbox-read-time pull-right">{{$s->created_at->format('d-m-Y g:i:s A')}}</span></h5>
                                    <p>{!!html_entity_decode($s->comment)!!}</p>
                                </div>
                                @endforeach
                                <!-- /.mailbox-read-message -->
                            </div>

                        </div>
                        <!-- /. box -->
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Leave a reply</h3>
                            </div>
                            <!-- /.box-header -->

                            <div class="box-body">
                                {!! Form::open(["url"=>route("support.reply.save"),"method"=>"POST", 'id'=>'form']) !!}
                                {!! Form::hidden("id", $support->id)    !!}
                                <div class="form-group">
                                    <label>Status:</label>
                                    {!! Form::select("status", $support_status, $support->status, ["class"=>"form-control"])!!}
                                    @if($errors->has("status"))
                                    <span class="text-danger flash">{{ $errors->first("status") }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    {!! Form::textarea("comment", '', ["class"=>"form-control wysihtml5", "placeholder"=>"Reply here..."])!!}
                                    @if($errors->has("comment"))
                                    <span class="text-danger flash">{{ $errors->first("comment") }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>   
                                    <a href="{{route("support.open")}}" class="btn btn-default"><i class="fa fa-reply"></i> Back</a>
                                </div>
                                {!! Form::close()   !!}
                            </div>

                        </div>
                        <!-- /. box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(".has-submenu").removeClass("active");
    $("#supportTab").addClass("active");
</script>
@stop