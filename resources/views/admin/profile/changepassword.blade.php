@extends('admin.master')
@section('content')
<div class="wraper container-fluid">
    <div class="row">
<div class="col-lg-12" id="" data-bind="">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_changepassword', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">Old Password</label>
                <div class="col-sm-8">
                    <input type="password" name="txtOldPassword" id="txtOldPassword" class="form-control" placeholder="Enter Old Password">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">New Password</label>
                <div class="col-sm-8">
                    <input type="password" name="txtNewPassword" id="txtNewPassword" placeholder="Enter New Password" class="form-control">
                </div>
            </div> 
            <div class="form-group">
                <label class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-8">
                    <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" placeholder="Enter New Password Again" class="form-control">
                </div>
            </div>  
        
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="payment_button" type="button" data-bind="click:$root.changePasswordClick" class="btn btn-primary">Change Password</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div> <!-- panel-body -->
    </div>
</div>    
</div>
</div>  
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var token = "{{ Session::get('token')}}";
          var changepasswordApiPath = "{{ config('brand_constant.api.profile.changepassword')}}";
        </script>
        <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.profile.changepassword')}}"></script>
    @endsection  
@endsection