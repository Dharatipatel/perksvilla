<!-- corporate Listing -->
        <div class="col-lg-12" id="divCountryList" data-bind="visible:countryListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addCountryClick">Add Country</a>
                    <table id="country_tbl" class="table table-striped table-bordered" data-bind="visible:countryList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: countries() -->
                              <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                
                                <td class="text-center">
                                     <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row"> <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>
                                    </a>
                                     <!-- /ko -->
                                </td>
                                <td>
                                    <a href="#" data-bind="click:$root.editCountryClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteCountryClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                   
                                </td>
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:countryNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>