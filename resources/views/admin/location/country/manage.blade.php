 <!-- Corporate Add/update -->
<div class="col-lg-12" id="divCountryAdd" data-bind="visible:countryAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_country', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="Country Name" name="txtCountryName" id="txtCountryName" type="text" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Status</label>
                <div class="col-sm-8">
                    <select class="form-control" name="txtStatus" data-bind="
                      value: selectedStatus,
                      options: availbleStatus,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select Status'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="" type="button" data-bind="click:createCountryClick,text:action_name" class="btn btn-purple"></button>
                    <!--ko if: action() != "add"-->
                    <button id="" type="button" data-bind="click:cancelCountryClick,text:'Cancel'" class="btn btn-danger"></button>
                    <!--/ko-->
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>