 <!-- Corporate Add/update -->
<div class="col-lg-12" id="divCityAdd" data-bind="visible:cityAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_city', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">City Name</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="City Name" name="txtCityName" id="txtCityName" type="text" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">State</label>
                <div class="col-sm-8">
                    <select class="form-control" name="txtState" data-bind="
                      value: selectedState,
                      options: availbleStates,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select State Name'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Status</label>
                <div class="col-sm-8">
                    <select class="form-control" data-bind="
                      value: selectedStatus,
                      options: availbleStatus,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select Status'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="" type="button" data-bind="click:createCityClick,text:action_name" class="btn btn-purple"></button>
                     <!--ko if: action() != "add"-->
                    <button id="" type="button" data-bind="click:cancelCityClick,text:'Cancel'" class="btn btn-danger"></button>
                    <!--/ko-->
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>