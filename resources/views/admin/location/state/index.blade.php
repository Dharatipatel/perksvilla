<!-- corporate Listing -->
        <div class="col-lg-12" id="divStateList" data-bind="visible:stateListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="panel-body">
                    <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addStateClick">Add State</a>
                    <table id="state_tbl" class="table table-striped table-bordered" data-bind="visible:stateList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>State Name</th>
                                <th>Country</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: states() -->
                              <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                <td><span data-bind="text: $data.country_name"></span></td>
                                <!-- <td><span data-bind="text: $data.statusText()"></span></td> -->
                                
                                <td class="text-center">
                                    <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row"><i class="fa fa-toggle-off" aria-hidden="true"></i>
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row"><i class="fa fa-toggle-on" aria-hidden="true"></i>
                                    </a>
                                     <!-- /ko -->
                                </td>
                                <td>
                                    <a href="#" data-bind="click:$root.editStateClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteStateClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                   
                                </td>
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:stateNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>