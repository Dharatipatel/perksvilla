 <!-- Corporate Add/update -->
<div class="col-lg-12" id="divStateAdd" data-bind="visible:stateAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            {!! Form::open(['id'=>'form_state', 'class'=>"form-horizontal","role"=>"form"]) !!}
            <div class="form-group">
                <label class="col-sm-3 control-label">State Name</label>
                <div class="col-sm-8">
                    <input class="form-control" placeholder="State Name" name="txtStateName" id="txtStateName" type="text" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Country</label>
                <div class="col-sm-8">
                    <select class="form-control" name="txtCountry" data-bind="
                      value: selectedCountry,
                      options: availbleCountries,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select Country'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Status</label>
                <div class="col-sm-8">
                    <select class="form-control" name="txtStatus" data-bind="
                      value: selectedStatus,
                      options: availbleStatus,
                      optionsText: 'text',
                      optionsValue: 'value',
                      valueAllowUnset: true,
                      optionsCaption: 'Select Status'
                    ">
                    </select>
                </div>
            </div>
            <div class="form-group m-b-0">
                <div class="col-sm-offset-3 col-sm-8">
                    <button id="" type="button" data-bind="click:createStateClick,text:action_name" class="btn btn-purple"></button>
                    <!--ko if: action() != "add"-->
                    <button id="" type="button" data-bind="click:cancelStateClick,text:'Cancel'" class="btn btn-purple"></button>
                    <!--/ko-->
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>