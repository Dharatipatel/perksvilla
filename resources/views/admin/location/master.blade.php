@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @if($type=='country')
        @include('admin.location.country.index')
        
        @include('admin.location.country.manage')
        
        @endif
        @if($type=='state')
            @include('admin.location.state.index')
        
            @include('admin.location.state.manage')
        @endif
        @if($type=='city')
            @include('admin.location.city.index')
        
            @include('admin.location.city.manage')
        @endif
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle" data-bind="text:deleteTitle"></h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" data-bind="text:deleteContent">
                   
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var countryListApiPath ="{{config('constant.api.country.list')}}";
    var addCountryApiPath ="{{config('constant.api.country.add')}}";
    var updateCountryApiPath ="{{config('constant.api.country.update')}}";
    var activeCountryApiPath ="{{config('constant.api.country.active')}}";
    var deactiveCountryApiPath ="{{config('constant.api.country.deactive')}}";
    var deleteCountryApiPath ="{{config('constant.api.country.delete')}}";
    var stateListApiPath ="{{config('constant.api.state.list')}}";
    var addStateApiPath ="{{config('constant.api.state.add')}}";
    var updateStateApiPath ="{{config('constant.api.state.update')}}";
    var deleteStateApiPath ="{{config('constant.api.state.delete')}}";
    var activeStateApiPath ="{{config('constant.api.state.active')}}";
    var deactiveStateApiPath ="{{config('constant.api.state.deactive')}}";
    var cityListApiPath ="{{config('constant.api.city.list')}}";
    var addCityApiPath ="{{config('constant.api.city.add')}}";
    var updateCityApiPath ="{{config('constant.api.city.update')}}";
    var deleteCityApiPath ="{{config('constant.api.city.delete')}}";
    var activeCityApiPath ="{{config('constant.api.city.active')}}";
    var deactiveCityApiPath ="{{config('constant.api.city.deactive')}}";
    var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
    var type = "{{$type}}";
    var country = "{{route('country')}}";
    var state = "{{route('state')}}";
    var city = "{{route('city')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="{{config('constant.script.location')}}"></script>
@endsection