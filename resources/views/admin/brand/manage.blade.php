 <!-- Brand Add/update -->
<div class="col-lg-12" id="divBrandAdd" data-bind="visible:brandAddContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title"><span data-bind="text:action_title"></span>     {{ $title }}</h3> 
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" id="brand_detailstab" href="#brand_details">Brand Detail</a></li>
                <li><a  id="locationtab" href="#location">Location</a></li>
                <li><a  id="offerstab" href="#offers">Offers</a></li>
                <li><a  id="bank_detailstab" href="#bank_details">Bank Details</a></li>
                <li><a  id="payment_requesttab" href="#payment_request">Payment Request</a></li>
                <li><a  id="total_salestab" href="#total_sales">Total Sales</a></li>
            </ul>
            <div class="tab-content">
                <div id="brand_details" class="tab-pane fade in active">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'form_brand', 'class'=>"form-horizontal","role"=>"form"]) !!}
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-4">
                                <input class="form-control" placeholder="Brand Name" name="txtBrandName" id="txtBrandName" type="text" value="">
                            </div>
                            <!-- ko if :$root.action() =="edit" -->
                            <div class="col-sm-4">
                                <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                            </div>
                             <!-- /ko -->
                        </div>
                        <div class="form-group">
                            <label for="fileInput" class="col-sm-3 control-label">Logo</label>
                            <div class="col-sm-9">
                                <input id="fileInput" type="file" name="fileInput" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Category</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="txtCategory" id="txtCategory" data-bind="
                                  value: selectedCategory,
                                  options: availbleCategory,
                                  optionsText: 'text',
                                  optionsValue: 'value',
                                  valueAllowUnset: true,
                                  optionsCaption: 'Select Category'
                                ">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" rows="5" placeholder="Brand Description" name="txtDescription" id="txtDescription" type="text" value=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Email" name="txtEmail" id="txtEmail" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile</label>
                            <div class="col-sm-8">
                                <input class="form-control number-only" placeholder="Mobile" name="txtContact" id="txtContact" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Password" name="txtPassword" id="txtPassword" type="password" value="" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Business Hours</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" placeholder="Business Hours" name="txtBusinessHours" id="txtBusinessHours" type="text" value=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Contact Person Name</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Contact Person Name" name="txtPersonName" id="txtPersonName" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Website Url</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Website Url" name="txtWebsiteUrl" id="txtWebsiteUrl" type="text" value="">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Facebook Page</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Facebook Page" name="txtFacebookPage" id="txtFacebookPage" type="text" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Commision(%)</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Commision" name="txtCommision" id="txtCommision" type="number" min="0" max="100">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="txtStatus" data-bind="
                                  value: selectedStatus,
                                  options: availbleStatus,
                                  optionsText: 'text',
                                  optionsValue: 'value',
                                  valueAllowUnset: true,
                                  optionsCaption: 'Select Status'
                                ">
                                </select>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-offset-3 col-sm-8">
                                <button id="createBrand" type="button" data-bind="click:createBrandClick,text:action_name" class="btn btn-purple"></button>
                                <!--ko if: action() != "add"-->
                                    <button id="" type="button" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger"></button>
                                <!--/ko-->
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div id="location" class="tab-pane fade">
                    <div class="panel-body">
                        {!! Form::open(['id'=>'form_location','class'=>"form-horizontal","role"=>"form"]) !!}
                        <div data-bind="foreach: { data: fields, as: 'field' }">
                            <div data-bind="attr: { id: 'addressDiv'+$index()},foreach: { data: items, as: 'item' }">
                                <div class="form-group">
                                      <div class="col-sm-3">
                                          <input class="form-control" placeholder="Address" 
                                          data-bind="value:item.address,attr: { id:'txtAddress'+$parentContext.$index() ,name: 'txtAddress'+$parentContext.$index()}" type="text" >
                                          <input class="form-control" placeholder="Address" 
                                          data-bind="value:item.id,attr: { id:'txtId'+$parentContext.$index() ,name: 'txtId'+$parentContext.$index()}" type="hidden" >
                                      </div>
                                      <div class="col-sm-2">
                                          <input class="form-control pincode" placeholder="Pincode" data-bind="value:item.pincode,attr: { id:'txtPincode'+$parentContext.$index() ,name: 'txtPincode'+$parentContext.$index()}" type="text" value="">
                                      </div>
                                      <div class="col-sm-2">
                                          <select class="form-control country" data-bind="
                                            value: item.country,
                                            options: $root.Countries()[$parentContext.$index()],
                                            optionsText: 'text',
                                            optionsValue: 'value',
                                            valueAllowUnset: true,
                                            optionsCaption: 'Select Country',
                                            event:{ change: $root.countryChanged},
                                            attr: { id:'txtCountry'+$parentContext.$index() ,name: 'txtCountry'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                          ">
                                          </select>
                                      </div>
                                      <div class="col-sm-2">
                                          <select class="form-control state" data-bind="
                                            value: item.state,
                                            options: $root.States()[$parentContext.$index()],
                                            optionsText: 'text',
                                            optionsValue: 'value',
                                            valueAllowUnset: true,
                                            optionsCaption: 'Select State Name',
                                            event:{ change: $root.stateChanged},
                                            attr: { id:'txtState'+$parentContext.$index() ,name: 'txtState'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                          ">
                                          </select>
                                      </div>
                                      <div class="col-sm-2">
                                          <select class="form-control city"  data-bind="
                                            value: item.city,
                                            options: $root.Cities()[$parentContext.$index()],
                                            optionsText: 'text',
                                            optionsValue: 'value',
                                            valueAllowUnset: true,
                                            optionsCaption: 'Select City Name',
                                            attr: { id:'txtCity'+$parentContext.$index() ,name: 'txtCity'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                          ">
                                          </select>
                                      </div>
                                      <!-- ko if:$parentContext.$index() != 0 -->
                                      <div class="col-sm-1">
                                          <a href="#" data-bind="click:$root.removeAddress.bind($data, $parentContext.$index())" title="Remove" class="text-danger"><i class="fa fa-times"></i></a>
                                     </div>
                                      <!-- /ko -->
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-b-0">
                            <div class="col-sm-2">
                                <a href="#" data-bind="click:addLocationClick" class="col-xs-12 btn btn-purple">Add Location</a>
                            </div>
                            <div class=" col-sm-4">
                                <button id="createAddress" type="button" data-bind="click:createAddressClick,text:actionAddress" class="col-xs-6 btn btn-purple"></button>
                                <!--ko if: action() != "add"-->
                                    <button  type="button" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger col-xs-offset-1 col-xs-5"></button>
                                <!--/ko-->
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div id="offers" class="tab-pane fade">
                    @include('admin.brand.offer')
                </div>
                <div id="bank_details" class="tab-pane fade">
                    <div class="panel-body">
                        <form id="form_bank" class="form-horizontal">
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Bank Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Bank Name" data-bind="value:bank_name,attr: { id:'txtBank_name',name: 'txtBank_name'}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Account Number</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Account Number" class="form-control" data-bind="value:account_number,attr: { id:'txtAccount_number',name: 'txtAccount_number'}" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">IFSC Code</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="IFSC Code" data-bind="value:ifsc,attr: { id:'txtIfsc',name: 'txtIfsc'}" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Branch Details</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Branch Details" class="form-control" data-bind="value:branch_details,attr: { id:'txtBranch_details',name: 'txtBranch_details'}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Account Type</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Account Type" data-bind="value:account_type,attr: { id:'txtAccount_type',name: 'txtAccount_type'}">
                                </div>
                            </div>
                          <div class="form-group row">
                              <div class="col-sm-4 offset-sm-3">
                                <!--<button type="submit" class="btn btn-secondary">Cancel</button>-->
                                <button type="button" data-bind="click:addBankDetails,text:actionBankDetail" class="btn btn-purple"></button>
                                <!--ko if: action() != "add"-->
                                    <button  type="button" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger"></button>
                                <!--/ko-->
                              </div>
                        </div>
                      </form>
                    </div>
                </div>
                <div id="payment_request" class="tab-pane fade">
                    @include('admin.brand.paymenttab')
                </div>
                <div id="total_sales" class="tab-pane fade">
                    @include('admin.brand.totalsalestab')
                </div>
            </div>
        </div> 
    </div>
</div>