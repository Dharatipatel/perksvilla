@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.highlightheader{
        padding: 30px 15px;
        margin: 20px 0;
}
    .has-shadow {
    -webkit-box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1), -1px 0 2px rgba(0, 0, 0, 0.05);
    box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.1), -1px 0 2px rgba(0, 0, 0, 0.05);
}
.bg-white {
    background: #fff !important;
}
.item{
    border-right: 1px solid #eee;
    padding: 15px 0;
}
.title {
    font-size: 1.1em;
    font-weight: 300;
    color: #777;
    margin: 0 20px;
    vertical-align: middle;
    display: inline-block;
}
.number {
    font-size: 1.8em;
    font-weight: 300;
    vertical-align: middle;
    display: inline-block;
}
.offerstatus.active{
    color: #3bc0c3 !important;
}
</style>
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">

        @include('admin.brand.index')

        @include('admin.brand.manage')
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Delete Brand</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure to delete Brand ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="emailModal" tabindex="-1" role="dialog" aria-labelledby="emailModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Send Email</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {!! Form::open(['id'=>'form_email_send', 'class'=>"form-horizontal","role"=>"form"]) !!}
                    {!! Form::hidden("brand_id", "", ["id"=>"hiddenBrandId"]) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email:</label>
                            <label class="col-sm-8" id="labelEmail"></label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password:</label>
                            <div class="col-sm-8">
                                <input class="form-control" placeholder="Password" name="txtSendPassword" id="txtSendPassword" type="password">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-bind="click:emailSendClick" class="btn btn-primary">Send</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var id = "{{ Session::get('id')}}";
        var token = "{{ Session::get('token')}}";
        var type = "{{$type}}";
        var brandListApiPath = "{{config('constant.api.brand.list')}}";
        var addBrandApiPath = "{{config('constant.api.brand.add')}}";
        var addAddressApiPath = "{{config('constant.api.address.add')}}";
        var updateAddressApiPath = "{{config('constant.api.address.update')}}";
        var updateBrandApiPath = "{{config('constant.api.brand.update')}}";
        var deleteBrandApiPath = "{{config('constant.api.brand.delete')}}";
        var activeBrandApiPath = "{{config('constant.api.brand.active')}}";
        var deactiveBrandApiPath = "{{config('constant.api.brand.deactive')}}";
        var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
        var countryListApiPath = "{{config('constant.api.country.list')}}";
        var stateListApiPath = "{{config('constant.api.state.list')}}";
        var cityListApiPath = "{{config('constant.api.city.list')}}";
        var addressListApiPath = "{{config('constant.api.country.address')}}";
        var offerListWebPath = "{{route('offeractive')}}";
        var brandListWebPath = "{{route('brandlist')}}";
        var offerListApiPath = "{{config('constant.api.offer.list')}}";
        var addBankDetailApiPath = "{{config('constant.api.bank.add')}}";
        var updateBankDetailApiPath = "{{config('constant.api.bank.update')}}";
        var getBankDetailApi = "{{config('constant.api.bank.get')}}";
        var updateBankDetailApi = "{{config('constant.api.bank.update')}}";
        var emailSendApiPath = "{{config('constant.api.brand.email.send')}}";
        var addOfferApiPath ="{{config('constant.api.offer.add')}}";
        var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
        var deleteOfferApiPath ="{{config('constant.api.offer.delete')}}";
        var activeOfferApiPath ="{{config('constant.api.offer.active')}}";
        var deactiveOfferApiPath ="{{config('constant.api.offer.deactive')}}";
        var addressListApiPath ="{{config('constant.api.country.address')}}";
        var offerApproveApiPath ="{{config('constant.api.offer.approve')}}";
        var offerDeactiveApiPath ="{{config('constant.api.offer.deactive')}}";
        var paymentListApiPath = "{{config('brand_constant.api.profile.transcation')}}";
        var paymentPaidApiPath = "{{config('constant.api.brand.paid')}}";
        var totalSalesApiPath = "{{config('constant.api.brand.totalsales')}}";
        var path ="{{URL::to('/')}}"+"/uploads/brand/";
        var static_image ="NoLogo.png";
        var offerRejectApiPath ="{{config('constant.api.offer.reject')}}";
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="{{config('constant.script.brand')}}"></script>
    @endsection