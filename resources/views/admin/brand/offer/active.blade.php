<table id="active_offer_tbl" class="table table-striped table-bordered" data-bind="visible:offerViewModel.offerList">
        <thead>
        <tr>
            <th>ID</th>
            <th>Offer Name</th>
            <th>Contact</th>
            <th>Offer Value</th>
            <th>Buy Price</th>
            <th>Publish Date</th>
            <th>End Date</th>
            <th>Total</th>
            <th>Brand Name</th>
            <th>Issued</th>
            <th>Redeem</th>
            <th>status</th>
        </tr>
        </thead>
        <tbody>
        <!-- ko foreach: offerViewModel.offers() -->
          <tr>
          <td><span data-bind="text: ($index()+1)"></span></td>
          <td><span data-bind="text: $data.name"></span></td>
          <td><span data-bind="text: $data.contact"></td>
          <td><span data-bind="text: $data.value"></span></td>
          <td><span data-bind="text: $data.price"></span></td>
          <td><span data-bind="text: $data.publish_date"></span></td>
          <td><span data-bind="text: $data.valid_to"></span></td>
          <td><span data-bind="text: $data.count"></span></td>
          <td><span data-bind="text: $data.brand_name"></span></td>
          
              <td><span data-bind="text: $data.issued_count"></td>
              <td><span data-bind="text: $data.redeem_count"></td>
          <td class="text-center">
              <a href="" data-bind="click:$root.offerViewModel.deactiveClick" class="on-default remove-row">
                <i class="fa fa-toggle-on" aria-hidden="true"></i>
              </a>
          </td>
          </tr>
        <!-- /ko -->
        </tbody>
    </table>
  
