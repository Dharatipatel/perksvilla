<div class="panel-body" data-bind="visible:offerViewModel.offerListContainer">
    <div class="col-xs-12">
          <div class="col-xs-6" style="margin-bottom: 15px;">
              <a href="" class="btn btn-primary" data-bind="click:offerViewModel.addOfferClick">Add 
            offer</a>
              <a href="#" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger"></a>
          </div>
          <div class="col-xs-6 text-right">
                <a style="padding: 6px 12px;" class="offerstatus active" href="" id="activeOffer" data-bind="click:offerViewModel.offerActiveClick">Active</a> | 
                <a style="padding: 6px 12px;" class="offerstatus" href="" id="pastOffer" data-bind="click:offerViewModel.offerPastClick">Past</a> |
                <a style="padding: 6px 12px;" class="offerstatus" href="" id="pendingOffer" data-bind="click:offerViewModel.offerPendingClick">Pending</a> |
                <a style="padding: 6px 12px;" class="offerstatus" href="" id="upcomingOffer" data-bind="click:offerViewModel.offerUpcomingClick">Upcoming</a> |
                <a style="padding: 6px 12px;" class="offerstatus" href="" id="rejectedOffer" data-bind="click:offerViewModel.offerRejectedClick">Rejected</a>
          </div>
    </div>
     <div data-bind="visible:offerViewModel.activeOfferContainer">
       @include('admin.brand.offer.active')
    </div>
    <div data-bind="visible:offerViewModel.pastOfferContainer">
        @include('admin.brand.offer.past')
    </div>
    <div data-bind="visible:offerViewModel.pendingOfferContainer">
      @include('admin.brand.offer.pending')
    </div>
    <div data-bind="visible:offerViewModel.upcomingOfferContainer">
      @include('admin.brand.offer.upcoming')
    </div>
    <div data-bind="visible:offerViewModel.rejectedOfferContainer">
      @include('admin.brand.offer.rejected')
    </div>
     <div data-bind="visible:offerViewModel.offerNoData" class="col-xs-12 text-center"><h1>No Data Available</h1></div>
</div>
<div class="panel-body" data-bind="visible:offerViewModel.offerAddContainer" >
   <div class="container-fluid">
        <div class="row" data-bind="visible:offerViewModel.step1Container">
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Fix Value Offer</h3>
                    </div>
                    <div class="card-body text-center">
                        <a href="" data-bind="click:offerViewModel.fixValueOfferCreateClick" class="btn btn-primary"> Create Offer</a>               
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4" >Free Offer</h3>
                    </div>
                    <div class="card-body text-center">
                        <a href="" data-bind="click:offerViewModel.freeOfferCreateClick" class="btn btn-primary"> Create Offer</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="margin-top:10px">
                <button  type="button" data-bind="click:offerViewModel.cancelOfferClick,text:'cancel'" class="btn btn-danger  col-xs-2"></button>
            </div>
        </div>
        <div class="row" data-bind="visible:offerViewModel.step2Container">
          <div class="col-xs-12" data-bind="visible:offerViewModel.addOffer">
              <a href="#" style="color: #3bc0c3" data-bind="click:offerViewModel.backToStep1Click">Back To Step1 </a>
          </div>
            <!-- Form Elements -->
            <div class="col-lg-12">
            <div class="card">
              <div class="card-close">
                
              </div>
              <div class="card-header d-flex align-items-center">
                <h3 class="h4" data-bind="text:offerViewModel.stepSubTitle"></h3>
              </div>
              <div class="card-body">
                <form class="form-horizontal" id="form_Offer">
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Title</label>
                    <div class="col-md-4">
                      <input type="text" name="txtTitle" id="txtTitle" class="form-control" placeholder="Enter Offer Title">
                    </div>
                  </div>
            <div class="line"></div>
                  
                  
            <div class="form-group row">
               <label class="col-sm-3 form-control-label">Offer Description</label>
               <div class="col-md-4">
                  <textarea class="form-control" name="txtOfferDescription" id="txtOfferDescription" placeholder="Enter Offer Description"></textarea>
               </div>
            </div>
            <div class="line"></div>
            <div class="form-group row">
               <label class="col-sm-3 form-control-label">Contact No</label>
               <div class="col-md-4">
                  <input type ="text" class="form-control" name="txtOfferContact" id="txtOfferContact" placeholder="Enter Contact Number" />
               </div>
            </div>
   <div class="line"></div>
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Value</label>
                    <div class="col-sm-9">
                      <div class="row">
                        <div class="col-md-3">
                          <input type="text" name="txtValue" id="txtValue" placeholder="Enter Actual Value" class="form-control">
                        </div>
                        <div class="col-md-3">
                          <input type="text" name="txtPrice" id="txtPrice"  class="form-control" disabled="disabled">
                        </div>
                       
                      </div>
                    </div>
                  </div>
                  <div class="line"></div>
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Applicable On<br><small class="text-primary">Specify applicable dates</small></label>
                    <div class="col-sm-9" >
                      
                      <div>
                        <input id="txtApplicableOn1" type="radio" checked="checked" value="1" name="txtApplicableOn">
                        <label for="txtApplicableOn1">Fix Date</label>
                      </div>
                      <div>
                        <input id="txtApplicableOn2" type="radio" value="2" name="txtApplicableOn">
                        <label for="txtApplicableOn2">Week Days (Mon-Fri)</label>
                      </div>
            <div>
                        <input id="txtApplicableOn3" type="radio" value="3" name="txtApplicableOn">
                        <label for="txtApplicableOn3">Weekends  (Sat-Sun)</label>
                      </div>
            <div>
                        <input id="txtApplicableOn4" type="radio"  value="4" name="txtApplicableOn">
                        <label for="txtApplicableOn4">All Days</label>
                      </div>

                    </div>
                  </div>
                  <div class="line"></div>
            <div class="form-group row" data-bind="visible:offerViewModel.fixDateVisibilty">
                    <label class="col-sm-3 form-control-label">Fix Date</label>
                    <div class="col-md-4">
                      <input type="text" id="fixDate" name="fixDate" placeholder="Select Date" class="form-control">
                    </div>
            <div class="line"></div>
                  </div>

            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Applicable At<br><small class="text-primary">Specify applicable branches</small></label>
                    <div class="col-sm-9">
              <!-- ko foreach: offerViewModel.address -->
              <div>
                  <input type="checkbox" data-bind="attr: { 'id': 'address' + $index() ,value: value }, checked: $root.offerViewModel.choosenAddress" name="address[]" />
                  <label data-bind="attr: { 'for': 'address' + $index() },text:textaddress"></label>
                </div>
              <!-- /ko -->
                    </div>

                  </div>
                  <div class="line"></div>

            <div class="form-group row" data-bind="visible:offerViewModel.validToVisibilty">
                    <label class="col-sm-3 form-control-label">Offer Valid Upto</label>
                    <div class="col-md-4">
                      <input type="text" id="validTo" name="validTo" placeholder="Select Date" class="form-control">
                    </div>
            <div class="line"></div>
                  </div>
                
                  <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Offer Limit/Count</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control" id="count" name="count" placeholder="Enter Number"><small class="help-block-none">Maximun count upto which coupon can be issue</small>
                    </div>
                  </div>
            <div class="line"></div>
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Publish Date</label>
                    <div class="col-md-4">
                      <input type="text" id="publishDate" name="publishDate" placeholder="Select Date" class="form-control">
                    </div>
                  </div>
                  <div class="line"></div>
                
            <div class="form-group row">
                    <label class="col-sm-3 form-control-label">Important Notes</label>
                    <div class="col-md-4">
            <textarea class="form-control" id="notes" name="notes" placeholder="Enter Important Notes related to this offer"></textarea>
                    </div>
                  </div>
            <div class="line"></div>
              
                  <div class="form-group row">
                    <div class="col-sm-6 offset-sm-3">
                      <button type="button" data-bind="click:offerViewModel.createOfferClick,text:offerViewModel.action_title" class="btn btn-primary col-xs-2"></button>
                      <button  type="button" data-bind="click:offerViewModel.cancelOfferClick,text:'cancel'" class="btn btn-danger col-xs-offset-1 col-xs-2"></button>
                      <button type="button" data-bind="attr:{'data-id':offerViewModel.offer_id},click:offerViewModel.rejectOfferClick,visible:offerViewModel.isReject" class="btn btn-danger col-xs-offset-1 col-xs-2">Reject</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>