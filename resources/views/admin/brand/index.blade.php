<!-- Brand Listing -->

        <div class="col-lg-12" id="divBrandList" data-bind="visible:brandListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">{{ $title }}</h3> 
                </div> 
                <div class="highlightheader col-lg-12 bg-white has-shadow">
                    <div class="col-lg-4">
                        <div class="item d-flex align-items-center">
                            <div class="title">
                                <span>Active<br>Offers</span>
                            </div>
                            <div class="number">
                                <span data-bind="text:active_offer_count()"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="item d-flex align-items-center">
                            <div class="title">
                                <span>Active<br>Brands</span>
                            </div>
                            <div class="number">
                                <span data-bind="text:active_count()"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="item d-flex align-items-center">
                            <div class="title">
                                <span> Coupons<br>Redeem</span>
                            </div>
                            <div class="number">
                                <span data-bind="text:redeem_count()"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <!-- <a href="" class="btn btn-primary m-b-5" data-bind="click:$root.addBrandClick">Add Brand</a> -->
                    <table id="brand_tbl" class="table table-striped table-bordered" data-bind="visible:brandList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <!-- <th>Email</th> -->
                                <th>Mobile</th>
                                <th>Person Name</th>
                                <!-- <th>Description</th> -->
                                <!-- <th>Business hours</th> -->
                                <!-- <th>website url</th> -->
                                <!-- <th>Facebook page</th> -->
                                <!-- <th>Address</th>
                                <th>Pincode</th> -->
                                <th>Offer Count</th>
                                <th>Added On</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: brands() -->
                              <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text: $data.name"></span></td>
                                <td><span data-bind="text: $data.category"></span></td>
                                <!-- <td><span data-bind="text: $data.email"></span></td> -->
                                <td><span data-bind="text: $data.contact"></span></td>
                                <td><span data-bind="text: $data.contact_person_name"></span></td>
                                <!-- <td><span data-bind="text: $data.description"></span></td> -->
                                <!-- <td><span data-bind="text: $data.business_hours"></span></td> -->
                                <!-- <td><span data-bind="text: $data.website_url"></span></td> -->
                                <!-- <td><span data-bind="text: $data.facebook_page"></span></td> -->
                                <!-- <td><span data-bind="text: $data.address"></span></td>
                                <td><span data-bind="text: $data.pincode"></span></td> -->
                                <td><a href="#" data-bind="click:$root.offerListClick" ><u><span data-bind="text: $data.offer_count()"></span></u></a></td>
                                <td>
                                    <a href="#" >
                                        <span data-bind="text: $data.created"></span>
                                    </a>
                                </td>
                                <!-- <td><span data-bind="text: $data.statusText()"></span></td> -->
                                <td class="text-center">
                                    <!-- ko if: $data.statusText() == "Deactive" -->
                                    <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">
                                        <i class="fa fa-toggle-off" aria-hidden="true"></i>
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Active" -->
                                    <a href="" data-bind="click:$root.deactiveClick" class="on-default remove-row">
                                        <i class="fa fa-toggle-on" aria-hidden="true"></i>   
                                    </a>
                                     <!-- /ko -->
                                </td>
                                <td class="text-center">
                                    <a href="#" data-bind="click:$root.editBrandClick" class="on-default edit-row"><i class="text-danger fa fa-pencil"></i></a>
                                    <a href="#" data-bind="click:$root.deleteBrandClick" class="on-default remove-row"><i class="text-success fa fa-trash-o"></i></a>
                                    <a title="Send Email, SMS" href="#" data-bind="click:$root.emailBrandClick" class="on-default send-email-row"><i class="text-info fa fa-envelope"></i></a>
                                    
                                </td>
                              </tr>
                            <!-- /ko -->
                            </tbody>
                    </table>
                    <div data-bind="visible:brandNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>