<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divPaymentRequestList" data-bind="visible:paymentListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Payment Request</h3> 
                </div> 
                <div class="panel-body"><a href="#" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger"></a>
                    <table id="payment_tbl" class="table table-striped table-bordered" data-bind="visible:paymentList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Brand Name</th>
                                <th>Request Amount</th>
                                <th>Transfer Amount</th>
                                <th>Available Balance</th>
                                <th>Request Time</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: paymentRequests() -->
                            <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text:$data.brand_name"></span></td>
                                <td><span data-bind="text:$data.withdraw_amount"></span></td>
                                <td><span data-bind="text:$data.transfer_amount()"></span></td>
                                <td><span data-bind="text:$data.availble_balance"></span></td>
                                <td><span data-bind="text:$data.created"></span></td>
                                <td class="text-center">
                                    <!-- ko if: $data.statusText() == "Pending" -->
                                    <a href="" data-bind="click:$root.activePaymentClick" class="on-default remove-row">
                                          <!-- <i class="fa fa-toggle-off" aria-hidden="true"></i> -->
                                         <span class="text-danger">Unpaid</span> 
                                    </a>
                                    <!-- /ko -->
                                    <!-- ko if: $data.statusText() == "Success" -->
                                    <span class="text-success">Paid</span>
                                    <!-- /ko -->
                                </td>
                            </tr>
                            <!-- /ko -->
                        </tbody>
                    </table>
                    <div data-bind="visible:paymentNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>
    </div>
</div>