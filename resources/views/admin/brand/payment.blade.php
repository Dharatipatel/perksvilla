@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
<style type="text/css">

</style>
@endsection
@section("content")
<div class="modal fade" id="viewBankDetail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Bank Detail</h4>
            </div>
            <div class="modal-body">
                <div>
                    <p>Bank Name : <span data-bind="text:bankDetailsViewModel.bank_name()"></span></p>
                </div>
                <div>
                    <p>A/c Type : <span data-bind="text:bankDetailsViewModel.account_type()"></span></p>
                </div>  
                <div>
                    <p>A/c No : <span data-bind="text:bankDetailsViewModel.account_number()"></span></p>
                </div>  
                <div>
                    <p>Ifsc code : <span data-bind="text:bankDetailsViewModel.ifsc()"></span></p>
                </div> 
                <div>
                    <p>Branch Details : <span data-bind="text:bankDetailsViewModel.branch_details()"></span></p>
                </div>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divPaymentRequestList" data-bind="visible:paymentListContainer">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="panel-title">{{ $title }}</h3> 
        </div> 
        <div class="panel-body">
            <table id="payment_tbl" class="table table-striped table-bordered" data-bind="visible:paymentList">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Brand Name</th>
                        <th>Request Amount</th>
                        <th>Transfer Amount</th>
                        <th>Available Balance</th>
                        <th>Request Time</th>
                        <th>Bank Detail</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- ko foreach: paymentRequests() -->
                    <tr>
                        <td><span data-bind="text:($index()+1)"></span></td>
                        <td><span data-bind="text:$data.brand_name"></span></td>
                        <td><span data-bind="text:$data.withdraw_amount"></span></td>
                        <td><span data-bind="text:$data.transfer_amount()"></span></td>
                        <td><span data-bind="text:$data.availble_balance"></span></td>
                        <td><span data-bind="text:$data.created"></span></td>
                        <td><a href="#" data-bind="click:$root.viewBankDetails"><span>View</span></a></td>
                        <td class="text-center">
                            <!-- ko if: $data.statusText() == "Pending" -->
                            <a href="" data-bind="click:$root.activeClick" class="on-default remove-row">
                                  <!-- <i class="fa fa-toggle-off" aria-hidden="true"></i> -->
                                 <span class="text-danger">Unpaid</span> 
                            </a>
                            <!-- /ko -->
                            <!-- ko if: $data.statusText() == "Success" -->
                            <span class="text-success">Paid</span>
                            <!-- /ko -->
                        </td>
                    </tr>
                    <!-- /ko -->
                </tbody>
            </table>
            <div data-bind="visible:paymentNoData" class="text-center"><h1>No Data Available</h1></div>
        </div>
    </div>
</div>
    </div>
    @endsection
    @section('script')
    <script type="text/javascript">
        var id = "{{ Session::get('id')}}";
        var token = "{{ Session::get('token')}}";
        var paymentListApiPath = "{{config('brand_constant.api.profile.transcation')}}";
        var getBankDetailApi ="{{config('constant.api.bank.get')}}";
        var paymentPaidApiPath = "{{config('constant.api.brand.paid')}}";
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.payment')}}"></script>
    @endsection