<div class="wraper container-fluid">
    <div class="row">
        <div class="col-lg-12" id="divPaymentRequestList" data-bind="visible:totalSalesListContainer">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading"> 
                    <h3 class="panel-title">Total Sales</h3> 
                </div> 
                <div class="panel-body"><a href="#" data-bind="click:cancelBrandClick,text:'cancel'" class="btn btn-danger"></a>
                    <table id="sales_tbl" class="table table-striped table-bordered" data-bind="visible:totalSalesList">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Offer Name</th>
                                <th>Sales</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- ko foreach: totalSales() -->
                            <tr>
                                <td><span data-bind="text:($index()+1)"></span></td>
                                <td><span data-bind="text:$data.offer_name"></span></td>
                                <td><span data-bind="text:$data.offer_price"></span></td>
                            </tr>
                            <!-- /ko -->
                        </tbody>
                    </table>
                    <div data-bind="visible:totalSalesNoData" class="text-center"><h1>No Data Available</h1></div>
                </div>
            </div>
        </div>
    </div>
</div>