@extends('admin.master')
@section("title")
{{ @$title. ' - '}}
@stop
@section("css")
    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")
<div class="wraper container-fluid">
    <div class="row">
        @include('admin.employee.index')
        
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Delete Employee</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    Are you sure to delete Employee ?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" data-bind ="click:deleteConfirm" class="btn btn-primary">Yes</button>
                  </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var id = "{{ Session::get('id')}}";
    var token = "{{ Session::get('token')}}";
    var corporate_id = "{{ $corporate }}";
    var employeeListApiPath ="{{config('constant.api.employee.list')}}";
    var addEmployeeApiPath ="{{config('constant.api.employee.add')}}";
    var updateEmployeeApiPath ="{{config('constant.api.employee.update')}}";
    var deleteEmployeeApiPath ="{{config('constant.api.employee.delete')}}";
    var activeEmployeeApiPath ="{{config('constant.api.employee.active')}}";
    var deactiveEmployeeApiPath ="{{config('constant.api.employee.deactive')}}";
    var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
    var employeeWebPath = "{{route('employeeadmin')}}";
    var corporateWebPath = "{{route('corporatelist')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
<script type="text/javascript" src="{{config('constant.script.employee')}}"></script>
@endsection