<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="title" content="{{env("PROJECT_NAME")}}">
        <title>@yield("title"){{env("PROJECT_NAME")}}</title>       
        <link rel="shortcut icon" href="img/favicon_1.ico">
        <!-- Google-Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic' rel='stylesheet'>

        <!-- Bootstrap core CSS -->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.bootstrap')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.bootstrap-reset')}}" />

        <!--Animation css-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.animate')}}" />

        <!--Icon-fonts css-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.ionicons')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.fontawesome')}}" />

        <!-- Custom styles for this template -->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.style')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.styleresponsive')}}" />
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.helper')}}" />
        
        <!-- Tostr Notification-->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.toastr')}}" />

        <!-- sweet alerts -->
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.sweetalert')}}" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src="resources/assets/admin/js/html5shiv.js"></script>
          <script src="resources/assets/admin/js/respond.min.js"></script>
        <![endif]-->
        @yield('css')
        
    </head>
    <body>
        @include('admin.sidebar')
        <!--Main Content Start -->
        <section class="content">
            @include('admin.topnav')
            <!-- Page Content Start -->
                @yield("content")
            <!-- Page Content Ends -->
            <!-- Footer Start -->
            <footer class="footer">
                © {{date("Y")}}, <a href="{{url('/')}}">PerksVilla</a>, All rights reserved
            </footer>
            <!-- Footer Ends -->
        </section>
        <script>
            var baseURL = "{{url('/').'/'}}";
            var _token = "{{csrf_token()}}";
            var logoutUrl = "{{route('logout')}}";
        </script>
        <script type="text/javascript" src="{{config('admin_constant.script.jquery')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.modernizr')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.pace')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.wow')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.scrollto')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.nicescroll')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.moment')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.validate')}}"></script>
        <!-- Counter-up -->
        <script type='text/javascript' src="{{config('admin_constant.script.waypoints')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.counterup')}}"></script>

        <!-- EASY PIE CHART JS -->
        <script type='text/javascript' src="{{config('admin_constant.script.easypiechart')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.jqueryeasypiechart')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.example')}}"></script>

        <!-- sparkline -->
        <script type='text/javascript' src="{{config('admin_constant.script.sparkline')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.chartsparkline')}}"></script> 

        <!-- sweet alerts -->
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalert')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalertinit')}}"></script> 

        <!-- Chat -->
        <script type='text/javascript' src="{{config('admin_constant.script.app')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.chat')}}"></script>

        <!-- Todo -->
        <script type='text/javascript' src="{{config('admin_constant.script.todo')}}"></script>
        
        <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.toastr')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.jquerymd5')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.moment')}}"></script>

        <script>
        /* ==============================================
             Counter Up
             =============================================== */
            jQuery(document).ready(function($) {
                $('.counter').counterUp({
                    delay: 100,
                    time: 1200
                });
            });
            var static_image ="NoLogo.png";
        </script>
        @yield("script")
    </body>
</html>