<!DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env("PROJECT_NAME")}} - {{@$title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
   <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{config('brand_constant.css.bootstrap')}}">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="{{config('brand_constant.css.fontawesome')}}">
    
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{config('brand_constant.css.fontastic')}}">
    
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{config('brand_constant.css.theme')}}">
    
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{config('brand_constant.css.custom')}}">
    
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{config('brand_constant.image.favicon')}}">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.toastr')}}">
    <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.sweetalert')}}" />
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>{{@$title}}</h1>
                  </div>
               <!--   <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>-->
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                    <h3>{{@$message}}</h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>&copy; 2018 - All Rights Reserved by <a href="http://www.perksvilla.com" class="external">{{env("PROJECT_NAME")}}</a>
  
        </p>
      </div>
    </div>
    <script type="text/javascript">
        var baseURL = "{{url('/').'/'}}";
        var _token = "{{csrf_token()}}";
    </script>
    <script type="text/javascript" src="{{config('brand_constant.script.jquery')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.popper')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.jquerycookie')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.chart')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.validate')}}"></script>
    <script type='text/javascript' src="{{config('admin_constant.script.sweetalert')}}"></script>
    <script type='text/javascript' src="{{config('admin_constant.script.sweetalertinit')}}"></script> 
    <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
    <script type="text/javascript" src="{{config('brand_constant.script.front')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
  </body>
</html>