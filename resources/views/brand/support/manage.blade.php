@extends('brand.master')
@section("css")
<link rel="stylesheet" href="{{config('constant.css.common.wysihtml5')}}" type="text/css">
<link rel="stylesheet" href="{{config('constant.css.common.wysihtml5_color')}}" type="text/css">
@endsection
@section("content")		
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">{{@$title}}</h2>
    </div>
</header>
<section class="tables">   
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">     
                        <a href="{{route("brand.support")}}" class="btn btn-primary m-b-5">Manage Message</a>
                        <br/>
                        <br/>
                        <div class="table-responsive">
                            {!! Form::open(["url"=>route("brand.support.save"),"method"=>"POST", 'id'=>'form', 'class'=>"form-horizontal","role"=>"form"]) !!}
                            {!! Form::hidden("id", @$support_detail->id)    !!}
                            <div class="form-group">
                                {!! Form::text("title", @$support_detail->title, ["class"=>"form-control", "placeholder"=>"Enter Title"])!!}
                                @if($errors->has("title"))
                                <span class="text-danger flash">{{ $errors->first("title") }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::select("category", @$categoties, @$support_detail->category, ["class"=>"form-control", "placeholder"=>"Select a Category"])!!}
                                @if($errors->has("category"))
                                <span class="text-danger flash">{{ $errors->first("category") }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::textarea("description", @$support_detail->description, ["class"=>"form-control wysihtml5", "placeholder"=>"Description"])!!}
                                @if($errors->has("description"))
                                <span class="text-danger flash">{{ $errors->first("description") }}</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button class="btn btn-purple"> <span>Send</span> <i class="fa fa-send m-l-10"></i> </button>  
                                <a  href="{{route("brand.support")}}" class="btn btn-primary"> <span>Back</span></a>
                            </div>
                            {!! Form::close()   !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<script type="text/javascript" src="{{config('constant.script.common.wysihtml5')}}"></script>
<script type="text/javascript" src="{{config('constant.script.common.bootstrap_wysihtml5')}}"></script>
<script>
jQuery(document).ready(function () {
    $('.has-submenu').removeClass('active');
    $('#supportTab').addClass('active');
    $('#messageDropdown').addClass('show');
    $('#addMessageTab').addClass('active');
    
//$('.wysihtml5').wysihtml5();
});
@if (session() -> has("success"))
        showMsg("success", "{{session()->get('success')}}");
@endif
</script>
@endsection