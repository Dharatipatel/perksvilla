@extends('brand.master')
@section('css')
    <style>
        label.error {
            bottom: -24px;
        }
    </style>
@endsection
@section('content')
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">My Profile</h2>
        </div>
    </header>
    <!-- Form Start-->
    <section class="forms"> 
        <div class="container-fluid">
            <div class="row">
            <!-- Form Elements -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Edit Profile</h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" id ="form_brand">
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Brand Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name ="txtBrandName"data-bind="value:brand"  placeholder="Enter Brand Name" >
                                    </div>
                                    <div class="col-md-4">
                                        <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                                        <!-- ko if:profile()!="NoLogo.png"-->
                                            <a href="#" data-bind="click:removeProfileClick" style="color:red"><i class="fa fa-remove"></i>Remove</a>
                                        <!-- /ko-->
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label for="fileInput" class="col-sm-3 form-control-label">Upload Logo</label>
                                    <div class="col-sm-9">
                                        <input id="fileInput" type="file" name="fileInput" accept="image/gif, image/jpeg, image/png" class="form-control-file">
                                    </div>
                                    
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Select Category</label>
                                     <div class="col-md-4">
                                        <select class="form-control" disabled="disabled" name="txtCategory" id="txtCategory" data-bind="
                                                      value: selectedCategory,
                                                      options: availbleCategory,
                                                      optionsText: 'text',
                                                      optionsValue: 'value',
                                                      valueAllowUnset: true,
                                                      optionsCaption: 'Select Category'
                                                    "> 
                                        </select>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Brand Description</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" name="txtDescription" data-bind="value:description"  placeholder="Enter Brand Description"></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Contact Person</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:personname"  name="txtPersonName" placeholder="Enter Contact Person" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Email Address</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:email" name="txtEmail"
                                        id="txtEmail"  placeholder="Enter Email Address" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Phone Number</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:contact" name="txtContact"
                                        id="txtContact"  placeholder="Enter Phone Number" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Business Hours</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" name="txtBusinessHours"  data-bind="value:business_hours"  placeholder="Enter Business Hours"></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Website URL</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="txtWebsiteUrl" data-bind="value:website_url"  placeholder="Enter Website URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Facebook Page</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="txtFacebookPage" data-bind="value:facebook_page"  placeholder="Enter Facebook URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div data-bind="foreach: { data: fields, as: 'field' }">
                                    <div data-bind="attr: { id: 'addressDiv'+$index()},foreach: { data: items, as: 'item' }" class="form-group row">
                                        <div class="col-sm-12" >
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <input class="form-control" placeholder="Address" 
                                                  data-bind="value:item.address,attr: { id:'txtAddress'+$parentContext.$index() ,name: 'txtAddress'+$parentContext.$index()}" type="text" >
                                                </div>
                                                    <input class="form-control"                                                 data-bind="value:item.id,attr: { id:'txtId'+$parentContext.$index() ,name: 'txtId'+$parentContext.$index()}" type="hidden" >
                                                <div class="col-sm-2">
                                                    <input class="form-control pincode" placeholder="Pincode" data-bind="value:item.pincode,attr: { id:'txtPincode'+$parentContext.$index() ,name: 'txtPincode'+$parentContext.$index()}" type="text" value="">
                                                </div>
                                                <div class="col-sm-3">
                                                    <select class="form-control city"  data-bind="
                                                    value: item.city,
                                                    options: $root.Cities()[$parentContext.$index()],
                                                    optionsText: 'text',
                                                    optionsValue: 'value',
                                                    valueAllowUnset: true,
                                                    optionsCaption: 'Select City Name',
                                                    attr: { id:'txtCity'+$parentContext.$index() ,name: 'txtCity'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                                    ">
                                                    </select>
                                                </div> 
                                                <div class="col-sm-2">
                                                    <!-- ko if:$parentContext.$index() != 0 -->
                                                        <a href="#" data-bind="click:$root.removeAddress.bind($data, $parentContext.$index())">Remove</a>
                                                    <!-- /ko -->
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <a href="#" data-bind="click:addLocationClick" class="btn btn-primary">
                                        Add Location
                                    </a>
                                    </div>
                                    <div class="col-sm-4">
                                        <button type="button" data-bind="click:updateProfileClick" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('script')
        <script type="text/javascript">
            var id = "{{ Session::get('id')}}";
            var bid = "{{ Session::get('bid')}}";
            var token = "{{ Session::get('token')}}";
            var type = "{{ $type }}";
            var viewProfileApiPath = "{{ config('brand_constant.api.profile.view')}}";
            var removeProfileApiPath = "{{ config('brand_constant.api.profile.remove')}}";
            var updateProfileApiPath = "{{ config('brand_constant.api.profile.update')}}";
            var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
            var addressListApiPath = "{{config('constant.api.country.address')}}";
            var cityListApiPath = "{{config('constant.api.city.list')}}";
            var path ="{{URL::to('/')}}"+"/uploads/brand/";
            var editwebPath ="{{route('editprofile')}}";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.profile.view')}}"></script>
    @endsection
@endsection