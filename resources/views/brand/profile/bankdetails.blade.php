@extends('brand.master')
@section('content')
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">My Profile</h2>
            </div>
          </header>
		  <!-- Form Start-->
		  <section class="forms"> 
            <div class="container-fluid">
              <div class="row">
             
               <!-- Form Elements -->
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                      
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Bank Details</h3>
                    </div>
                    <div class="card-body">
                         <form id="form_bank" class="form-horizontal">
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Bank Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Bank Name" data-bind="value:bank_name,attr: { id:'txtBank_name',name: 'txtBank_name'}" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Account Number</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Account Number" class="form-control" data-bind="value:account_number,attr: { id:'txtAccount_number',name: 'txtAccount_number'}" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">IFSC Code</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="IFSC Code" data-bind="value:ifsc,attr: { id:'txtIfsc',name: 'txtIfsc'}" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Branch Details</label>
                                <div class="col-md-4">
                                    <input type="text" placeholder="Branch Details" class="form-control" data-bind="value:branch_details,attr: { id:'txtBranch_details',name: 'txtBranch_details'}" disabled="disabled">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Account Type</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="Account Type" data-bind="value:account_type,attr: { id:'txtAccount_type',name: 'txtAccount_type'}" disabled="disabled">
                                </div>
                            </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var bid = "{{ Session::get('bid')}}";
          var token = "{{ Session::get('token')}}";
          var getBankDetailApi ="{{config('constant.api.bank.get')}}";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.profile.bankdetails')}}"></script>
    @endsection
@endsection