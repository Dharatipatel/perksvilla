@extends('brand.master')
@section("css")
   <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section('content')
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">My Wallet</h2>
            </div>
          </header>
          
        
          <!-- Dashboard Header Section    -->
		  
		  <section class="dashboard-counts no-padding-bottom">
            <div class="container-fluid">
              <div class="row bg-white has-shadow">
               
			    
                <div class="col-xl-4 col-sm-4">
                  <div class="item d-flex align-items-center">
                    <div class="icon bg-violet"><i class="icon-padnote"></i></div>
                    <div class="title"><span>Available<br>Balance</span>
                      <div class="progress">
                        <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet"></div>
                      </div>
                    </div>
                    <div class="number"><strong data-bind="text:'Rs '+availble_balance()"></strong></div>
                  </div>
                </div>
               
                <!-- Item -->
                
                
    				<div class="col-lg-8">                           
                <div class="card">
                    <div class="card-body">
                        <form class="form-inline" id="form_withdraw">
                            <h3 class="h4" style="margin-right:15px; margin-top:10px;">Withdraw Amount</h3>
    					              <div class="form-group">
                                <label for="txtAmount" class="sr-only">Name</label>
                                <input id="txtAmount" data-bind="event:{keyup:amountUpdate},value:amount,valueUpdate: 'afterkeydown'" name="txtAmount" type="text" placeholder="Enter Amount" class="mr-3 form-control">
                            </div>
                            <div class="form-group">
                                <button type="button" data-bind="click:withDrawButton" class="btn btn-primary">Withdraw</button>
                            </div>
                            <p style="font-size: 0.8rem" class="text-danger" data-bind="visible:isCommision">Rs <span data-bind="text:credit_amount()"></span> will be credited to your bank account</p>
                        </form>
                    </div>
                </div>
                <p style="color:#4631e7;font-size: 0.8rem">Note: <span data-bind="text:commision()+'% '"></span> Perksvilla commision will be deducated from your requested amount</p>
            </div>
        </div>
    </div>
</section>
<section class="tables">    		
   <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-close">
                    </div>
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Withdraw Summary</h3>
                    </div>
                    <div class="card-body" data-bind="visible:transactionList">
                        <div class="table-responsive">
                            <table class="table" id="transactionList">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amount Requested</th>
                                        <th>Withdrawal Date</th>
                                        <th>Amount Credited </th>
                        							  <th>Creditted to</th>
                        							  <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- ko foreach:transactions() -->
                                      <tr>
                                          <th scope="row" data-bind="text:($index()+1)">1</th>
                                          <td data-bind="text:'Rs '+withdraw_amount()"></td>
                                          <td data-bind="text:created"></td>
                                          <td data-bind="text:'Rs '+transfer_amount()"></td>
                                          <td data-bind="text:credited_to()"></td>
                                          <!-- ko if:status() == 0 -->
            							                <td class="text-danger" data-bind="text:statusText()"></td>
                                          <!-- /ko -->
                                          <!-- ko if:status() == 1 -->
                                          <td class="text-success" data-bind="text:statusText()"></td>
                                          <!-- /ko -->
                                      </tr>
                                    <!--  /ko -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body" data-bind="visible:transactionNoData">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('script')
<script type="text/javascript">
  var id = "{{ Session::get('id')}}";
  var token = "{{ Session::get('token')}}";
  var bid = "{{ Session::get('bid')}}";
  var withDrawAmountApiPath = "{{ config('brand_constant.api.profile.wallet')}}";
  var withDrawTranscationApiPath = "{{ config('brand_constant.api.profile.transcation')}}";
  var brandBalanceApiPath = "{{ config('brand_constant.api.profile.balance')}}";
</script>
<script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
<script type="text/javascript" src="{{config('brand_constant.script.profile.wallet')}}"></script>
@endsection
@endsection