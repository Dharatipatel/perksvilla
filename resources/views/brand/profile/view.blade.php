@extends('brand.master')
@section('content')
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">My Profile</h2>
        </div>
    </header>
    <!-- Form Start-->
    <section class="forms"> 
        <div class="container-fluid">
            <div class="row">
            <!-- Form Elements -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Edit Profile</h3>
                        </div>
                        <div class="card-body">
                            <form class="form-horizontal" >
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Brand Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:brand" disabled="disabled" placeholder="Enter Brand Name" >
                                    </div>
                                    <div class="col-md-4">
                                        <img class="img-circle" data-bind="attr: { src: imagePath() + profile() }" height="100" width="100">
                                    </div>
                                </div>
                    
                               
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Select Category</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="txtCategory" disabled="disabled" data-bind="
                                                          value: selectedCategory,
                                                          options: availbleCategory,
                                                          optionsText: 'text',
                                                          optionsValue: 'value',
                                                          valueAllowUnset: true,
                                                          optionsCaption: 'Select Category'
                                                        ">
                                        </select>
                                    </div>
                                
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Brand Description</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" data-bind="value:description" disabled="disabled" placeholder="Enter Brand Description"></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Contact Person</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:personname" disabled="disabled" placeholder="Enter Contact Person" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Email Address</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:email" disabled="disabled" placeholder="Enter Email Address" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Phone Number</label>
                                    <div class="col-md-4">
                                        <input type="text" data-bind="value:contact" disabled="disabled" placeholder="Enter Phone Number" class="form-control">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Business Hours</label>
                                    <div class="col-md-4">
                                        <textarea class="form-control" data-bind="value:business_hours" disabled="disabled" placeholder="Enter Business Hours"></textarea>
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Website URL</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:website_url" disabled="disabled" placeholder="Enter Website URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Facebook Page</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" data-bind="value:facebook_page" disabled="disabled" placeholder="Enter Facebook URL">
                                    </div>
                                </div>
                                <div class="line"></div>
                                <div data-bind="foreach: { data: fields, as: 'field' }">
                                    <div data-bind="attr: { id: 'addressDiv'+$index()},foreach: { data: items, as: 'item' }" class="form-group row">
                                        <div class="col-sm-9" >
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input class="form-control" placeholder="Address" 
                                                  data-bind="value:item.address,attr: { id:'txtAddress'+$parentContext.$index() ,name: 'txtAddress'+$parentContext.$index()}" disabled="disabled" type="text" >
                                                </div>
                                                <div class="col-sm-3">
                                                    <input class="form-control pincode" placeholder="Pincode" data-bind="value:item.pincode,attr: { id:'txtPincode'+$parentContext.$index() ,name: 'txtPincode'+$parentContext.$index()}" disabled="disabled" type="text" value="">
                                                </div>
                                                <div class="col-sm-3">
                                                    <select class="form-control city"  disabled="disabled" data-bind="
                                                    value: item.city,
                                                    options: $root.Cities()[$parentContext.$index()],
                                                    optionsText: 'text',
                                                    optionsValue: 'value',
                                                    valueAllowUnset: true,
                                                    optionsCaption: 'Select City Name',
                                                    attr: { id:'txtCity'+$parentContext.$index() ,name: 'txtCity'+$parentContext.$index(),'data-id':$parentContext.$index()}
                                                    ">
                                                    </select>
                                                </div>                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @section('script')
        <script type="text/javascript">
          var id = "{{ Session::get('id')}}";
          var token = "{{ Session::get('token')}}";
          var bid = "{{ Session::get('bid')}}";
          var type = "{{ $type }}";
          var viewProfileApiPath = "{{ config('brand_constant.api.profile.view')}}";
          var getCategoriesApiPath = "{{config('constant.api.category.list')}}";
          var addressListApiPath = "{{config('constant.api.country.address')}}";
          var cityListApiPath = "{{config('constant.api.city.list')}}";
          var path ="{{URL::to('/')}}"+"/uploads/brand/";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.profile.view')}}"></script>
    @endsection
@endsection