@extends('brand.master')
@section("css")
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
@endsection   
@section("content")   
    <!-- Page Header-->
      <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom" data-bind="text:stepTitle"></h2>
        </div>
    </header>
		<section class="forms" data-bind="visible:step1Container"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Fix Value Offer</h3>
                        </div>
                        <div class="card-body text-center">
                            <a href="" data-bind="click:fixValueOfferCreateClick" class="btn btn-primary"> Create Offer</a>    					  
                        </div>
                    </div>
                </div>
				        <div class="col-lg-4">
                    <div class="card">
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4" >Free Offer</h3>
                        </div>
                        <div class="card-body text-center">
                            <a href="" data-bind="click:freeOfferCreateClick" class="btn btn-primary"> Create Offer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  <section class="forms" data-bind="visible:step2Container"> 
    @include('brand.offer.step2')
  </section>
    @section("script")
        <script type="text/javascript">
            var addressListApiPath ="{{config('constant.api.country.address')}}";
            var offerListApiPath ="{{config('constant.api.offer.list')}}";
            var addOfferApiPath ="{{config('constant.api.offer.add')}}";
            var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
            var deleteOfferApiPath ="{{config('constant.api.offer.delete')}}";
            var activeOfferApiPath ="{{config('constant.api.offer.active')}}";
            var deactiveOfferApiPath ="{{config('constant.api.offer.deactive')}}";
            var offerList = "{{route('brandoffer')}}";
            var type = "{{$type}}";
        </script>
        <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('corporate_constant.script.offer.offer')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    @endsection
@endsection