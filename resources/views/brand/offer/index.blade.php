@extends('brand.master')
@section("css")
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" /> 
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
    <style>
      .offerstatus.active{
          color:#ff7676 ;
      }
      .reject{
        display: none;
      }
    </style>
@endsection
@section("content")
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Cancel Offer</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure you wanted to cancel this offer?
                        <!--ko if:offerViewModel.status() ==1  -->
                        <p class="text-danger">PS - Corporates will not be able to further issue this offer once you cancel it. However all the coupons which are already issued for this offers still needs to be redeemed by you.
                        </p>
                        <!--/ko-->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-bind ="click:cancelConfirm" class="btn btn-primary">Yes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                     <div class="modal-body">
                    <div class="container-fluid">
                    @include('popup')
                  </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>    		
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">My Offers</h2>
            </div>
          </header>
          
        
          <!-- Dashboard Header Section    -->
		  		  
	<section class="tables" data-bind="visible:listContainer">   
			<div class="container-fluid">
              <div class="row">
			  
                <div class="col-lg-12">
                  <div class="card">
                    
					
					<div class="card-close">
                      <div class="dropdown">
					                <div>
                              <a class="offerstatus active" href="" id ="acitveTab" data-bind="click:offerActiveClick">Active</a> | 
                              <a class="offerstatus" id ="upcomingTab" href="" data-bind="click:offerUpcomingClick">Upcoming</a> |
                              <a class="offerstatus" id ="pendingTab" href="" data-bind="click:offerPendingClick">Pending</a></div>
                                          
                      </div>
                    </div>
					
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Offer Listing</h3>
                    </div>
					
                    <div class="card-body">
					
                      <div class="table-responsive">
                        <table class="table" id="offerList" data-bind="visible:offerList">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Offer Name</th>
                              <!-- <th>Contact No</th> -->
                              <th>Offer Value</th>
              							  <th>Buy Price</th>
              							  <th>Valid Upto</th>
              							  <th>Total</th>
                              <!--ko if: offerViewModel.status() ==1  -->
              							  <th>Issued</th>
              							  <th>Redeem</th>
                              <!--/ko-->
                              <!--ko if: offerViewModel.status() ==2  -->
                              <th>Approved Date</th>
                              <th>Published Date</th>
                              <!--/ko-->
                              <th>Added On</th>
                              <!--ko if: offerViewModel.status() ==3  -->
                              <th>Status</th>
                              <!--/ko-->
              							  <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <!-- ko foreach: offers() -->
                            <tr>
                              <td scope="row"><span data-bind="text:($index()+1)"></span></th>
                              <td> <a href="#" data-bind="click:$root.viewOfferClick"><span data-bind="text: $data.name"></span></a></td>
                              <!-- <td><span data-bind="text: $data.contact()"></span></td> -->
                              <td><span data-bind="text: 'Rs ' +$data.value()"></span></td>
                              <td><span data-bind="text: 'Rs ' +$data.price()"></span></td>
							                <td><span data-bind="text: $data.valid_to"></span></td>
                              <td><span data-bind="text: $data.count"></span></td>
                              <!--ko if: offerViewModel.status() ==1  -->
                              <td><span data-bind="text: $data.issued_count"></span></td>
                              <td><span data-bind="text: $data.redeem_count"></span></td>
                              <!--/ko-->
                              <!--ko if: offerViewModel.status() ==2  -->
                              <td><span data-bind="text: $data.modified"></span></td>
                              <td><span data-bind="text: $data.publish_date"></span></td>
                              <!--/ko-->
							                <td><span data-bind="text: $data.created"></span></td>
                              <!--ko if: offerViewModel.status() ==3  -->
                              <td><span class="text-danger">Under Review</span></td>
                              <!--/ko-->
							                <td>
                                <!--ko if:offerViewModel.status() ==3  -->
                                <a href="#" data-bind="click:$root.editOfferClick">Edit</a>
                                <!--/ko-->
                                <!--ko if:offerViewModel.status() == 2 || offerViewModel.status() == 1-->
                                <a href="#" data-bind="click:$root.viewOfferClick">View</a>
                                <!--/ko-->
                                <a href="#" data-bind="click:$root.cancelClick"> | Cancel</a>
                              </td>
                            </tr>
							             <!-- /ko -->
                          </tbody>
                        </table>
                        <div data-bind="visible:offerNoData" class="text-center"><h1>No Data Available</h1></div>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </section>
          
     <section class="forms" data-bind="visible:editContainer"> 
        @include('brand.offer.step2')
     </section>     
  @section("script")
        <script type="text/javascript">
            var id = "{{ Session::get('id')}}";
            var bid = "{{ Session::get('bid')}}";
            var token = "{{ Session::get('token')}}";
            var type = "{{$type}}";
            var status = "{{$status}}";
            var offerList = "{{route('brandoffer')}}";
            var addressListApiPath ="{{config('constant.api.country.address')}}";
            var offerListApiPath ="{{config('constant.api.offer.list')}}";
            var addOfferApiPath ="{{config('constant.api.offer.add')}}";
            var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
            var deleteOfferApiPath ="{{config('constant.api.offer.delete')}}";
            var activeOfferApiPath ="{{config('constant.api.offer.active')}}";
            var deactiveOfferApiPath ="{{config('constant.api.offer.deactive')}}";
            var path ="{{URL::to('/')}}"+"/uploads/brand/";
        </script>
        <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
        <script type="text/javascript" src="{{config('corporate_constant.script.offer.offer')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> 
    @endsection
@endsection