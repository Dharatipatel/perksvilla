@extends('brand.master')
@section("css") 
    <style>
      .offerstatus.active{
          color:#ff7676 ;
      }
     .table thead th {
          vertical-align: middle;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section("content")   
          <header class="page-header">
            <div class="container-fluid">
              <h2 class="no-margin-bottom">My Offers</h2>
            </div>
          </header>
          <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                     <div class="modal-body">
                    <div class="container-fluid">
                    @include('popup')
                  </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
          <!-- Dashboard Header Section    -->
		  
		
		  
		  <section class="tables">   
            		
			<div class="container-fluid">
              <div class="row">
			  
                <div class="col-lg-12">
                  <div class="card">
                    
					
					<div class="card-close">
                      <div class="dropdown">
					  <div>
              <a href="" id='expiredTab' class="offerstatus active"  data-bind="click:offerExpiredClick">Expired</a> |
              <a href="" id='cancelledTab' class="offerstatus"  data-bind="click:offerCancelledClick">Cancelled</a> | 
              <a href="" id='rejectedTab' class="offerstatus"  data-bind="click:offerDeletedClick">Rejected</a></div>
                                          
                      </div>
                    </div>
					
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Past Offers</h3>
                    </div>
					
                    <div class="card-body">
					
                      <div class="table-responsive">
                        <table class="table" id="offerList" data-bind="visible:offerList">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>Offer Name</th>
                              <th>Contact</th>
                              <th>Offer Value</th>
              							  <th>Buy Price</th>
              							  <th>Publish Date</th>
              							  <th>End Date</th>
              							  <th>Total</th>
                              <!--ko if: status() !=5  -->
              							  <th>Issued</th>
              							  <th>Redeem</th>
							                <th>Conversion Ratio</th>
                              <!--/ko-->
                              <!--ko if: status() ==4  -->
                              <th>Cancel Date</th>
                              <!--/ko-->
                              <!--ko if: status() ==5  -->
                              <th>Rejected Date</th>
                              <!--/ko-->
                            </tr>
                          </thead>
                          <tbody>
                            <!-- ko foreach: offers() -->
                            <tr>
                              <td scope="row"><span data-bind="text:($index()+1)"></span></th>
                              <td><a href="" data-bind="click:$root.viewOfferClick"><span data-bind="text: $data.name"></span></a></td>
                              <td><span data-bind="text: $data.contact()"></span></td>
                              <td><span data-bind="text: 'Rs ' +$data.value()"></span></td>
                              <td><span data-bind="text: 'Rs ' +$data.price()"></span></td>
                              <td><span data-bind="text: $data.publish_date"></span></td>
                              <td><span data-bind="text: $data.valid_to"></span></td>
                              <td><span data-bind="text: $data.count"></span></td>
                              <!--ko if: status() !=5  -->
							                <td><span data-bind="text: $data.issued_count"></span></td>
                              <td><span data-bind="text: $data.redeem_count"></span></td>
                              <td><span data-bind="text: $data.conversion_ratio() + '%'"></span></td>
                              <!--/ko-->
                              <!--ko if: status() ==4  -->
                               <td><span data-bind="text: $data.modified_date()"></span></td>
                              <!--/ko -->
                              <!--ko if: status() ==5  -->
                               <td><span data-bind="text: $data.modified_date()"></span></td>
                              <!--/ko -->
                            </tr>
							
                       <!-- /ko -->
                          </tbody>
                        </table>
                         <div data-bind="visible:offerNoData" class="text-center"><h1>No Data Available</h1></div>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
          </section>
    @section("script")
        <script type="text/javascript">
        var id = "{{ Session::get('id')}}";
        var bid = "{{ Session::get('bid')}}";
        var token = "{{ Session::get('token')}}";
        var type = "{{$type}}";
         var status = "{{$status}}"
        var path ="{{URL::to('/')}}"+"/uploads/brand/";
        var offerList = "{{route('pastoffer')}}";
        var addressListApiPath ="{{config('constant.api.country.address')}}";
        var offerListApiPath ="{{config('constant.api.offer.list')}}";
        var addOfferApiPath ="{{config('constant.api.offer.add')}}";
        var updateOfferApiPath ="{{config('constant.api.offer.update')}}";
        var deleteOfferApiPath ="{{config('constant.api.offer.delete')}}";
        var activeOfferApiPath ="{{config('constant.api.offer.active')}}";
        var deactiveOfferApiPath ="{{config('constant.api.offer.deactive')}}";
        </script>
        <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
<script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
        <script type="text/javascript" src="{{config('corporate_constant.script.offer.offer')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script> 
    @endsection
@endsection