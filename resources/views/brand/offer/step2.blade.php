<div class="container-fluid">
    <div class="row">

    <!-- Form Elements -->
    <div class="col-lg-12">
    <div class="card">
      <div class="card-close">
        
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4" data-bind="text:stepSubTitle"></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" id="form_Offer">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Offer Title</label>
            <div class="col-md-4">
              <input type="text" name="txtTitle" id="txtTitle" class="form-control" placeholder="Enter Offer Title">
            </div>
          </div>
    <div class="line"></div>
          
          
   <div class="form-group row">
      <label class="col-sm-3 form-control-label">Offer Description</label>
      <div class="col-md-4">
         <textarea class="form-control" name="txtDescription" id="txtDescription" placeholder="Enter Offer Description"></textarea>
      </div>
   </div>
   <div class="line"></div>
   <div class="form-group row">
      <label class="col-sm-3 form-control-label">Contact No</label>
      <div class="col-md-4">
         <input type ="text" class="form-control" name="txtContact" id="txtContact" placeholder="Enter Contact Number" />
      </div>
   </div>
   <div class="line"></div>
   <div class="form-group row">
            <label class="col-sm-3 form-control-label">Offer Value</label>
            <div class="col-sm-9">
              <div class="row">
                <div class="col-md-3">
                  <input type="text" name="txtValue" id="txtValue" placeholder="Enter Actual Value" class="form-control">
                </div>
                <div class="col-md-3">
                  <input type="text" name="txtPrice" id="txtPrice"  class="form-control" disabled="disabled">
                </div>
               
              </div>
            </div>
          </div>
          <div class="line"></div>
    <div class="form-group row">
            <label class="col-sm-3 form-control-label">Offer Applicable On<br><small class="text-primary">Specify applicable dates</small></label>
            <div class="col-sm-9" >
              
              <div>
                <input id="txtApplicableOn1" type="radio" checked="checked" value="1" name="txtApplicableOn">
                <label for="txtApplicableOn1">Fix Date</label>
              </div>
              <div>
                <input id="txtApplicableOn2" type="radio" value="2" name="txtApplicableOn">
                <label for="txtApplicableOn2">Week Days (Mon-Fri)</label>
              </div>
    <div>
                <input id="txtApplicableOn3" type="radio" value="3" name="txtApplicableOn">
                <label for="txtApplicableOn3">Weekends  (Sat-Sun)</label>
              </div>
    <div>
                <input id="txtApplicableOn4" type="radio"  value="4" name="txtApplicableOn">
                <label for="txtApplicableOn4">All Days</label>
              </div>

            </div>
          </div>
          <div class="line"></div>
    <div class="form-group row" data-bind="visible:fixDateVisibilty">
            <label class="col-sm-3 form-control-label">Select Date</label>
            <div class="col-md-4">
              <input type="text" id="fixDate" name="fixDate" placeholder="Select Date" class="form-control">
            </div>
    <div class="line"></div>
          </div>

    <div class="form-group row">
            <label class="col-sm-3 form-control-label">Offer Applicable At<br><small class="text-primary">Specify applicable branches</small></label>
            <div class="col-sm-9">
      <!-- ko foreach: address -->
      <div class="col-sm-12 no-padding">
          <input class="address_checkbox" type="checkbox" data-bind="attr: { 'id': 'address' + $index()  ,value: value}, checked: $root.choosenAddress" name="address[]" style="vertical-align: middle;" />
          <label class="col-sm-11 no-padding" data-bind="attr: { 'for': 'address' + $index() },text: textaddress" style="vertical-align: top;"></label>
        </div>
      <!-- /ko -->
            </div>

          </div>
          <div class="line"></div>

    <div class="form-group row" data-bind="visible:validToVisibilty">
            <label class="col-sm-3 form-control-label">Offer Valid Upto</label>
            <div class="col-md-4">
              <input type="text" id="validTo" name="validTo" placeholder="Select Date" class="form-control">
            </div>
    <div class="line"></div>
          </div>
        
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Offer Limit/Count</label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="count" name="count" placeholder="Enter Number"><small class="help-block-none">Maximun count upto which coupon can be issue</small>
            </div>
          </div>
    <div class="line"></div>
    <div class="form-group row">
            <label class="col-sm-3 form-control-label">Publish Date</label>
            <div class="col-md-4">
              <input type="text" id="publishDate" name="publishDate" placeholder="Select Date" class="form-control">
            </div>
          </div>
          <div class="line"></div>
        
    <div class="form-group row">
            <label class="col-sm-3 form-control-label">Important Notes</label>
            <div class="col-md-4">
    <textarea class="form-control" id="notes" name="notes" placeholder="Enter Important Notes related to this offer"></textarea>
            </div>
          </div>
    <div class="line"></div>
      
          <div class="form-group row">
            <div class="col-sm-4 offset-sm-3">
              <!--<button type="submit" class="btn btn-secondary">Cancel</button>-->
              <button type="button" id="editOffer" data-bind="text:action_title,click:createOfferClick" class="btn btn-primary"></button>
              <button type="button" data-bind="text:'Close',click:cancelOfferClick" class="btn btn-danger"></button>
              <button type="button" data-bind="attr:{'data-id':offer_id},click:rejectOfferClick,visible:isReject" class="btn btn-danger">Reject</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    </div>
    </div>
    </div>