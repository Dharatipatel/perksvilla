@extends('brand.master')
@section("css")
   <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.datatable')}}" />
@endsection
@section('content')
<header class="page-header">
    <div class="container-fluid">
        <h2 class="no-margin-bottom">Order History</h2>
    </div>
</header>
<div class="modal fade" id="viewOfferModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             <div class="modal-body">
            <div class="container-fluid">
            @include('popup')
          </div>
        </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<section class="dashboard-counts no-padding-bottom">
    <div class="container-fluid">
        <div class="row bg-white has-shadow">
            <div class="col-xl-4 col-sm-6">
                <div class="item d-flex align-items-center">
                    <div class="icon bg-red">
                        <i class="icon-user"></i>
                    </div>
                    <div class="title">
                        <span>New<br>Customers</span>
                        <div class="progress">
                            <div role="progressbar" style="width: 70%; height: 4px;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-red">
                            </div>
                        </div>
                    </div>
                    <div class="number">
                        <strong data-bind="text:new_customer"></strong>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6">
                <div class="item d-flex align-items-center">
                    <div class="icon bg-orange">
                        <i class="icon-check"></i>
                    </div>
                    <div class="title">
                        <span>Total<br>Sales</span>
                        <div class="progress">
                            <div role="progressbar" style="width: 50%; height: 4px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-orange">
                            </div>
                        </div>
                    </div>
                    <div class="number">
                        <strong data-bind="text:'Rs ' +total_sales()"></strong>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-6">
                <div class="item d-flex align-items-center">
                    <div class="icon bg-violet">
                        <i class="icon-padnote"></i>
                    </div>
                    <div class="title">
                        <span>Available<br>Balance</span>
                        <div class="progress">
                            <div role="progressbar" style="width: 25%; height: 4px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" class="progress-bar bg-violet">
                            </div>
                        </div>
                    </div>
                    <div class="number">
                        <strong data-bind="text:'Rs ' + total_balance()"></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="tables">               
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-close">
                    </div>
                    <div class="card-header d-flex align-items-center">
                        <h3 class="h4">Summary</h3>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="orderHistoryList">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Person Name</th>
                                        <th>Offer Name</th>
                                        <th>Coupon Value</th>
                                        <th>Coupon Code</th>
                                        <th>Final Bill</th>
                                        <th>Issue Date</th>         
                                        <th>Redeem Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <!-- ko  foreach:orderhistory --> 
                                    <tr>
                                        <th scope="row" data-bind="text:($index()+1)"></th>
                                        <td data-bind="text: first_name() +' '+ last_name()"></td>
                                        <td><a href="#" data-bind="click:$root.viewOfferClick"><span data-bind="text: offer_name()"></span></a></td>
                                        <td data-bind="text: 'Rs ' +coupen_value()"></td>
                                        <td data-bind="text: coupon()"></td>
                                        <td data-bind="text: 'Rs ' +bill_amount()"></td>
                                        <td data-bind="text: issue_date()"></td>
                                        <td data-bind="text: redeem_date()"></td>
                                    </tr>
                                  <!-- /ko-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('script')
    <script type="text/javascript">
        var path ="{{URL::to('/')}}"+"/uploads/brand/";
        var brandOrderHistoryApiPath = "{{config('constant.api.reward.brandorderhistory')}}";
        var brandStatisticsApi = "{{config('constant.api.brand.statistics')}}";
        var offerDetailApi = "{{config('constant.api.offer.list')}}";
        var addressListApiPath ="{{config('constant.api.country.address')}}";
    </script>
    <script type="text/javascript" src="{{config('constant.script.datatables.jquery')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.datatables.bootstrap')}}"></script>
    <script type="text/javascript" src="{{config('constant.script.reward.orderhistory')}}"></script>
@endsection
@endsection