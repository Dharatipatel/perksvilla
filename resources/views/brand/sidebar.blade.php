<style type="text/css">
    nav.side-navbar a i{
        width: 25px;
    }
</style>
<!-- Side Navbar -->
<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            @if(file_exists("uploads/brand/".Session::get('bid').".png"))
            <img src="{{URL::to('/')}}/uploads/brand/{{ Session::get('bid')}}.png?t={{time()}}" alt="Perksvilla" class="img-fluid rounded-circle">
            @else
            <img src="{{URL::to('/')}}/uploads/brand/NoLogo.png?t={{time()}}" alt="Perksvilla" class="img-fluid rounded-circle">
            @endif
        </div>
        <div class="title">
            <h1 class="h4">{{Session::get('brand_name')}}</h1>
        </div>
    </div>
    <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
    <ul class="list-unstyled">
        <li class=" has-submenu" id="dashboardTab">
            <a href="{{route('dashboard')}}">
                <i class="icon-home"></i>Dashboard 
            </a>
        </li>
        <li class="has-submenu" id="offerTab">
            <a href="#myofferdropdown" aria-expanded="false" data-toggle="collapse">
                <i class="icon-interface-windows"></i>My Offers
            </a>
            <ul id="myofferdropdown" class="collapse list-unstyled ">
                <li id="addOfferTab">
                    <a href="{{route('addoffer')}}">Create Offer</a>
                </li>
                <li id="listOfferTab">
                    <a href="{{route('brandoffer')}}">Offers Listing</a>
                </li>
                <li id="pastOfferTab">
                    <a href="{{route('pastoffer')}}">Past Offers</a>
                </li>
            </ul>
        </li>
        <li class="has-submenu" id="couponTab">
            <a href="{{route('coupon')}}">
                <i class="icon-grid"></i>Redeem Coupon
            </a>
        </li>	
        <li class="has-submenu" id="orderHistoryTab">
            <a href="{{route('orderhistory')}}">
                <i class="fa fa-bar-chart"></i>Order History
            </a>
        </li>
        <li class="has-submenu" id="walletTab">
            <a href="{{route('brandwallet')}}">
                <i class="fa fa-money" aria-hidden="true"></i>My Wallet
            </a>
        </li>
        <li class="has-submenu" id="profileTab">
            <a href="#myprofiledropdown" aria-expanded="false" data-toggle="collapse">
                <i class="icon-user"></i>My Profile</a>
            <ul id="myprofiledropdown" class="collapse list-unstyled ">
                <li id="viewProfileTab">
                    <a href="{{route('viewprofile')}}">View Profile</a>
                </li>
                <li id="editProfileTab">
                    <a href="{{route('editprofile')}}">Edit Profile</a>
                </li>
                <li id="changePasswordTab">
                    <a href="{{route('changepassword')}}">Change Password</a>
                </li>
                <li id="bankDetailTab">
                    <a href="{{route('bankdetails')}}">Bank Details</a>
                </li>
            </ul>
        </li>
        <li class="has-submenu" id="supportTab">
            <a href="#messageDropdown" aria-expanded="false" data-toggle="collapse">
                <i class="fa fa-life-saver"></i>Support
            </a>
            <ul id="messageDropdown" class="collapse list-unstyled ">
                <li id="addMessageTab">
                    <a href="{{route('brand.support.manage')}}">Create Message</a>
                </li>
                <li id="listMessageTab">
                    <a href="{{route('brand.support')}}">Message List</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>
