<li class="nav-item dropdown"> <a id="notifications" rel="nofollow" @if($count) onclick="readNotification()" @endif data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell-o"></i>
    @if($count)
        <span class="badge bg-red badge-corner">{{$count}}</span>
    @endif
  <ul aria-labelledby="notifications" class="dropdown-menu col-xs-12">
    @if(count($notification))
        @foreach($notification as $n) 
            <li>
                <a rel="nofollow" href="javascript:void(0)" class="dropdown-item">
                    <div class="notification">
                        <div class="notification-content">
                            <i class="fa fa-envelope bg-green" style="vertical-align: top;"></i>
                            <span>{!! $n->message !!}</span>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach
    @else
        <li>
            <a rel="nofollow" href="#" class="dropdown-item">
                <div class="notification">
                    <div class="notification-content">
                        <i class="fa fa-envelope bg-green"></i>
                        No new notifications
                    </div>
                </div>
            </a>
        </li>
    @endif           
  </ul>
</li>