@extends('brand.master')
@section('css')
<style type="text/css">
    #txtEmailPhone-error{
        bottom: -38px;
    }
    #txtCoupenCode-error{
        bottom: -20px;
    }
</style>
@endsection
@section("content")		
    <header class="page-header">
        <div class="container-fluid">
            <h2 class="no-margin-bottom">Redeem Coupon</h2>
        </div>
    </header>
    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLongTitle">Offer Detail</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                 <div class="modal-body">
                <div class="container-fluid">
                @include('popup')
              </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>  
	<section class="forms"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">
                    <div class="col-lg-12">                           
                        <div class="card">
                            <div class="card-close">
                            </div>
                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4">Enter Details</h3>
                            </div>
                            <div class="card-body">
                                <form class="form-inline" id="coupen_form">
                                    <div class="form-group" style="position: relative;">
                                        <label for="txtCoupenCode" class="sr-only">Name</label>
                                        <input type="text" name="txtCoupenCode" id="txtCoupenCode" data-bind="value: coupenCode" placeholder="Enter Code" class="mr-3 form-control">
                                    </div>
                                    <div class="form-group" style="position: relative;">
                                        <label for="txtEmailPhone" class="sr-only">Username</label>
                                        <input name="txtEmailPhone" id="txtEmailPhone" type="text" data-bind="value: emailPhone" placeholder="Phone/Email" class="mr-3 form-control">
                                    </div>
                                    
                                    <div class="form-group" style="margin-top: 30px;">
                                        <button type="button" data-bind="click:searchCoupenDetails" class="btn btn-primary">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    				        <!-- Form Elements -->
                    <div class="col-lg-12" data-bind="visible:isCouponAvailble">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4">Coupon Details</h3>
                            </div>
                            <div class="card-body">
                                <form class="form-horizontal">
                                    <div class="form-group row">
                                        <label class="col-sm-3 form-control-label">Issued By</label>
                                        <div class="col-sm-9">
                                            <input type="text" disabled="" data-bind="value: issued_by" placeholder="Company Name" class="form-control">
                                        </div>
                                    </div>
                        						<div class="form-group row">
                                        <label class="col-sm-3 form-control-label">Issued For</label>
                                        <div class="col-sm-9">
                                            <input type="text" disabled="" data-bind="value: issued_for" placeholder="Person Name" class="form-control">
                                        </div>
                                    </div>
                        						<div class="form-group row">
                                        <label class="col-sm-3 form-control-label">Issued Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" disabled="" data-bind="value: issued_date_show" placeholder="15 Feb 2018" class="form-control">
                                        </div>
                                    </div>
                        						<div class="form-group row">
                                        <label class="col-sm-3 form-control-label" data-bind="text:expiry_label"></label>
                                        <div class="col-sm-9">
                                            <input type="text" disabled="" data-bind="value: expire_date_show" placeholder="30 Mar 2018" class="form-control">
                                        </div>
                                    </div>
                        		    <div class="form-group row">
                                        <label class="col-sm-3 form-control-label">Coupon Value</label>
                                        <div class="col-sm-9">
                                          <input type="text" disabled=""  data-bind="value: coupon_value_show" placeholder="Rs 1250" class="form-control">
                                    </div>
                                    <button style="margin-left: 10px;" type="button" data-bind="click:viewCoupenDetails,visible:isCouponAvailble" class="btn btn-primary">View Offer</button>
                                    </div>
                                <!--<div class="line"></div>-->
                                </form>
                            </div>
                        </div>
                    </div>
    				
                    <!-- Form Elements -->
                    <div class="col-lg-12" data-bind="visible:isCouponAvailble">
                        <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4">Redeem Now</h3>
                            </div>
                            <div class="card-body">
                            <form class="form-horizontal" id="redeem_form">
                                <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Coupon Code</label>
                                    <div class="col-sm-9">
                                        <input type="text" disabled="" placeholder="XUR32GK9" data-bind="value: coupenCode" name ="txtRedeemCoupenCode" class="form-control">
                                    </div>
                                </div>
        						            <div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Bill Amount</label>
                                    <div class="col-sm-9">
                                        <input type="text" name ="txtBillAmount" id ="txtBillAmount" class="form-control"  data-bind="value: billAmount,valueUpdate:'afterkeydown',event:{'keyup':billAmountChange}"><small class="help-block-none text-danger">Customer Needs to Pay Rs <span data-bind="text:customerPayble"></span></small>
                                  </div>
                                </div>
        						<div class="form-group row">
                                    <label class="col-sm-3 form-control-label">Total Members</label>
                                    <div class="col-sm-9">
                                        <input type="text" data-bind="value: member"  name ="txtMember" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row" data-bind="visible:redeemVisibility">
                                    <label class="col-sm-3 form-control-label">Enter OTP</label>
                                    <div class="col-sm-9">
                                        <input type="text" data-bind="value: otp"  name ="txtOtp" id ="txtOtp" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-4 offset-sm-3" data-bind="visible:sendOtpVisibility">
                                        <button type="button" class="btn btn-primary" data-bind="click:sendOtp">Send Otp</button>
                                    </div>
                                </div>
                                <div class="form-group row" data-bind="visible:redeemVisibility">
                                    <div class="col-sm-4 offset-sm-3">
                                        <button type="button" class="btn btn-primary" data-bind="click:redeemCoupen">Redeem</button>
                                        <a href="#" data-bind="click:sendOtp"><span>Resend OTP</span></a>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12" data-bind="visible:isNoCouponAvailble">
                          <div class="card">
                            <div class="card-header d-flex align-items-center">
                                <h3 class="h4 text-danger" data-bind="text:errorText"></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card">
                        <div class="card-close">
                        </div>
                        <div class="card-header d-flex align-items-center">
                            <h3 class="h4">Download Mobile App</h3>
                        </div>
                        <div class="card-body">
                            <p>
                                You don’t have to visit website every time to redeem the coupons. Use our simple & interactive Mobile App for verifying & redeeming customer coupons. Visit App store link given below to download Android & iOS App. 
                            </p>
                            <div>
                                <a href="https://play.google.com/store/apps/details?id=com.perksvillabrandapp" target="_blank"><img class="img-responsive" src="{{ url('/') }}/assets/brand/img/badge_new.png"></a>
                                <a href="" target="_blank"><img class="img-responsive" src="{{ url('/') }}/assets/brand/img/ios-app.svg" style="width:153px;height:46px"></a>
                            </div>
                            <p class="text-danger" style="margin-top: 15px;">PS: Use your current credentials to login into the app</p>
                        </div>
                    </div>
                </diV>
              </div>
        </div>
    </section>
    @section("script")
        <script type="text/javascript">
            var couponDetailApiPath = "{{config('constant.api.coupon.coupon')}}";
            var redeemCouponApiPath = "{{config('constant.api.coupon.redeem')}}";
            var sendOtpApiPath = "{{config('constant.api.coupon.otp')}}";
            var couponWebPath = "{{route('coupon')}}";
            var orderWebPath = "{{route('orderhistory')}}";
            var addressListApiPath ="{{config('constant.api.country.address')}}";
            var path ="{{URL::to('/')}}"+"/uploads/brand/";
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.coupon.coupon')}}"></script>
    @endsection
@endsection