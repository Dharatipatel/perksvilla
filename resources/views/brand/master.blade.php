<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Brand Dashboard</title>
        <meta name="description" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{config('brand_constant.css.bootstrap')}}">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="{{config('brand_constant.css.fontawesome')}}">

        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="{{config('brand_constant.css.fontastic')}}">

        <!-- Google fonts - Poppins -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{config('brand_constant.css.theme')}}">

        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="{{config('brand_constant.css.custom')}}">

        <!-- Favicon-->
        <link rel="shortcut icon" href="{{config('brand_constant.image.favicon')}}">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link rel="stylesheet" type="text/css" href="{{config('admin_constant.css.toastr')}}">
        <link  rel="stylesheet" type="text/css" href="{{config('admin_constant.css.sweetalert')}}" />
        <script>
            var baseURL = "{{url('/').'/'}}";
            var _token = "{{csrf_token()}}";
        </script>
        @yield('css')
    </head>
    <body>
        <div class="page">
            @include('brand.header')
            <div class="page-content d-flex align-items-stretch"> 
                @include('brand.sidebar')
                <div class="content-inner">
                    @yield("content")
                    <footer class="main-footer">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <p>&copy; 2018 - All Rights Reserved by PerksVilla</p>
                                </div>   
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
        <!-- JavaScript files-->
        <script type="text/javascript">
            var logoutUrl = "{{route('logout')}}";
            var id = "{{ Session::get('id')}}";
            var bid = "{{ Session::get('bid')}}";
            var token = "{{ Session::get('token')}}";
            var static_image = "NoLogo.png";
            var notification_url = "{{route('brand.notification')}}";
            var notification_read_url = "{{route('brand.notification.read')}}";
            var message_url = "{{route('brand.support.count')}}";
            var notification_update_interval = "{{env('NOTIFICATION_UPDATE_INTERVAL_BRAND')}}"; //  seconds
        </script>
        <script type="text/javascript" src="{{config('brand_constant.script.jquery')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.popper')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.bootstrap')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.jquerycookie')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.chart')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.validate')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.toastr')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalert')}}"></script>
        <script type='text/javascript' src="{{config('admin_constant.script.sweetalertinit')}}"></script> 
        <script type="text/javascript" src="{{config('admin_constant.script.knockout')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.front')}}"></script>
        <script type="text/javascript" src="{{config('admin_constant.script.moment')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.ko-model')}}"></script>
        <script type="text/javascript" src="{{config('constant.script.common.helper')}}"></script>
        <script type="text/javascript" src="{{config('brand_constant.script.notification')}}"></script>
        @yield("script")
    </body>
</html>