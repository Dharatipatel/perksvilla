<!-- Main Navbar-->
<header class="header">
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
                <!-- Navbar Header-->
                <div class="navbar-header">
                    <!-- Navbar Brand -->
                    <a href="{{route('dashboard')}}" class="navbar-brand">
                        <div class="brand-text brand-big">
                            <span>Brand </span>
                            <strong> Dashboard</strong>
                        </div>
                        <div class="brand-text brand-small">
                            <strong>BD</strong>
                        </div>
                    </a>
                    <!-- Toggle Button-->
                    <a id="toggle-btn" href="#" class="menu-btn active">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <!-- Navbar Menu -->
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
    	            <!-- My Profile -->
    	            <li class="nav-item dropdown">
                        <a id="languages" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link language dropdown-toggle">
                            <span class="d-none d-sm-inline-block">My Profile</span>
                        </a>
                        <ul aria-labelledby="languages" class="dropdown-menu">
                            <li><a rel="nofollow" href="{{route('viewprofile')}}" class="dropdown-item"> View Profile</a></li>
                            <li><a rel="nofollow" href="{{route('editprofile')}}" class="dropdown-item"> Edit Profile</a></li>
                            <li><a rel="nofollow" href="{{route('changepassword')}}" class="dropdown-item"> Change Password</a></li>
    		                    <li><a rel="nofollow" href="{{route('bankdetails')}}" class="dropdown-item"> Bank Details</a></li>
                        </ul>
                    </li>
                    <!-- Notification -->
                    <li class="nav-item dropdown"  id="top_header_container"></li>
                    <!-- Messages-->
                    <li class="nav-item dropdown">
                        <a id="messages" rel="nofollow" href="{{route('brand.support')}}" aria-haspopup="true" aria-expanded="false" class="nav-link">
                            <span id="messages_count"></span>
                            <i class="fa fa-envelope-o"></i>
                        </a>
                    </li>
                    <!-- Logout -->
                    <li class="nav-item">
                        <a href="{{route('logout')}}" class="nav-link logout">
                            Logout
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
    