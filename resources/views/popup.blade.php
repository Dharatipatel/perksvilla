
<style type="text/css">
.modal-dialog{max-width: 650px;}
.modal-body{font-size:13px;}
.modal-body h4{font-size:16px}
ul.dialog{margin:20px 0 0px; padding:0}
ul.dialog li{list-style:none; float:left; font-size:16px; display:inline-block; padding:0px 10px; font-weight:bold; color:#000; line-height:18px}
ul.dialog li:first-child {padding-left:0}
ul.dialog li:last-child {border:none;}
.brder-right{border-right:solid 1px #ccc;}

.mygrey-bg{background:#eee; border-top:solid 2px #ccc; padding:20px 0}
.myterms-cond{font-size:12px;}
.m-b-50{margin-bottom:50px}
.m-t-30{margin-top:30px}
ul.dialog li:not(:first-child){
    border-left:solid 2px #000;
}
</style>
<div class="row">
    <div class="col-md-3 product_img">
        <img data-bind="attr:{src:brand_logo}" class="img-responsive" width="100" height="100" /> 
    </div>
    <div class="col-md-9 product_content">
        <h4 data-bind="text:offer_title"></h4>
        <p  data-bind="text:offer_description"></p>
        <p>
            <!-- ko if:facebook_page() != "" -->
            <a base  style="margin-right: 15px" target="_blank" data-bind="attr: { href: facebook_page() }">
                <img src="{{config('constant.image.facebook')}}" />
            </a>
            <!-- /ko -->
            <!-- ko if:website_url() != "" -->
            <a base  style="margin-right: 15px" target="_blank"  data-bind="attr: { href: website_url() }">
                <img src="{{config('constant.image.website')}}" />
            </a>
            <!-- /ko -->
            
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ul class="dialog">
            <li data-bind="text:'Coupon Value: Rs '+coupon_value()"></li>
            <li data-bind="text:'Valid Till: '+valid_to()"></li>
            <li data-bind="text:'Coupon Code: '+coupon_code(),visible:isCoupon"></li>
        </ul>
    </div>
</div>
<div class="row m-t-30">
    <div class="col-sm-6 brder-right">
        <h4 >Applicable On:
            <small data-bind="text:applicable_on"></small>
        </h4>
        <h4>Applicable At:</h4>
        <!-- ko foreach: selected_address -->
            <p data-bind="text: 'Outlet '+($index()+1)+': ' + $data"></p>
        <!-- /ko -->
        <h4>Business Hours:</h4>
        <p data-bind="text:business_hours"></p>
        <!-- ko if:contact() != null -->
            <span>
                <img src="{{config('constant.image.call')}}" style="margin-right: 5px;" />
                <b><span data-bind="text:contact()"></span></b>
            </span>
        <!-- /ko -->
    </div>
    <div class="col-sm-6">
        <h4>Important Notes</h4>
        <span data-bind="html:notes"></span>
    </div>
</div>