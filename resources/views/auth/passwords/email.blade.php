@extends('velonic.login_master')
@section("title")
Forgot Password - 
@stop
@section("content")
<div class="wrapper-page animated fadeInDown">
    <div class="panel panel-color panel-primary">
        <div class="panel-heading"> 
            <h3 class="text-center m-t-10"> Forgot Password </h3>
        </div> 
        @if($errors->has('reset'))
        <div class="col-md-12 alert alert-danger alert-dismissable"><p>{{ $errors->first('reset') }}</p></div>
        @endif  
        @if(session()->has('success'))
        <div class="col-md-12 alert alert-success alert-dismissable"><p>{{ session()->get('success') }}</p></div>
        @endif  
        @if(session()->has('status'))
        <div class="col-md-12 alert alert-success alert-dismissable"><p>{{ session()->get('status') }}</p></div>
        @endif 
        {!! Form::open(["url"=>url('/password/email'),"method"=>"POST", 'id'=>'login_form', 'class'=>"form-horizontal m-t-40"]) !!}
        {{ csrf_field() }}
        <div class="form-group ">
            <div class="col-xs-12">
                <label>After submitting your email, you will get an email with password reset link. Please check your email.</label>
                {{ Form::email('email',old("email"),['class'=>'form-control','placeholder'=>'E-mail address','id'=>"email", "autofocus"=>true]) }}
                @if($errors->has("email"))
                <span class="text-danger flash">{{ $errors->first("email") }}</span>
                @endif
            </div>
        </div>
        <div class="form-group text-right">
            <div class="col-xs-12">
                <button class="btn btn-purple w-md" type="submit">Send Password Reset Link</button>
            </div>
        </div>
        <div class="form-group m-t-30">
            <div class="col-sm-12">
                <a href="{{ url('login') }}">Password remembered?</a>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection