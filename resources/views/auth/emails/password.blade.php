<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"> <!-- utf-8 works for most cases -->
            <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
                <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
                    <title></title>
                    <style type="text/css">

                        /* What it does: Remove spaces around the email design added by some email clients. */
                        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
                        html,
                        body {
                            Margin: 0 !important;
                            padding: 0 !important;
                            height: 100% !important;
                            width: 100% !important;
                        }
.admin-logo{width: 200px}

.admin-logo img{max-width: 150px}
                        /* What it does: Stops email clients resizing small text. */
                        * {
                            -ms-text-size-adjust: 100%;
                            -webkit-text-size-adjust: 100%;
                        }

                        /* What it does: Forces Outlook.com to display emails full width. */
                        .ExternalClass {
                            width: 100%;
                        }

                        /* What is does: Centers email on Android 4.4 */
                        div[style*="margin: 16px 0"] {
                            margin:0 !important;
                        }

                        /* What it does: Stops Outlook from adding extra spacing to tables. */
                        table,
                        td {
                            mso-table-lspace: 0pt !important;
                            mso-table-rspace: 0pt !important;
                        }

                        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
                        table {
                            border-spacing: 0 !important;
                            border-collapse: collapse !important;
                            table-layout: fixed !important;
                            Margin: 0 auto !important;
                        }
                        table table table {
                            table-layout: auto; 
                        }

                        /* What it does: Uses a better rendering method when resizing images in IE. */
                        img {
                            -ms-interpolation-mode:bicubic;
                        }

                        /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
                        .yshortcuts a {
                            border-bottom: none !important;
                        }

                        /* What it does: A work-around for iOS meddling in triggered links. */
                        .mobile-link--footer a,
                        a[x-apple-data-detectors] {
                            color:inherit !important;
                            text-decoration: underline !important;
                        }
.email-container tabel{border: 0 !important}
                    </style>

                    <!-- Progressive Enhancements -->
                    <style>
.admin-logo{    color: #fcc814;

    font-size: 24px;
    padding: 0 10px;
    font-weight: 900;
    text-transform: uppercase;
     text-decoration: none;}
                        /* What it does: Hover styles for buttons */
                        .button-td,
                        .button-a {
                            transition: all 100ms ease-in;
                        }

                        /* Media Queries */
                        @media screen and (max-width: 600px) {

                            .email-container {
                                width: 100% !important;
                            }

                            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
                            .fluid,
                            .fluid-centered {
                                max-width: 100% !important;
                                height: auto !important;
                                Margin-left: auto !important;
                                Margin-right: auto !important;
                            }
                            /* And center justify these ones. */
                            .fluid-centered {
                                Margin-left: auto !important;
                                Margin-right: auto !important;
                            }

                            /* What it does: Forces table cells into full-width rows. */
                            .stack-column,
                            .stack-column-center {
                                display: block !important;
                                width: 100% !important;
                                max-width: 100% !important;
                                direction: ltr !important;
                            }
                            /* And center justify these ones. */
                            .stack-column-center {
                                text-align: center !important;
                            }

                            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
                            .center-on-narrow {
                                text-align: center !important;
                                display: block !important;
                                Margin-left: auto !important;
                                Margin-right: auto !important;
                                float: none !important;
                            }
                            table.center-on-narrow {
                                display: inline-block !important;
                            }

                        }

                    </style>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
                    </head>
                    <body bgcolor="#7a29fe" width="100%" style="Margin: 0;     font-family: 'Open Sans', sans-serif;">
                        <table bgcolor="#013c78" cellpadding="0" cellspacing="0" border="0" height="100%" width="100%" style="border-collapse:collapse;"><tr><td valign="top">
                                    <center style="width: 100%;">

                                        
                                        <table align="center" width="680" class="email-container" style="-webkit-box-shadow: 0px 0px 40px 0px rgba(0,0,0,0.5);
-moz-box-shadow: 0px 0px 40px 0px rgba(0,0,0,0.5);
box-shadow: 0px 0px 40px 0px rgba(0,0,0,0.5); background: #fff; margin-top: 40px !important;">
                                            <tr>
                                                <td style="padding: 20px 0; text-align: center">
                                                    <a href="{{url("/")}}" class="admin-logo" >{{env("PROJECT_NAME")}}</a>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Email Header : END -->
                                        <!-- Email Body : BEGIN -->
                                        <table cellspacing="0" cellpadding="100" border="0" align="center" bgcolor="#ffffff" width="680" class="email-container">

                                            <tr>
                                                <td>
                                                    Click here to reset your password: <a href="{{ $link = url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}"> {{ $link }} </a>
                                                </td>
                                            </tr>

                                            <!-- 1 Column Text : BEGIN -->
                                            <tr>
                                                <td style="padding: 10px 40px; text-align:center;  font-size: 16px; mso-height-rule: exactly; line-height: 25px; color: #2A3F54;">
                                                    Kind Regards, <br />
                                                    <span style="font-weight:bold; color: #fcc814">The {{ env("PROJECT_NAME") }} Team</span>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- Email Body : END -->

                                        <!-- Email Footer : BEGIN -->
                                        <table align="center" width="680" class="email-container">
                                            <tr>
                                                <td style="padding: 40px 10px;width: 100%;font-size: 12px;  mso-height-rule: exactly; line-height:18px; text-align: center; color: #fff;">
                                                    <br><br>
                                                            {{ env("PROJECT_NAME") }}
                                                            </td>
                                                            </tr>
                                                            </table>
                                                            <!-- Email Footer : END -->

                                                            </center>
                                                            </td></tr></table>
                                                            </body>
                                                            </html>

