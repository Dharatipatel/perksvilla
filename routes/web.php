<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Route::get('/', function () {
//     return view('welcome');
// });
Route::namespace('Web\brand')->group(function () {
    Route::get('brand/login', ['as' => 'brandlogin', 'uses' => 'UserController@login']);
    Route::get('brand/forgot-password', ['as' => 'forgotpassword', 'uses' => 'UserController@forgotPassword']);
});

Route::namespace('Web\corporate')->group(function () {
    Route::get('corporate/login', ['as' => 'corporatelogin', 'uses' => 'UserController@login']);
    Route::get('corporate/forgot-password', ['as' => 'corporateforgotpassword', 'uses' => 'UserController@forgotPassword']);
});
Route::namespace('Web\corporate')->middleware('CheckSessionCorporate')->prefix('corporate')->group(function () {

    Route::get('dashboard', ['as' => 'corporatedashboard', 'uses' => 'DashboardController@index']);
    Route::get('perksvilla', ['as' => 'perksvilla', 'uses' => 'RewardController@index']);
    Route::get('orderhistory', ['as' => 'corporateorderhistory', 'uses' => 'RewardController@orderHistory']);
    Route::prefix('profile')->group(function () {
        Route::get('edit', ['as' => 'corporateeditprofile', 'uses' => 'UserController@editProfile']);
        Route::get('view', ['as' => 'corporateviewprofile', 'uses' => 'UserController@viewProfile']);
        Route::get('changepassword', ['as' => 'corporatechangepassword', 'uses' => 'UserController@changePassword']);
    });
    Route::prefix('employee')->group(function () {
        Route::get('/', ['as' => 'employee', 'uses' => 'EmployeeController@employee']);
        Route::get('add', ['as' => 'addemployee', 'uses' => 'EmployeeController@addEmployee']);
    });
    Route::prefix('reward')->group(function () {
        Route::get('/', ['as' => 'reward', 'uses' => 'RewardController@reward']);
        Route::get('add', ['as' => 'addreward', 'uses' => 'RewardController@addReward']);
        Route::get('success', ['uses' => 'RewardController@success', 'as' => 'reward.success']);
        Route::post('fail', ['uses' => 'RewardController@fail', 'as' => 'reward.fail']);
    });
    /*  Support  */
    Route::group(["prefix" => "message"], function () {
        Route::get('/', ['uses' => 'SupportController@index', 'as' => "corporate.support"]);
        Route::get('add', ['uses' => 'SupportController@getManage', 'as' => "corporate.support.manage"]);
        Route::get('reply/{id?}', ['uses' => 'SupportController@replyShow', 'as' => "corporate.support.reply"]);
        Route::get('delete/{id}', ['uses' => 'SupportController@delete', 'as' => "corporate.support.delete"]);
        Route::get('lists', ['uses' => 'SupportController@lists', 'as' => "corporate.support.lists"]);
        Route::post('save', ['uses' => 'SupportController@save', 'as' => "corporate.support.save"]);
        Route::post('reply/save', ['uses' => 'SupportController@replySave', 'as' => "corporate.support.reply.save"]);
        Route::get('count', ['uses' => 'SupportController@supportMessageCount', 'as' => "corporate.support.count"]);
    });

    Route::post('notification', ['as' => 'corporate.notification', 'uses' => 'UserController@notification']);
    Route::post('notification/read', ['as' => 'corporate.notification.read', 'uses' => 'UserController@notificationRead']);
});

Route::namespace('Web\brand')->middleware('CheckSessionBrand')->prefix('brand')->group(function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('coupon', ['as' => 'coupon', 'uses' => 'CouponController@index']);
    Route::get('orderhistory', ['as' => 'orderhistory', 'uses' => 'OrderHistoryController@index']);
    Route::post('notification', ['as' => 'brand.notification', 'uses' => 'UserController@notification']);
    Route::post('notification/read', ['as' => 'brand.notification.read', 'uses' => 'UserController@notificationRead']);

    Route::get('wallet', ['as' => 'brandwallet', 'uses' => 'UserController@myWallet']);

    Route::prefix('offer')->group(function () {
        Route::get('/', ['as' => 'brandoffer', 'uses' => 'OfferController@index']);
        Route::get('add', ['as' => 'addoffer', 'uses' => 'OfferController@addOffer']);
        Route::get('past', ['as' => 'pastoffer', 'uses' => 'OfferController@pastOffer']);
    });

    Route::prefix('profile')->group(function () {
        Route::get('edit', ['as' => 'editprofile', 'uses' => 'UserController@editProfile']);
        Route::get('view', ['as' => 'viewprofile', 'uses' => 'UserController@viewProfile']);
        Route::get('changepassword', ['as' => 'changepassword', 'uses' => 'UserController@changePassword']);
        Route::get('wallet', ['as' => 'wallet', 'uses' => 'UserController@myWallet']);
        Route::get('bankdetails', ['as' => 'bankdetails', 'uses' => 'UserController@bankDetails']);
    });
    /*  Support  */
    Route::group(["prefix" => "message"], function () {
        Route::get('/', ['uses' => 'SupportController@index', 'as' => "brand.support"]);
        Route::get('add', ['uses' => 'SupportController@getManage', 'as' => "brand.support.manage"]);
        Route::get('reply/{id?}', ['uses' => 'SupportController@replyShow', 'as' => "brand.support.reply"]);
        Route::get('delete/{id}', ['uses' => 'SupportController@delete', 'as' => "brand.support.delete"]);
        Route::get('lists', ['uses' => 'SupportController@lists', 'as' => "brand.support.lists"]);
        Route::post('save', ['uses' => 'SupportController@save', 'as' => "brand.support.save"]);
        Route::post('reply/save', ['uses' => 'SupportController@replySave', 'as' => "brand.support.reply.save"]);
        Route::get('count', ['uses' => 'SupportController@supportMessageCount', 'as' => "brand.support.count"]);
    });
});

Route::namespace('Web\admin')->group(function () {
    Route::get('backoffice/login',['as' => 'adminlogin', 'uses' => 'UserController@login']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
    Route::post('setsession', 'UserController@setSession');
});

Route::namespace('Web\admin')->middleware('CheckSession')->prefix('admin')->group(function () {
    Route::prefix('category')->group(function () {
        Route::get('brand', ['as' => 'brandcategory', 'uses' => 'CategoryController@brand']);
        Route::get('corporate', ['as' => 'employeecategory', 'uses' => 'CategoryController@employee']);
        Route::get('reward', ['as' => 'rewardcategory', 'uses' => 'CategoryController@reward']);
    });
    Route::prefix('location')->group(function () {
        Route::get('country', ['as' => 'country', 'uses' => 'LocationController@country']);
        Route::get('state', ['as' => 'state', 'uses' => 'LocationController@state']);
        Route::get('city', ['as' => 'city', 'uses' => 'LocationController@city']);
    });
    Route::prefix('brand')->group(function () {
        Route::get('add', ['as' => 'brandadd', 'uses' => 'BrandController@addBrand']);
        Route::get('list', ['as' => 'brandlist', 'uses' => 'BrandController@listBrand']);
        Route::get('payment/request', ['as' => 'brandpayment', 'uses' => 'BrandController@paymentRequestBrand']);
    });
    Route::prefix('corporate')->group(function () {
        Route::get('list', ['as' => 'corporatelist', 'uses' => 'CorporateController@listCorporate']);
        Route::get('add', ['as' => 'corporateadd', 'uses' => 'CorporateController@addCorporate']);
        Route::get('order/history', ['as' => 'orderhistorycorporate', 'uses' => 'CorporateController@orderHistoryCorporate']);
    });
    Route::prefix('reward')->group(function () {
        Route::get('/', ['as' => 'generalreward', 'uses' => 'RewardController@getRewards']);
        Route::get('usercreated', ['as' => 'usercreatedreward', 'uses' => 'RewardController@getRewards']);
    });
    Route::prefix('offer')->group(function () {
        Route::get('pending', ['as' => 'offerpending', 'uses' => 'OfferController@offer']);
        Route::get('active', ['as' => 'offeractive', 'uses' => 'OfferController@offer']);
        Route::get('upcoming', ['as' => 'offerupcoming', 'uses' => 'OfferController@offer']);
        Route::get('past', ['as' => 'offerpast', 'uses' => 'OfferController@offer']);
        Route::get('reject', ['as' => 'offerreject', 'uses' => 'OfferController@offer']);
    });
    Route::prefix('coupon')->group(function () {
        Route::get('active', ['as' => 'activecoupon', 'uses' => 'CouponController@coupon']);
        Route::get('used', ['as' => 'usedoupon', 'uses' => 'CouponController@coupon']);
        Route::get('expired', ['as' => 'expiredcoupon', 'uses' => 'CouponController@coupon']);
    });
    Route::prefix('sms-templates')->group(function () {
        Route::get('/', ['as' => 'sms.templates', 'uses' => 'SMSTemplateController@index']);
        Route::get('add', ['as' => 'sms.templates.add', 'uses' => 'SMSTemplateController@index']);
    });
    Route::get('employee', ['as' => 'employeeadmin', 'uses' => 'EmployeeController@employee']);
    Route::get('dashboard', ['as' => 'admindashboard', 'uses' => 'DashboardController@index']);
    /*  mail template   */
    Route::prefix('profile')->group(function () {
        Route::get('edit', ['as' => 'adminprofile', 'uses' => 'UserController@editProfile']);
        Route::get('changepassword', ['as' => 'adminchangepassword', 'uses' => 'UserController@changePassword']);
    });
    Route::group(['prefix' => 'mailtemplates'], function () {

        Route::get('/', ['uses' => 'MailtemplateController@index', 'as' => 'admin.mailtemplates.index']);
        Route::get('create', ['uses' => 'MailtemplateController@create', 'as' => 'admin.mailtemplates.create']);
        Route::post('store', ['uses' => 'MailtemplateController@store', 'as' => 'admin.mailtemplates.save']);
        Route::get('list_mailtemplates', ['uses' => 'MailtemplateController@get_list', 'as' => 'admin.mailtemplates.list']);
        Route::get('edit/{id}', ['uses' => 'MailtemplateController@edit', 'as' => 'admin.mailtemplates.edit']);
        Route::post('update/{id?}', ['uses' => 'MailtemplateController@store', 'as' => 'admin.mailtemplates.update']);
        Route::get('delete/{id}', ['uses' => 'MailtemplateController@delete', 'as' => 'admin.mailtemplates.delete']);
        Route::get("preview/{mail_id}", ["uses" => "MailtemplateController@preview", "as" => "admin.mailtemplates.preview"]);
    });

    /*  Support */
    Route::group(['prefix' => 'message'], function () {

        Route::get('open', ['uses' => 'SupportController@open', 'as' => 'support.open']);
        Route::get('closed', ['uses' => 'SupportController@closed', 'as' => 'support.closed']);
        Route::get('lists/{status}', ['uses' => 'SupportController@lists', 'as' => "support.lists"]);
        Route::get('delete/{id}', ['uses' => 'SupportController@delete', 'as' => "support.delete"]);
        Route::get('reply/{id?}', ['uses' => 'SupportController@replyShow', 'as' => "support.reply"]);
        Route::post('reply/save', ['uses' => 'SupportController@replySave', 'as' => "support.reply.save"]);
    });
});

//  sms
Route::group(['prefix' => 'sms'], function () {
    Route::get('/', ['uses' => 'SMSController@index', 'as' => 'sms.index']);
    Route::post('send', ['uses' => 'SMSController@send', 'as' => 'sms.send']);
});

//  payUMoney
Route::group(['prefix' => 'payment'], function () {
    Route::get('/', ['uses' => 'PaymentController@pay', 'as' => 'payment.pay']);
    Route::post('success', ['uses' => 'PaymentController@success', 'as' => 'payment.success']);
    Route::post('fail', ['uses' => 'PaymentController@fail', 'as' => 'payment.fail']);
});

//  Cron
Route::group([], function () {
    Route::get('send-coupon-code', ['uses' => 'CronController@send_coupon_code', 'as' => 'cron.send.coupon.code']);
    Route::get('updateOfferStatus', ['uses' => 'CronController@updateOfferStatus', 'as' => 'cron.update.status']);
    Route::get('reminderCouponCode', ['uses' => 'CronController@reminderCouponCode', 'as' => 'cron.reminder.coupon']);
    Route::get('monthlySummary', ['uses' => 'CronController@monthlySummary', 'as' => 'cron.month.summary']);
});

Route::namespace('Auth')->prefix('brand')->group(function () {

    Route::prefix('password')->group(function () {
        Route::post('forgot', ['uses' => 'ForgotPasswordController@forgotPasswordEmail', 'as' => 'brand.password.forgot']);
        Route::get('reset/{token}', ['uses' => 'ResetPasswordController@forgotPasswordReset', 'as' => 'brand.password.reset']);
        Route::post('reset', ['uses' => 'ResetPasswordController@forgotPasswordResetSave', 'as' => 'brand.password.reset.save']);
    });
});

Route::namespace('Auth')->prefix('corporate')->group(function () {

    Route::prefix('password')->group(function () {
        Route::post('forgot', ['uses' => 'ForgotPasswordController@forgotPasswordEmail', 'as' => 'corporate.password.forgot']);
        Route::get('reset/{token}', ['uses' => 'ResetPasswordController@forgotPasswordReset', 'as' => 'corporate.password.reset']);
        Route::post('reset', ['uses' => 'ResetPasswordController@forgotPasswordResetSave', 'as' => 'corporate.password.reset.save']);
    });
});
