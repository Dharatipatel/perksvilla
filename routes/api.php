<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Api')->prefix('user')->group(function () {
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    Route::post('forgotPassword', 'UserController@forgotPassword');
});

Route::namespace('Api\admin')->prefix('employee')->middleware('AuthHeader')->group(function () {
    /* Employee */
    Route::post('/', 'EmployeeController@addEmployee');
    Route::get('/', 'EmployeeController@getEmployee');
    Route::get('dashboard', 'EmployeeController@getEmployeeDashboard');
    Route::post('update', 'EmployeeController@updateEmployee');
    Route::post('active', 'EmployeeController@activeEmployee');
    Route::post('deactive', 'EmployeeController@deactiveEmployee');
    Route::post('delete', 'EmployeeController@deleteEmployee');
    Route::post('import', 'EmployeeController@importEmployee');
    Route::post('login', 'EmployeeController@getEmployee');
});
Route::namespace('Api')->prefix('profile')->middleware('AuthHeader')->group(function () {
    Route::post('changepassword', 'UserController@changePassword');
    Route::get('view', 'UserController@viewAdminProfile');
    Route::post('update', 'UserController@updateAdminProfile');
});
Route::namespace('Api\admin')->prefix('admin')->middleware('AuthHeader')->group(function () {
    Route::get('statistics', 'AdminController@getStatistics');
});
Route::namespace('Api\admin')->prefix('brand')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'BrandController@addBrand');
    Route::get('/', 'BrandController@getBrand');
    Route::get('statistics', 'BrandController@getStatistics');
    Route::post('email/send', 'BrandController@emailSend');
    Route::get('brandoffer', 'BrandController@getBrandOffer');
    Route::post('update', 'BrandController@updateBrand');
    Route::post('remove', 'BrandController@removeBrandProfile');
    Route::post('active', 'BrandController@activeBrand');
    Route::post('deactive', 'BrandController@deactiveBrand');
    Route::post('delete', 'BrandController@deleteBrand');
    Route::post('wallet', 'BrandController@brandWallet');
    Route::get('transcation', 'BrandController@brandWalletTranscation');
    Route::get('balance', 'BrandController@getBalance');
    Route::post('paidRequest', 'BrandController@paidRequest');
    Route::get('sales', 'BrandController@getBrandTotalSales');
    Route::get('/{id}', 'BrandController@getBrandById');
    Route::post('address', 'BrandController@addBrandAddress');
    Route::post('address/update', 'BrandController@addBrandAddress');
    Route::post('address/update', 'BrandController@addBrandAddress');
});
Route::namespace('Api\admin')->prefix('bank')->middleware('AuthHeader')->group(function () {

    Route::post('/', 'BankController@addBankDetails');
    Route::post('update', 'BankController@updateBankDetails');
    Route::get('/', 'BankController@getBankDetails');
});
Route::namespace('Api\brand')->prefix('offer')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'OfferController@addOffer');
    Route::post('update', 'OfferController@updateOffer');
    Route::get('/', 'OfferController@getOffer');
    Route::post('approve', 'OfferController@approveOffer');
    Route::post('deactive', 'OfferController@deactiveOffer');
    Route::post('reject', 'OfferController@rejectOffer');
});

Route::namespace('Api\corporate')->prefix('reward')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'RewardController@addReward');
    Route::get('/', 'RewardController@getReward');
    Route::get('statistics', 'RewardController@getRewardStatistics');
    Route::get('orderhistory', 'RewardController@getOrderhistory');
    Route::get('orderhistorystatistic', 'RewardController@getOrderhistoryStatistic');
    Route::get('brandorderhistory', 'RewardController@getBrandOrderHistory');
    Route::post('update/payment/status', 'RewardController@updatePaymentStatus');
});

Route::namespace('Api\admin')->prefix('category')->middleware('AuthHeader')->group(function () {
    /* Category */
    Route::post('add', 'CategoryController@addCategory');
    Route::get('/', 'CategoryController@getCategories');
    Route::get('/{id}', 'CategoryController@getCategory');
    Route::post('update', 'CategoryController@updateCategory');
    Route::post('delete', 'CategoryController@deleteCategory');
    Route::post('deactive', 'CategoryController@deactiveCategory');
    Route::post('active', 'CategoryController@activeCategory');
});

Route::namespace('Api\admin')->prefix('corporate')->middleware('AuthHeader')->group(function () {
    /* Corporate */
    Route::get('statistics', 'CorporateController@getStatistics');
    Route::post('/', 'CorporateController@addCorporate');
    Route::get('/', 'CorporateController@getCorporate');
    Route::get('/{id}', 'CorporateController@getCorporateById');
    Route::post('update', 'CorporateController@updateCorporate');
    Route::post('remove', 'CorporateController@removeCorporateProfike');
    Route::post('active', 'CorporateController@activeCorporate');
    Route::post('deactive', 'CorporateController@deactiveCorporate');
    Route::post('delete', 'CorporateController@deleteCorporate');
    Route::post('email/send', 'CorporateController@emailSend');
});

Route::namespace('Api\admin\location')->prefix('country')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'CountryController@addCountry');
    Route::get('/', 'CountryController@getCountry');
    Route::post('update', 'CountryController@updateCountry');
    Route::post('active', 'CountryController@activeCountry');
    Route::post('deactive', 'CountryController@deactiveCountry');
    Route::post('delete', 'CountryController@deleteCountry');
    Route::get('address', 'CountryController@getBrandAddress');
});

Route::namespace('Api\admin\location')->prefix('state')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'StateController@addState');
    Route::get('/', 'StateController@getState');
    Route::get('/{id}', 'StateController@getStateById');
    Route::post('update', 'StateController@updateState');
    Route::post('active', 'StateController@activeState');
    Route::post('deactive', 'StateController@deactiveState');
    Route::post('delete', 'StateController@deleteState');
});

Route::namespace('Api\admin\location')->prefix('city')->middleware('AuthHeader')->group(function () {
    /* Brand */
    Route::post('/', 'CityController@addCity');
    Route::get('/', 'CityController@getCity');
    Route::get('/{id}', 'CityController@getCityById');
    Route::post('update', 'CityController@updateCity');
    Route::post('active', 'CityController@activeCity');
    Route::post('deactive', 'CityController@deactiveCity');
    Route::post('delete', 'CityController@deleteCity');
});

Route::namespace('Api\brand')->prefix('coupon')->middleware('AuthHeader')->group(function () {
    /* Coupen  */
    Route::post('/', 'CouponController@getCoupon');
    Route::get('list', 'CouponController@getCoupons');
    Route::post('redeem', 'CouponController@redeemCoupon');
    Route::post('sendOTP', 'CouponController@sendOTP');
});

Route::namespace('Api\admin')->prefix('sms_template')->middleware('AuthHeader')->group(function () {
    /* SMS template */
    Route::post('/', 'SMSTemplateController@addSMSTemplate');
    Route::get('/', 'SMSTemplateController@getSMSTemplate');
    Route::get('/{id}', 'SMSTemplateController@getSMSTemplateById');
    Route::post('update', 'SMSTemplateController@updateSMSTemplate');
    Route::post('active', 'SMSTemplateController@activeSMSTemplate');
    Route::post('deactive', 'SMSTemplateController@deactiveSMSTemplate');
    Route::post('delete', 'SMSTemplateController@deleteSMSTemplate');
});

/*App Api*/
Route::namespace('Api\admin')->prefix('employee')->group(function () {
    /* Employee */
    Route::post('otp/send',   'EmployeeController@sendOTP');
    Route::post('otp/verify', 'EmployeeController@verifyOTP');
    Route::middleware('EmployeeHeader')->group(function(){
        Route::get('detail',      'EmployeeController@getEmployeeDetail');
        Route::get('reward',      'EmployeeController@getEmployeeReward');
        Route::post('notification',      'EmployeeController@getEmployeeNotification');
        Route::post('notification/count',      'EmployeeController@getEmployeeNotificationCount');
    });
});