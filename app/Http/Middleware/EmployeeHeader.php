<?php

namespace App\Http\Middleware;
use Response;
use Closure;
use App\Http\Models\Employee;
class EmployeeHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(){
        $this->employee = new Employee;
    }
    public function handle($request, Closure $next)  
    {  
        if(!isset($_SERVER['HTTP_AUTHORIZATION'])){  
            return Response::json('unauthorized',401);  
        } else {
            try{
                $token_array=explode("|",base64_decode($_SERVER['HTTP_AUTHORIZATION']));
                $data =array(
                    'id' => $token_array[0],
                    'token' =>$token_array[1],
                );
                $employee = $this->employee->checkToken($data);
              
                if($employee){
                    $request->merge(array("id" => $data['id']));
                    return $next($request);
                }
                else{
                    return Response::json('unauthorized',401); 
                }
            } catch(\Exception $e){
                 return Response::json('unauthorized',401); 
            }
            
        } 
          
    }  
}
