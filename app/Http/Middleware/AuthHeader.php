<?php

namespace App\Http\Middleware;
use Response;
use Closure;
use App\Http\Models\User;
class AuthHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function __construct(){
        $this->user =new User;
    }
    public function handle($request, Closure $next)  
    {  

        if(!isset($_SERVER['HTTP_AUTHORIZATION'])){  
            return Response::json('unauthorized',401);  
        } else {
            try{
                $token_array=explode("|",base64_decode($_SERVER['HTTP_AUTHORIZATION']));
                $data =array(
                    'id' => $token_array[0],
                    'token' =>$token_array[1],
                );
                $user = $this->user->checkToken($data);
              
                if($user){
                    if($user->role_id==2){
                        $request->merge(array("brand_id" => $user->bid ));
                        $request->merge(array("user_id" => $data['id']));
                    } else if($user->role_id==3){
                        $request->merge(array("corporate_id" => $user->cid));
                        $request->merge(array("user_id" => $data['id']));
                    } 
                    return $next($request);
                }
                else{
                    return Response::json('unauthorized',401); 
                }
            } catch(\Exception $e){
                 return Response::json('unauthorized',401); 
            }
            
        } 
          
    }  
}
