<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class CheckSessionCorporate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        if(Session::get('role')== 3 && Session::has('loginvalue')){
            return $next($request);
        }else{
            return redirect('corporate/login');
        }
    }
}
