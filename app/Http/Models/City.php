<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class City extends Model
{
   
    public function addCity($data){
    	$city = DB::table('city')->insertGetId($data);
    	return $city;
    }

    public function getCity($data){
    	$city = DB::table('city')
                 ->leftjoin('state', 'city.state_id', '=', 'state.id')
                 ->select('city.*', 'state.name as state_name')
                 ->when(array_key_exists('state', $data), function($query) use ($data){
                    return $query->where('state.id', $data['state']);
                 })
                 ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->where('city.status', '=',$data['status']);
                 })
                 ->paginate(2000);
    	return $city;
    }

    public function activeCity($data,$id){

    	$active_city = DB::table('city')->where('id', $id)->update($data);
    	return $active_city;	
    }

    public function deactiveCity($data,$id){
    	$deactive_city = DB::table('city')->where('id', $id)->update($data);
    	return $deactive_city;	
    }

    public function updateCity($data,$id){
    	$update_city = DB::table('city')->where('id', $id)->update($data);
    	return $update_city;	
    }

    public function deleteCity($data){
    	$delete_city = DB::table('city')->where('id', '=', $data['id'])->delete();
    	// $delete_brand = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $delete_city;
    }

    public function getBrandById($id){
        $city = DB::table('city')
                ->leftjoin('users', 'brand.uid', '=', 'users.id')
                ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                ->select('brand.*', 'users.email', 'users.contact','category.name as brand_category_name')
                ->where('users.id', $id)->first();
        return $city;
    }
    public function getStateCountry($data){
        $city = DB::table('city')
                 ->leftjoin('state', 'city.state_id', '=', 'state.id')
                 ->leftjoin('country', 'state.country_id', '=', 'country.id')
                 ->select('state.id as state_id','country.id as country_id')
                 ->where('city.status', '=',1)
                 ->where('city.id', '=',$data['city_id'])
                 ->first();
        return $city;
    }
   
}