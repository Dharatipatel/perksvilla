<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SupportCategory extends Model
{
    protected $table = "support_category";
}
