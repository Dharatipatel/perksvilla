<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Mailtemplate extends Model {

    protected $fillable = [
        'title',
        'subject', 'text', 'tags',
    ];

}
