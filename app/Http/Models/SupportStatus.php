<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SupportStatus extends Model {

    protected $table = 'support_status';

}
