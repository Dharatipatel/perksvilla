<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SupportReply extends Model {

    protected $table = 'support_reply';
    protected $fillable = [
        'comment',
        'user_id',
        'support_id'
    ];

    public static function getById($taskId) {

        return SupportReply::where("support_id", "=", $taskId)
                        ->join("users", "users.id", "=", "support_reply.user_id")
                        ->select("support_reply.*", "users.email", "users.role_id",
                                DB::raw(" (select brand.name from brand WHERE brand.uid = support_reply.user_id) as brand_name"),
                                DB::raw(" (select corporate.name from corporate WHERE corporate.uid = support_reply.user_id) as corporate_name"))
                        ->orderBy('support_reply.created_at')
                        ->get();
    }

}
