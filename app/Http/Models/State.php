<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class State extends Model
{
   
    public function addState($data){
    	$state = DB::table('state')->insertGetId($data);
    	return $state;
    }

    public function getState($data){
    	$states = DB::table('state')
                 ->leftjoin('country', 'state.country_id', '=', 'country.id')
                 ->select('state.*', 'country.name as country_name')
                 ->when(array_key_exists('country', $data), function($query) use ($data){
                    return $query->where('country.id', $data['country']);
                 })
                 ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->where('state.status', '=',$data['status']);
                 })
                ->paginate(2000);
    	return $states;
    }

    public function activeState($data,$id){

    	$active_state = DB::table('state')->where('id', $id)->update($data);
    	return $active_state;	
    }

    public function deactiveState($data,$id){
    	$deactive_state = DB::table('state')->where('id', $id)->update($data);
    	return $deactive_state;	
    }

    public function updateState($data,$id){
    	$update_state = DB::table('state')->where('id', $id)->update($data);
    	return $update_state;	
    }

    public function deleteState($data){
    	$delete_state = DB::table('state')->where('id', '=', $data['id'])->delete();
    	return $delete_state;
    }

    // public function getBrandById($id){
    //     $brand = DB::table('state')
    //             ->leftjoin('users', 'brand.uid', '=', 'users.id')
    //             ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
    //             ->select('brand.*', 'users.email', 'users.contact','category.name as brand_category_name')
    //             ->where('users.id', $id)->first();
    //     return $brand;
    // }
   
}