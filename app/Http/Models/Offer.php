<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Offer extends Model
{
   
    public function addOffer($data){
    	$offer = DB::table('offer')->insertGetId($data);
    	return $offer;
    }
    public function getCoupen($data){
        $coupon_detail = DB::table('order_history')
                ->join('employee', 'order_history.employee_id', '=', 'employee.id')
                ->join('reward', 'order_history.reward_id', '=', 'reward.id')
                ->join('offer', 'reward.offer_id', '=', 'offer.id')
                ->join('brand', 'offer.brand_id', '=', 'brand.id')
                ->select('brand.id as brand_id','brand.logo as brand_logo','brand_address_id','website_url','facebook_page','business_hours','title','offer.description','notes','order_history.*','employee.corporate_id','employee.first_name' ,'employee.last_name','employee.contact','employee.email','offer.contact as offer_contact',
                        DB::raw("(select corporate.name from corporate where corporate.id = employee.corporate_id) as company_name"),
                        DB::raw("(SELECT offer.valid_to from reward JOIN offer on reward.offer_id = offer.id WHERE reward.id = order_history.reward_id ) as expiry_date"),
                        DB::raw("(SELECT offer.apply_on from reward JOIN offer on reward.offer_id = offer.id WHERE reward.id = order_history.reward_id ) as apply_on"),
                        DB::raw("(SELECT offer.value from reward JOIN offer on reward.offer_id = offer.id WHERE reward.id = order_history.reward_id ) as coupen_value ")
                  )
                ->where('order_history.coupen', '=',$data['coupen'])
                // ->where('order_history.redeem', '!=',1)
                
                ->where(function($query) use ($data){
                        $query->where('offer.offer_status_id', 1);
                        $query->orwhere('offer.offer_status_id', 4);
                })
                ->where('reward.payment_status', '=',1)
                ->where(function($query) use ($data){
                        $query->where('employee.email', '=',$data['email_phone']);
                        $query->orwhere('employee.contact', '=',$data['email_phone']);
                })
                ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('brand.id', '=',$data['brand_id']);
                })
                
                ->first();
        return $coupon_detail;
    }
    public function getOffer($data){
    	$offers = DB::table('offer')
                ->leftjoin('brand', 'offer.brand_id', '=', 'brand.id')
                ->leftjoin('category','brand.brand_category', '=' ,'category.id')
                ->select('brand.name as brand_name','brand.business_hours' , 'brand.website_url' , 'brand.facebook_page', 'brand.logo' ,'category.name as category' , 'offer.*',
                    DB::raw("(SELECT count(order_history.id) from reward JOIN order_history on reward.id=order_history.reward_id WHERE offer_id = offer.id and order_history.redeem=1 and reward.payment_status=1 )as redeem_count"),
                    DB::raw("(SELECT count(order_history.id) from reward JOIN order_history on reward.id=order_history.reward_id WHERE offer_id = offer.id and reward.payment_status=1)as issued_count")
                )
                ->when(array_key_exists('bid', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['bid']);
                })
                ->when(array_key_exists('offer_status_id', $data), function($query) use ($data){
                    return $query->whereIn('offer.offer_status_id', array($data['offer_status_id']));
                })
                ->when(array_key_exists('brand_category', $data), function($query) use ($data){
                    return $query->where('brand.brand_category', $data['brand_category']);
                })
                ->when(array_key_exists('coupentype', $data) && in_array(0, $data['coupentype']) && count($data['coupentype']) == 1, function($query) use ($data){
                    return $query->where('offer.price', '=',0);
                })
                ->when(array_key_exists('coupentype', $data) && in_array(1, $data['coupentype']) && count($data['coupentype']) == 1, function($query) use ($data){
                    return $query->where('offer.price', '>',0);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(0, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data){
                    return $query->whereBetween('offer.price', [0, 500]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(1, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data){
                    return $query->whereBetween('offer.price', [500, 2000]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(2, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data){
                    return $query->whereBetween('offer.price', [2000, 5000]);
                })
                ->when(array_key_exists('pricerange', $data) && in_array(3, $data['pricerange']) && count($data['pricerange']) == 1, function($query) use ($data){
                    return $query->where('offer.price', '>' ,5000);
                })
                ->when(array_key_exists('count', $data), function($query) use ($data){
                    return $query->where('offer.remaining_count', '>' ,0);
                })
                ->when(array_key_exists('offer_id', $data), function($query) use ($data){
                    return $query->where('offer.id', '=' ,$data['offer_id']);
                })
                ->orderby('offer.created','desc')
                ->paginate(2000);

    	return $offers;
    }
    public function approveOffer($data,$id){

    	$approve_brand = DB::table('offer')->where('id', $id)->update($data);
    	return $approve_brand;	
    }
    public function deactiveOffer($data,$id){
    	$deactive_brand = DB::table('offer')->where('id', $id)->update($data);
    	return $deactive_brand;	
    }
    public function deactiveOfferByBrand($data,$id){
      $deactive_brand = DB::table('offer')->where('brand_id', $id)->update($data);
      return $deactive_brand; 
    }
    public function updateOffer($data,$id){
        $update_offer = DB::table('offer')->where('id', $id)->update($data);
        return $update_offer;    
    }
    public function updateRemainingCount($id,$count){
     $update_brand = DB::table('offer')->where('id', '=', $id)->decrement('remaining_count',$count);
     return $update_brand;   
    }
    public function updateOTP($data){
        $update_otp = DB::table('order_history')
                        ->where('employee_id', '=', $data['employee_id'])
                        ->where('coupen', '=', $data['coupen'])
                        ->update(array('otp'=> $data['otp']));
        return $update_otp;   
    }

    public function getOfferReward($data){
        $offers = DB::table('reward')
                ->join('offer', 'reward.offer_id', '=', 'offer.id')
                ->select('offer.*')
                ->where('reward.id', '=', $data['reward_id'])
                ->first();
    }

    public function offerCount($data){
        return DB::table('offer')
              ->where('remaining_count','>',0)
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('brand_id', $data['brand_id']);
                })
              ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('offer_status_id', $data['status']);
                })
              ->count();
    }
    public function expectedVisitCount($data){
        return DB::table('order_history')
              ->join('reward','order_history.reward_id','reward.id')
              ->join('offer', 'reward.offer_id','offer.id')
              ->where('reward.payment_status', 1)
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['brand_id']);
                })
              ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('order_history.redeem', $data['status']);
                })
              ->when(array_key_exists('expected_visit', $data), function($query) use ($data){
                    return $query->whereDate('offer.valid_to', '>=' ,date('Y-m-d'));
                })
              ->count();
    }

    public function totalMember($data){
        return DB::table('order_history')
              ->join('reward','order_history.reward_id','reward.id')
              ->join('offer', 'reward.offer_id','offer.id')
              ->where('reward.payment_status', 1)
              ->where('order_history.redeem',1)
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['brand_id']);
                })
              ->sum('member');
    }
    public function getSales($data){
         return DB::table('order_history')
              ->join('reward','order_history.reward_id','reward.id')
              ->join('offer' ,'reward.offer_id','offer.id')
              ->where('reward.payment_status', 1)
              ->where('order_history.redeem',1)
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['brand_id']);
                })
               ->when(array_key_exists('monthly', $data), function($query) use ($data){
                    return $query->whereMonth('redeem_date','=',date('m'));
                })
              ->sum('bill_amount');

    }
    public function getTodayRedeem($data){
         return DB::table('order_history')
              ->join('reward','order_history.reward_id','reward.id')
              ->join('offer' ,'reward.offer_id','offer.id')
              ->where('reward.payment_status', 1)
              ->where('order_history.redeem',1)
              ->whereDate('redeem_date','=',date('Y-m-d'))
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['brand_id']);
                })
              ->count();

    }
    public function getTodayOrders(){
         return DB::table('order_history')
              ->join('reward','order_history.reward_id','reward.id')
              ->where('reward.payment_status', 1)
              ->whereDate('order_history.created','=',date('Y-m-d'))
              ->count();

    }
}