<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $fillable = ['subject', 'message', 'received_id', 'sender_id', 'notification_type_id', 'is_read', 'url'];

    public function userNotifications($id) {
        return Notification::where(array("user_id" => $id))->whereNull('employee_id')->latest()->limit(10)->get();
    }

    public function addNotification($data){
        $notifications = DB::table('notifications')->insertGetId($data);
        return $notifications;
    }
    public function addNotifications($data){
    	$notifications = DB::table('notifications')->insert($data);
    	return $notifications;
    }
    public function readNotification($data){
    	$notifications = DB::table('notifications')->where('user_id', $data)->update(array('status' =>1 , ));
    	return $notifications;
    }
    public function getNotification($data){
        $notifications = DB::table('notifications')->where($data)->latest()->paginate(200);
        return $notifications;
    }
    public function getNotificationCount($data){
        $notifications = DB::table('notifications')
                        ->when(array_key_exists('employee', $data), function($query) use ($data){
                            return $query->whereNull('employee_id');
                        })
                        ->where('user_id',$data['user_id'])
                        ->where('status',$data['status'])
                        ->count();
        return $notifications;
    }
    public function readNotificationEmployee($data){
        $notifications = DB::table('notifications')->where('employee_id', $data['employee_id'])->update(array('status' =>1 , ));
        return $notifications;
    }
}
