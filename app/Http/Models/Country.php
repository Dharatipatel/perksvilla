<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Country extends Model
{
   
    public function addCountry($data){
    	$country = DB::table('country')->insertGetId($data);
    	return $country;
    }

    public function getCountry($data){
    	$countries = DB::table('country')
                    ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->where('status', '=',$data['status']);
                    })
                    ->paginate(2000);
    	return $countries;
    }

    public function activeCountry($data,$id){

    	$active_country = DB::table('country')->where('id', $id)->update($data);
    	return $active_country;	
    }

    public function deactiveCountry($data,$id){
    	$deactive_country = DB::table('country')->where('id', $id)->update($data);
    	return $deactive_country;	
    }

    public function updateCountry($data,$id){
    	$update_country = DB::table('country')->where('id', $id)->update($data);
    	return $update_country;	
    }

    public function deleteCountry($data){
    	$delete_country = DB::table('country')->where('id', '=', $data['id'])->delete();
    	// $delete_country = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $delete_country;
    }

    public function getCountryById($id){
        $country = DB::table('country')->where('id', $id)->first();
        return $country;
    }
   
}