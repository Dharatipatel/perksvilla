<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Http\Models\Notification;
use App\Custom\Common;

class Order extends Model
{
   
    
    public function addOrders($data){
        $data = DB::table('order_history')->insert($data);
        return $data;
    }
    public function redeemCoupen($data){
        $redeem_coupen = DB::table('order_history')
        				->where('coupen', $data['coupen'])
        				->where('reward_id',$data['reward_id'])
        				->update(array(
        					'bill_amount' =>$data['bill_amount'],
        					'member' =>$data['member'],	
        					'redeem' =>1,	
        					'redeem_date' =>$data['redeem_date']	
        				));

        if($redeem_coupen){
            $employee_id =  DB::table('order_history')->where(array('coupen'=> $data['coupen'],'reward_id'=>$data['reward_id']))->first()->employee_id;
            $this->notification = new Notification;
            $user = $this->getUserId($data['reward_id']);
            $notification_data = array(
                                    'subject'    => 'Coupon Redeemed',
                                    'message'    => 'Your '.$this->getOfferName($data['reward_id'])->title.' coupon was redeemed successfully',
                                    'user_id'    => $user->user_id,
                                    'sender_id'  => 1,
                                    'employee_id'=> $employee_id,
                                    'job_id'     => 0,
                                    'status'     => 0,
                                    'type'       => 0,
                                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
            $notify = $this->notification->addNotification($notification_data); 
            $this->updateBrandWallet($data['reward_id']);
            $app_token =  DB::table('employee')->where('id',$employee_id)->first()->app_token;
            if($app_token != NULL){
                $app_token_arr[] = $app_token;
                $msg = array(
                    'body'      => 'Your '.$this->getOfferName($data['reward_id'])->title.' coupon was redeemed successfully',
                    'title'     => 'Coupon Redeemed',
                    'subtitle'  => 'Coupon Redeemed',
                    'vibrate'   => 1,
                    'sound'     => 1,
                    // 'largeIcon' => 'large_icon',
                    // 'smallIcon' => 'small_icon'
                );

                $fields = array(
                    'registration_ids' => $app_token_arr,
                    'notification' => $msg,
                    'data' => array(
                        'action' => 'Bills'
                    )
                );

                Common::send($fields);
            }
        }
        return $redeem_coupen;

    }
    public function getUserId($reward_id){
        $reward_detail=DB::table('order_history')
                        ->join('employee' ,'order_history.employee_id' ,'employee.id')
                        ->join('corporate' ,'employee.corporate_id','corporate.id')
                        ->select('corporate.uid as user_id')
                        ->where('order_history.reward_id',$reward_id)->first();

        return $reward_detail;

    }
    public function updateBrandWallet($reward_id){
        $reward_detail=DB::table('reward')
        ->join('offer' ,'reward.offer_id' ,'offer.id')
        ->join('brand' ,'offer.brand_id','brand.id')
        ->select('offer.price','brand.uid')
        ->where('reward.id',$reward_id)->first();
        $brand_wallet_add = DB::table('user_wallet')->where('user_id', $reward_detail->uid)->increment('balance', ($reward_detail->price));
    }
    public function otpVerified($data)
    {
        $redeem_coupen = DB::table('order_history')
                        ->where('coupen', $data['coupen'])
                        ->where('otp', $data['otp'])
                        ->first();   
        return $redeem_coupen;
    }
    public function getOfferName($reward_id){
        $reward_detail=DB::table('reward')
                        ->join('offer' ,'reward.offer_id' ,'offer.id')
                        ->select('offer.title')
                        ->where('reward.id',$reward_id)->first();
        return $reward_detail;
    }
    
}
