<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Corporate extends Model {

    protected $table = "corporate";
    public $timestamps = false;

    public function addCorporate($data) {
        $corporate = DB::table('corporate')->insertGetId($data);
        return $corporate;
    }

    public function getCorporate($data) {
        $corporate = DB::table('corporate')
                ->join('users', 'corporate.uid', '=', 'users.id')
                ->leftjoin('category', 'corporate.category', '=', 'category.id')
                ->select('corporate.*', 'users.email', 'users.contact', 'users.status as status', 'category.name as corporate_category_name', DB::raw("(SELECT COUNT(employee.id) FROM employee WHERE employee.corporate_id = corporate.id) as employee_count"), DB::raw("(SELECT count(*) FROM `order_history` JOIN employee on order_history.employee_id =employee.id where employee.corporate_id = corporate.id) as coupen_count")
                )
                ->paginate(2000);
        return $corporate;
    }

    public function getActiveCorporateCount() {
        return $active_brand_count = DB::table('corporate')
                ->leftjoin('users', 'corporate.uid', '=', 'users.id')
                ->where('users.status', '=', 1)
                ->count();
    }

    public function activeCorporate($data, $id) {

        $active_corporate = DB::table('users')->where('id', $id)->update($data);
        return $active_corporate;
    }

    public function deactiveCorporate($data, $id) {
        $deactive_corporate = DB::table('users')->where('id', $id)->update($data);
        return $deactive_corporate;
    }

    public function updateCorporate($data, $id) {
        $update_corporate = DB::table('corporate')->where('id', $id)->update($data);
        return $update_corporate;
    }

    public function deleteCorporate($data) {
        $delete_corporate_user =DB::transaction(function() use ($data) {
            $delete_user  = DB::table('users')->where('id', '=', $data['id'])->delete();
            $delete_corporate = DB::table('corporate')->where('uid', '=', $data['id'])->delete();
            return $delete_corporate;
        });
       
        return $delete_corporate_user;
    }


    public function getCorporateById($id) {
        $corporate = DB::table('corporate')
                        ->join('users', 'corporate.uid', '=', 'users.id')
                        // ->join('category', 'brand.brand_category', '=', 'category.id')
                        ->select('corporate.*', 'users.email', 'users.contact')
                        ->where('users.id', $id)->first();
        return $corporate;
    }

    public function savePassword($data) {
        try {
            $model = Corporate::find($data->id);

            $user = User::find($model->uid);

            $user->password = md5($data->password);
            $user->modified = date("Y-m-d H:i:s");
            $user->is_mail_sent = 1;
            $user->save();

            $corporate = Corporate::join("users", "users.id", "=", "corporate.uid")
                    ->select("users.email", "corporate.contact_person_name as user_name", "users.contact")
                    ->where("users.id", $user->id)
                    ->first();
            return $corporate;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function getById($id) {
        $brand = DB::table('corporate')
                        ->leftjoin('users', 'corporate.uid', '=', 'users.id')
                        ->select('corporate.contact_person_name as name', 'users.email', 'users.contact', 'users.id')
                        ->where('users.id', $id)->first();
        return $brand;
    }

}
