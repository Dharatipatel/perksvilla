<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Brand;
use Carbon\Carbon;

class Reward extends Model
{
   
    public function addReward($data){
    	$offer = DB::table('reward')->insertGetId($data);
    	return $offer;
    }
    
    public function getReward($data){
    	$offers = DB::table('order_history')
                ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->leftjoin('offer','reward.offer_id', '=' ,'offer.id')
                ->select('order_history.bill_amount','order_history.coupen','reward.created','issue_date','order_history.redeem_date','employee.first_name','employee.designation','employee.last_name' ,'reward.category_id' ,'reward.offer_id' , 'order_history.redeem','offer.value as coupon_value','offer.price as buy_price','offer.brand_id as brand_id','offer.title as offer_name',
                    DB::raw("(SELECT  name FROM  category where category.id = reward.category_id ) as category_name"), 
                    // DB::raw("(SELECT offer.value FROM offer where offer.id = reward.offer_id)  as coupon_value "),
                    // DB::raw("(SELECT offer.price FROM offer where offer.id = reward.offer_id)  as buy_price "),
                    // DB::raw("(SELECT offer.brand_id FROM offer where offer.id = reward.offer_id)  as brand_id "),
                    // DB::raw("(SELECT offer.title FROM offer where offer.id = reward.offer_id)  as offer_name "),
                    DB::raw("(SELECT brand.name FROM offer JOIN brand on offer.brand_id = brand.id where offer.id = reward.offer_id)  as brand_name") 
                )
                ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
                ->when(array_key_exists('status', $data), function($query) use ($data){
                      if($data['status']==0){
                          return $query->where('order_history.redeem', $data['status'])->whereDate('offer.valid_to','>=',date('Y-m-d'));
                      }
                      elseif($data['status']==1){
                          return $query->where('order_history.redeem', $data['status']);
                      }
                      elseif($data['status']==2){
                          return $query->where('order_history.redeem', 0)->whereDate('offer.valid_to','<',date('Y-m-d'));
                      }
                })
                ->when(array_key_exists('payment_status', $data), function($query) use ($data){
                    return $query->where('reward.payment_status', $data['payment_status']);
                })
                ->when(array_key_exists('employee_id', $data), function($query) use ($data){
                    return $query->where('order_history.employee_id', $data['employee_id']);
                })
                ->orderBy('order_history.modified','desc')
                ->paginate(2000);

    	return $offers;
    }
    public function getRewardEmployee($data){
      $offers = DB::table('order_history')
                ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->leftjoin('offer','reward.offer_id', '=' ,'offer.id')
                ->leftjoin('brand','brand.id', '=' ,'offer.brand_id')
                ->select('order_history.bill_amount','order_history.coupen','reward.created','issue_date','order_history.redeem_date','employee.first_name','employee.designation','employee.last_name' ,'reward.category_id' ,'reward.offer_id' , 'order_history.redeem','offer.value as coupon_value','offer.price as buy_price' ,'offer.brand_id as brand_id','offer.title as offer_name','offer.valid_to as expiry_date','offer.brand_address_id','offer.apply_on', 'brand.business_hours','offer.offer_status_id as offer_status', 'offer.notes', 'brand.facebook_page','brand.website_url', 'brand.name as brand_name','brand.logo  as logo', 'offer.contact',
                    DB::raw("(SELECT  name FROM  category where category.id = reward.category_id ) as category_name"), 
                    DB::raw("(SELECT category.name from brand join category on brand.brand_category = category.id where brand.id = offer.brand_id)  as brand_category")
                )
                ->where('reward.payment_status',1)
                ->when(array_key_exists('employee_id', $data), function($query) use ($data){
                    return $query->where('order_history.employee_id', $data['employee_id']);
                })
                ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('order_history.redeem', $data['status']);
                })
                ->orderBy('order_history.redeem','asc')
                ->orderBy('offer.offer_status_id','asc')
                ->paginate(20);

      return $offers;
    }
    public function getCoupons($data){
      $offers = DB::table('order_history')
                ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->leftjoin('offer','reward.offer_id', '=' ,'offer.id')
                ->select('order_history.bill_amount','order_history.coupen','order_history.redeem_date','issue_date','employee.first_name','employee.last_name', 'offer.value as coupon_value','offer.price as buy_price' ,'offer.brand_id as brand_id','offer.title as offer_name','offer.valid_to as expiry_date','offer.id as offer_id',
                    DB::raw("(SELECT brand.name FROM offer JOIN brand on offer.brand_id = brand.id where offer.id = reward.offer_id)  as brand_name"),
                    DB::raw("(SELECT corporate.name FROM corporate JOIN employee on corporate.id =employee.corporate_id WHERE employee.id  = order_history.employee_id)  as corporate_name")
                )
                ->where('reward.payment_status',1)
                ->when(array_key_exists('status', $data), function($query) use ($data){
                      if($data['status']==0){
                          return $query->where('order_history.redeem', $data['status'])->whereDate('offer.valid_to','>=',date('Y-m-d'));
                      }
                      elseif($data['status']==1){
                          return $query->where('order_history.redeem', $data['status']);
                      }
                      elseif($data['status']==2){
                          return $query->where('order_history.redeem', 0)->whereDate('offer.valid_to','<',date('Y-m-d'));
                      }
                })
                ->orderBy('order_history.modified','desc')
                ->paginate(2000);

      return $offers;
    }
    
    public function getRewardUsed($data){
        return DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              ->leftjoin('reward', 'reward.id', '=', 'order_history.reward_id')
              ->when(array_key_exists('redeem', $data), function($query) use ($data){
                    return $query->where('redeem', $data['redeem']);
                })
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->where('reward.payment_status',1)
              ->count();
    }
    public function getCoupenIssued(){
        return DB::table('order_history')->count();
    }
    public function getInvestedAmount($data){
        return DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              ->when(array_key_exists('redeem', $data), function($query) use ($data){
                    return $query->where('redeem', $data['redeem']);
                })
              ->leftjoin('reward', 'reward.id', '=', 'order_history.reward_id')
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->where('reward.payment_status',1)
              ->sum('coupon_price');
    }
    public function getBillAmoount($data){
        $coupon_price =  DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              ->leftjoin('reward', 'reward.id', '=', 'order_history.reward_id')
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->where('reward.payment_status',1)
              ->sum('coupon_price');

        $coupon_value =  DB::table('order_history')
              ->leftjoin('employee', 'order_history.employee_id', '=', 'employee.id')
              ->leftjoin('reward', 'reward.id', '=', 'order_history.reward_id')
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->where('reward.payment_status',1)
              ->sum('coupon_value');
        return ($coupon_value-$coupon_price);
    }
    public function getBrandOrderHistory($data){

        return DB::table('order_history')
              ->join('employee', 'order_history.employee_id', '=', 'employee.id')
              ->join('reward', 'order_history.reward_id', '=', 'reward.id')
              ->join('offer', 'reward.offer_id', '=', 'offer.id')
              ->select( 'order_history.*','employee.first_name' ,'employee.last_name','order_history.redeem_date',
                DB::raw("(SELECT brand.name from offer join brand on offer.brand_id =brand.id WHERE offer.id =reward.offer_id) as brand_name") ,
                  'reward.offer_id','offer.title as offer_name'
              )
              ->when(array_key_exists('redeem', $data), function($query) use ($data){
                    return $query->where('order_history.redeem', $data['redeem']);
                })
              ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                    return $query->where('offer.brand_id', $data['brand_id']);
                })
              ->orderBy('order_history.modified','desc')
              ->paginate(2000);
        
    }
  public function updatePaymentId($data,$id){
    $approve_brand = DB::table('reward')->where('id', $id)->update($data);
      return $approve_brand;
  }
   public function updatePaymentStatus($data,$user_id){
      $this->brand = new Brand;
      $reward = DB::table('reward')
            ->join('offer', 'reward.offer_id', '=', 'offer.id')
            ->select('offer.id as offerid' ,'offer.title' ,'offer.brand_id' ,'reward.eid','offer.price','reward.id as reward_id',
              DB::raw("(SELECT category.name from category  WHERE category.id = reward.category_id ) as rewardtype")
            )
            ->where('payment_request_id', $data['payment_request_id'])->first();
      if($data['payment_status']==1){
         $employee_count = count(explode(',',$reward->eid));
         $update_offer_count = DB::table('offer')->where('id', '=', $reward->offerid)->decrement('remaining_count',$employee_count);
         $employee_id = explode(',', $reward->eid);
         $i = 0;
         foreach ($employee_id as $key => $value) {
            $empl_data[$i] = array(
                                  'subject'    => 'Coupon Received',
                                  'message'    =>  "You are rewarded for ".$reward->rewardtype,
                                  'user_id'    => $this->brand->getUserById($reward->brand_id)->id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'employee_id'=> $value,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
            $i++;
         }
         $emp_noti = DB::table('notifications')->insert($empl_data);
         $notification_data = array(
                                  'subject'    => 'Coupon Purchased',
                                  'message'    => '1 Company purchased your '.$reward->title.' coupon',
                                  'user_id'    => $this->brand->getUserById($reward->brand_id)->id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
         $notifications = DB::table('notifications')->insertGetId($notification_data);
      }
      $payment_success =  DB::table('reward')->where('payment_request_id', $data['payment_request_id'])->update($data);
      return $reward->reward_id;  
    }
    public function getOrderHistory($data){
        return DB::table('order_history')
              ->join('employee', 'order_history.employee_id', '=', 'employee.id')
              ->join('reward', 'order_history.reward_id', '=', 'reward.id')
              ->select( 'order_history.*', 'reward.payment_request_id as transaction_id',
                DB::raw("GROUP_CONCAT(CONCAT(employee.first_name ,' ', employee.last_name))  as employee_name") ,
                DB::raw("SUM(coupon_price) as total_amount "),
                'reward.created as purchase_date',
                DB::raw("(SELECT brand.name from offer join brand on offer.brand_id =brand.id WHERE offer.id = reward.offer_id) as brand_name"),
                DB::raw("(SELECT offer.title from offer WHERE offer.id = reward.offer_id) as offer_name"),
                DB::raw("(SELECT offer.id from offer WHERE offer.id = reward.offer_id) as offer_id"),
                DB::raw("(SELECT corporate.name from corporate  WHERE corporate.id =employee.corporate_id) as corporate_name")
              )
              ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
              ->where('reward.payment_status', 1)
              ->orderBy('reward.created','desc')
              ->groupBy('reward_id')
              ->paginate(2000);
    }
}