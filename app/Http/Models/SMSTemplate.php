<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SMSTemplate extends Model {

    protected $fillable = [
        'title',
        'text'
    ];
    protected $table = "sms_templates";

    public function addSMSTemplate($data) {
        $sms_templates = DB::table('sms_templates')->insertGetId($data);
        return $sms_templates;
    }

    public function getSMSTemplate($data) {
        $sms_templates = DB::table('sms_templates')
                ->paginate(2000);
        return $sms_templates;
    }

    public function updateSMSTemplate($data, $id) {
        $update_sms_templates = DB::table('sms_templates')->where('id', $id)->update($data);
        return $update_sms_templates;
    }

    public function deleteSMSTemplate($data) {
        $delete_sms_templates = DB::table('sms_templates')->where('id', '=', $data['id'])->delete();
        return $delete_sms_templates;
    }

}
