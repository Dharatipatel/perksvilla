<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Exception;

class Brand extends Model {

    protected $table = "brand";
    public $timestamps = false;

    public function addBrand($data) {
        $brand = DB::table('brand')->insertGetId($data);
        $brand_balance = DB::table('user_wallet')->insertGetId(array('user_id' =>$data['uid'] ,'balance' => 0));
        return $brand;
    }

    public function addWallet($data) {
        $brand_wallet_add = DB::table('user_wallet')->where('user_id', $data['user_id'])->decrement('balance', $data['withdraw_amount']);
        $brand = DB::table('withdraw_request')->insertGetId($data);
        return $brand;
    }

    public function getWalletTransaction($data) {
        $transaction = DB::table('withdraw_request')
                ->select('withdraw_request.*', 
                    DB::raw("(SELECT brand.name FROM brand where brand.uid = withdraw_request.user_id)  as brand_name "),
                    DB::raw("(SELECT brand.id FROM brand where brand.uid = withdraw_request.user_id)  as brand_id "),
                    DB::raw("(SELECT brand.commision FROM brand where brand.uid = withdraw_request.user_id)  as commision "),
                    DB::raw("(SELECT bank_details.name FROM brand join bank_details on brand.id = bank_details.brand_id where brand.uid = withdraw_request.user_id)  as bank_name "),
                    DB::raw("(SELECT bank_details.account_number FROM brand join bank_details on brand.id = bank_details.brand_id where brand.uid = withdraw_request.user_id)  as account_number ")
                    )
                ->when(array_key_exists('user_id', $data), function($query) use ($data) {
                    return $query->where('user_id', '=', $data['user_id']);
                })
                ->orderby('modified' ,'desc')
                ->get();
        return $transaction;
    }
    public function paymentRequestCount() {
        return $active_brand_count = DB::table('withdraw_request')
                                    ->join('users','withdraw_request.user_id','users.id')
                                    ->where('users.status' ,1)
                                    ->where('withdraw_request.status' ,0)
                                    ->count();
    }
    public function getBalance($data) {
        $amount = DB::table('user_wallet')
                ->when(array_key_exists('user_id', $data), function($query) use ($data) {
                    return $query->where('user_id', '=', $data['user_id']);
                })
                ->paginate(2000);
        return $amount;
    }

    public function getBalanceOfUser($data) {
        $amount = DB::table('user_wallet')
                ->select('user_wallet.*' ,'brand.commision')
                ->join('brand','user_wallet.user_id','brand.uid')
                ->where('user_id', '=', $data['user_id'])
                ->first();
        return $amount;
    }

    public function getBrand($data) {
        $brands = DB::table('brand')
                ->leftjoin('users', 'brand.uid', '=', 'users.id')
                ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                // ->leftjoin('brand_address', 'brand.id', '=', 'brand_address.brand_id')
                ->select('brand.*', 'users.email', 'users.contact', 'users.status as status' , 'category.name as brand_category_name', DB::raw("(SELECT COUNT(offer.id) FROM offer WHERE offer.brand_id =brand.id) as    offer_count")
                )
                ->paginate(2000);
                     
                      
        return $brands;
    }
    public function getActiveBrandCount(){
        return $active_brand_count = DB::table('brand')
                            ->leftjoin('users', 'brand.uid', '=', 'users.id')
                            ->where('users.status','=',1)
                            ->count();
    }
    public function getDeactiveBrandCount(){
        return $active_brand_count = DB::table('offer')
                            ->leftjoin('brand','offer.brand_id', '=' ,'brand.id')
                            ->leftjoin('users', 'brand.uid', '=', 'users.id')
                            ->where('users.status','=',1)
                            ->where('offer.offer_status_id','=',1)
                            ->count();
    }
    public function getRedeemCoupenCount(){
        return DB::table('order_history')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->leftjoin('offer','reward.offer_id', '=' ,'offer.id')
                ->leftjoin('brand','offer.brand_id', '=' ,'brand.id')
                ->where('order_history.redeem',1)
                ->count();
    }
    public function getBrandOffer($data) {
        $brand_offers = DB::table('brand')
                ->join('offer', 'brand.id', '=', 'offer.brand_id')
                ->join('category', 'brand.brand_category', '=', 'category.id')
                ->join('users', 'brand.uid', '=', 'users.id')
                ->select('brand.id as brand_id', 'brand.logo as brand_logo', 'brand.name as brand_name', 'category.name as brand_category_name', 'offer.brand_id', 
                    DB::raw("(SELECT COUNT(`offer`.`id`) FROM `offer`  WHERE `offer`.`brand_id` = `brand`.`id` and `offer`.`price` = 0 and `offer`.`offer_status_id` = 1 and `offer`.`remaining_count` > 0) as freecoupen"), 
                    DB::raw("(SELECT COUNT(`offer`.`id`) FROM `offer`  WHERE `offer`.`brand_id` = `brand`.`id` and `offer`.`price` != 0 and `offer`.`offer_status_id` = 1 and `offer`.`remaining_count` > 0) as fixcoupen")
                )
                ->where('offer.offer_status_id', 1)
                ->where('offer.remaining_count', '>' ,0)
                ->where('users.status', 1)
                ->where('category.status', 1)
                ->groupBy('offer.brand_id')
                ->paginate(2000);
        return $brand_offers;
    }

    public function paidRequest($data, $id) {

        $active_brand = DB::table('withdraw_request')->where('id', $id)->update($data);
        return $active_brand;
    }
    public function activeBrand($data, $id) {

        $active_brand = DB::table('users')->where('id', $id)->update($data);
        return $active_brand;
    }
    
    public function deactiveBrand($data, $id) {
        $deactive_brand = DB::table('users')->where('id', $id)->update($data);
        return $deactive_brand;
    }

    public function updateBrand($data, $id) {
        $update_brand = DB::table('brand')->where('id', $id)->update($data);
        return $update_brand;
    }

    public function deleteBrand($data) {
        $delete_brand_user =DB::transaction(function() use ($data) {
            $delete_user  = DB::table('users')->where('id', '=', $data['id'])->delete();
            $delete_brand = DB::table('brand')->where('uid', '=', $data['id'])->delete();
            return $delete_brand;
        });
       
        return $delete_brand_user;
    }

    public function getBrandById($id) {
        $brand = DB::table('brand')
                        ->leftjoin('users', 'brand.uid', '=', 'users.id')
                        ->leftjoin('category', 'brand.brand_category', '=', 'category.id')
                        ->leftjoin('brand_address', 'brand.id', '=', 'brand_address.brand_id')
                        ->select('brand.*', 'brand_address.*', 'users.email', 'users.contact', 'category.name as brand_category_name')
                        ->where('users.id', $id)->first();
        return $brand;
    }
    
    public function getById($id) {
        $brand = DB::table('brand')
                        ->leftjoin('users', 'brand.uid', '=', 'users.id')
                        ->select('brand.name', 'users.email', 'users.contact', 'users.id','brand.id as brand_id')
                        ->where('users.id', $id)->first();
        return $brand;
    }

    public function getUserById($id) {
        $brand = DB::table('brand')
                        ->leftjoin('users', 'brand.uid', '=', 'users.id')
                        ->select('brand.name', 'users.email', 'users.contact', 'users.id')
                        ->where('brand.id', $id)->first();
        return $brand;
    }

    public function getAddress($brand) {

        $address = DB::table('brand_address')
                ->leftjoin('city', 'brand_address.city', '=', 'city.id')
                ->leftjoin('state', 'brand_address.state', '=', 'state.id')
                ->leftjoin('country', 'brand_address.country', '=', 'country.id')
                ->select('brand_address.id', 'brand_address.address', 'brand_address.pincode', 'brand_address.city as city_id', 'brand_address.state as state_id', 'brand_address.country as country_id', 'city.name as city', 'state.name as state', 'country.name as country')
                ->where('brand_address.brand_id', '=', $brand)
                ->paginate(2000);
        return $address;
    }
    public static function getAddressById($id) {

        $address = DB::table('brand_address')
                ->leftjoin('city', 'brand_address.city', '=', 'city.id')
                ->leftjoin('state', 'brand_address.state', '=', 'state.id')
                ->leftjoin('country', 'brand_address.country', '=', 'country.id')
                ->select('brand_address.id', 'brand_address.address', 'brand_address.pincode', 'brand_address.city as city_id', 'brand_address.state as state_id', 'brand_address.country as country_id', 'city.name as city', 'state.name as state', 'country.name as country')
                ->whereIn('brand_address.id', $id)->get();
        return $address;
    }
    public function addAddress($brand_address) {
        $brand_address = DB::table('brand_address')->insert($brand_address);
        return $brand_address;
    }
    public function isAddressExist($brand_address) {
        $brand_address = DB::table('brand_address')->where($brand_address)->first();
        return $brand_address;
    }
    public function updateAddress($brand_address,$id) {
        $brand_address = DB::table('brand_address')->where('id', $id)->update($brand_address);
        return $brand_address;
    }
    public function removeAddress($id) {
        $brand_address = DB::table('brand_address')
                ->where('brand_address.id', '=', $id)
                ->delete();
        return $brand_address;
    }

    public function getBrandCount($data) {
        return DB::table('brand')
               ->leftjoin('users', 'brand.uid', '=', 'users.id')
              ->where('users.status', $data['status'])->count();
    }

    public function savePassword($data) {
        try {
            $model = Brand::find($data->id);
            
            $user = User::find($model->uid);
            
            $user->password = md5($data->password);
            $user->modified = date("Y-m-d H:i:s");
            $user->is_mail_sent = 1;
            $user->save();

            $brand = Brand::join("users", "users.id", "=", "brand.uid")
                    ->select("users.email", "brand.contact_person_name as user_name", "users.contact")
                    ->where("users.id", $user->id)
                    ->first();
            return $brand;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getBrandTotalSales($data){
        return DB::table('order_history')
                ->leftjoin('reward','order_history.reward_id', '=' ,'reward.id')
                ->leftjoin('offer','reward.offer_id', '=' ,'offer.id')
                ->leftjoin('brand','offer.brand_id', '=' ,'brand.id')
                ->select('offer.title','offer.price')
                ->where('order_history.redeem',1)
                ->when(array_key_exists('brand_id', $data), function($query) use ($data){
                        return $query->where('brand.id',$data['brand_id'] );              
                 })
                ->paginate(2000);
    }

    public function getBalanceByBrand($data) {
        $amount = DB::table('user_wallet')
                ->where('user_id', '=', $data['user_id'])
                ->first();
        return $amount->balance;
    }
}
