<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model {

    public $timestamps = false;

    public function register($data) {
        $user = DB::table('users')->insertGetId($data);
        return $user;
    }

    public function login($data) {
        $user = DB::table('users')->where($data)->first();
        return $user;
    }

    public function checkToken($data) {
        $user = DB::table('users')->where($data)->first();
        if($user->role_id==2){
            return $this->getUserById($user->id);
        } else if($user->role_id==3){
            return $this->getCorporateById($user->id);
        } else{
            return $user;
        }
    }

    public function updateUser($data, $id) {
        $user = DB::table('users')->where('id', $id)->update($data);
        return $user;
    }

    public function updateToken($data) {
        $user = DB::table('users')->where('id', $data['id'])->update(['token' => $data['token'], 'modified' => $data['modified']]);
        return $user;
    }

    public function checkPassword($data) {
        $password = DB::table('users')->where($data)->first();
        return $password;
    }

    public function changePassword($data, $id) {
        $update_password = DB::table('users')->where('id', $id)->update($data);
        return $update_password;
    }

    public function getUserById($id) {
        $user = DB::table('users')
                        ->leftjoin('brand', 'users.id', '=', 'brand.uid')
                        ->select('brand.*', 'users.email', 'users.contact', 'users.role_id','brand.id as bid')
                        ->where(['users.id' => $id])->first();
        $path = asset("/uploads/brand") . "/" . $user->logo;
        if(!file_exists($path)){
           $path = ''; 
        }
        $user->logo_image = $path;
        return $user;
    }

    public function getCorporateById($id) {
        $corporate = DB::table('users')
                        ->leftjoin('corporate', 'users.id', '=', 'corporate.uid')
                        ->select('corporate.*', 'users.email', 'users.contact', 'users.role_id' ,'corporate.id as cid')
                        ->where(['users.id' => $id])->first();
        return $corporate;
    }

    public function getAdminDetail($data) {
        $user = DB::table('users')->where($data)->first();
        return $user;
    }

    public function updateAdminDetail($data, $id) {
        $user = DB::table('users')->where('id', $id)->update($data);
        return $user;
    }

    public function isUserExist($data) {
        $user = DB::table('users')
                ->where('id', '!=', $data['id'])
                ->where('email', '=', $data['email'])
                ->first();
        return $user;
    }

    public function isMobileExist($data) {
        $user = DB::table('users')
                ->where('id', '!=', $data['id'])
                ->where('contact', '=', $data['contact'])
                ->first();
        return $user;
    }

    public function savePasswordResetToken($data) {
        $user = DB::table('password_resets')->insert($data);
        return true;
    }

    public function getUser($id) {
        $user = DB::table('users')
                        ->select("first_name", "last_name", "email", "contact")
                        ->where(['users.id' => $id])->first();
        return $user;
    }
}
