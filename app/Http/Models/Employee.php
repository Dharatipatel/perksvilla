<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class Employee extends Model
{
    public function addEmployee($data){
    	$employee = DB::table('employee')->insertGetId($data);
    	return $employee;
    }
    public static function getEmployeeDashboard($data){
        $employees = DB::table('employee')
                    ->select('employee.*')
                    ->when(array_key_exists('cid', $data), function($query) use ($data){
                            return $query->where('employee.corporate_id', $data['cid']);
                    })
                    ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->whereIn('employee.status', array($data['status']));
                    })
                    ->where(function($query) use ($data){
                            $query->orwhereMonth('birth_date', '=', date('m'));
                            $query->orwhereMonth('joining_date', '=', date('m'));
                            $query->orwhere(function($query) use ($data){
                                $query->whereMonth('joining_date', '=', Carbon::now()->subMonth()->month);
                                $query->whereYear('joining_date', '=', Carbon::now()->year);
                            });
                        })
                    ->paginate(2000);
        return $employees;
    }
    public function getEmployee($data){
    	$employees = DB::table('employee')
                    ->select('employee.*',
                        DB::raw("(SELECT COUNT(order_history.employee_id) from order_history join reward on order_history.reward_id = reward.id WHERE order_history.employee_id = employee.id and reward.payment_status=1) as reward_count"),
                        DB::raw("(SELECT corporate.name from corporate WHERE corporate.id = employee.corporate_id) as corporate_name")
                    )
                    ->when(array_key_exists('cid', $data), function($query) use ($data){
                            return $query->where('employee.corporate_id', $data['cid']);
                    })
                    ->when(array_key_exists('status', $data), function($query) use ($data){
                        return $query->whereIn('employee.status', array($data['status']));
                    })
                    ->when(array_key_exists('q', $data), function($query) use ($data){
                        return $query->where(function($query) use ($data){
                            $query->where('employee.first_name', 'like', '%'.$data['q'].'%');
                            $query->orwhere('employee.last_name', 'like', '%'.$data['q'].'%');
                            $query->orwhere('employee.email', 'like', '%'.$data['q'].'%');
                            $query->orwhere('employee.contact', 'like', '%'.$data['q'].'%');
                        });
                                    
                    })
                    ->when(array_key_exists('eid', $data), function($query) use ($data){
                        return $query->whereIn('employee.id',$data['eid'] );
                                    
                    })
                    ->paginate(2000);
    	return $employees;
    }
    public function getEmployeeDetail($data){

        $employees = DB::table('employee')
                    ->select('employee.*',
                        DB::raw("(SELECT COUNT(order_history.employee_id) from order_history join reward on order_history.reward_id = reward.id WHERE order_history.employee_id = employee.id and reward.payment_status=1) as reward_count"),
                        DB::raw("(SELECT name from corporate WHERE corporate.id = employee.corporate_id) as company_name")
                    )
                    ->where('employee.id',$data['eid'])
                    ->first();
        return $employees;
    }
    public function activeEmployee($data,$id){

    	$active_user = DB::table('employee')->where('id', $id)->update($data);
    	return $active_user;	
    }

     public function getActiveEmployeeCount(){
        return $active_brand_count = DB::table('employee')
                            ->where('employee.status','=',1)
                            ->count();
    }

    public function deactiveEmployee($data,$id){
    	$deactive_user = DB::table('employee')->where('id', $id)->update($data);
    	return $deactive_user;	
    }

    public function updateEmployee($data){
    	$update_employee = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $update_employee;	
    }

    public function deleteEmployee($data){
    	$delete_employee = DB::table('employee')->where('id', '=', $data['id'])->delete();
    	// $delete_employee = DB::table('employee')->where('id', $data['id'])->update($data);
    	return $delete_employee;
     }
    public function getEmployeeCount($data){

        return DB::table('employee')
                ->when(array_key_exists('status', $data), function($query) use ($data){
                    return $query->where('status', $data['status']);
                })
                ->when(array_key_exists('cid', $data), function($query) use ($data){
                    return $query->where('employee.corporate_id', $data['cid']);
                })
                ->count();
    }
    public function isEmployeeEmailExist($data){
        $user = DB::table('employee')
                    ->where('email','=',$data['email'])
                    ->when(array_key_exists('id', $data), function($query) use ($data){
                        return $query->where('id','!=',$data['id']);
                    })
                    ->first();
        return $user;
    }
    public function isEmployeeMobileExist($data){
        $user = DB::table('employee')
                    ->where('contact','=',$data['contact'])
                    ->when(array_key_exists('id', $data), function($query) use ($data){
                        return $query->where('id','!=',$data['id']);
                    })
                    ->first();
        return $user;
    }

    public function isEmployeeRewardIssued($data){
        $user = DB::table('order_history')
                    ->join('reward','order_history.reward_id','reward.id')
                    ->join('offer','reward.offer_id','offer.id')
                    ->where('order_history.employee_id',$data['eid'])
                    ->where('order_history.redeem',0)
                    ->where('reward.payment_status',1)
                    ->whereDate('offer.valid_to','>=' ,date('Y-m-d'))
                    ->get();
        return $user;
    }
    public function addOtp($data){
        return DB::table('otp_codes')->insert($data);
    }
    public function verifyOtp($data){
        $otp = DB::table('otp_codes')
                    ->where($data)
                    ->orderby('id','desc')
                    ->first();
        return $otp;
    }

    public function checkToken($data) {
        $employee = DB::table('employee')->where($data)->first();
        return $employee;
    }
}