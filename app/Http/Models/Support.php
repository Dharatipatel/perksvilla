<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Support extends Model {

    protected $table = 'support';
    protected $fillable = [
        'type',
        'description',
        'user_id',
        'category'
    ];

    public static function getById($supportId, $userCheck = true) {

        $support = Support::where("support.id", "=", $supportId);

        if ($userCheck === true) {
            
            $support->where("support.user_id", "=", session()->get("id"));
        }

        return $support->join("users", "users.id", "=", "support.user_id")
                        ->select("support.*", "users.email", "users.role_id",
                                DB::raw(" (select brand.name from brand WHERE brand.uid = support.user_id) as brand_name"),
                                DB::raw(" (select corporate.name from corporate WHERE corporate.uid = support.user_id) as corporate_name")
                        )
                        ->first();
    }
    
    public function ticketCount($data){
        return $active_brand_count = DB::table('support')
                            ->where('support.status','=',$data['status'])
                            ->count();
    }
    public function replys(){
        return $this->hasMany('App\Http\Models\SupportReply');
    }
    public function new_replys() {
        return $this->replys()->where('is_read','=', 1)->where('user_id','!=',\Session::get('id'));
    }
}
