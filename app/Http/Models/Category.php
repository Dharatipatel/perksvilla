<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    public function addCategory($data){
    	$category = DB::table('category')->insertGetId($data);
    	return $category;
    }

    public function getCategory($data){
        
    	$category = DB::table('category')
                    ->select('category.*',DB::raw("(SELECT  corporate.name FROM corporate where corporate.uid = category.user_id ) as user_name"))
                    ->whereIn('type',array($data['type']))
                    ->when(array_key_exists('status', $data),function($query) use ($data){
                        return $query->where('category.status', $data['status']); 
                    })
                    ->when(array_key_exists('user_id', $data),function($query) use ($data){
                        return $query->where(function($query) use ($data){
                             $query->whereNull('category.user_id'); 
                             $query->orwhere('category.user_id','=',$data['user_id']);   
                        });   
                    })
                    ->paginate(2000);
    	return $category;
    }


    public function updateCategory($data){
    	$update_category = DB::table('category')->where('id', $data['id'])->update($data);
    	return $update_category;	
    }

    public function deleteCategory($data){
        if(array_key_exists('custom' ,$data)){
    	   $delete_category = DB::table('category')->where('id', $data['id'])->update(array('status'=>$data['status'],'modified'=>$data['modified']));
        }else{
           $delete_category = DB::table('category')->where('id', '=', $data['id'])->delete();
        }
    	return $delete_category;
    }

    public function getCategoryById($id){
        $category = DB::table('category')->where('id', $id)->first();
        return $category;
    }
    public function activeCategory($data,$id){

        $active_category = DB::table('category')->where('id', $id)->update($data);
        return $active_category;   
    }

    public function deactiveCategory($data,$id){
        $deactive_category = DB::table('category')->where('id', $id)->update($data);
        return $deactive_category; 
    }
}