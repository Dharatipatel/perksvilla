<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Custom\SMSAPI;
use App\Custom\Common;
use App\Http\Models\OrderHistory;
use App\Http\Models\Offer;
use App\Http\Models\Brand;
use App\Http\Models\Notification;
use App\Http\Models\Corporate;
use App\Http\Models\Employee;
use DB;
use Carbon\Carbon;
use App\Custom\Email;

class CronController extends Controller {

    public function send_coupon_code() {

        try {
            $model = OrderHistory::join("employee", "employee.id", "=", "order_history.employee_id")
                    ->join("reward", 'order_history.reward_id', 'reward.id')
                    ->join("offer", 'reward.offer_id', 'offer.id')
                    ->select(
                        "employee.contact", "employee.email", "coupen as coupon", "offer.valid_to", "offer.title as offer_name", 'offer.value as coupon_value', 'offer.apply_on', 'offer.notes', 'offer.brand_address_id', 'offer.contact as offer_contact',
                        DB::raw("employee.id as employee_id", "CONCAT(employee.first_name, ' ', employee.last_name) as user_name"), 
                        DB::raw("(SELECT corporate.name from corporate  WHERE corporate.id = employee.corporate_id ) as conpany_name"), 
                        DB::raw("(SELECT brand.uid from  brand where brand.id = offer.brand_id) as user_id"), 
                        DB::raw("(SELECT category.name from category  WHERE category.id = reward.category_id ) as rewardtype"), 
                        DB::raw("(SELECT brand.logo from brand where brand.id = offer.brand_id)  as logo"), 
                        DB::raw("(SELECT brand.facebook_page from brand where brand.id = offer.brand_id)  as facebook_page"), 
                        DB::raw("(SELECT brand.website_url from brand where brand.id = offer.brand_id)  as website_url"), 
                        DB::raw("(SELECT brand.business_hours from brand where brand.id = offer.brand_id)  as business_hours")
                    )
                    ->where('order_history.is_flag', '=', 0)
                    ->where('reward.payment_status', 1)
                    ->whereDate("issue_date", "=", Carbon::now()->format("Y-m-d"))
                    ->get();
            $address = "";
            $i = 0;
            $this->notification = New Notification;
            foreach ($model as $m) {
                $user_id = $m->user_id;
                $offer_title = $m->offer_name;
                $address_id = explode(',', $m->brand_address_id);
                $addresses = Brand::getAddressById($address_id);
                foreach ($addresses as $key => $value) {
                    $address .= '<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; font-weight: normal; padding: 5px 0; margin: 0; line-height: 18px;"><strong><u>Outlet ' . ($key + 1) . ' :</u></strong> ' . $value->address . ', ' . $value->city . ' ,' . $value->state . ' ,' . $value->country . ' ,' . $value->pincode . '</p>';
                }
                //  send mail
                $apply_on = "";
                $email = new Email;
                if ($m->apply_on == 1) {
                    $apply_on = date('d-m-Y', strtotime($m->valid_to));
                } else if ($m->apply_on == 2) {
                    $apply_on = "Week Days (Mon-Fri)";
                } else if ($m->apply_on == 3) {
                    $apply_on = "Weekends  (Sat-Sun)";
                } else if ($m->apply_on == 4) {
                    $apply_on = "All Days";
                }
                $mailData = [
                    "name" => $m->user_name,
                    "email" => $m->email,
                    "code" => $m->coupon,
                    "rewardtype" => $m->rewardtype,
                    "offer_name" => $m->offer_name,
                    "valid_to" => date('d-m-Y', strtotime($m->valid_to)),
                    "coupon_value" => $m->coupon_value,
                    "facebook_url" => '<a style="margin-right: 10px;" href="' . $m->facebook_page . '"><img src="http://portal.perksvilla.com/assets/corporate/img/fb-icon.png" /></a>',
                    "website_url" => '<a href="' . $m->website_url . '"><img src="http://portal.perksvilla.com/assets/corporate/img/web-icon.png" /></a>',
                    'apply_on' => $apply_on,
                    'business_hours' => $m->business_hours,
                    'notes' => preg_replace("/\r\n|\r|\n/", '<br/>', $m->notes),
                    'addresses' => $address,
                    'brand_logo' => '<img style="max-width: 100%;" src="http://portal.perksvilla.com/uploads/brand/' . $m->logo . '">',
                    'contact_no' => '<span><img src="http://portal.perksvilla.com/assets/corporate/img/call-icon.png" style="margin-right: 5px;margin-left: 8px;" /><b><span>'.$m->offer_contact.'</span></b></span>'
                ];

                $email->send("coupon_code", $mailData);

                //  send SMS
                $sms = new SMSAPI;
                $smsData["numbers"] = [$m->contact];

                $smsData["details"] = [
                    "name" => $m->user_name,
                    "code" => $m->coupon,
                    "companyname" => $m->conpany_name,
                    "rewardtype" => $m->rewardtype,
                    "expiredate" => date('d-m-Y', strtotime($m->valid_to)),
                ];
                $result = $sms->send("coupon_code", $smsData);
                if ($result) {
                    DB::table('order_history')
                            ->where('coupen', '=', $m->coupon)
                            ->update(array('is_flag' => 1));
                }
                
                
                $message = "You are rewarded for ".$m->rewardtype;
                $title = 'Coupon Received';
                $notification_data[$i] = array(
                    'subject' => $title,
                    'message' => $message,
                    'user_id' => $m->user_id,
                    'sender_id' => 1,
                    'employee_id' => $m->employee_id,
                    'job_id' => 0,
                    'status' => 0,
                    'type' => 0,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $i++;
                $this->push_notificationSend($m->employee_id, $message,$title);
            }
            if (count($model) > 0) {
                $notify = $this->notification->addNotifications($notification_data);
                $notification = array(
                                  'subject'    => 'Coupon Purchased',
                                  'message'    => '1 Company purchased your '.$offer_title.' coupon',
                                  'user_id'    => $user_id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
                $notification_brand = $this->notification->addNotification($notification);
                dd("Coupon code sent to " . count($model) . " users. SMS: {$result}");
            } else {
                dd("There is no employee to send message and email");
            }
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function updateOfferStatus() {

        try {
            $expired_query = DB::table('offer')
                    ->whereDate('valid_to', '<', date('Y-m-d'))
                    ->update(array('offer_status_id' => 6));

            $offers = DB::table('offer')
                    ->whereDate('publish_date', '=', date('Y-m-d'))
                    ->where('offer_status_id', '=', 2)
                    ->get();

            if (count($offers) > 0) {
                $this->brand = new Brand;
                $this->notification = new Notification;
                foreach ($offers as $offer) {
                    $notification_data = array(
                        'subject' => 'Offer Confirmation',
                        'message' => 'Offer ' . $offer->title . " is Live Now",
                        'user_id' => $this->brand->getUserById($offer->brand_id)->id,
                        'sender_id' => 1,
                        'job_id' => 0,
                        'status' => 0,
                        'type' => 0,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    );
                    $notify = $this->notification->addNotification($notification_data);
                }
            }
            $active_query = DB::table('offer')
                    ->whereDate('publish_date', '=', date('Y-m-d'))
                    ->where('offer_status_id', '=', 2)
                    ->update(array('offer_status_id' => 1));

            dd("Offer Status Updated SuccessFully");
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function reminderCouponCode() {
        $this->notification = New Notification;
        $model = OrderHistory::join("employee", "employee.id", "=", "order_history.employee_id")
                ->join("reward", 'order_history.reward_id', 'reward.id')
                ->join("offer", 'reward.offer_id', 'offer.id')
                ->select(DB::raw( "employee.id as employee_id","CONCAT(employee.first_name, ' ', employee.last_name) as user_name"), "employee.email", "offer.valid_to", "offer.title as offer_name", DB::raw("(SELECT category.name from category  WHERE category.id = reward.category_id ) as rewardtype"), DB::raw("(SELECT brand.name from brand where brand.id = offer.brand_id)  as brandname"),
                    DB::raw("(SELECT brand.uid from brand where brand.id = offer.brand_id)  as user_id")
                )
                ->where('reward.payment_status', 1)
                ->whereDate("valid_to", "=", Carbon::now()->addDays(7)->format('Y-m-d'))
                ->orwhereDate("valid_to", "=", Carbon::now()->addDays(2)->format('Y-m-d'))
                ->get();
        $email = new Email;
        foreach ($model as $m) {
            $mailData = [
                "name" => $m->user_name,
                "email" => $m->email,
                "rewardtype" => $m->rewardtype,
                "offer_name" => $m->offer_name,
                "brandname" => $m->brandname,
                "date" => date('d-m-Y', strtotime($m->valid_to)),
            ];
            $result = $email->send("coupon_reminder", $mailData);
            $i = 0;
            $message = "Your ".$m->offer_name." coupon will expire in 1 week";
            $title   = 'Coupon Expiring';
                $notification_data[$i] = array(
                    'subject' => $title,
                    'message' => $message,
                    'user_id' => $m->user_id,
                    'sender_id' => 1,
                    'employee_id' => $m->employee_id,
                    'job_id' => 0,
                    'status' => 0,
                    'type' => 0,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $i++;
            $notify = $this->notification->addNotifications($notification_data);
            $this->push_notificationSend($m->employee_id, $message,$title);
        }
        if (count($model) > 0)
            dd("Coupon code sent to " . count($model));
        else
            dd("There is no employee to send message and email");
    }

    public function monthlySummary() {
        $corporates = Corporate::join('users', 'users.id', '=', 'corporate.uid')
                ->select('users.email', 'corporate.id', 'corporate.name')
                ->where('users.status', 1)
                ->get();
        foreach ($corporates as $key => $corporate) {
            $data = array(
                'cid' => $corporate->id,
                'status' => 1,
            );
            $html = "";
            $employees = Employee::getEmployeeDashboard($data);

            if (count($employees) > 0) {
                foreach ($employees as $key => $employee) {
                    $birthday = Carbon::parse($employee->birth_date);
                    $joining_date = Carbon::parse($employee->joining_date);
                    $message = "";
                    if ($birthday->month == Carbon::now()->month) {
                        $message = 'having birthday on ' . $birthday->format('jS  F');
                    } else if ($joining_date->month == Carbon::now()->month && $joining_date->year == Carbon::now()->year) {
                        $message = 'is a New Joinee of Current Month';
                    } else if ($joining_date->month == Carbon::now()->subMonth()->month && $joining_date->year == Carbon::now()->year) {
                        $message = 'is a New Joinee of Last Month';
                    } else if ($joining_date->month == Carbon::now()->month) {
                        $message = 'Will complete ' . $joining_date->diffForHumans(null, true) . ' on ' . $joining_date->format('jS  F');
                    }
                    $html .= '<tr><td> ' . $employee->first_name . ' ' . $employee->last_name . ' </td><td> ' . $message . ' </td></tr>';
                }

                $mail_data = array(
                    'companyname' => $corporate->name,
                    'name' => $corporate->name,
                    'email' => $corporate->email,
                    'employees' => '<table style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #787878; line-height: 20px; padding: 0 0 0 0; margin: 0;" border="1" width="100%" cellspacing="0" cellpadding="0">
    <tbody><tr><td colspan="2"><div align="center"><strong>What\'s New in this Month? </strong></div></td></tr>' . $html . '</tbody></table>'
                );
                $email = New Email;
                $result = $email->send('monthly_summary', $mail_data);
            }
        }
    }

    public function push_notificationSend($employee_id, $message,$title) {

        $app_token = DB::table('employee')->where('id', $employee_id)
                        ->first()
                ->app_token;

        if ($app_token != NULL) {
            $msg = array(
                'body' => $message,
                'title' => $title,
                'subtitle' => $title,
                'vibrate' => 1,
                'sound' => 1,
                    // 'largeIcon' => 'large_icon',
                    // 'smallIcon' => 'small_icon'
            );

            $fields = array(
                'registration_ids' => array($app_token),
                'notification' => $msg,
                'data' => array(
                    'action' => 'CouponList'
                )
            );
            Common::send($fields);
        }
    }

}
