<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\ForgotPasswordRequest;
use App\Custom\Common;
use App\Custom\Email;
use App\Http\Models\User;

class ForgotPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset emails and
      | includes a trait which assists in sending these notifications from
      | your application to your users. Feel free to explore this trait.
      |
     */

use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
        $this->user = new User;
    }

    public function forgotPasswordEmail(ForgotPasswordRequest $request) {
        try {
            $data = array();
            $user = User::where("email", $request->email)->where("role_id", $request->role_id)
                    ->when($request->role_id == 3, function($query)use ($data){
                        return $query->join('corporate','users.id','corporate.uid');
                    })
                    ->when($request->role_id == 2, function($query){
                        return $query->join('brand','users.id','brand.uid');
                    })
                    ->first();

            if ($user == null) {
                return response()->json([
                            'error' => true,
                            'message' => "We cannot find any account associated with this email id. Please enter correct email address to proceed further.",
                                ], 400);
            }
            $token = Common::getPasswordResetToken(40);
            $data = [
                "email" => $request->email,
                "token" => $token,
                "created_at" => date("Y-m-d H:i:s"),
            ];


            $result = $this->user->savePasswordResetToken($data);
            if ($result) {
                $link = "{$request->resetpassword}/" . $token . "?email=" . urlencode($request->email);
                $email = new Email;
                $mailData = [
                    "name" => empty($user->name) ? $user->email : $user->name ,
                    "email" => $request->email,
                    "link" => "<a href='{$link}'>{$link}</a>"
                ];

                $email->send("forgot_password", $mailData);

                return response()->json([
                            'error' => false,
                            'data' => [],
                            'message' => "We have sent an email with password reset link to {$request->email}",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Error in password reset reqest",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

}
