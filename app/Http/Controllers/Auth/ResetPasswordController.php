<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Http\Requests\ResetPasswordRequest;
use App\Custom\Common;
use App\Http\Models\User;
use Illuminate\Http\Request;
use DB;

class ResetPasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */

use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
        $this->user = new User;
    }

    public function forgotPasswordReset($token, Request $request) {
        try {
            $user = User::where("email", $request->email)->first();
            if ($user == null) {
                $data = [
                    "title" => "Password Reset",
                    "message" => "Email does not exist"
                ];

                return view("message")->with($data);
            }

            $tkn = DB::table("password_resets")->where("email", $request->email)->where("token", $token)->first();
            if ($tkn == null) {
                $data = [
                    "title" => "Password Reset",
                    "message" => "Invalid password reset link"
                ];

                return view("message")->with($data);
            }

            if ($user->role_id == 2) {
                $type = "brand";
            } else if ($user->role_id == 3) {
                $type = "corporate";
            }
            $data = array(
                'title' => 'Reset Password',
                'type' => $type,
                'email' => $request->email,
                'token' => $token
            );
            return view('auth.passwords.reset')->with($data);
        } catch (\Exception $e) {
            $data = [
                "title" => "Password Reset",
                "message" => "Error in password reset"
            ];

            return view("message")->with($data);
        }
    }

    public function forgotPasswordResetSave(ResetPasswordRequest $request) {
        try {
            $user = User::where("email", $request->email)->first();
            if ($user == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Email does not exist",
                                ], 400);
            }

            $tkn = DB::table("password_resets")->where("email", $request->email)->where("token", $request->reset_token)->first();
            if ($tkn == null) {
                return response()->json([
                            'error' => true,
                            'message' => "Invalid token",
                                ], 400);
            }
            $data = ["password" => md5($request->password)];
            $result = $this->user->changePassword($data, $user->id);

            if ($result) {
                if ($user->role_id == 2) {
                    $login_url = route("brandlogin");
                } else if ($user->role_id == 3) {
                    $login_url = route("corporatelogin");
                }
                return response()->json([
                            'error' => false,
                            'data' => ["url" => $login_url],
                            'message' => "Password changed successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Password reset failed",
                                ], 400);
            }

        } catch (\Exception $e) {
            return response()->json([
                            'error' => true,
                            'message' => "Error in password reset",
                                ], 400);
        }
    }

}
