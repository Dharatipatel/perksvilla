<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class OrderHistoryController extends Controller
{
    public function index(){
        if (Session::has('loginvalue')) {
            $data = array(
                'title' => 'Offer'
            );
            return view('brand.orderhistory.index')->with($data);
        } else {
            return redirect('login');
        }
    }
}