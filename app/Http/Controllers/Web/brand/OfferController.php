<?php
namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;


class OfferController extends Controller
{
    public function index(){

        $data = array(
            'title' => 'Offer',
            'type'  => 'list',
        );
        if(Request::has('pending')){
            $data['status'] = 3;
        } elseif(Request::has('active')){
            $data['status'] = 1;
        } elseif(Request::has('upcoming')){
            $data['status'] = 2;
        } else {
            $data['status'] = 1;
        }
        return view('brand.offer.index')->with($data);
    }
    public function addOffer(){
        $data = array(
            'title' => 'Add Offer',
            'type'  => 'add',
        );
        return view('brand.offer.add')->with($data);
    }
    public function pastOffer(){
        $data = array(
            'title' => 'Past Offer',
            'type'  => 'past',
        );
        if(Request::has('expired')){
            $data['status'] = 6;
        } elseif(Request::has('cancelled')){
            $data['status'] = 4;
        } elseif(Request::has('deleted')){
            $data['status'] = 5;
        } else {
            $data['status'] = 6;
        }
        return view('brand.offer.past')->with($data);
    }
}