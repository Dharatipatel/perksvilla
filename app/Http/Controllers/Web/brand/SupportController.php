<?php

namespace App\Http\Controllers\Web\brand;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Models\SupportStatus;
use App\Http\Models\Support;
use Yajra\DataTables\Facades\DataTables;
use DB;
use Auth;
use App\Http\Models\SupportCategory;
use App\Http\Requests\SupportRequest;
use App\Http\Requests\SupportReplyRequest;
use App\Http\Models\SupportReply;
use App\Custom\Email;
use App\Http\Models\User;
use App\Http\Models\Brand;

class SupportController extends Controller {

    var $viewContent = [];

    public function index() {
        $this->viewContent['title'] = "Message";
        $this->viewContent['type'] = "list";
        $this->viewContent['menu'] = "support";
        $this->viewContent['status'] = SupportStatus::pluck('name', 'id');
        $supports = Support::where('user_id',\Session::get('id'))->get();
        foreach ($supports as $key => $support) {
            SupportReply::where('support_id',$support->id)->update(array('is_new' => 0,));
        }
        return view('brand.support.index', $this->viewContent);
    }

    public function lists() {
        $model = Support::join("support_category", "support_category.id", "=", "support.category")
                ->select("support.*", DB::raw("support_category.name as category_name"))
                ->where("support.user_id", session()->get("id"))
                ->latest();

        return Datatables::eloquent($model)
                        ->addColumn('title', function ($model) {
                            $new = '';
                            if($model->new_replys->count() >0){
                                $new ='<span>('.$model->new_replys->count().')</span>';
                            }
                           return $model->title.$new;
                        })
                        ->addColumn('category_name', function ($model) {
                            return $model->category_name;
                        })
                        ->addColumn('created_at', function ($model) {
                            return date("d-m-Y g:i:s A", strtotime($model->created_at));
                        })
                        ->addColumn('action', function ($model) {
                            $reply_url = '<a href=' . route("brand.support.reply", $model->id) . ' class="edit-list-btn btn btn-default btn-xs"><i class="fa fa-reply"></i> Reply</a> ';
                            $remove_url = $model->status != 2 ? '<a href="javascript:;" data-url="' . route("brand.support.delete", $model->id) . '" onclick="deleteRow(\'' . $model->id . '\');" class="edit-list-btn btn btn-danger btn-xs" id="del_' . $model->id . '"><i class="fa fa-trash"></i> Delete</a>' : '';
                            return $reply_url . $remove_url;
                        })
                        ->rawColumns(['title','action'])
                        ->make(true);
    }

    public function getManage($id = '') {

        $status_support = SupportStatus::pluck('name', 'id');
        $this->viewContent['status_support'] = $status_support;
        $this->viewContent['categoties'] = SupportCategory::pluck("name", "id");
        $this->viewContent['title'] = 'Add Message';
        $this->viewContent['menu'] = 'support';
        $this->viewContent['type'] = 'add';
        $support_detail = [];

        if (empty($id) === false) {

            $this->viewContent['page_title'] = 'Manage Message';

            try {
                $support_detail = Support::where("id", $id)->where("user_id", session()->get("id"))->first();
            } catch (\Exception $e) {
                return \Redirect::route('brand.support')->with('success', 'No data found for selected id');
            }
        }

        $this->viewContent['support_detail'] = $support_detail;
        return view('brand.support.manage', $this->viewContent);
    }

    public function save(SupportRequest $request) {
        $support = new Support;
        $brand = new Brand;
        $user = $brand->getById(session()->get("id"));
        $msg = 'Message created successfully';

        if (empty($request->get('id')) === false) {

            $support = Support::where("id", $request->get('id'))
                            ->where("user_id", $user->id)->first();

            $msg = 'Message updated successfully';
        }

        $support->title = $request->get('title');
        $support->description = htmlentities($request->get('description'));
        $support->category = $request->get('category');
        $support->status = 1;
        $support->user_id = $user->id;
        $support->save();

        if (empty($request->id)) {
            $model = new SupportReply;
            $model->comment = htmlentities($request->get('description'));
            $model->support_id = $support->id;
            $model->user_id = $user->id;
            $model->save();

            // $email = new Email;
            // $mailData = [
            //     'name' => $user->name,
            //     'email' => $user->email,
            // ];

            // $email->send('support_ticket_user_confirmation', $mailData);
        }

        return \Redirect::route("brand.support")->with('success', $msg);
    }

    function delete($id) {

        $data = Support::where("id", $id)->where("user_id", session()->get("id"))->first();

        if (count($data) > 0) {
            $delete = $data->delete();
            if ($delete) {
                session()->flash('success', 'Message removed successfully !');
                return redirect(route("brand.support"));
            } else {
                return redirect(route("brand.support"))->withErrors(array('message' => 'Error in Message remove!'));
            }
        } else {
            return redirect(route("brand.support"))->withErrors(array('message' => "Message does not exist"));
        }
    }

    public function replyShow($id) {
        // try {
        $support = Support::getById($id);
        $support_reply = SupportReply::getById($support->id);
        $new_update=SupportReply::where('support_id',$support->id)->where('user_id','!=',\Session::get('id'))->update(array('is_read' => 0,));
        $this->viewContent['title'] = "Message Reply";
        $this->viewContent['menu'] = 'support';
        $this->viewContent['support'] = $support;
        $this->viewContent['support_reply'] = $support_reply;
        return view('brand.support.reply', $this->viewContent);
        //  } catch (\Exception $e) {
        //     return \Redirect::route('brand.support')->with('error', 'No data found for selected id');
        // }
    }

    public function replySave(SupportReplyRequest $request) {
        try {
            $id = $request->id;
            $support = Support::getById($id, true);
            $msg = 'Message reply sent successfully';
            $model = new SupportReply;
            $model->comment = htmlentities($request->comment);
            $model->support_id = $support->id;
            $model->user_id = session()->get("id");
            $model->save();

            $email = new Email;
            $title = SupportReply::join('support', 'support.id', '=', 'support_reply.support_id')
                            ->select('support.title')
                            ->where('support_reply.support_id', $request->id)
                            ->first()->title;
            $brand = new Brand;
            $user = $brand->getById(session()->get("id"));
            // $mailData = [
            //     'name' => $user->name,
            //     'admin' => 'Admin',
            //     'email' => env('MAIL_CONTACT_US'),
            //     'user_email' => $user->email,
            //     'date' => $model->created_at,
            //     'link' => '<a href=' . route("support.reply", $request->id) . '>Reply</a>',
            //     'title' => $title,
            //     'reply' => $request->comment
            // ];

            // $email->send('support_reply_to_admin', $mailData);
            return \Redirect::back()->with('success', $msg);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }
    public function supportMessageCount(){
        try{
            $data = array(
                            'is_new'  => 1,
                            'support.user_id' => \Request::get('id'),
                        );
            $count = SupportReply::join('support','support.id','support_reply.support_id')->where($data)->where('support_reply.user_id','!=',\Request::get('id'))->count();
            $msg=($count>0)?'<span class="badge bg-red badge-corner">'.$count.'</span>':"";
            return response()->json([
                        'error'   => false,
                        'data'    => $msg
                    ]);
        } catch (\Exception $e) {
            return response()->json([
                        'error'   => true,
                        'data'    => $e->getMessage()
                ]);
        }
    }
}
