<?php

namespace App\Http\Controllers\Web\brand;

use App\Http\Controllers\Controller;
use Request;
use Session;
use App\Http\Models\Notification;

class UserController extends Controller {

    public function __construct() {
        $this->notification = new Notification;
    }

    public function login() {

        return view('brand.login');
    }

    public function logout() {
        $data = Session::flush();
        return redirect('login');
    }

    public function setSession() {
        $input = Request::all();
        Session::regenerate();
        if (!Session::has('loginvalue')) {
            if (Request::has('token')) {
                session(['token' => $input['token']]);
            }
            if (Request::has('_token')) {
                session(['_token' => $input['_token']]);
            }
            if (Request::has('email')) {
                session(['email' => $input['email']]);
            }
            if (Request::has('role_id')) {
                session(['role' => $input['role_id']]);
            }
            if (Request::has('id')) {
                session(['id' => $input['id']]);
            }
            if (Request::has('bid')) {
                session(['bid' => $input['bid']]);
            }
            session(['loginvalue' => 1]);
        }
        $data = Session::all();
        return $data;
    }

    public function editProfile() {

        $data = array(
            'title' => 'Offer',
            'type' => 'edit'
        );
        return view('brand.profile.edit')->with($data);
    }

    public function viewProfile() {

        $data = array(
            'title' => 'Offer',
            'type' => 'view'
        );
        return view('brand.profile.view')->with($data);
    }

    public function changePassword() {

        $data = array(
            'title' => 'Offer'
        );
        return view('brand.profile.changepassword')->with($data);
    }

    public function myWallet() {

        $data = array(
            'title' => 'wallet'
        );
        return view('brand.profile.wallet')->with($data);
    }

    public function bankDetails() {

        $data = array(
            'title' => 'Offer'
        );
        return view('brand.profile.bankdetails')->with($data);
    }

    public function forgotPassword() {
        $data = array(
            'title' => 'Brand forgot Password'
        );
        return view('brand.forgot-password')->with($data);
    }

    public function notification() {
        try {
            $notification = $this->notification->userNotifications(Request::get("id"));
            $data['user_id'] = Request::get('id');
            $data['status'] = 0;
            $data['employee'] = 0;
            $count = $this->notification->getNotificationCount($data);
            $view = view("brand.include.notification", ["notification" => $notification,'count' => $count])->render();
            return response()->json([
                        'error' => false,
                        'data' => $view,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching notification",
                            ], 400);
        }
    }
    public function notificationRead(){
        try {
            $notification = $this->notification->readNotification(Request::get("user_id"));
            return response()->json([
                        'error' => false,
                        'data' => $notification,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => "Error while fetching notification",
                            ], 400);
        }
    }

}
