<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class EmployeeController extends Controller
{
    public function __construct(){
       
    }

    public function employee(){
        $corporate = "";
       		if(Request::has('id')){
       			$corporate = Request::get('id');
       		}
        $data = array(
            'title' => 'Employee',
            'corporate' => $corporate
        );
        return view('admin.employee.master')->with($data);
    }
}