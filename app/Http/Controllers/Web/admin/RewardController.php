<?php
namespace App\Http\Controllers\Web\admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Request;
use Session;


class RewardController extends Controller
{
    public function __construct(){
       
    }
    
    public function getRewards(){
         $data = array(
            'title' => 'Reward',
           
        );
        if(Route::currentRouteName()=='generalreward'){
            $data['type'] = 'general';
        } else if (Route::currentRouteName()=='usercreatedreward'){
             $data['type'] = 'user';
        }
       
        return view('admin.reward.master')->with($data);
    }

   
}