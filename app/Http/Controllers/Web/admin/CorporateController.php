<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class CorporateController extends Controller
{
    public function __construct(){
       
    }

    public function listcorporate(){
       
        $data = array(
            'title' => 'Corporate',
            'type'  => 'list'
        );
        return view('admin.corporate.master')->with($data);
    }
    public function addcorporate(){
       
        $data = array(
            'title' => 'Corporate',
            'type'  => 'add'
        );
        return view('admin.corporate.master')->with($data);
    }
    public function orderHistoryCorporate(){
       
        $data = array(
            'title' => 'Order History',
            'type'  => ''
        );
        return view('admin.corporate.orderhistory')->with($data);
    }
}