<?php

namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\Mailtemplate;
use App\Http\Requests\Admin\MailtemplateRequest;
use Yajra\DataTables\Facades\DataTables;
use DB;

class MailtemplateController extends Controller {

    public function index() {
        $data['title'] = "Mail Template";
        $data['action'] = "Manage";
        return view('admin.mailtemplate.index', $data);
    }

    public function create() {
        $data['title'] = "Mail Template";
        return view('admin.mailtemplate.manage', $data);
    }

    public function get_list() {
        $model = Mailtemplate::select("*")->orderBy('id', 'DESC');

        return Datatables::eloquent($model)
                        ->addColumn('title', function ($model) {
                            return $model->title;
                        })
                        ->addColumn('subject', function ($model) {
                            return $model->subject;
                        })
                        ->addColumn('text', function ($model) {
                            return html_entity_decode($model->text);
                        })
                        ->addColumn('tags', function ($model) {
                            return $model->tags;
                        })
                        ->addColumn('action', function ($model) {
                            $edit_url = '<a href=' . route('admin.mailtemplates.edit', ['id' => $model->id]) . ' class="edit-list-btn"><i class="text-danger fa fa-pencil"></i></a>';
                            $deletelink = route('admin.mailtemplates.delete', ['id' => $model->id]);
                            $remove_url = '<a href="javascript:;" data-url="' . $deletelink . '" onclick="deleteRow(\'' . $model->id . '\');" class="edit-list-btn" id="del_' . $model->id . '"><i class="text-success fa fa-trash-o"></i></a>';

                            return $edit_url . " " . $remove_url;
                        })
                        ->make(true);
    }

    public function edit($id) {
        $mailtemplate = Mailtemplate::find($id);



        if ($mailtemplate) {
            return view('admin.mailtemplate.manage', ["title" => "Mail Template", "mail_template" => $mailtemplate]);
        } else {
            session()->flash('error-mailtemplate', 'Mail Template does not exist!');
            return redirect()->route('admin.mailtemplates.index')->withErrors(array('message' => 'Mailtemplate does not exist!'));
        }
    }

    public function store(MailtemplateRequest $request, $id = null) {
        if ($id != '') {
            $mailtemplate = Mailtemplate::find($id);
        } else {
            $mailtemplate = new Mailtemplate;
        }
        $mailtemplate->subject = htmlentities($request->subject);
        $mailtemplate->text = htmlentities($request->text);
        $mailtemplate->tags = $request->tags;
        $mailtemplate->title = $request->title;
        if ($mailtemplate->save()) {
            if ($id != '') {
                $request->session()->flash('success', 'Mail template Updated successfully!');
                return redirect()->route('admin.mailtemplates.edit', ['id' => $id]);
            } else {
                $request->session()->flash('success', 'Mail template Added successfully!');
                return redirect()->route('admin.mailtemplates.index');
            }
        } else {
            if ($id != '') {
                $request->session()->flash('error', 'Error in Mail template Update!');
                return redirect()->route('admin.mailtemplates.edit', ['id' => $id])->withErrors(array('message' => 'Error in mailtemplate Update!'));
            } else {
                return redirect()->route('admin.mailtemplates.index')->withErrors(array('message' => 'Error in mail template Add!'));
            }
        }
    }

    function delete($id, Request $request) {
        $mailtemplate = Mailtemplate::find($id);
        if (count($mailtemplate) > 0) {
            if ($mailtemplate->delete()) {
                $request->session()->flash('success', 'Mail template removed successfully !');
                return redirect()->route('admin.mailtemplates.index');
            } else {
                return redirect()->route('admin.mailtemplates.index')->withErrors(array('message' => 'Error in Mail template remove !'));
            }
        } else {
            return redirect()->route('admin.mailtemplates.index')->withErrors(array('message' => 'Mail template not exist'));
        }
    }

    public function preview($id) {

        $mail_templates_detail = Mailtemplate::find($id);
        $layout = "mail_layout";

        return \View::make("auth.emails.inc.{$layout}", ['template' => html_entity_decode($mail_templates_detail->text)]);
    }

}
