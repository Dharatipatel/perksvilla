<?php
namespace App\Http\Controllers\Web\admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Request;
use Session;


class SMSTemplateController extends Controller
{
    public function __construct(){
       
    }

    public function index(){
       
        $data = array(
            'title' => 'SMS Template'
        );
        if(Route::currentRouteName() == "sms.templates"){
              $data['type'] = "list" ;
          } else if(Route::currentRouteName() == "sms.templates.add"){
              $data['type'] = "add";
          } 
        return view('admin.sms_template.master')->with($data);
    }
}