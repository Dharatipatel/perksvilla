<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class LocationController extends Controller
{
    public function __construct(){
       
    }

    public function country(){
        
            $data = array(
                'title' => 'Country',
                'type' => 'country'
            );
            return view('admin.location.master')->with($data);
        
        
    }
    public function state(){
       
            $data = array(
                'title' => 'State',
                'type' => 'state'
            );
            return view('admin.location.master')->with($data);
        
        
    }
    public function city(){
        
            $data = array(
                'title' => 'city',
                'type' => 'city'
            );
            return view('admin.location.master')->with($data);
        
    }
}