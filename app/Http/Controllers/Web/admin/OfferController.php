<?php
namespace App\Http\Controllers\Web\admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Request;
use Session;


class OfferController extends Controller
{
    public function __construct(){
       
    }
    
    public function offer(Request $request){
       		$brand = "";
          $type="";
          if(Route::currentRouteName() == "offerpending"){
              $type = 3;
          } else if(Route::currentRouteName() == "offerpast"){
              $type = 6;
          } else if(Route::currentRouteName() == "offeractive"){
              $type = 1;
          } else if(Route::currentRouteName() == "offerupcoming"){
              $type = 2;
          } else if(Route::currentRouteName() == "offerreject"){
              $type = 5;
          }

       		if(Request::has('id')){
       			$brand = Request::get('id');
       		}
            $data = array(
                'title' => 'Offer',
                'brand' =>  $brand,
                'type'  =>  $type,
            );
            return view('admin.offer.master')->with($data);
    }
     
}