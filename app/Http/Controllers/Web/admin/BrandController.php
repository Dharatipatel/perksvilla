<?php
namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use Request;
use Session;


class BrandController extends Controller
{
    public function __construct(){
       
    }
    
    public function listBrand(){
        $data = array(
            'title' => 'Brand',
            'type'  => 'list'
        );
        return view('admin.brand.master')->with($data);
    }
    public function addBrand(){
       
        $data = array(
            'title' => 'Brand',
            'type'  => 'add'
        );
        return view('admin.brand.master')->with($data);
       
    }
    public function paymentRequestBrand(){
       
        $data = array(
            'title' => 'Payment Request',
            'type'  => ''
        );
        return view('admin.brand.payment')->with($data);
       
    }
}