<?php

namespace App\Http\Controllers\Web\admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Support;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\Admin\SupportReplyRequest;
use DB;
use App\Http\Models\SupportReply;
use App\Custom\Email;
use App\Http\Models\User;
use App\Http\Models\SupportStatus;

class SupportController extends Controller {

    var $viewContent = [];

    public function open() {
        $data['menu'] = "Opened Message";
        $data['title'] = "Opened Message";
        $data['action'] = "Manage";
        return view('admin.support.open', $data);
    }

    public function closed() {
        $data['menu'] = "Closed Message";
        $data['title'] = "Closed Message";
        $data['action'] = "Manage";
        return view('admin.support.closed', $data);
    }

    public function lists($status) {

        $model = Support::join("support_status", "support_status.id", "=", "support.status")
                ->join("support_category", "support_category.id", "=", "support.category")
                ->join("users", "users.id", "=", "support.user_id")
                ->select("support.*", 
                    DB::raw("support_category.name as category_name, support_status.name as status_name, users.email as user_name ,users.role_id"),
                    DB::raw(" (select brand.name from brand WHERE brand.uid = support.user_id) as brand_name"),
                    DB::raw(" (select corporate.name from corporate WHERE corporate.uid = support.user_id) as corporate_name")
                )
                ->latest()
                ->where('support.status', $status);
       
        return Datatables::eloquent($model)
                        ->addColumn('company_name', function ($model) {
                            if($model->brand_name!= NULL){
                                return $model->brand_name;
                            } else if($model->corporate_name!= NULL){
                                return $model->corporate_name;
                            }
                        })
                        ->addColumn('type', function ($model) {
                            if($model->role_id==2){
                                return "Brand";
                            } else if($model->role_id==3){
                                return "Corporate";
                            }
                        })
                        ->addColumn('title', function ($model) {
                            $new = '';
                            if($model->new_replys->count() >0){
                                $new ='<span>('.$model->new_replys->count().')</span>';
                            }
                            return $model->title.$new;
                        })
                        ->addColumn('category_name', function ($model) {
                            return $model->category_name;
                        })
                        ->addColumn('created_at', function ($model) {
                            return date("d-m-Y g:i:s A", strtotime($model->created_at));
                        })
                        ->addColumn('action', function($model) {
                            $edit_url = '<a href=' . route("support.reply", $model->id) . ' class="edit-list-btn "><i class="fa fa-reply"></i></a> ';
                            $remove_url = '<a href="javascript:;" data-url="' . route("support.delete", $model->id) . '" onclick="deleteRow(\'' . $model->id . '\');" class="edit-list-btn " id="del_' . $model->id . '"><i class="fa fa-trash-o"></i></a>';
                            return $edit_url . $remove_url;
                        })
                        ->rawColumns(['title','action'])
                        ->make(true);
    }

    function delete($id) {

        $data = Support::find($id);

        if ($data != null) {
            $delete = $data->delete();
            if ($delete) {
                session()->flash('success', 'Message removed successfully !');
                return redirect(route("support.open"));
            } else {
                return redirect(route("support.open"))->withErrors(array('message' => 'Error in Message remove!'));
            }
        } else {
            return redirect(route("support.open"))->withErrors(array('message' => "Message does not exist"));
        }
    }

    public function replyShow($id) {
        $support = Support::getById($id, false);
        $support_reply = SupportReply::getById($support->id);
        $new_update=SupportReply::where('support_id',$support->id)->where('user_id','!=',\Session::get('id'))->update(array('is_read' => 0,));
        $this->viewContent['title'] = "Message";
        $this->viewContent['menu'] = 'support';
        $this->viewContent['support'] = $support;
        $this->viewContent['support_reply'] = $support_reply;
        $this->viewContent['support_status'] = SupportStatus::pluck('name', 'id');
        return view('admin.support.manage', $this->viewContent);
    }

    public function replySave(SupportReplyRequest $request) {
        $msg = 'Message reply sent successfully';
        $model = new SupportReply;
        $model->comment = htmlentities($request->comment);
        $model->support_id = $request->id;
        $model->user_id = session()->get("id");
        $model->save();

        $s = Support::find($request->id);
        $s->status = $request->status;
        $s->save();

        $email = new Email;
        $support = SupportReply::where('support_id', $request->id)
                ->where('user_id', '!=', session()->get("id"))
                ->first();
        $user = User::find($support->user_id);
        $title = SupportReply::join('support', 'support.id', '=', 'support_reply.support_id')
                        ->select('support.title')
                        ->where('support_reply.support_id', $request->id)
                        ->first()->title;

        $type = 'brand';
        if ($user->role_id == 3) {
            $type = 'corporate';
        }
        // $mailData = [
        //     'name' => $user->email,
        //     'admin' => 'Admin',
        //     'email' => $user->email,
        //     'admin_email' => env('MAIL_CONTACT_US'),
        //     'date' => $model->created_at,
        //     'link' => '<a href=' . route("{$type}.support.reply", $request->id) . '>Reply</a>',
        //     'title' => $title,
        //     'reply' => $request->comment
        // ];

        // $email->send('support_reply_to_user', $mailData);

        return \Redirect::back()->with('success', $msg);
    }

}
