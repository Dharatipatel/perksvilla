<?php
namespace App\Http\Controllers\Web\admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use Request;
use Session;


class CouponController extends Controller
{
    public function __construct(){
       
    }
    
    public function coupon(Request $request){
       		$brand = "";
          $type="";
          if(Route::currentRouteName() == "activecoupon"){
              $type = 0;
          } else if(Route::currentRouteName() == "usedoupon"){
              $type = 1;
          } else if(Route::currentRouteName() == "expiredcoupon"){
              $type = 2;
          }
          $data = array(
              'title' => 'Offer',
              'type'  =>  $type,
          );
          return view('admin.coupon.master')->with($data);
    }
     
}