<?php
namespace App\Http\Controllers\Web\corporate;

use App\Http\Controllers\Controller;
use Request;
use Session;


class RewardController  extends Controller
{
    public function __construct(){
       
    }
    
    public function reward(){
        $data = array(
            'title' => 'Reward',
            'type' =>'list',
        );
         $data['status'] = 0;
        if(Request::has('used')){
            $data['status'] = 1;
        } elseif(Request::has('active')){
            $data['status'] = 0;
        } 
        return view('corporate.reward.index')->with($data);   
    }
    public function addReward(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'add',
        );
        return view('corporate.reward.master')->with($data);   
    }
    public function index(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'perksvilla',
        );
        return view('corporate.reward.perksvilla')->with($data);   
    }
    public function orderHistory(){
        $data = array(
            'title' => 'Reward',
            'type'  => 'orderhistory',
        );
        return view('corporate.reward.orderhistory')->with($data);   
    }
    public function success(Request $request){
        $pay_id = Request::get('payment_id');
        $req    = Request::get('payment_request_id');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('INSTAMOJO_URL').'/payments/'.$pay_id.'/');
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("X-Api-Key:".env('INSTAMOJO_API_KEY'),
            "X-Auth-Token:".env('INSTAMOJO_AUTH_TOKEN')));
        $response = curl_exec($ch);
        curl_close($ch); 

        $data=json_decode($response,TRUE);
        if($data['payment']['status'] =="Credit"){
            // Session::flash('payment_success','Transaction is successfully Done.');
            session(['payment_success' =>'Transaction is successfully Done']);
            return view('corporate.reward.success')->with(array('success'=>1,'payment_request_id' => $req,'payment_id' => $pay_id));
        }
        else if($data['payment']['status'] =="Failed"){
            Session::flash('payment_fail','Transaction is Not Done.');
            return view('corporate.reward.success')->with(array('success'=>0,'payment_request_id' => $req,'payment_id' => $pay_id));
        }
        
    }
    public function fail(Request $request){
       
    }   
}
