<?php
namespace App\Http\Controllers\Api\corporate;

use App\Http\Controllers\Controller;
use App\Http\Controllers\CronController;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Reward;
use App\Http\Models\Order;
use App\Http\Models\User;
use App\Http\Models\Offer;
use App\Http\Models\Brand;
use App\Http\Requests\StoreRewardRequest;
use App\Custom\SMSAPI;
use App\Http\Models\OrderHistory;
use App\Http\Models\Notification;
use App\Custom\Email;
use Illuminate\Support\Facades\DB;
class RewardController  extends Controller
{
	public function __construct(){
		$this->reward = new Reward;
      $this->order = new Order;
      $this->offer = new Offer;
      $this->user = new User;
      $this->notification = new Notification;
      $this->cron = new CronController;
   }

   public function addReward(StoreRewardRequest $request){
    	$input = $request->validated();	
    	try{
    		   $reward_data = array(
          	   'eid' => implode(',' ,array_keys($input['employee'])),
          		'category_id' => $input['category_id'],
               'offer_id' => $input['offer_id'],
          		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
          		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
               'payment_status' => ($input['coupon_price'] > 0) ?0:1,
        	   ); 
         $user = $this->user->getCorporateById(Request::get('user_id'));
         $reward_result = $this->reward->addReward($reward_data);
         if($input['coupon_price']==0){
            $payment = true;
         } else {
            $payment_data = array(
                           'purpose'   => "Coupon Purchase",
                           'amount'    => count($input['employee'])*($input['coupon_price']),
                           'phone'     => $user->contact,
                           'buyer_name'=> $user->name,
                           'email'     => $user->email,
                        );            
            $payment  = $this->createOrder($payment_data);
         }  
         // echo "<pre>";
         // print_r($payment);
         // exit;       
         if($payment){
            $data=array();
            $i=0;
            $price = count($input['employee'])*($input['coupon_price']);
            foreach($input['employee'] as $key => $value){
               $data[$i]['employee_id'] = $key;
               $data[$i]['coupen']  = $this->generateCoupenCode(6);
               $data[$i]['issue_date']  = date('Y-m-d', strtotime($value));
               $data[$i]['reward_id'] = $reward_result;
               $data[$i]['redeem'] = 0;
               $data[$i]['is_flag'] = 0;
               $data[$i]['member'] = 0;
               $data[$i]['bill_amount'] = 0;
               $data[$i]['coupon_value'] = $input['coupon_value'];
               $data[$i]['coupon_price'] = $input['coupon_price'];
               $data[$i]['created'] = Carbon::now()->format('Y-m-d H:i:s');
               $i++;
            } 
            $order = $this->order->addOrders($data);
            if($input['coupon_price']==0){
               $update_offer_count = $this->offer->updateRemainingCount($input['offer_id'],count($input['employee']));
               $couponcode = $this->sendCouponCode($reward_result);
               $order_confirmation = $this->sendOrderConfirmation($reward_result);
               $purchase_notification = $this->sendPurchaseNotification($reward_result);
               $payment_update = $this->reward->updatePaymentId(array('payment_request_id' => str_random(32)),$reward_result);
               return response()->json([
                          'error'   => false,
                          'data' =>[]
                     ],200);
            } else {
               if($order && $payment['success']){
                  $payment_update = $this->reward->updatePaymentId(array('payment_request_id' => $payment['payment_request']['id']),$reward_result);
                  return response()->json([
                          'error'   => false,
                          'data'    => $reward_result,
                          'long_url'=> $payment['payment_request']['longurl']
                     ],200);
               } else{
                  return response()->json([
                     'error'   => true,
                     'message' =>"Something went Wrong1",
                  ],400);               
               }
            
           }
         } else{
            return response()->json([
               'error'   => true,
               'message' =>"Something went Wrong2",
            ],400);
         }
    		  
      } catch(\Exception $e) {
        	return response()->json([
				   'error'   => true,
				   'message' => $e->getMessage(),
				],500);
      } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
			],500);
      }
    }
   public function getReward(){
      $data = array();
		if(Request::has('status')){
			$data['status'] = Request::get('status');
		}
      if(Request::has('cid')){
         $data['cid'] = Request::get('cid');
      }
      if(Request::has('type')){
         $data['type'] = Request::get('type');
      }
      if(Request::has('payment_status')){
         $data['payment_status'] = Request::get('payment_status');
      }
       
    	try{
			$result = $this->reward->getReward($data);
			return response()->json([
			      'error'   => false,
			      'data' => $result
			   ],200);
      } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
      } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
      }
   }
    public function getRewardStatistics(){
        $data = array();
        if(Request::has('redeem')){
            $data['redeem'] = Request::get('redeem');
        }
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }
      try{
          $result = $this->reward->getRewardUsed($data);
          return response()->json([
              'error'   => false,
              'data' => $result
          ],200);
            } catch(\Exception $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
            } catch(\QueryException $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
            }
    }
    private function generateCoupenCode($digit){
        $random_number=''; // set up a blank st*ring
        $count=0;
        while ( $count < $digit ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }
    public function getOrderhistory(){
        $data = array();
        if(Request::has('status')){
           $data['status'] = Request::get('status');
        }
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }       
        try{
            $result = $this->reward->getOrderHistory($data);
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
    public function getOrderhistoryStatistic(){
        $data = array();
        if(Request::has('cid')){
            $data['cid'] = Request::get('cid');
        }       
        try{
            $result = array(
              'total_reward' => $this->reward->getRewardUsed($data),
              'total_saved'  => $this->reward->getBillAmoount($data),
              'total_invested' => $this->reward->getInvestedAmount($data),
            );
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
    public function getBrandOrderHistory(){
        $data = array();
        if(Request::has('redeem')){
           $data['redeem'] = Request::get('redeem');
        }
        if(Request::has('brand_id')){
           $data['brand_id'] = Request::get('brand_id');
        }
        try{
            $result = $this->reward->getBrandOrderHistory($data);
            return response()->json([
                'error'   => false,
                'data' => $result
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        } catch(\QueryException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],400);
        }
    }
   public function updatePaymentStatus(){
      $user_id = Request::get('user_id');
      try{
         $data= array(
            'payment_request_id' =>Request::get('payment_request_id'),
            'payment_id' =>Request::get('payment_id'),
            'payment_status'   => Request::get('payment_status'),
            'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
         );
         $reward_id = $this->reward->updatePaymentStatus($data,$user_id);
         if($reward_id){
            $couponcode = $this->sendCouponCode($reward_id);
            $order_confirmation = $this->sendOrderConfirmation($reward_id);
            $purchase_notification = $this->sendPurchaseNotification($reward_id);
            return response()->json([
               'error'   => false,
               'message' => "Order is Confirmed",
            ],200);
         } else{
            return response()->json([
               'error'   => true,
               'message' =>"Something went Wrong",
           ],500);
         }
      } catch(\Exception $e) {
         return response()->json([
            'error'   => true,
            'message' => $e->getMessage(),
        ],500);
      } catch(\QueryException $e) {
          return response()->json([
            'error'   => true,
            'message' => $e->getMessage(),
        ],500);
      }
   }
   private function sendCouponCode($reward_id){
      try {
         $model = OrderHistory::join("employee", "employee.id", "=", "order_history.employee_id")
                    ->join("reward" ,'order_history.reward_id','reward.id')
                    ->join("offer" ,'reward.offer_id','offer.id')
                    ->select(DB::raw("employee.id as employee_id","CONCAT(employee.first_name, ' ', employee.last_name) as user_name"), "employee.contact", "employee.email", "coupen as coupon", "offer.valid_to", "offer.title as offer_name",'offer.value as coupon_value','offer.apply_on','offer.notes','offer.brand_address_id','offer.contact as offer_contact',
                        DB::raw("(SELECT brand.uid from  brand where brand.id = offer.brand_id) as user_id"), 
                        DB::raw("(SELECT corporate.name from corporate  WHERE corporate.id = employee.corporate_id ) as conpany_name"),
                        DB::raw("(SELECT category.name from category  WHERE category.id = reward.category_id ) as rewardtype"),
                        DB::raw("(SELECT brand.logo from brand where brand.id = offer.brand_id)  as logo") ,
                        DB::raw("(SELECT brand.facebook_page from brand where brand.id = offer.brand_id)  as facebook_page") ,
                        DB::raw("(SELECT brand.website_url from brand where brand.id = offer.brand_id)  as website_url"),
                        DB::raw("(SELECT brand.business_hours from brand where brand.id = offer.brand_id)  as business_hours")
                    )
                    ->where('order_history.is_flag' ,'=',0)
                    ->where('reward.id','=',$reward_id)
                    ->where('reward.payment_status', 1)
                    ->whereDate("issue_date", "=", Carbon::now()->format("Y-m-d"))
                    ->get();
                    $address ="";
          $i =0;
         foreach ($model as $m) {
            $user_id = $m->user_id;
            $offer_title = $m->offer_name;
            $address_id = explode(',', $m->brand_address_id);
            $addresses = Brand::getAddressById($address_id);
            foreach($addresses as $key => $value){
               $address .= '<p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000; font-weight: normal; padding: 5px 0; margin: 0; line-height: 18px;"><strong><u>Outlet '.($key+1).' :</u></strong> '.$value->address.', '.$value->city.' ,'.$value->state.' ,'.$value->country.' ,'.$value->pincode.'</p>';
            }
            //  send mail
            $apply_on ="";
            $email = new Email;
            if ($m->apply_on == 1) {
               $apply_on = date('d-m-Y',strtotime($m->valid_to));
            } else if ($m->apply_on == 2) {
               $apply_on = "Week Days (Mon-Fri)";
            } else if ($m->apply_on == 3) {
               $apply_on = "Weekends  (Sat-Sun)";
            } else if ($m->apply_on == 4) {
               $apply_on = "All Days";
            }
            $mailData = [
               "name" => $m->user_name,
               "email" => $m->email,
               "code" => $m->coupon,
               "rewardtype" => $m->rewardtype,
               "offer_name" => $m->offer_name,
               "valid_to" => date('d-m-Y',strtotime($m->valid_to)),
               "coupon_value" => $m->coupon_value,
               "facebook_url" => '<a style="margin-right: 10px;" href="'.$m->facebook_page.'"><img src="http://portal.perksvilla.com/assets/corporate/img/fb-icon.png" /></a>',
               "website_url" => '<a href="'.$m->website_url.'"><img src="http://portal.perksvilla.com/assets/corporate/img/web-icon.png" /></a>',
               'apply_on' => $apply_on,
               'business_hours' => $m->business_hours,
               'notes' => preg_replace("/\r\n|\r|\n/",'<br/>',$m->notes),
               'addresses' => $address,
               'brand_logo' => '<img style="max-width: 100%;" src="http://portal.perksvilla.com/uploads/brand/'.$m->logo.'">',
               'contact_no' => '<span><img src="http://portal.perksvilla.com/assets/corporate/img/call-icon.png" style="margin-right: 5px;margin-left: 8px;" /><b><span>'.$m->offer_contact.'</span></b></span>'
            ];    
            $email->send("coupon_code", $mailData);

            //  send SMS
            $sms = new SMSAPI;
            $smsData["numbers"] = [$m->contact];

            $smsData["details"] = [
               "name" => $m->user_name,
               "code" => $m->coupon,
               "companyname" =>$m->conpany_name,
               "rewardtype" => $m->rewardtype,
               "expiredate" => date('d-m-Y',strtotime($m->valid_to)),
            ];
            $result  = $sms->send("coupon_code", $smsData);
            $message = "You are rewarded for ".$m->rewardtype;
            $title   = 'Coupon Received'; 
            $notification_data[$i] = array(
                 'subject' => $title,
                 'message' => $message,
                 'user_id' => $m->user_id,
                 'sender_id' => 1,
                 'employee_id' => $m->employee_id,
                 'job_id' => 0,
                 'status' => 0,
                 'type' => 0,
                 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $i++;
            $this->cron->push_notificationSend($m->employee_id, $message,$title);
            
            if($result){
               DB::table('order_history')
                     ->where('coupen', '=', $m->coupon)
                     ->update(array('is_flag' => 1));
          
            }
         }
         
         if (count($model) > 0) {
            $notify = $this->notification->addNotifications($notification_data);
            $notification = array(
                                  'subject'    => 'Coupon Purchased',
                                  'message'    => '1 Company purchased your '.$offer_title.' coupon',
                                  'user_id'    => $user_id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
            $notification_brand = $this->notification->addNotification($notification);
         }
      } catch (\Exception $e) {
          echo $e->getMessage();
            return response()->json([
               'error'   => true,
               'message' => "Email is not Sent for coupon code",
            ],500);
      }
   }

   private function sendOrderConfirmation($reward_id){
      try {

            $model = OrderHistory::join("employee", "employee.id", "=", "order_history.employee_id")
                     ->join('reward', 'order_history.reward_id', '=', 'reward.id')
                     ->select( 'order_history.*', 'reward.payment_id',
                        DB::raw("GROUP_CONCAT(CONCAT(employee.first_name ,' ', employee.last_name))  as employee_name") ,
                        DB::raw("SUM(coupon_price) as total_amount "),
                        'reward.created as purchase_date',
                        DB::raw("(SELECT  name FROM  category where category.id = reward.category_id ) as rewardtype"),
                        DB::raw("(SELECT brand.name from offer join brand on offer.brand_id =brand.id WHERE offer.id = reward.offer_id) as brand_name"),
                        DB::raw("(SELECT offer.title from offer WHERE offer.id = reward.offer_id) as offer_name"),
                        DB::raw("(SELECT corporate.name from corporate  WHERE corporate.id = employee.corporate_id) as corporate_name"),
                         DB::raw("(SELECT users.email from users join corporate on users.id = corporate.uid  WHERE corporate.id = employee.corporate_id) as email")
                     )
                     ->where('reward.id','=',$reward_id)
                     ->where('reward.payment_status', 1)
                     ->orderBy('reward.created','desc')
                     ->groupBy('reward_id')
                     ->first();
            $mailData = [
                    "name" => $model['corporate_name'],
                    "email" => $model['email'],
                    "rewardtype" => $model['rewardtype'],
                    "employee" => $model['employee_name'],
                    "rewardname" => $model['offer_name'],
                    "brandname" => $model['brand_name'],
                    "amount" => $model['total_amount'],
                ];
            $email = new Email;
            $email->send("order_summary", $mailData);
        } catch (\Exception $e) {
             return response()->json([
               'error'   => true,
               'message' => "Email is not Sent For Order Confiration",
            ],500);
        }   
   }

   private function sendPurchaseNotification($reward_id){
      try {

            $model = DB::table('reward')
                     ->join('offer', 'reward.offer_id', '=', 'offer.id')
                     ->join('brand', 'offer.brand_id', '=', 'brand.id')
                     ->join('users', 'brand.uid', '=', 'users.id')
                     ->select('reward.eid','offer.title as offer_name','users.email','brand.name as brand_name')
                     ->where('reward.id', $reward_id)
                     ->first();
            $mailData = [
                    "name" => $model->brand_name,
                    "email" => $model->email,
                    "offername" => $model->offer_name,
                    "brandname" => $model->brand_name,
                    "count" => count(explode(',',$model->eid)),
                ];
            $email = new Email;
            $email->send("coupon_purchase", $mailData);
        } catch (\Exception $e) {
             return response()->json([
               'error'   => true,
               'message' => "Email is not Sent For Purchase Notification",
            ],500);
        }   
   }
   private function createOrder($data){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, env('INSTAMOJO_URL').'/payment-requests/');
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER,array("X-Api-Key:".env('INSTAMOJO_API_KEY'),
            "X-Auth-Token:".env('INSTAMOJO_AUTH_TOKEN')));
      $payload = Array(
        'purpose' => $data['purpose'],
        'amount' => $data['amount'],
        'phone' => $data['phone'],
        'buyer_name' => $data['buyer_name'],
        'redirect_url' => route('reward.success'),
        'send_email' => false,
        'webhook' => route('reward.success'),
        'send_sms' => false,
        'email' => $data['email'],
        'allow_repeated_payments' => false
      );
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
      $response = curl_exec($ch);
      curl_close($ch);
      return $payment=json_decode($response,true); 
   }
}