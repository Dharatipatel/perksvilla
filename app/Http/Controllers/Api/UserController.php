<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Brand;
use App\Http\Models\Corporate;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Http\Requests\Admin\EmailSendRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Custom\Common;
use DB;
use App\Custom\Email;
use Session;

class UserController extends Controller {

    public function __construct() {
        $this->user = new User;
        $this->brand = new Brand;
        $this->corporate = new Corporate;
    }

    public function login() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'email' => 'required',
                    'password' => 'required',
                    'role_id' => 'required',
                        ], [
                    'email.required' => 'Please enter email',
                    'email.email' => 'Please enter a valid email',
                    "password.required" => "Please enter password",
                    "role_id.required" => "User type is required",
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            } else {

                $data = array(
                    'email' => $input['email'],
                    'password' => $input['password'],
                    'role_id' => $input['role_id'],
                );
                $result = $this->user->login($data);
                if ($result) {
                    if ($result->status == 0) {
                        return response()->json([
                                    'error' => true,
                                    'message' => "User is inactive",
                                        ], 400);
                    } else if ($result->status == 1) {
                        $token = $this->createToken();
                        $updated_token = $this->user->updateToken(array('id' => $result->id, 'token' => $token, 'modified' => Carbon::now()->format('Y-m-d H:i:s')));
                        Session::put("login_token", $token);
                        if ($result->role_id == 2) {
                            $user_detail = $this->user->getUserById($result->id);
                        } else if ($result->role_id == 3) {
                            $user_detail = $this->user->getCorporateById($result->id);
                        } else {
                            $user_detail = array();
                        }

                        if ($updated_token) {
                            return response()->json([
                                        'data' => array('id' => $result->id, 'token' => $token, 'detail' => $user_detail),
                                            ], 200);
                        } else {
                            return response()->json([
                                        'error' => true,
                                        'message' => "Error in token update",
                                            ], 400);
                        }
                    }
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Invalid email or password",
                                    ], 404);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        }
    }

    public function register() {
        $input = Request::all();
        $validator = Validator::make($input, [
                    'email' => 'required',
                    'mobile' => 'required',
                    'password' => 'required',
                    'user_category' => 'required',
        ]);
        try {
            if ($validator->fails()) {
                return response()->json([
                            'error' => true,
                            'message' => $validator->errors()->first()
                ]);
            } else {
                $token = $this->createToken();
                $data = array(
                    'email' => $input['email'],
                    'contact' => $input['mobile'],
                    'password' => $input['password'],
                    'token' => $token,
                    'role_id' => $input['user_category'],
                    'status' => 0,
                    'created' => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $result = $this->user->register($data);
                if ($result) {
                    // if($input['user_category']==2){
                    // 	$brand_data =array(
                    // 		'uid' => $result,
                    // 		'name' =>  $input['brand_name'] ,
                    // 		'brand_category' =>  $input['brand_category'],
                    // 		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
                    // 		'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 	);
                    // } else if ($input['user_category']==3){
                    // 	$corporate_data =array(
                    // 		'uid' => $result,
                    // 		'company_name' =>  $input['company_name'] ,
                    // 		'company_size' =>  $input['company_size'],
                    // 		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
                    // 		'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                    // 	);
                    // }
                    return response()->json([
                                'error' => false,
                                'data' => array('id' => $result, 'token' => $token),
                    ]);
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                    ]);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
            ]);
        }
    }

    public function changePassword(ChangePasswordRequest $request) {
        $input = $request->validated();
        try {
            $data = array(
                'id' => $input['id'],
                'password' => $input['oldpassword'],
            );
            $password = $this->user->checkPassword($data);
            if ($password) {
                if ($input['oldpassword'] == $input['newpassword']) {
                    return response()->json([
                                'error' => true,
                                'message' => "You haven’t made any changes to your password",
                                    ], 200);
                } else {
                    $update_data = array(
                        'password' => $input['newpassword'],
                        'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                    );
                    $update_password = $this->user->changePassword($update_data, $input['id']);
                    if ($update_password) {
                        return response()->json([
                                    'error' => true,
                                    'message' => "Password updated Successfully",
                                        ], 200);
                    } else {
                        return response()->json([
                                    'error' => true,
                                    'message' => "Something Went Wrong",
                                        ], 400);
                    }
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Old Password is Wrong",
                                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    private function createToken() {
        return Common::getToken(32);
    }

    public function viewAdminProfile() {
        $data = array();
        if (Request::has('id')) {
            $data['id'] = Request::get('id');
        }
        try {
            $result = $this->user->getAdminDetail($data);
            return response()->json([
                        'error' => true,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function updateAdminProfile(UpdateAdminRequest $request) {
        $input = $request->validated();
        $data = array();

        $data['email'] = $input['email'];
        $data['modified'] = Carbon::now()->format('Y-m-d H:i:s');

        try {
            $result = $this->user->updateAdminDetail($data, $input['id']);
            return response()->json([
                        'error' => true,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function emailSend(EmailSendRequest $request) {

        try {
            $data = $request;
            $result = $this->brand->savePassword($data);
            if ($result != null) {

                $email = new Email;
                $mailData = [
                    "name" => $result->user_name,
                    "email" => $result->email,
                    "password" => $data->password
                ];

                $email->send("user_password", $mailData);

                return response()->json([
                            'error' => false,
                            'data' => [],
                            'message' => "Email sent successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

}
