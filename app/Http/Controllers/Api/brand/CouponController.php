<?php
namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Custom\Email;
use App\Custom\SMSAPI;
use App\Http\Models\Offer;
use App\Http\Models\Reward;
use App\Http\Models\Order;
use App\Http\Models\Employee;
use App\Http\Requests\GetCoupenRequest;
use App\Http\Requests\RedeemCoupenRequest;
use App\Http\Requests\SendOtpRequest;

class CouponController extends Controller
{
	  public function __construct(){
            $this->offer = new Offer;
            $this->reward = new Reward;
		    $this->order = new Order;
            $this->email = new Email;
            $this->sms   = new SMSAPI;
            $this->employee   = new Employee;
    }

    public function getCoupons(){
        try{
           if(Request::has('status')){
               $data['status'] = Request::get('status');
           }
            $result = $this->reward->getCoupons($data);
            return response()->json([
                'error'   => false,
                'data' => $result,
            ],200);
            
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        }
    
    }
    public function getCoupon(GetCoupenRequest $request){
        $input = $request->validated();
        $is_before_error = false;
        $is_afetr_error = false;
        $error_msg;
        $data = array(
            'coupen' => $input['txtCoupenCode'],
            'email_phone' =>$input['txtEmailPhone'],
        );
	    
    	try{
           if(Request::has('brand_id')){
               $data['brand_id'] = Request::get('brand_id');
           }
			$result = $this->offer->getCoupen($data);
            if($result){
                if($result->redeem){
                    return response()->json([
                    'error'   => false,
                    'data' => null,

                    'message'=>"This coupon code is already used"

                    ],200);
                } else {
                	if(strtotime($result->issue_date)>strtotime(date('Y-m-d'))){
			            $is_before_error = true;
			            $error_msg ="You Can not Issue Today";
			            return response()->json([
			                    'error'   => true,
			                    'message' => $error_msg,
			                ],400);
			        }
			        if(strtotime($result->expiry_date)<strtotime(date('Y-m-d'))){
			            $is_afetr_error = true;
			            $error_msg ="Coupen is Expired"; 
			        } else {
			            if($result->apply_on==1){
			                if( strtotime($result->expiry_date) != strtotime(date('Y-m-d')) ){
			                    $is_afetr_error = true;
			                    $error_msg = "This Coupon is applicable on " . date('d F,Y',strtotime($result->expiry_date))." Only"; 
			                }
			            } else if($result->apply_on==2){
			                if( date('D') ==  "Sat" || date('D') ==  "Sun" ){
			                    $is_afetr_error = true;
			                    $error_msg = "This Coupon is applicable only on weekdays"; 
			                }

			            } else if($result->apply_on==3){
			                if( date('N') < 6  ){
			                    $is_afetr_error = true;
			                    $error_msg = "This Coupon is applicable only on weekends"; 
			                }
			            } else if($result->apply_on==4){
			                $is_afetr_error = false;
			            }
			        }
			        if($is_afetr_error){
			            return response()->json([
			                    'error'   => true,
			                    'message' => $error_msg,
			                ],400);
			        } else{
	                    return response()->json([
	                        'error'   => false,
	                        'data' => $result
	                    ],200);    
			        }
                }
            } else {
                return response()->json([
                'error'   => false,
                'data' => null,
                'message'=>"Coupon Code or Email Id not Exist"
                ],200);
            }
			
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }

    public function redeemCoupon(RedeemCoupenRequest $request){
        $input = $request->validated();
        if($input['txtBillAmount'] <=0){
            $errors['txtBillAmount'] = array('Bill Amount should grater than 0');
            return response()->json([
                        'errors' => $errors,
                        'error'   => true,
                        'message' => "Bill Amount should grater than 0",
                            ], 422);
        }  
        
        $data = array(
            'coupen' => $input['txtRedeemCoupenCode'],
            'bill_amount' =>$input['txtBillAmount'],
            'member' =>$input['txtMember'],
            'reward_id' =>$input['reward_id'],
            'contact' =>$input['txtContact'],
            'redeem_date' =>Carbon::now(),
        );
        $otp_data = array(
                'contact' => $input['txtContact'],
                'otp_code' => $input['txtOtp'],
            );
        try{
            $otp = $this->employee->verifyOtp($otp_data);

            if(!$otp){
                $errors['txtOtp'] = array('OTP is Wrong');
                return response()->json([
                        'errors' => $errors,
                        'error' => true,
                        'message' => "OTP is Wrong",
                            ], 422);
            } else {
                $dt = Carbon::now();
                if($otp->type ==2){
                    if($dt->diffInMinutes($otp->created_at)>15){
                        // $result = $this->order->redeemCoupen($data);
                        return response()->json([
                            'error'   => true,
                            'message' => 'OTP is Expired'
                        ],404);
                    }
                    $result = $this->order->redeemCoupen($data);
                        return response()->json([
                            'error'   => false,
                            'data' => $result,
                            'message' => 'Coupon redeem Succeessfully'
                        ],200);
                } else{
                    $errors['txtOtp'] = array('OTP is Wrong');
                    return response()->json([
                        'errors' => $errors,
                        'error' =>  true,
                        'message' => "OTP is Wrong",
                            ], 422);
                }
            }
            
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        }
    }

    public  function sendOTP(SendOtpRequest $request){
        $input = $request->validated();
        $otp = $this->generateOTP(6);
        $data =  array(
                    'employee_id' =>  $input['employeeId'], 
                    'coupen'      =>  $input['txtCoupon'],
                    'otp'         => $otp,
                );
        try{
            $otp_data = array(
                'contact'   =>  $input['txtPhone'],
                'data_id'   =>  $input['employeeId'],
                'otp_code'  =>  $otp,
                'type' =>2,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            );
            $sendOTP = $this->employee->addOtp($otp_data);
                    
            // $sendOTP = $this->offer->updateOTP($data);
            if($sendOTP){
                $mailData = [
                    "name"  => $input['employee_name'],
                    "email" => $input['txtEmail'],
                    "code"  => $otp
                ];
                $this->email->send("otp_code", $mailData);
               
                $smsData["numbers"] = [$input['txtPhone']];
                $smsData["details"] = [
                    "name" => $input['employee_name'],
                    "code" => $otp
                ];
                $result = $this->sms->send("otp_code", $smsData);
                if($result){
                    return response()->json([
                        'error'   => false,
                        'message' => "OTP send Succeessfully",
                    ],200);
                } else{
                    return response()->json([
                        'error'   => true,
                        'message' => "Something Went Wrong ",
                    ],400);
                }
            } else{
                return response()->json([
                    'error'   => true,
                    'message' => "Something Went Wrong ",
                ],400);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        }
       
    }
    private function generateOTP($digit){
        $random_number=''; // set up a blank st*ring
        $count=0;
        while ( $count < $digit ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }
}