<?php
namespace App\Http\Controllers\Api\brand;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Brand;
use App\Http\Models\Offer;
use App\Http\Models\Notification;
use App\Http\Requests\StoreOfferRequest;
use App\Http\Requests\UpdateOfferRequest;
use App\Http\Requests\DeleteBrandRequest;
use App\Custom\Email;

class OfferController extends Controller
{
	public function __construct(){
      $this->offer = new Offer;
      $this->notification = new Notification;
		  $this->brand = new Brand;
    }

    public function addOffer(StoreOfferRequest $request){
    	$input = $request->validated();	
    	try{
    		$offer_data= array(
        		'title' => $input['title'],
        		'description'    => $input['description'],
        		'value'    => $input['value'],
        		'price'    => $input['price'],
            'apply_on' => $input['apply_on'] ,
        		'contact' => $input['contact'] ,
        		'brand_address_id'  => implode(",",$input['brand_address_id']),
        		'valid_to'  => date('Y-m-d',strtotime($input['valid_to'])),
            'count'  => $input['count'],
        		'remaining_count'  => $input['count'],
        		'publish_date'  =>date('Y-m-d', strtotime($input['publish_date'])),
        		'notes'   => $input['notes'],
        		'brand_id' => $input['brand_id'],
            'offer_status_id' => 3,
        		'created'  => Carbon::now(),
        		'modified'  => Carbon::now(),
        	);
    		$offer_result = $this->offer->addOffer($offer_data);
    		if($offer_result){
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $offer_result),
				],201);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],400);
    		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateOffer(UpdateOfferRequest $request){
        $input = $request->validated(); 
        try {
            $offer_data= array(
               'title' => $input['title'],
               'description'    => $input['description'],
               'value'    => $input['value'],
               'price'    => $input['price'],
               'apply_on' => $input['apply_on'] ,
               'contact' => $input['contact'] ,
               'brand_address_id'  => implode(",",$input['brand_address_id']),
               'valid_to'  => date('Y-m-d',strtotime($input['valid_to'])),
               'remaining_count'  => $input['count'],
               'count'  => $input['count'],
               'publish_date'  =>date('Y-m-d', strtotime($input['publish_date'])),
               'notes'   => $input['notes'],
               'modified'  => Carbon::now(),
             );
           $offer_result = $this->offer->updateOffer($offer_data,$input['id']);
            if($offer_result){
                   return response()->json([
                   'error'   => false,
                   'data' =>array('id' => $offer_result),
               ],201);
            } else{
                   return response()->json([
                   'error'   => true,
                   'message' =>"Something went Wrong",
               ],400);
            }
        } catch(\Exception $e) {
                 return response()->json([
                   'error'   => true,
                   'message' => $e->getMessage(),
               ],500);
        } catch(\QueryException $e) {
                 return response()->json([
                   'error'   => true,
                   'message' => $e->getMessage(),
                ],500);
        }
    }
    public function detailOffer(DetailOfferRequest $request){

        $input = $request->validated(); 
        try{
            $offer_result = $this->offer->detailOffer($input['offer_id']);
            if($offer_result){
                return response()->json([
                'error'   => false,
                'data' =>$offer_result,
            ],201);
              } else{
                return response()->json([
                'error'   => true,
                'message' =>"Something went Wrong",
            ],400);
            }
            } catch(\Exception $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],500);
            } catch(\QueryException $e) {
              return response()->json([
                'error'   => true,
                'message' => $e->getMessage(),
            ],500);
        }
    }
    public function getOffer(){
        $data = array();
		    if(Request::has('status')){
			     $data['offer_status_id'] = Request::get('status');
		    }
        if(Request::has('bid') && Request::get('bid') != null){
            $data['bid'] = Request::get('bid');
        }
        if(Request::has('brand_category')){
            $data['brand_category'] = Request::get('brand_category');
        }
        if(Request::has('coupentype')){
            $data['coupentype'] = Request::get('coupentype');
        }
        if(Request::has('pricerange')){
            $data['pricerange'] = Request::get('pricerange');
        }
        if(Request::has('count')){
            $data['count'] = Request::get('count');
        }
        if(Request::has('offer_id')){
            $data['offer_id'] = Request::get('offer_id');
        }
    	try{
			$result = $this->offer->getOffer($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
 
    public function approveOffer(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
          $data= array(
            'modified'  => Carbon::now(),
          );
          $notification_msg="";
          $offer = $this->offer->getoffer(array('offer_id' => $input['id']));
          if(Request::has('publish_date')){
              if(strtotime(Request::get('publish_date')) > strtotime(date('Y-m-d'))){
                  $data['offer_status_id'] = 2;
                   $notification_msg="Offer ".$offer[0]->title." is Approved .";
              }
              else if(strtotime(Request::get('publish_date')) == strtotime(date('Y-m-d'))){
                  $data['offer_status_id'] = 1;
                   $notification_msg="Offer ".$offer[0]->title." is Approved and Live Now.";
              }
          } else {
              $data['offer_status_id'] =1;
               $notification_msg="Offer ".$offer[0]->title." is Approved and Live Now.";
          }

          $notification_data = array(
                                  'subject'    => 'Offer Confirmation',
                                  'message'    =>  $notification_msg,
                                  'user_id'    => $this->brand->getUserById($offer[0]->brand_id)->id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now(),
                                  'updated_at' => Carbon::now(),
                                );
          $notify = $this->notification->addNotification($notification_data);
        	$result = $this->offer->approveOffer($data,$input['id']);
        	if($result){
               $email = new Email;
               $brand_data = $this->brand->getUserById($offer[0]->brand_id);
               $mailData = [
                     "name" => $brand_data->name,
                     "brandname" => $brand_data->name,
                     "offername" => $offer[0]->title,
                     "email" => $brand_data->email,
               ];
               $email->send("offer_accepted", $mailData);
         
        		return response()->json([
				    'error'   => false,
				    'message' => "Offer Approved Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveOffer(DeleteBrandRequest $request){
      
      $input = $request->validated();
      try{
        
          $data= array(
            'modified'  => Carbon::now(),
          );
          if(Request::has('status')){
              $data['offer_status_id']=Request::get('status');
          }else {
              $data['offer_status_id']=0;
          }
          $result = $this->offer->deactiveOffer($data,$input['id']);
          if($result){
            
            return response()->json([
            'error'   => false,
            'message' => "Offer Deactivated Successfully",
        ],200);
          } else{
            return response()->json([
            'error'   => true,
            'message' =>"Something went Wrong",
        ],500);
          }

        } catch(\Exception $e) {
          return response()->json([
            'error'   => true,
            'message' => $e->getMessage(),
        ],500);
        } catch(\QueryException $e) {
          return response()->json([
            'error'   => true,
            'message' => $e->getMessage(),
        ],500);
        }
    }
    public function rejectOffer(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'modified'  => Carbon::now(),
        	);
        
          $data['offer_status_id']=5;
        	$result = $this->offer->deactiveOffer($data,$input['id']);
          $offer = $this->offer->getoffer(array('offer_id' => $input['id']));
        	if($result){
              $notification_data = array(
                                  'subject'    => 'Offer Confirmation',
                                  'message'    => $offer[0]->title ." is Rejected",
                                  'user_id'    => $this->brand->getUserById($offer[0]->brand_id)->id,
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now(),
                                  'updated_at' => Carbon::now(),
                                );

          $notify = $this->notification->addNotification($notification_data);
         $email = new Email;
               $brand_data = $this->brand->getUserById($offer[0]->brand_id);
               $mailData = [
                     "name" => $brand_data->name,
                     "brandname" => $brand_data->name,
                     "offername" => $offer[0]->title,
                     "email" => $brand_data->email,
                ];

                $email->send("offer_rejected", $mailData);
        		return response()->json([
				    'error'   => false,
				    'message' => "Offer Rejected Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
}