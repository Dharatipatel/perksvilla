<?php

namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Brand;
use App\Http\Models\City;
use App\Http\Models\Offer;
use App\Http\Models\Notification;
use App\Http\Models\Corporate;
use App\Http\Models\Employee;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;
use App\Http\Requests\DeleteBrandRequest;
use Illuminate\Support\Facades\File;
use App\Http\Requests\UpdateWalletBrandRequest;
use App\Http\Requests\AddAddressRequest;
use App\Http\Requests\Admin\EmailSendRequest;
use App\Custom\Email;
use App\Custom\SMSAPI;

class BrandController extends Controller {

    public function __construct() {
        $this->brand = new Brand;
        $this->user  = new User;
        $this->city  = new City;
        $this->offer = new Offer;
        $this->corporate = new Corporate;
        $this->employee = new Employee;
        $this->notification = new Notification;
    }

    public function addBrand(StoreBrandRequest $request) {
        $input = $request->validated();
        $token = $this->createToken();
        try {
            $user_data = array(
                'email' => $input['email'],
                'contact' => $input['contact'],
                'password' => $input['password'],
                'token' => $token,
                'role_id' => 2,
                'created' => Carbon::now()->format('Y-m-d H:i:s'),
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => $input['status'],
            );
            $user_result = $this->user->register($user_data);
            if ($user_result) {
                $brand_data = array(
                    'uid' => $user_result,
                    'name' => $input['brand_name'],
                    'brand_category' => $input['category_name'],
                    'description' => $input['description'],
                    'contact_person_name' => $input['person_name'],
                    'business_hours' => $input['business_hours'],
                    'commision' => $input['commision'],
                    'created' => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                if ($request->has('website_url')) {
                    $website_url = $request->get('website_url');
                    $brand_data['website_url'] = (strpos($website_url,'http://') !== false || strpos($website_url,'https://') !== false) ? $website_url : 'http://'.$website_url;
                }
                if ($request->has('facebook_page')) {
                    $facebook_page = $request->get('facebook_page');
                    $brand_data['facebook_page'] = (strpos($facebook_page,'http://') !== false || strpos($facebook_page,'https://') !== false) ? $facebook_page : 'https://'.$facebook_page;
                }
                $brand_result = $this->brand->addBrand($brand_data);
                if ($brand_result) {
                    $brand_img_data = array();
                    if ($request->has('image') && $request->get('image') != null) {
                        if(env('APP_ENV') == 'local'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/brand';
                        } else if(env('APP_ENV') == 'production'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/brand';
                        }
                        if(!is_dir($image_folder)){
                            mkdir($image_folder, 0777, true);
                        } 
                        $upload = file_put_contents($image_folder.'/' . $brand_result . '.png', base64_decode($request->get('image')));
                        if ($upload) {
                            // chmod($image_folder.'/' . $brand_result . '.png', 0777);
                            $brand_img_data['logo'] = $brand_result . ".png";
                        }
                        $brand_update_result = $this->brand->updateBrand($brand_img_data, $brand_result);
                        if ($brand_update_result) {
                            return response()->json([
                                        'error' => false,
                                        'data' => array('id' => $brand_result),
                                            ], 201);
                        } else {
                            return response()->json([
                                        'error' => true,
                                        'message' => "Something went Wrong",
                                            ], 400);
                        }
                    } else {
                        return response()->json([
                                    'error' => false,
                                    'data' => array('id' => $brand_result),
                                        ], 201);
                    }
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                                    ], 400);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function getStatistics(){
       try{
            $brand_id = Request::get('brand_id');
            $user_id = Request::get('user_id');
            $data = array(
                'active_offer_count'   => $this->offer->offerCount(array('status'=>1,'brand_id'=>$brand_id)),
                'expected_visit'       => $this->offer->expectedVisitCount(array('brand_id'=>$brand_id,'status'=>0,'expected_visit'=>true)),
                'new_customer'         => $this->offer->expectedVisitCount(array('brand_id'=>$brand_id,'status'=>1)),
                'total_reach'          => ($this->offer->totalMember(array('brand_id'=>$brand_id))+$this->offer->expectedVisitCount(array('brand_id'=>$brand_id,'status'=>1))),
                'corporate_count'      => $this->corporate->getActiveCorporateCount(),
                'employee_count'       => $this->employee->getActiveEmployeeCount(),
                'monthly_sales'        => $this->offer->getSales(array('monthly'=>1,'brand_id'=>$brand_id)),
                'total_balance'        => $this->brand->getBalanceByBrand(array('user_id'=>$user_id)),
                'total_sales'          => $this->offer->getSales(array('brand_id'=>$brand_id)),
                'brand_count'          => $this->brand->getActiveBrandCount(),
            );
            if(!empty($data)){
                return response()->json([
                    'error'   => false,
                    'data' =>$data,
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } 
    }
    public function updateBrand(UpdateBrandRequest $request) {
        $isexist = false;
        $errors = array();
        $input = $request->validated();
        $token = $this->createToken();
        try {
            $user_data = array(
                'email' => $input['email'],
                'contact' => $input['contact'],
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                'status' => $input['status'],
            );
            if ($this->user->isUserExist(array('email' => $input['email'], 'id' => $input['uid']))) {
                $isexist = true;
                $errors['email'] = array('Email is Exist');
            }
            if ($this->user->isMobileExist(array('contact' => $input['contact'], 'id' => $input['uid']))) {
                $isexist = true;
                $errors['contact'] = array('Mobile Number is Exist');
            }
            if ($isexist) {
                return response()->json([
                            'errors' => $errors,
                            'message' => "Invalid Data",
                                ], 422);
            }

            $user_result = $this->user->updateUser($user_data, $input['uid']);
            if ($user_result) {
                $brand_data = array(
                    'name' => $input['brand_name'],
                    'brand_category' => $input['category_name'],
                    'description' => $input['description'],
                    'contact_person_name' => $input['person_name'],
                    'business_hours' => $input['business_hours'],
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                if(array_key_exists('commision',$input)){
                    $brand_data['commision'] = $input['commision'];
                }
                if ($request->has('website_url')) {
                    $website_url = $request->get('website_url');
                    $brand_data['website_url'] = (strpos($website_url,'http://') !== false || strpos($website_url,'https://') !== false) ? $website_url : 'http://'.$website_url;
                }
                if ($request->has('facebook_page')) {
                    $facebook_page = $request->get('facebook_page');
                    $brand_data['facebook_page'] = (strpos($facebook_page,'http://') !== false || strpos($facebook_page,'https://') !== false) ? $facebook_page : 'https://'.$facebook_page;
                }
                if ($request->has('image') && $request->get('image') != "") {
                    if(env('APP_ENV') == 'local'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/brand';
                        } else if(env('APP_ENV') == 'production'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/brand';
                        }
                    $upload = file_put_contents($image_folder . '/' . $input['id'] . '.png', base64_decode($request->get('image')));
                    if ($upload){
                        // chmod($image_folder.'/' . $input['id'] . '.png', 0777);
                        $brand_data['logo'] = $input['id'] . ".png";
                    }
                }
                if (Request::has('updateaddress')) {
                    $update_address = $this->updateBrandAddress(Request::get('addressArray'), $input['id']);
                }
                $brand_result = $this->brand->updateBrand($brand_data, $input['id']);
                if ($brand_result) {
                    return response()->json([
                                'error' => false,
                                'message' => "Brand Updated Successfully",
                                    ], 200);
                } else {
                    return response()->json([
                                'error' => true,
                                'message' => "Something went Wrong",
                                    ], 400);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function brandWallet(UpdateWalletBrandRequest $request) {
        $input = $request->validated();
        try {
            $user_balance = $this->brand->getBalance(array('user_id' => $input['user_id']));

            if (!empty($user_balance) && $user_balance[0]->balance >= $input['txtAmount']) {
                $user_data = array(
                    'user_id' => $input['user_id'],
                    'withdraw_amount' => $input['txtAmount'],
                    'availble_balance' => ($user_balance[0]->balance-$input['txtAmount']),
                    'created' => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $result = $this->brand->addWallet($user_data);
                if ($result) {
                    return response()->json([
                                'error' => false,
                                'data' => $result,
                                    ], 200);
                } else {
                    return response()->json([
                                'error' => false,
                                'data' => "Something Went wrong",
                                    ], 200);
                }
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "You don’t have sufficient balance",
                                ], 400);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function brandWalletTranscation() {
        $data = array();
        if (Request::has('user_id')) {
            $data['user_id'] = Request::get('user_id');
        }

        try {
            $result = $this->brand->getWalletTransaction($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBalance() {
        $data = array();
        if (Request::has('user_id')) {
            $data['user_id'] = Request::get('user_id');
        }
        try {
            $result = $this->brand->getBalanceOfUser($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrand() {
        $limit = 100;
        if (Request::has('limit')) {
            $limit = Request::get('limit');
        }

        try {
            $result = $this->brand->getBrand($limit);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                        'active_brand_count' => $this->brand->getActiveBrandCount(),
                        'active_offer_count' => $this->brand->getDeactiveBrandCount(),
                        'coupon_redeem_count' => $this->brand->getRedeemCoupenCount(),
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrandOffer() {
        try {
            $data = array();
            $result = $this->brand->getBrandOffer($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function getBrandById($id) {

        try {
            $result = $this->brand->getBrandById($id);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'data' => $result,
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Test",
                                ], 404);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function paidRequest(DeleteBrandRequest $request)
    {
        
        $input = $request->validated();
        try {
            $data = array(
                'status' => 1,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $notification_data = array(
                                  'subject'    => 'Withdraw Confirmation',
                                  'message'    => 'Your Payment request for Rs '.$request->get('amount').' is processed',
                                  'user_id'    => $request->get('brand_uid'),
                                  'sender_id'  => 1,
                                  'job_id'     => 0,
                                  'status'     => 0,
                                  'type'       => 0,
                                  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                  'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                                );
            $notify = $this->notification->addNotification($notification_data);
            $result = $this->brand->paidRequest($data, $input['id']);
            if ($result) {
                $email = new Email;
                $brand_data = $this->brand->getBrandById($request->get('brand_uid'));
                $perksvilla_charges = round((int)($request->get('amount'))*($brand_data->commision/100));
                $mailData = [
                    "name" => $brand_data->name,
                    "email" => $brand_data->email,
                    "amount" => $request->get('amount'),
                    "payment_requested" => $request->get('amount'),
                    "perksvilla_charges" => $perksvilla_charges,
                    "credited_amount" => (int)($request->get('amount'))-$perksvilla_charges,
                ];

                $email->send("payment_transfer", $mailData);
                return response()->json([
                            'error' => false,
                            'message' => "Withdraw Request Paid Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    
    }
    public function activeBrand(DeleteBrandRequest $request) {
        $input = $request->validated();
        try {
            $data = array(
                'status' => 1,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->brand->activeBrand($data, $input['id']);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Brand Activated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function deactiveBrand(DeleteBrandRequest $request) {
        $input = $request->validated();
        try {

            $data = array(
                'status' => 0,
                'modified' => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->brand->deactiveBrand($data, $input['id']);
            if ($result) {
                $offer_data['offer_status_id'] = 5;
                $brand = $this->brand->getById($input['id']);
                $offer_result=$this->offer->deactiveOfferByBrand($offer_data,$brand->brand_id);
                return response()->json([
                            'error' => false,
                            'message' => "Brand Deactivated Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function deleteBrand(DeleteBrandRequest $request) {
        try {
            $input = $request->validated();
            $input['status'] = 3;
            $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
            $result = $this->brand->deleteBrand($input);
            if ($result) {
                return response()->json([
                            'error' => false,
                            'message' => "Brand deleted Successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    private function createToken() {
        return str_random(32);
    }

    public function addBrandAddress(AddAddressRequest $request) {
        $input = $request->validated();
        try {
            $brand_address_data = array();
            $address_exist = $this->brand->getAddress($input['bid']);
            $address_keys = array();
            
            foreach ($input['addressArray'] as $key => $value) {
                array_push($address_keys, $value['id']);
                if($value['id']!=null) {
                    $brand_address_data['address'] = $value['address'];
                    $brand_address_data['pincode'] = $value['pincode'];
                    $brand_address_data['country'] = $value['country'];
                    $brand_address_data['state'] = $value['state'];
                    $brand_address_data['city'] = $value['city'];
                    $brand_address_update_result = $this->brand->updateAddress($brand_address_data,$value['id']);
                } else {
                    $brand_address_data['brand_id'] = $input['bid'];
                    $brand_address_data['address'] = $value['address'];
                    $brand_address_data['pincode'] = $value['pincode'];
                    $brand_address_data['country'] = $value['country'];
                    $brand_address_data['state'] = $value['state'];
                    $brand_address_data['city'] = $value['city'];
                    $brand_address_result = $this->brand->addAddress($brand_address_data);
                }
               
            }
            if (count($address_exist) > 0) {
                foreach($address_exist as $a_key => $a_value){
                   if(!in_array($a_value->id, $address_keys)){
                    $brand_address_remove_result = $this->brand->removeAddress($a_value->id);
                   }
                }
            }
            return response()->json([
                        'error' => false,
                        'data'  => "Successfully Created",
                            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 500);
        }
    }

    public function emailSend(EmailSendRequest $request) {

        try {
            $data = $request;
            $result = $this->brand->savePassword($data);
            if ($result != null) {

                $email = new Email;
                $url = url("/") . '/brand/login';
                $mailData = [
                    "name" => $result->user_name,
                    "email" => $result->email,
                    "password" => $data->password,
                    "url"=>"<a href='{$url}'>{$url}</a>"
                ];

                $email->send("user_password", $mailData);

                //  send SMS
                $sms = new SMSAPI;
                $smsData["numbers"] = [$result->contact];
                $mailData['url'] = $url;
                $smsData["details"] = $mailData;

                $result = $sms->send("user_password", $smsData);

                return response()->json([
                            'error' => false,
                            'data' => [],
                            'message' => "Email sent successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }

    public function updateBrandAddress($address, $bid) {
        $brand_address_data = array();
        $address_exist = $this->brand->getAddress($bid);
        $address_keys = array();
        foreach ($address as $key => $value) {
            array_push($address_keys, $value['id']);
            if($value['id']!=null) {
                $location = $this->city->getStateCountry(array('city_id' => $value['city']));
                $brand_address_data['address'] = $value['address'];
                $brand_address_data['pincode'] = $value['pincode'];
                $brand_address_data['country'] = $location->country_id;
                $brand_address_data['state'] = $location->state_id;
                $brand_address_data['city'] = $value['city'];
                $brand_address_update_result = $this->brand->updateAddress($brand_address_data,$value['id']);
               
            } else {
                $location = $this->city->getStateCountry(array('city_id' => $value['city']));
                $brand_address_data['brand_id'] = $bid;
                $brand_address_data['address'] = $value['address'];
                $brand_address_data['pincode'] = $value['pincode'];
                $brand_address_data['country'] = $location->country_id;
                $brand_address_data['state'] = $location->state_id;
                $brand_address_data['city'] = $value['city'];
                $brand_address_result = $this->brand->addAddress($brand_address_data);
            }
        }
        if (count($address_exist) > 0) {
            foreach($address_exist as $a_key => $a_value){
               if(!in_array($a_value->id, $address_keys)){
                $brand_address_remove_result = $this->brand->removeAddress($a_value->id);
               }
            }
        }
        return true;
    }
    // public function updateBrandAddress($address, $bid) {
    //     $brand_address_data = array();
    //     $address_exist = $this->brand->getAddress($bid);
    //     if (count($address_exist) > 0) {
    //         $deleted = $this->brand->removeAddress($bid);
    //     }
    //     foreach ($address as $key => $value) {
    //         $location = $this->city->getStateCountry(array('city_id' => $value['city']));
    //         $brand_address_data[$key]['brand_id'] = $bid;
    //         $brand_address_data[$key]['address'] = $value['address'];
    //         $brand_address_data[$key]['pincode'] = $value['pincode'];
    //         $brand_address_data[$key]['country'] = $location->country_id;
    //         $brand_address_data[$key]['state'] = $location->state_id;
    //         $brand_address_data[$key]['city'] = $value['city'];
    //     }
    //     $brand_address_result = $this->brand->addAddress($brand_address_data);
    // }
    public function getBrandTotalSales(){
        $data = array();
        if (Request::has('brand_id')) {
            $data['brand_id'] = Request::get('brand_id');
        }
        try {
            $result = $this->brand->getBrandTotalSales($data);
            return response()->json([
                        'error' => false,
                        'data' => $result,
                       
                            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        } catch (\QueryException $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }
    public function removeBrandProfile(DeleteBrandRequest $request){

        try{
            $input = $request->validated();
            $input['logo'] = NULL;
            $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
            if(env('APP_ENV') == 'local'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/brand';
            } else if(env('APP_ENV') == 'production'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/brand';
            }
            $image_path = $image_folder . '/'.$input['id'].'.png';

            if (File::exists($image_path)) {
                unlink($image_path);
            }
            $result = $this->brand->updateBrand($input, $input['id']);
            if($result){
                return response()->json([
                    'error'   => false,
                    'message' =>"brand Logo deleted Successfully",
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    
    }

}
