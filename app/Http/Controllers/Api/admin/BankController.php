<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Bank;
use App\Http\Requests\StoreBankRequest;
use App\Http\Requests\UpdateBankRequest;

class BankController extends Controller
{
	public function __construct(){
		  $this->bank = new Bank;
		
    }

    public function addBankDetails(StoreBankRequest $request){
    	$input = $request->validated();
    	try{
    		
    		$bank_data= array(
        		'brand_id'    => $input['brand_id'],
        		'name'    => $input['name'],
        		'ifsc'    => $input['ifsc'],
        		'account_number'  => $input['account_number'],
        		'branch_details'  => $input['branch_details'],
        		'account_type'  => $input['account_type'],
                'created'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
    		$bank_result = $this->bank->addBankDetails($bank_data);
            if($bank_result){
                return response()->json([
                    'error'   => false,
                    'data' =>array('id' => $bank_result),
                ],201);
            } else {
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],400);
            }
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateBankDetails(UpdateBankRequest $request){

    	$input = $request->validated(); 	
    	try{
    		
    		$bank_data= array(
                'name'    => $input['name'],
                'ifsc'    => $input['ifsc'],
                'account_number'  => $input['account_number'],
                'branch_details'  => $input['branch_details'],
                'account_type'  => $input['account_type'],
                'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
            );
           
    		$bank_data = $this->bank->updateBankDetails($bank_data,$input['bank_detail_id']);
    		if($bank_data){
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $bank_data),
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],400);
    		}
    	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getBankDetails(){
        $data = array();
        if(Request::has('bid')){
            $data['bid'] = Request::get('bid');
        } 
    	try{
			$result = $this->bank->getBankDetails($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
}