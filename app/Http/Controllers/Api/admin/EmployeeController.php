<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Employee;
use App\Http\Models\Reward;
use App\Http\Models\Brand;
use App\Http\Models\Notification;
use App\Custom\SMSAPI;
use App\Http\Requests\AddEmployeeRequest;
use App\Http\Requests\EmployeeLoginRequest;
use App\Http\Requests\EmployeeVerifyOTPRequest;
use App\Http\Requests\EmployeeDetailRequest;

class EmployeeController extends Controller
{
	public function __construct(){
		  $this->employee = new Employee;
		  $this->sms   = new SMSAPI;
		  $this->reward   = new Reward;
		  $this->brand   = new Brand;
		  $this->notification   = new Notification;
    }

    public function addEmployee(AddEmployeeRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'first_name'    => $input['first_name'],
        		'last_name'    => $input['last_name'],
        		'designation'    => $input['designation'],
        		'email'    => $input['email'],
        		'gender'    => $input['gender'],
        		'status'    => $input['status'],
        		'contact'  => $input['contact'],
        		'joining_date'  => date('Y-m-d',strtotime($input['joining_date'])),
        		'birth_date'  => date('Y-m-d',strtotime($input['birth_date'])),
        		'corporate_id'  => $input['corporate_id'],
        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->employee->addEmployee($data);
        	
        	if($result){
        		if(Request::has('profile_pic') && Request::get('profile_pic') != null ){
        			if(env('APP_ENV') == 'local'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/employee';
                    } else if(env('APP_ENV') == 'production'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/employee';
                    }
                    if(!is_dir($image_folder)){
                        mkdir($image_folder, 0777, true);
                    } 
                    $upload=file_put_contents($image_folder.'/'.$result.'.png', base64_decode(Request::get('profile_pic')));
                    if($upload){
                    	// chmod($image_folder.'/' . $result . '.png', 0777);
                        $data['profile_pic'] = $result.".png";
                    	$data['id'] = $result;
                		$update_result = $this->employee->updateEmployee($data);
                	}
                }
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $result),
				],200);        		
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    public function getEmployee(){

    	try{
	    	$data = array();
		    if(Request::has('status')){
			     $data['status'] = Request::get('status');
		    }
	        if(Request::has('cid')){
	            $data['cid'] = Request::get('cid');
	        }
	        if(Request::has('q')){
	            $data['q'] = Request::get('q');
	        }
	        if(Request::has('eid')){
	            $data['eid'] = Request::get('eid');
	        }
			$result = $this->employee->getEmployee($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function getEmployeeDashboard(){

    	try{
	    	$data = array();
		    if(Request::has('status')){
			     $data['status'] = Request::get('status');
		    }
	        if(Request::has('cid')){
	            $data['cid'] = Request::get('cid');
	        }
			$result = $this->employee->getEmployeeDashboard($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function activeEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
	        'id' => 'required|numeric'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	$data= array(
	        		'status'    => 1,
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
	        	$result = $this->employee->activeEmployee($data,$input['id']);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' => "Employee Activated Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function deactiveEmployee(){
    	
    	$input = Request::all();
    	$validator = Validator::make($input, [
	        'id' => 'required|numeric'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	$data= array(
	        		'status'    => 0,
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
	        	$result = $this->employee->deactiveEmployee($data,$input['id']);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' => "Employee Deactivated Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function updateEmployee(){
    	$input = Request::all();
    	$isexist=false;
        $errors = array();
    	$validator = Validator::make($input, [
    		'id' => 'required|numeric',
	        'first_name' 		=> 'required',
	        'last_name' 		=> 'required',
	        'gender' 		=> 'required',
	        'designation' 		=> 'required',
	        'contact' 		=> 'required|numeric',
	        'email' => 'required',
	        'joining_date' => 'required',
	        'birth_date' => 'required',
	        'status' => 'required'
    	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				],500);
	        } else {
	        	if($this->employee->isEmployeeEmailExist(array('email'=>$input['email'],'id'=>$input['id']))){
	                $isexist = true;
	                $errors['email'] = array('Email is Exist');
	            }   
	            if($this->employee->isEmployeeMobileExist(array('contact'=>$input['contact'],'id'=>$input['id']))){
	                $isexist = true;
	                $errors['contact'] = array('Mobile Number is Exist');
	            }
	            if($isexist){
	                return response()->json([
	                    'errors' => $errors,
	                    'message' => "Invalid Data",
	                        ], 422);
	            }  
	            $data= array(
	        		'first_name'    => $input['first_name'],
	        		'last_name'    => $input['last_name'],
	        		'designation'    => $input['designation'],
	        		'email'    => $input['email'],
	        		'gender'    => $input['gender'],
	        		'status'    => $input['status'],
	        		'contact'  => $input['contact'],
	        		'joining_date'  => date('Y-m-d',strtotime($input['joining_date'])),
	        		'birth_date'  => date('Y-m-d',strtotime($input['birth_date'])),
	        		'id' => $input['id'],
	        		'corporate_id' => $input['corporate_id'],
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        		); 
	        	if(Request::has('profile_pic') && Request::get('profile_pic') != null){
	        		if(env('APP_ENV') == 'local'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/employee';
                    } else if(env('APP_ENV') == 'production'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/employee';
                    }
                    $upload = file_put_contents($image_folder.'/'.$input['id'].'.png', base64_decode(Request::get('profile_pic')));
	                    if($upload){
	                    	// chmod($image_folder.'/' . $input['id'] . '.png', 0777);
	                        $data['profile_pic'] = $input['id'].".png";
	                    }
	                	
                }
                
	        	$result = $this->employee->updateEmployee($data);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' =>"Employee update Successfully",
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],500);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function deleteEmployee(){
    	$input = Request::all();
    	$validator = Validator::make($input, [
    		'id' => 'required|numeric',
	       	]);
    	try{
	    	if ($validator->fails()) {
	    		return response()->json([
				    'error'   => true,
				    'message' => $validator->errors()->first()
				]);
	        } else {
	        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');

	        	if(count($this->employee->isEmployeeRewardIssued(array('eid' =>$input['id'])))>0){
	        		return response()->json([
					    'error'   => true,
					    'message' => "This Employee's reward not Used"
					],400);
	        	} 
	        	$result = $this->employee->deleteEmployee($input);
	        	if($result){
	        		return response()->json([
					    'error'   => false,
					    'message' =>"Employee deleted Successfully",
					]);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					]);
	        	}
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function importEmployee(){
    	$isexist = false;
    	$errors = array();
    	$result;
    	try{
	    	$name = $_FILES['employeecsv']['name'];
  			$size = $_FILES['employeecsv']['size'];
			$filename = md5(uniqid().time()).".csv";
            $tmp = $_FILES['employeecsv']['tmp_name'];
            if(env('APP_ENV') == 'local'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/csv';
            } else if(env('APP_ENV') == 'production'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/csv';
            }
            if(!is_dir($image_folder)){
                mkdir($image_folder, 0777, true);
            }
            if (move_uploaded_file($tmp, $image_folder.'/'.$filename)) {
            	$file = fopen($image_folder.'/'.$filename,"r");
            	$i=0;$j=0;
            	while($employee = fgetcsv($file)){
					if($i>0 && (!empty($employee))){
						if($this->employee->isEmployeeEmailExist(array('email'=>$employee[4]))){
	                		$isexist = true;
	                		$errors[$j]['email'] = $employee[4];
	            		}   
	            		if($this->employee->isEmployeeMobileExist(array('contact'=>$employee[3]))){
	                		$isexist = true;
	                		$errors[$j]['contact'] = $employee[3];
	           	 		}
	           	 		if(!$isexist){
	           	 			$data = array(
				        		'first_name'    => $employee[0],
				        		'last_name'    => $employee[1],
				        		'designation'    => $employee[2],
				        		'contact'  => $employee[3],
				        		'email'    => $employee[4],
				        		'joining_date'  => date('Y-m-d',strtotime(str_replace("/","-",$employee[5]))),
				        		'birth_date'    => date('Y-m-d',strtotime(str_replace("/","-",$employee[6]))),
				        		'gender'  => $employee[7],
				        		'corporate_id'  => Request::get('corporate_id'),
				        		'status'   => 0,
				        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
				        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
			        		); 
							$result = $this->employee->addEmployee($data);
	           	 		}
	           	 		$isexist = false;
			        }
			        $i++;$j++;
			    }
			    if(count($errors)>0){
	                return response()->json([
	                    'errors' => $errors,
	                    'message' => "Invalid Data",
	                        ], 422);
	            }  
				if($result){
					return response()->json([
					    'error'   => true,
					    'message' => "Employee Inserted Successfully",
					],200);
				}else{
					return response()->json([
					    'error'   => true,
					    'message' => "Something went Wrong",
					],400);
				}
		    } else {
		    	return response()->json([
				    'error'   => true,
				    'message' => "File is not uploaded",
				],500);
		    }    	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    /* Employee Login For App*/
    public function sendOTP(){
    	$input = Request::all();
        $validator = Validator::make($input, [
            "contact" => "required|numeric",
        ]);
    	try{
    		if ($validator->fails()) {
	            return response()->json([
	                'error'   => true,
	                'message' => $validator->errors()->first()
	            ]);
	        }
    		$employee = $this->employee->isEmployeeMobileExist(array('contact'=>$input['contact']));
    		if ($employee) {
    			$otp = $this->generateOTP(6);
    			$smsData["numbers"] = [$input['contact']];
                $smsData["details"] = [
                    "name" => $employee->first_name." ".$employee->last_name,
                    "code" => $otp
                ];
                $result = $this->sms->send("Login_OTP", $smsData);
                if($result){
                	$otp_data = array(
                		'contact' 	=>	$input['contact'],
                		'data_id' 	=>	$employee->id,
                		'otp_code' 	=>  $otp,
                		'type' =>1,
                	);
                	$otp_sent = $this->employee->addOtp($otp_data);
                	if($otp_sent){
		    			return response()->json([
						    'error'   => false,
						    'data'    => $employee,
                                                    'message'   => "OTP sent successfully"
						],200);
		    		} else {
		    			return response()->json([
						    'error'   => false,
						    'message' => "Something went Wrong",
						],400);
		    		}
	    		} else {
	    			return response()->json([
					    'error'   => false,
					    'message' => "Something went Wrong",
					],400);
	    		}

    		} else {
    			return response()->json([
				    'error'   => true,
				    'message' => "Your mobile number is either incorrect or not registered with PerksVilla. Please contact your employer to register yourself with PerksVilla.",
				],404);
    		}
    	} catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);

    	} catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    public function verifyOTP(EmployeeVerifyOTPRequest $request){
    	$input = $request->validated();
    	try{
    		$otp_data = array(
    			'contact' => $input['contact'],
    			'otp_code' => $input['otp'],
    		);
    		$otp = $this->employee->verifyOtp($otp_data);
    		if($otp){
    			 
    			if($otp->type ==1){
    				$token = $this->createToken(32);
    				$update_employee = $this->employee->updateEmployee(array('token' =>$token,'id'=>$otp->data_id));
    				if($update_employee){
    					return response()->json([
						    'error'   => false,
						    'token'   => base64_encode($otp->data_id."|".$token),
						],200);
    				} else {
    					return response()->json([
						    'error'   => true,
						    'data'    => "Something went Wrong",
						],500);
    				}
    			} else{
    				return response()->json([
					    'error'   => true,
					    'message' => "OTP is Wrong",
					],404);
    			} 
    		} else {
    			return response()->json([
				    'error'   => true,
				    'message' => "OTP is Wrong",
				],404);

    		}
    	} catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);

    	} catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    private function generateOTP($digit){
        $random_number=''; // set up a blank st*ring
        $count=0;
        while ( $count < $digit ) {
            $random_digit = mt_rand(0, 9);
            
            $random_number .= $random_digit;
            $count++;
        }
        return $random_number;
    }
    private function createToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max-1)];
        }

        return $token;
    }
    public function getEmployeeDetail(EmployeeDetailRequest $request){
    	try{
    		$input = $request->validated();
    		$data['eid'] = $input['id'];
			$result = $this->employee->getEmployeeDetail($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function getEmployeeReward(EmployeeDetailRequest $request){
    	try{
    		$input = $request->validated();
    		$data['employee_id'] = $input['id'];
			$result = $this->reward->getRewardEmployee($data);
			foreach($result as $key => $value){
				$addresses = $this->brand->getAddressById(explode(',', $value->brand_address_id));
				$result[$key]->addresses = $addresses; 
			}
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function getEmployeeNotification(){
    	try{
    		$data['employee_id'] = Request::get('id');
    		$read_notification = $this->notification->readNotificationEmployee($data); 
			$result = $this->notification->getNotification($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    }
    public function getEmployeeNotificationCount(){
    	try{
    		$data['employee_id'] = Request::get('id');
    		$data['status'] = 0;
			$result = $this->notification->getNotificationCount($data);
			return response()->json([
			    'error'   => false,
			    'count' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				]);
        }
    } 
}