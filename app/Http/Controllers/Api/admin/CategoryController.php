<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Category;
use App\Http\Requests\DeleteBrandRequest;

class CategoryController extends Controller
{
    public function __construct(){
          $this->category = new Category;
    }
    public function getCategories(){
        $input = Request::all();
        $validator = Validator::make($input, [
            'type'         => 'required|numeric',
        ]);
        try{
            if ($validator->fails()) {
                return response()->json([
                    'error'   => true,
                    'message' => $validator->errors()->first()
                ]);
            } else {
                $result = $this->category->getCategory($input);
                return response()->json([
                    'error'   => false,
                    'data' => $result,
                    'user_created' => $this->category->getCategory($input)
                ]);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ]);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ]);
        } 
    }
    public function addCategory(){
        $input = Request::all();
        $validator = Validator::make($input, [
            'name'        => 'required',
            'status'     =>'required',
            'type'     =>'required',
            'description'     =>'required',
        ]);
        try{
            if ($validator->fails()) {
                return response()->json([
                    'error'   => true,
                    'message' => $validator->errors()->first()
                ]);
            } else {
                $data= array(
                    'name'    => $input['name'],
                    'status'    => $input['status'],
                    'type'    => $input['type'],
                    'description'    => $input['description'],
                    'created'  => Carbon::now()->format('Y-m-d H:i:s'),
                    'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
                );
                if(Request::has('user_id')){
                    $data['user_id'] = $input['user_id'];
                }
                $result = $this->category->addCategory($data);
                if($result){
                    return response()->json([
                        'error'   => false,
                        'data' =>array('id' => $result),
                    ],201);
                } else{
                    return response()->json([
                        'error'   => true,
                        'message' =>"Something went Wrong",
                    ],500);
                }
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    }
    public function deleteCategory(){
        $input = Request::all();
        $validator = Validator::make($input, [
            'id' => 'required|numeric',
            ]);
        try{
            if ($validator->fails()) {
                return response()->json([
                    'error'   => true,
                    'message' => $validator->errors()->first()
                ]);
            } else {
                if(Request::has('custom')){
                    $input['custom'] =  true;
                }
                $input['status'] = 2;
                $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
                $result = $this->category->deleteCategory($input);
                if($result){
                    return response()->json([
                        'message' =>"Category deleted Successfully",
                    ],200);
                } else{
                    return response()->json([
                        'message' =>"Something went Wrong",
                    ],500);
                }
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    }
    public function updateCategory(){
        $input = Request::all();
        $validator = Validator::make($input, [
            'id'  => 'required',
            'name'     => 'required',
            'status'   => 'required',
        ]);
        try{
            if ($validator->fails()) {
                return response()->json([
                    'error'   => true,
                    'message' => $validator->errors()->first()
                ],400);
            } else {
                $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
                $result = $this->category->updateCategory($input);
                if($result){
                    return response()->json([
                        'error'   => false,
                        'message' =>"Category update Successfully",
                    ],200);
                } else{
                    return response()->json([
                        'error'   => true,
                        'message' =>"Something went Wrong",
                    ],400);
                }
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        }

    }
    public function getCategory($id){
        try{
            $result = $this->category->getCategoryById($id);
            if($result){
                return response()->json([
                    'error'   => false,
                    'data' => $result,
                 ],200);
            }else{
                 return response()->json([
                    'error'   => true,
                    'message' => "Invalid Category",
                ],404);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } 
    }

     public function activeCategory(DeleteBrandRequest $request){
        $input = $request->validated();
        try{
            $data= array(
                'status'    => 1,
                'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->category->activeCategory($data,$input['id']);
            if($result){
                return response()->json([
                    'error'   => false,
                    'message' => "Category Activated Successfully",
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    }

    public function deactiveCategory(DeleteBrandRequest $request){
        
        $input = $request->validated();
        try{
            
            $data= array(
                'status'    => 0,
                'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
            );
            $result = $this->category->deactiveCategory($data,$input['id']);
            if($result){
                return response()->json([
                    'error'   => false,
                    'message' => "Category Deactivated Successfully",
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }

        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    }
}