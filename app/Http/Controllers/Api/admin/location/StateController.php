<?php
namespace App\Http\Controllers\Api\admin\location;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\State;
use App\Http\Requests\StoreStateRequest;
use App\Http\Requests\UpdateStateRequest;
use App\Http\Requests\DeleteBrandRequest;

class StateController extends Controller
{
	public function __construct(){
		  $this->state = new State;
    }

    public function addState(StoreStateRequest $request){
    	$input = $request->validated();	
    	try{
        		$state_data= array(
                    'name'    => $input['txtStateName'],
                    'country_id'    => $input['txtCountry'],
	        		'status'    => $input['status'],
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
        		$state_result = $this->state->addState($state_data);
        		if($state_result){
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $state_result),
					],201);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateState(UpdateStateRequest $request){

    	$input = $request->validated();
    	  	
    	try{
    		
    		$state_data= array(
                'name'    => $input['txtStateName'],
                'country_id'    => $input['txtCountry'],
        		'status'    => $input['status'],
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
            
    		$state_result = $this->state->updateState($state_data,$input['id']);
    		if($state_result){
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $state_result),
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],400);
    		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getState(){
        $data=array();
        if(Request::has('country')){
            $data['country'] = Request::get('country');
        }
        if(Request::has('status')){
            $data['status'] = Request::get('status');
        }
    	try{
			$result = $this->state->getState($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    // public function getCountryById($id){
    	
    // 	try{
    //         $result = $this->brand->getBrandById($id);
    //         if($result){
    //             return response()->json([
    //                 'error'   => false,
    //                 'data' => $result,
    //              ],200);
    //         }else{
    //              return response()->json([
    //                 'error'   => true,
    //                 'message' => "Invalid Brand",
    //             ],404);
    //         }
    //     } catch(\Exception $e) {
    //         return response()->json([
    //                 'error'   => true,
    //                 'message' => $e->getMessage(),
    //             ],400);
    //     } catch(\QueryException $e) {
    //         return response()->json([
    //                 'error'   => true,
    //                 'message' => $e->getMessage(),
    //             ],400);
    //     } 
    // }
    public function activeState(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'status'    => 1,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->state->activeState($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "State Activated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveState(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'status'    => 0,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->state->deactiveState($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "State Deactivated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    
    public function deleteState(DeleteBrandRequest $request){
    	try{
	    	$input = $request->validated();
        	$input['status'] = 3;
        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
        	$result = $this->state->deleteState($input);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' =>"State deleted Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
}