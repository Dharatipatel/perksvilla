<?php
namespace App\Http\Controllers\Api\admin\location;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\Country;
use App\Http\Models\Brand;
use App\Http\Requests\StoreCountryRequest;
use App\Http\Requests\UpdateCountryRequest;
use App\Http\Requests\DeleteBrandRequest;

class CountryController extends Controller
{
	public function __construct(){
          $this->country = new Country;
		  $this->brand = new Brand;
    }

    public function addCountry(StoreCountryRequest $request){
    	$input = $request->validated();	
    	try{
        		$country_data= array(
	        		'name'    => $input['txtCountryName'],
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
        		$country_result = $this->country->addCountry($country_data);
        		if($country_result){
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $country_result),
					],201);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateCountry(UpdateCountryRequest $request){

    	$input = $request->validated();
    	  	
    	try{
    		
    		$country_data= array(
                'name'    => $input['txtCountryName'],
        		'status'    => $input['status'],
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
            
    		$country_result = $this->country->updateCountry($country_data,$input['id']);
    		if($country_result){
        		return response()->json([
				    'error'   => false,
				    'data' =>array('id' => $country_result),
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],400);
    		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getCountry(){
        $data=array();
		if(Request::has('status')){
			$data['status'] = Request::get('status');
		}

    	try{
			$result = $this->country->getCountry($data);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    
    public function activeCountry(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'status'    => 1,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->country->activeCountry($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Country Activated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveCountry(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'status'    => 0,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->country->deactiveCountry($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Country Deactivated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    
    public function deleteCountry(DeleteBrandRequest $request){
    	try{
	    	$input = $request->validated();
        	$input['status'] = 3;
        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
        	$result = $this->country->deleteCountry($input);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' =>"Country deleted Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function getBrandAddress(){
       
        try{
            if(Request::has('bid') && Request::get('bid') != null){
                $bid =  Request::get('bid');
            }else {
                $bid =  Request::get('brand_id');
            }
            $result = $this->brand->getAddress($bid);
            return response()->json([
                'error'   => false,
                'data' => $result,
            ],200);
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        }
    
    }

}