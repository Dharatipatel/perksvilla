<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Corporate;
use App\Http\Models\Reward;
use App\Http\Models\Brand;
use App\Http\Models\Employee;
use App\Http\Models\Offer;
use App\Http\Models\Support;


class AdminController extends Controller
{
	public function __construct(){
		  $this->corporate = new Corporate;
          $this->user = new User;
          $this->reward = new Reward;
          $this->employee = new Employee;
          $this->brand = new Brand;
          $this->offer = new Offer;
		  $this->support = new Support;
    }
    public function getStatistics(){
       try{
            $use = array();
            $unuse = array();
            $employee = array();
    
            $unuse['redeem'] =  0;
            $use['redeem'] =1;
            $employee['status'] = 1;  
            $data = array(
                'active_employee'        => $this->employee->getEmployeeCount($employee),
                'active_brand'           => $this->brand->getBrandCount(array('status'=>1)),
                'active_corporate'       => $this->corporate->getActiveCorporateCount(),
                'active_offers'          => $this->offer->offerCount(array('status'=>1)),
                'pending_offers'         => $this->offer->offerCount(array('status'=>3)),
                'upcoming_offers'        => $this->offer->offerCount(array('status'=>2)),
                'today_order'            => $this->offer->getTodayOrders(),
                'today_redeem'           => $this->offer->getTodayRedeem(array()),
                'total_sales'            => $this->offer->getSales(array()),
                'open_tickets'           => $this->support->ticketCount(array('status'=>1)),
                'closed_tickets'         => $this->support->ticketCount(array('status'=>2)),
                'payment_request'        => $this->brand->paymentRequestCount(),
            );
            if(!empty($data)){
                return response()->json([
                    'error'   => false,
                    'data' =>$data,
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } 
    }
}