<?php
namespace App\Http\Controllers\Api\admin;

use App\Http\Controllers\Controller;
use Request;
use Validator;
use Carbon\Carbon;
use App\Http\Models\User;
use App\Http\Models\Corporate;
use App\Http\Models\Reward;
use App\Http\Models\Brand;
use App\Http\Models\Employee;
use App\Http\Models\Offer;
use Illuminate\Support\Facades\File;
use App\Http\Requests\StoreCorporateRequest;
use App\Http\Requests\UpdateCorporateRequest;
use App\Http\Requests\DeleteBrandRequest;
use App\Http\Requests\Admin\EmailSendRequest;
use App\Custom\Email;
use App\Custom\SMSAPI;

class CorporateController extends Controller
{
	public function __construct(){
		  $this->corporate = new Corporate;
          $this->user = new User;
          $this->reward = new Reward;
          $this->employee = new Employee;
          $this->brand = new Brand;
		  $this->offer = new Offer;
    }

    public function addCorporate(StoreCorporateRequest $request){
    	$input = $request->validated();
    	$token = $this->createToken();   	
    	try{
    		$user_data =array(
    			'email'    => $input['email'],
        		'contact'  => $input['contact'],
        		'password'  => $input['password'],
        		'token'    =>$token,
        		'role_id' => 3,
        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'status'   => $input['status'],
    		);        	
        	$user_result = $this->user->register($user_data);
        	if($user_result){
        		$corporate_data= array(
	        		'uid' => $user_result,
                    'name'    => $input['name'],
                    'company_size'    => $input['company_size'],
	        		'category'    => $input['category'],
	        		'description'    => $input['description'],
	        		'contact_person_name'  => $input['person_name'],
	        		'address'  => $input['address'],
	        		'created'  => Carbon::now()->format('Y-m-d H:i:s'),
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
                if ($request->has('website_url')) {
                    $website_url = $request->get('website_url');
                    $corporate_data['website_url'] = (strpos($website_url,'http://') !== false || strpos($website_url,'https://') !== false) ? $website_url : 'http://'.$website_url;
                }
                if ($request->has('facebook_page')) {
                    $facebook_page = $request->get('facebook_page');
                    $corporate_data['facebook_page'] = (strpos($facebook_page,'http://') !== false || strpos($facebook_page,'https://')!== false) ? $facebook_page : 'https://'.$facebook_page;
                }
                if ($request->has('linkedin_page')) {
                    $linkedin_page = $request->get('linkedin_page');
                    $corporate_data['linkedin_page'] = (strpos($linkedin_page,'http://')!== false || strpos($linkedin_page,'https://')!== false) ? $linkedin_page : 'https://'.$linkedin_page;
                }
        		$corporate_result = $this->corporate->addCorporate($corporate_data);
        		if($corporate_result){
                    if($request->has('image') && $request->get('image') != null){
                        if(env('APP_ENV') == 'local'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/corporate';
                        } else if(env('APP_ENV') == 'production'){
                            $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/corporate';
                        }
                        if(!is_dir($image_folder)){
                            mkdir($image_folder, 0777, true);
                        }
                        $corporate_image_data = array();
                        $upload=file_put_contents($image_folder.'/'.$corporate_result.'.png', base64_decode($request->get('image')));
                        if($upload){
                            // chmod($image_folder.'/' . $corporate_result . '.png', 0777);
                            $corporate_image_data['logo'] = $corporate_result.".png";
                            $corporate_image_result = $this->corporate->updateCorporate($corporate_image_data,$corporate_result);
                        }
                    }
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $corporate_result),
					],201);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    }
    public function updateCorporate(UpdateCorporateRequest $request){
        $isexist=false;
        $errors = array();
    	$input = $request->validated();
    	$token = $this->createToken();   	
    	try{
    		$user_data =array(
    			'email'    => $input['email'],
        		'contact'  => $input['contact'],
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        		'status'   => $input['status'],
    		);
            if($this->user->isUserExist(array('email'=>$input['email'],'id'=>$input['uid']))){
                $isexist = true;
                $errors['email'] = array('Email is Exist');
            }   
            if($this->user->isMobileExist(array('contact'=>$input['contact'],'id'=>$input['uid']))){
                $isexist = true;
                $errors['contact'] = array('Mobile Number is Exist');
            }
            if($isexist){
                return response()->json([
                    'errors' => $errors,
                    'message' => "Invalid Data",
                        ], 422);
            }        	
        	$user_result = $this->user->updateUser($user_data,$input['uid']);
        	if($user_result){
        		$corporate_data= array(
	        		'name'    => $input['name'],
	        		'company_size'    => $input['company_size'],
	        		'description'    => $input['description'],
	        		'contact_person_name'  => $input['person_name'],
	        		'category'  => $input['category'],
	        		'address'  => $input['address'],
	        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
	        	);
                if ($request->has('website_url')) {
                    $website_url = $request->get('website_url');
                    $corporate_data['website_url'] = (strpos($website_url,'http://') !== false || strpos($website_url,'https://')!== false ) ? $website_url : 'http://'.$website_url;
                }
                if ($request->has('facebook_page')) {
                    $facebook_page = $request->get('facebook_page');
                    $corporate_data['facebook_page'] = (strpos($facebook_page,'http://')!== false  || strpos($facebook_page,'https://')!== false ) ? $facebook_page : 'https://'.$facebook_page;
                }
                if ($request->has('linkedin_page')) {
                    $linkedin_page = $request->get('linkedin_page');
                    $corporate_data['linkedin_page'] = (strpos($linkedin_page,'http://') !== false || strpos($linkedin_page,'https://')!== false ) ? $linkedin_page : 'https://'.$linkedin_page;
                }
                if($request->has('image') && $request->get('image') != null){
                    if(env('APP_ENV') == 'local'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/corporate';
                    } else if(env('APP_ENV') == 'production'){
                        $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/corporate';
                    }
                    $upload=file_put_contents($image_folder.'/'.$input['id'].'.png', base64_decode($request->get('image')));
                    if($upload){
                        // chmod($image_folder.'/' . $input['id'] . '.png', 0777);
                        $corporate_data['logo'] = $input['id'].".png";
                    }
                }
        		$corporate_result = $this->corporate->updateCorporate($corporate_data,$input['id']);
        		if($corporate_result){
	        		return response()->json([
					    'error'   => false,
					    'data' =>array('id' => $corporate_result),
					],200);
	        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        	} else{
	        		return response()->json([
					    'error'   => true,
					    'message' =>"Something went Wrong",
					],400);
        		}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }

    
    }
    public function getCorporate(){
    	$limit = 10000;
		if(Request::has('limit')){
			$limit = Request::get('limit');
		}

    	try{
			$result = $this->corporate->getCorporate($limit);
			return response()->json([
			    'error'   => false,
			    'data' => $result,
                'active_corporate' => $this->corporate->getActiveCorporateCount(),
                'active_employee' => $this->employee->getActiveEmployeeCount(),
                'issue_coupen' => $this->reward->getCoupenIssued(),
			],200);
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],400);
        }
    }
    public function getCorporateById($id){
    	
    	try{
            $result = $this->corporate->getCorporateById($id);
            if($result){
                return response()->json([
                    'error'   => false,
                    'data' => $result,
                 ],200);
            }else{
                 return response()->json([
                    'error'   => true,
                    'message' => "Invalid Corporate",
                ],404);
            }
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],400);
        } 
    }
    public function activeCorporate(DeleteBrandRequest $request){
    	$input = $request->validated();
    	try{
        	$data= array(
        		'status'    => 1,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->corporate->activeCorporate($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Corporate Activated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }

    public function deactiveCorporate(DeleteBrandRequest $request){
    	
    	$input = $request->validated();
    	try{
	    	
        	$data= array(
        		'status'    => 0,
        		'modified'  => Carbon::now()->format('Y-m-d H:i:s'),
        	);
        	$result = $this->corporate->deactiveCorporate($data,$input['id']);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' => "Corporate Deactivated Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}

        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    
    public function deleteCorporate(DeleteBrandRequest $request){
    	try{
	    	$input = $request->validated();
        	$input['status'] = 3;
        	$input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
        	$result = $this->corporate->deleteCorporate($input);
        	if($result){
        		return response()->json([
				    'error'   => false,
				    'message' =>"Corporate deleted Successfully",
				],200);
        	} else{
        		return response()->json([
				    'error'   => true,
				    'message' =>"Something went Wrong",
				],500);
        	}	
        } catch(\Exception $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        } catch(\QueryException $e) {
        	return response()->json([
				    'error'   => true,
				    'message' => $e->getMessage(),
				],500);
        }
    }
    public function getStatistics(){
       try{
            $use = array();
            $unuse = array();
            $employee = array();
            if(Request::has('cid')){
                $unuse['cid'] = Request::get('cid');
                $use['cid'] = Request::get('cid');
                $employee['cid'] = Request::get('cid');
                $amount_invested['cid'] = Request::get('cid');  
            }
            $unuse['redeem'] =  0;
            $use['redeem'] =1;
            $employee['status'] = 1;  
            $data = array(
                'employee'        => $this->employee->getEmployeeCount($employee),
                'brand'           => $this->brand->getBrandCount(array('status'=>1)),
                'unused_reward'   => $this->reward->getRewardUsed($unuse),
                'used_reward'     => $this->reward->getRewardUsed($use),
                'amount_saved'    => $this->reward->getBillAmoount($amount_invested),
                'amount_invested' => $this->reward->getInvestedAmount($amount_invested),
                'social'          => 0,
                'availble_deals'  => $this->offer->offerCount(array('status'=>1)),
            );
            if(!empty($data)){
                return response()->json([
                    'error'   => false,
                    'data' =>$data,
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } 
    }
    private function createToken(){
    	return str_random(32);
    }
    
    public function emailSend(EmailSendRequest $request) {

        try {
            $data = $request;
            $result = $this->corporate->savePassword($data);
            if ($result != null) {

                $url = url("/") . '/corporate/login';
                $email = new Email;
                $mailData = [
                    "name" => $result->user_name,
                     "email" => $result->email,
                    "password" => $data->password,
                    "url"=>"<a href='{$url}'>{$url}</a>"
                ];

                $email->send("user_password", $mailData);
                
                //  send SMS
                $sms = new SMSAPI;
                $smsData["numbers"] = [$result->contact];
                $mailData['url'] = $url;
                $smsData["details"] = $mailData;

                $result = $sms->send("user_password", $smsData);

                return response()->json([
                            'error' => false,
                            'data' => [],
                            'message' => "Email sent successfully",
                                ], 200);
            } else {
                return response()->json([
                            'error' => true,
                            'message' => "Something went Wrong",
                                ], 500);
            }
        } catch (\Exception $e) {
            return response()->json([
                        'error' => true,
                        'message' => $e->getMessage(),
                            ], 400);
        }
    }
    public function removeCorporateProfike(DeleteBrandRequest $request){
        try{
            $input = $request->validated();
            $input['logo'] = NULL;
            $input['modified'] = Carbon::now()->format('Y-m-d H:i:s');
            if(env('APP_ENV') == 'local'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'] . '/'.env('PROJECT_NAME').'/uploads/corporate';
            } else if(env('APP_ENV') == 'production'){
                $image_folder =$_SERVER['DOCUMENT_ROOT'].'/uploads/corporate';
            }
            $image_path = $image_folder.'/'.$input['id'].'.png';

            if (File::exists($image_path)) {
                unlink($image_path);
            }
            $result = $this->corporate->updateCorporate($input, $input['id']);
            if($result){
                return response()->json([
                    'error'   => false,
                    'message' =>"Corporate Logo deleted Successfully",
                ],200);
            } else{
                return response()->json([
                    'error'   => true,
                    'message' =>"Something went Wrong",
                ],500);
            }   
        } catch(\Exception $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        } catch(\QueryException $e) {
            return response()->json([
                    'error'   => true,
                    'message' => $e->getMessage(),
                ],500);
        }
    }
}