<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Custom\SMSAPI;
use Illuminate\Http\Request;

class SMSController extends Controller {

    public function index() {
        return view("sms");
    }

    public function send(Request $request) {
        try {
            $sms = new SMSAPI;
            $options = [
                "sms" => [
                    [
                        "message" => $request->message,
                        "to" => explode(",", $request->number)
                    ]
                ]
            ];

            $result = $sms->send($options);
            echo "<pre>";
            print_r($result);
            exit;
        } catch (\Exception $e) {
            echo "<pre>";
            print_r($e->getMessage());
            exit;
        }
    }

}
