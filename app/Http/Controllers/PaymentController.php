<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Custom\Common;

class PaymentController extends Controller {

    public function pay(Request $request) {
        //$data = $request->all();
        $data = [
            "amount" => floatval(1.5),
            "firstname" => "Udayraj",
            "email" => "udayraj.kachhadiya@gmail.com",
            "phone" => 7874505049,
            "productinfo" => "Graphite Pencils",
        ];

        include(app_path() . "/libs/PayUmoney/form.php");
    }

    public function success(Request $request) {
        $data = $request->all();
        return view("payumoney.success", ["data" => $data]);
    }

    public function fail(Request $request) {
        $data = $request->all();
        return view("payumoney.fail", ["data" => $data]);
    }

}
