<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class AddAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolDeleteSMSTemplateRequest
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {

        $rules = [
            "bid" => "required",
            "addressArray" => "required",
        ];
        // foreach($this->request->get('addressArray') as $key => $val)
        // {
        //     $rules['address'] = 'required';
        //     $rules['pincode'] = 'required';
        //     $rules['country'] = 'required';
        //     $rules['state'] = 'required';
        //     $rules['city'] = 'required';
        // }
        return $rules;
    }

    public function messages(){
        $messages = [
            'bid.required' =>"Brand ID not available",
            'addressArray.required' =>"Addresses not available",
        ];
        // foreach($this->request->get('addressArray') as $key => $val)
        // {
        //     $messages['address.required'] = 'address ' . $key.' is required';
        //     $messages['pincode.required'] = 'pincode '. $key.' is required';
        //     $messages['country.required'] = 'country ' . $key.' is required';
        //     $messages['state.required']   = 'state ' . $key.'  is required';
        //     $messages['city.required']    = 'city ' . $key.' is  required';
        // }
        return $messages;
    }
}
