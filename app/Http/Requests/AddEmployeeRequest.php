<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class AddEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolDeleteSMSTemplateRequest
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {

        $rules = [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'designation'       => 'required',
            'gender'            => 'required',
            'contact'           => 'required|numeric|unique:employee',
            'email'             => 'required|unique:employee',
            'joining_date'      => 'required',
            'birth_date'        => 'required',
            'corporate_id'      => 'required',
            'status'      => 'required',
        ];
        return $rules;
    }

    public function messages(){
        $messages = [
            'first_name.required'        => 'First Name is Required',
            'last_name.required'         => 'Last Name is Required',
            'designation.required'       => 'Designation is Required',
            'gender.required'            => 'Gender is Required',
            'contact.required'           => 'Contact is Required',
            'contact.numeric'            => 'Contact Should be numeric',
            'contact.unique'             => 'Contact Should be unique',
            'email.required'             => 'Email is Required',
            'email.unique'               => 'Email Should be unique',
            'joining_date.required'      => 'joining date is Required',
            'birth_date.required'        => 'Birth Date is Required',
            'corporate_id.required'      => 'Corporate  is Required',
            'status.required'            => 'status  is Required',
        ];
        return $messages;
    }
}
