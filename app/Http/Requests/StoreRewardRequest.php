<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreRewardRequest  extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "employee"=> "required",
            "category_id"=> "required",
            "offer_id"=> "required",
            "coupon_value"=> "required",
            "coupon_price"=> "required",
        ];

    }

    public function messages(){
        return [
            'employee.required' =>'Employee Data is required',
            'category_id.required' =>'Category Id   is required',
            'offer_id.required' =>'Offer Id  is required',
            'coupon_value.required' =>'coupon value  is required',
            'coupon_price.required' =>'coupon price  is required',
        ];
    }
}
