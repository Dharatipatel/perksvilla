<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class SendOtpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "employeeId"=> "required",
            "txtPhone"=> "required",
            "txtEmail"=> "required",
            "txtCoupon" => "required",
            "employee_name" =>"required"
        ];
    }

    public function messages(){
        return [
            'employeeId.required' =>'Employee id is required',
            'txtPhone.required' =>'Contact number is required',
            'txtEmail.required' =>'Email is required',
            'txtCoupon.required' =>'Coupon  code is required',
            'employee_name.required' =>'Employee name code is required',
        ];
    }
}
