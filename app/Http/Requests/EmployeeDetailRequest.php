<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolDeleteSMSTemplateRequest
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {

        $rules = [
            "id" => "required|numeric",
        ];
        return $rules;
    }

    public function messages(){
        $messages = [
            'id.required' =>"ID is required",
            'id.numeric'  =>"ID should be numeric",
        ];
        return $messages;
    }
}
