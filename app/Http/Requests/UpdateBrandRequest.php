<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "uid" => "required",
            "id" => "required",
            "brand_name"=> "required",
            "category_name"=> "required",
            "description"=> "required",
            "email"=> "bail|required",
            "contact"=> "bail|required",
            "business_hours"=> "required",
            "person_name"=> "required",
            // "website_url"=> "required",
            // "facebook_page"=> "required",
            "status" => "required",
            "commision"=> "sometimes|required|numeric|min:0|max:100"
        ];
    }

    public function messages(){
        return [
            'uid.required' =>"user id is required",
            'id.required' =>"brand id is required",
            'brand_name.required' =>'Brand Name  is require',
            'category_name.required' =>'Category Name  is required',
            'description.required' =>'Description  is required',
            'email.required' =>'Email is required',
            'contact.required' =>'contact is required',
            'business_hours.required' =>'business hours is required',
            'person_name.required' =>'person name is required',
            // 'website_url.required' =>'website url is required',
            // 'facebook_page.required' =>'facebook page is required',
            'status.required' =>'status is required',
            'commision.required' =>'commision is required',
            'commision.numeric' =>'commision should be number',
            'commision.min' =>'Please enter a value greater than or equal to 1.',
            'commision.max' =>'Please enter a value less than or equal to 100.',
        ];
    }
}
