<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class SupportReplyRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "comment" => "required",
            "status" => "required",
        ];
    }

    public function messages() {
        return [
            'comment.required' => 'Please leave a comment',
            'status.required' => 'Please select status',
        ];
    }

}
