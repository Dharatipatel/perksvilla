<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "id" => "bail|required|numeric",
            "newpassword"=> "required",
            "oldpassword"=> "required",
        ];
    }

    public function messages(){
        return [
            'id.required' =>"Id field is Required",
            'id.numeric' =>"Id field should be numeric",
            'newpassword.required' =>"New Password field is Required",
            'oldpassword.required' =>"Old Password field is Required",
        ];
    }
}
