<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCorporateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "uid" => "required",
            "id" => "required",
            "name"=> "required",
            "company_size"=> "required",
            "description"=> "required",
            "email"=> "bail|required",
            "contact"=> "bail|required",
            "category"=> "required",
            "person_name"=> "required",
            // "facebook_page"=> "required",
            // "website_url"=> "required",
            "address"=> "required",
            // "linkedin_page"=> "required",
            "status"=> "required"
        ];
    }

    public function messages(){
        return [
            'uid.required' =>"user id is required",
            'id.required' =>"Corporate id is required",
            'name.required' =>'Corporate Name  is require',
            'company_size.required' =>'Company size  is required',
            'description.required' =>'Description  is required',
            'email.required' =>'Email is required',
            'contact.required' =>'contact is required',
            'category.required' =>'Category is required',
            // 'website_url.required' =>'website url is required',
            'person_name.required' =>'person name is required',
            // 'facebook_page.required' =>'facebook page is required',
            'address.required' =>'address is required',
            // 'linkedin_page.required' =>'Linked In Page is required',
            'status.required' =>'status is required',
        ];
    }
}
