<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
         $rules = [
            "brand_id"=> "required",
            "name"=> "required",
            "ifsc"=> "required",
            "account_number"=> "required",
            "branch_details"=> "required",
            "account_type"=> "required",
        ];
       
        return $rules;
    }

    public function messages(){
        $messages = [
            'brand_id.required' =>'Brand ID  is required',
            'name.required' =>'Bank Name is required',
            'ifsc.required' =>'IFSC Cde is required',
            'account_number.required' =>'Account number is required',
            'branch_details.required' =>'Branch Details is required',
            'account_type.required' =>'Account type is required',
        ];
      
        return  $messages;
    }
}
