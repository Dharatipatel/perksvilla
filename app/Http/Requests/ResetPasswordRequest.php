<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "email" => "required|email",
            "password" => "required|confirmed",
            "password_confirmation" => "required",
            "reset_token" => "required",
        ];
    }

    public function messages() {
        return [
            "email.required" => "Email field id required",
            "email.email" => "Please enter a valid email",
            "password.required" => "Please enter password",
            "password_confirmation.required" => "Please confirm password",
            "password.confirmed" => "Password does not match",
            "reset_token.required" => "Invalid password reset token",
        ];
    }

}
