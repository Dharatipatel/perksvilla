<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "id" => "bail|required|numeric",
            "email"=> "required",
           
        ];
    }

    public function messages(){
        return [
            'id.required' =>"Id field is Required",
            'id.numeric' =>"Id field should be numeric",
            'email.required' =>"Email field is Required",
        ];
    }
}
