<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreOfferRequest  extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "title"=> "required",
            "description"=> "required",
            "value"=> "required",
            "price"=> "required",
            "apply_on"=> "required",
            "brand_address_id"=> "required",
            "valid_to"=> "required",
            "count"=> "required",
            "publish_date"=> "required|before:valid_to",
            "notes"=> "required",
            "brand_id"=> "required",
            "contact"=> "required",
        ];

    }

    public function messages(){
        return [
            'title.required' =>'Offer title  is required',
            'description.required' =>'Offer Description  is required',
            'value.required' =>'Offer value  is required',
            'price.required' =>'Offer Price  is required',
            'apply_on.required' =>'applicable on is required',
            'brand_address_id.required' =>'brand address is required',
            'valid_to.required' =>'Valide to is required',
            'count.required' =>'offer count hours is required',
            'publish_date.required' =>'publish date is required',
            'publish_date.before' =>'publish date not grater than valid to date or fix date',
            'notes.required' =>'important notes is required',
            'brand_id.required' =>'brand_id  is required',
            'contact.required'  =>'Please Enter Contact Number',
        ];
    }
}
