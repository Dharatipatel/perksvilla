<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return boolDeleteSMSTemplateRequest
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {

        $rules = [
            "contact" => "required|numeric",
        ];
        return $rules;
    }

    public function messages(){
        $messages = [
            'contact.required' =>"Mobile Number is required",
            'contact.numeric'  =>"Mobile Number should be numeric",
        ];
        return $messages;
    }
}
