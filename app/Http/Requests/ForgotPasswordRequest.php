<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "email"=> "required|email",
            "role_id"=> "required|numeric",
        ];
    }

    public function messages(){
        return [
            "email.required"    =>  "Email field id required",
            "email.email"    =>  "Please enter a valid email",
            "role_id.required"    =>  "Please enter a User Role ",
            "role_id.numeric"    =>  "User Role must be a number",
        ];
    }
}
