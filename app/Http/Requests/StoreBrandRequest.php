<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
         $rules = [
            "brand_name"=> "required",
            "category_name"=> "required",
            "description"=> "required",
            "email"=> "bail|required|unique:users",
            "contact"=> "bail|required|size:10|unique:users",
            "password"=> "required",
            "business_hours"=> "required",
            "person_name"=> "required",
            // "website_url"=> "required",
            // "facebook_page"=> "required",
            "commision"=> "required|numeric|min:0|max:100",
            "status"=> "required",
        ];
       
        return $rules;
    }

    public function messages(){
        $messages = [
            'brand_name.required' =>'Brand Name  is required',
            'category_name.required' =>'Category Name  is required',
            'description.required' =>'Description  is required',
            'email.required' =>'Email is required',
            'email.unique' =>'Email is already exist',
            'contact.required' =>'contact is required',
            'contact.size' =>'contact should be 10 digits',
            'contact.unique' =>'contact is already exist',
            'password.required' =>'password is required',
            'business_hours.required' =>'business hours is required',
            'person_name.required' =>'person name is required',
            'commision.required' =>'commision is required',
            'commision.numeric' =>'commision should be number',
            'commision.min' =>'Please enter a value greater than or equal to 1.',
            'commision.max' =>'Please enter a value less than or equal to 100.',
            // 'website_url.required' =>'website url is required',
            // 'facebook_page.required' =>'facebook page is required',
        ];
      
        return  $messages;;
    }
}
