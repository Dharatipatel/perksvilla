<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class RedeemCoupenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules()
    {
        return [
            "txtRedeemCoupenCode"=> "required",
            "txtBillAmount"=> "required",
            "txtMember"=> "required",
            "reward_id"=> "required",
            "txtOtp" =>"required",
            "txtEmail" =>"required",
            "txtContact" =>"required|numeric"
        ];
    }

    public function messages(){
        return [
            'txtRedeemCoupenCode.required' =>'Coupon Code is required',
            'txtBillAmount.required' =>'Bill Amount number is required',
            'txtMember.required' =>'Member is required',
            'reward_id.required' =>'Reward id is required',
            'txtOtp.required' =>'OTP is required',
            'txtEmail.required' =>'Email is required',
            'txtContact.required' =>'Contact is required',
            'txtContact.numeric' =>'Contact should be numeric',
        ];
    }
}
