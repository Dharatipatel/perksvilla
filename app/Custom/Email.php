<?php

namespace App\Custom;

use Illuminate\Http\Request;
use App\Http\Models\Mailtemplate;
use Mail;

class Email {

    public function send($tmplName, $mailData) {

        $mailTmpl = Mailtemplate::where("title", "=", $tmplName)->first();

        $template = $this->parse($mailTmpl->text, $mailData);
        $mailData['subject'] = $this->parse($mailTmpl->subject, $mailData);
        $mailData['template_name'] =$tmplName;
    
        Mail::send("auth.emails.inc.mail_layout", ["template" => $template], function($message) use($mailData) {
            if($mailData['template_name']=='coupon_code' || $mailData['template_name']=='otp_code'){
                $message->from('perks@perksvilla.com','Perksvilla');
            }
            $message->to($mailData['email'], $mailData['name'])
                    ->subject($mailData['subject']);

            if (isset($mailData['cc'])) {
                $message->cc($mailData['cc']);
            }

            if (isset($mailData['bcc'])) {
                $message->bcc($mailData['bcc']);
            }

            if (isset($mailData['attachments'])) {
                foreach ($mailData['attachments'] as $v) {
                    $message->attach($v);
                }
            }
        });
    }

    public function parse($template, $mailData) {

        $template = htmlspecialchars_decode($template);

        foreach ($mailData as $key => $value) {

            if ($key != "attachments") {
                $template = str_replace('<' . $key . '>', $value, htmlspecialchars_decode($template));
            }
        }


        return $template;
    }

}
