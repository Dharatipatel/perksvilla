<?php

namespace App\Custom;

use App\Http\Models\User;
use DB;

class Common {

    //use for transaction id too...
    public static function refer_code($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public static function get_id() {

        $randomCode = Common::refer_code(30);
        return $randomCode;
    }

    public static function getPasswordResetToken($length = 40) {
        $randomCode = Common::refer_code($length);
        $check = DB::table("password_resets")->where("token", "=", $randomCode)->count();
        while ($check > 0) {
            $randomCode = Common::refer_code($length);
            $check = DB::table("password_resets")->where("token", "=", $randomCode)->count();
        }
        return $randomCode;
    }

    public static function getToken($length = 32) {
        $randomCode = Common::refer_code($length);
        $check = User::where("token", "=", $randomCode)->count();
        while ($check > 0) {
            $randomCode = Common::refer_code($length);
            $check = User::where("token", "=", $randomCode)->count();
        }
        return $randomCode;
    }
    public static function send($fields) {

        $headers = array
            (
            'Authorization:key=' . 'AAAARR2LPsE:APA91bEa4bVL8i8udeRIvbzyHa91fj9pjqSSTEJEr-yrqVYXBv_CvcG0wwXeGPKBRVplssr0xWbEDCT8-8yhka8Grm8L4fbZHdsdR0v7OPUjOL08lT2Rx6qzs40TjOxq17oPJYHidWwWtu5NqIfkWv6ZprRFtgAo_w',
            'Content-Type: application/json'
        );
        //dd($headers);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        dd($result);
    }

}
