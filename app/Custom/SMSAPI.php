<?php

namespace App\Custom;

use App\Http\Models\SMSTemplate;

class SMSAPI {

    private $authKey;
    private $senderId;
    private $route = "default";
    private $API_URL;

    function __construct() {
        $this->API_URL = env("SMS_URL");
        $this->authKey = env("SMS_AUTH_KEY");
        $this->senderId = env("SMS_SENDER_ID");
        $this->route = env("SMS_ROUTE");
    }

    public function request($options = array()) {
        $postData = array(
            //'authkey' => $this->authKey,
            'sender' => $this->senderId,
            'route' => $this->route,
            'country' => "91",
        );

        foreach ($options as $key => $value) {
            $postData[$key] = $value;
        }
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->API_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => array(
                "authkey:{$this->authKey}",
                "content-type: application/json"
            )
                //,CURLOPT_FOLLOWLOCATION => true
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        $server_output = json_encode(curl_exec($ch));

        curl_close($ch);

        return json_decode($server_output);
    }

    public function send($tmplName, $options) {
        $data = [];
        $smsTmpl = SMSTemplate::where("title", "=", $tmplName)->first();
        $template = $this->parse($smsTmpl->text, $options["details"]);
        $data["sms"][] = ["message" => $template, "to" => $options["numbers"]];
        return $this->request($data);
    }

    public function parse($template, $smsData) {

        $template = htmlspecialchars_decode($template);

        foreach ($smsData as $key => $value) {
            $template = str_replace('<' . $key . '>', $value, htmlspecialchars_decode($template));
        }

        return $template;
    }

}
