<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'script' => [
        'login' => [
            'login' => $appPath . 'assets/corporate/js/login.js',
            'forgotPassword' => $appPath . 'assets/corporate/js/forgotpassword.js',
        ],
        'profile' => [
            'changepassword' => $appPath . 'assets/corporate/js/profile/changepassword.js',
            'view' => $appPath . 'assets/corporate/js/profile/view.js',
        ],
        'offer' => [
            'offer' => $appPath . 'assets/corporate/js/offer/offer.js',
        ],
        'employee' => [
            'employee' => $appPath . 'assets/corporate/js/employee/employee.js',
        ],
        'reward' => [
            'reward' => $appPath . 'assets/corporate/js/reward/reward.js',
            'list' => $appPath . 'assets/corporate/js/reward/list.js',
            'success' => $appPath . 'assets/corporate/js/reward/success.js',
            'perksvilla' => $appPath . 'assets/corporate/js/reward/perksvilla.js',
            'orderhistory' => $appPath . 'assets/corporate/js/reward/orderhistory.js',
        ],
        'dashboard' => $appPath . 'assets/corporate/js/dashboard.js',
        'sha' => $appPath . 'assets/common/js/jsSHA/src/sha512.js',
        
        'support' => [
            'support' => $appPath . 'assets/corporate/js/support/support.js',
        ],
        'notification' => $appPath . 'assets/corporate/js/notification.js',
    ],
    'image' => [
        'favicon' => $appPath . 'assets/corporate/img/favicon.ico',
    ],
    'api' => [
        'profile' => [
            'changepassword' => $apiPath . "profile/changepassword",
            'view' => $apiPath . "corporate/id",
            'update' => $apiPath . "corporate/update",
            'remove' => $apiPath . "corporate/remove",
            'statistics' => $apiPath . 'corporate/statistics',
        ],
        'employee' => [
            'list' => $apiPath . "employee",
            'add' => $apiPath . "employee",
            'active' => $apiPath . "employee/active",
            'update' => $apiPath . "employee/update",
            'deactive' => $apiPath . "employee/deactive",
            'import' => $apiPath . "employee/import",
            'delete' => $apiPath . "employee/delete",
        ],
    ],
    "web" => [
        "forgotpassword" => "{$appPath}corporate/password/forgot",
        "resetpassword" => "{$appPath}corporate/password/reset",
    ]
];
