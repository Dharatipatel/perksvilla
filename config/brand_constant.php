<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'script' => [
        'jquery' => $appPath . 'assets/brand/vendor/jquery/jquery.min.js',
        'popper' => $appPath . 'assets/brand/vendor/popper.js/umd/popper.min.js',
        'bootstrap' => $appPath . 'assets/brand/vendor/bootstrap/js/bootstrap.min.js',
        'jquerycookie' => $appPath . 'assets/brand/vendor/jquery.cookie/jquery.cookie.js',
        'chart' => $appPath . 'assets/brand/vendor/chart.js/Chart.min.js',
        'validate' => $appPath . 'assets/brand/vendor/jquery-validation/jquery.validate.min.js',
        'front' => $appPath . 'assets/brand/js/front.js',
        'notification' => $appPath . 'assets/brand/js/notification.js',
        'login' => [
            'login' => $appPath . 'assets/brand/js/login.js',
            'forgotPassword' => $appPath . 'assets/brand/js/forgotpassword.js',
        ],
        'profile' => [
            'changepassword' => $appPath . 'assets/brand/js/profile/changepassword.js',
            'view' => $appPath . 'assets/brand/js/profile/view.js',
            'wallet' => $appPath . 'assets/brand/js/profile/wallet.js',
            'bankdetails' => $appPath . 'assets/brand/js/profile/bankdetails.js',
        ],
        'coupon' => [
            'coupon' => $appPath . 'assets/brand/js/coupon/coupon.js',
        ],
        'support' => [
            'support' => $appPath . 'assets/brand/js/support/support.js',
        ],
        'dashboard' => $appPath . 'assets/brand/js/dashboard.js',
    ],
    'css' => [
        'bootstrap' => $appPath . 'assets/brand/vendor/bootstrap/css/bootstrap.min.css',
        'fontastic' => $appPath . 'assets/brand/css/fontastic.css',
        'fontawesome' => $appPath . 'assets/brand/vendor/font-awesome/css/font-awesome.min.css',
        'theme' => $appPath . 'assets/brand/css/style.default.css',
        'custom' => $appPath . 'assets/brand/css/custom.css',
    ],
    'image' => [
        'favicon' => $appPath . 'assets/brand/img/favicon.ico',
    ],
    'api' => [
        'profile' => [
            'changepassword' => $apiPath . "profile/changepassword",
            'view' => $apiPath . "brand/id",
            'remove' => $apiPath . "brand/remove",
            'update' => $apiPath . "brand/update",
            'wallet' => $apiPath . "brand/wallet",
            'transcation' => $apiPath . "brand/transcation",
            'balance' => $apiPath . "brand/balance",
        ],
    ],
    "web"   =>  [
        "forgotpassword"    =>  "{$appPath}brand/password/forgot",
        "resetpassword"    =>  "{$appPath}brand/password/reset",
    ]
];
