<?php

$apiPath = url("/") . "/api/";
$appPath = url("/") . "/";
return [
    'api' => [
        'login' => $apiPath . 'user/login',
        'brand' => [
            'list' => $apiPath . 'brand',
            'add' => $apiPath . 'brand',
            'update' => $apiPath . 'brand/update',
            'delete' => $apiPath . 'brand/delete',
            'active' => $apiPath . 'brand/active',
            'deactive' => $apiPath . 'brand/deactive',
            'brandoffer' => $apiPath . 'brand/brandoffer',
            'paid' => $apiPath . 'brand/paidRequest',
            'totalsales' => $apiPath . 'brand/sales',
            'statistics' => $apiPath . 'brand/statistics',
            'email' => [
                'send' => $apiPath . 'brand/email/send',
            ]
        ],
        'address' => [
            'add' => $apiPath . 'brand/address',
            'update' => $apiPath . 'brand/address/update',
        ],
        'bank' => [
            'add' => $apiPath . 'bank',
            'get' => $apiPath . 'bank',
            'update' => $apiPath . 'bank/update',
        ],
        'category' => [
            'list' => $apiPath . 'category',
            'add' => $apiPath . 'category/add',
            'update' => $apiPath . 'category/update',
            'delete' => $apiPath . 'category/delete',
            'active' => $apiPath . 'category/active',
            'deactive' => $apiPath . 'category/deactive',
        ],
        'corporate' => [
            'list' => $apiPath . 'corporate',
            'add' => $apiPath . 'corporate',
            'update' => $apiPath . 'corporate/update',
            'delete' => $apiPath . 'corporate/delete',
            'active' => $apiPath . 'corporate/active',
            'deactive' => $apiPath . 'corporate/deactive',
            'email' => [
                'send' => $apiPath . 'corporate/email/send',
            ]
        ],
        'country' => [
            'list' => $apiPath . 'country',
            'add' => $apiPath . 'country',
            'update' => $apiPath . 'country/update',
            'delete' => $apiPath . 'country/delete',
            'active' => $apiPath . 'country/active',
            'deactive' => $apiPath . 'country/deactive',
            'address' => $apiPath . 'country/address',
        ],
        'state' => [
            'list' => $apiPath . 'state',
            'add' => $apiPath . 'state',
            'update' => $apiPath . 'state/update',
            'delete' => $apiPath . 'state/delete',
            'active' => $apiPath . 'state/active',
            'deactive' => $apiPath . 'state/deactive',
        ],
        'city' => [
            'list' => $apiPath . 'city',
            'add' => $apiPath . 'city',
            'update' => $apiPath . 'city/update',
            'delete' => $apiPath . 'city/delete',
            'active' => $apiPath . 'city/active',
            'deactive' => $apiPath . 'city/deactive',
        ],
        'offer' => [
            'list' => $apiPath . 'offer',
            'add' => $apiPath . 'offer',
            'update' => $apiPath . 'offer/update',
            'delete' => $apiPath . 'offer/delete',
            'active' => $apiPath . 'offer/active',
            'approve' => $apiPath . 'offer/approve',
            'deactive' => $apiPath . 'offer/deactive',
            'reject' => $apiPath . 'offer/reject',
        ],
        'employee' => [
            'list' => $apiPath . 'employee',
            'add' => $apiPath . 'employee',
            'update' => $apiPath . 'employee/update',
            'delete' => $apiPath . 'employee/delete',
            'active' => $apiPath . 'employee/active',
            'deactive' => $apiPath . 'employee/deactive',
            'dashboard' => $apiPath . 'employee/dashboard',
        ],
        'reward' => [
            'list' => $apiPath . 'reward',
            'add' => $apiPath . 'reward',
            'statistics' => $apiPath . 'reward/statistics',
            'orderhistory' => $apiPath . 'reward/orderhistory',
            'orderhistorystatistic' => $apiPath . 'reward/orderhistorystatistic',
            'brandorderhistory' => $apiPath . 'reward/brandorderhistory',
            'update' => $apiPath . 'reward/update/payment/status',
        ],
        'coupon' => [
            'coupon' => $apiPath . 'coupon',
            'redeem' => $apiPath . 'coupon/redeem',
            'list' => $apiPath . 'coupon/list',
            'otp' => $apiPath . 'coupon/sendOTP',
        ],
        'profile' => [
            'view' => $apiPath . 'profile/view',
            'update' => $apiPath . 'profile/update',
        ],
        'sms_template' => [
            'list' => $apiPath . 'sms_template',
            'add' => $apiPath . 'sms_template',
            'update' => $apiPath . 'sms_template/update',
            'delete' => $apiPath . 'sms_template/delete',
            'active' => $apiPath . 'sms_template/active',
            'deactive' => $apiPath . 'sms_template/deactive',
        ],
        'admin' =>[
            'statistics' => $apiPath . 'admin/statistics',
        ],
    ],
    'script' => [
        'common' => [
            'helper' => $appPath . 'assets/common/js/helper.js',
            'moment' => $appPath . 'assets/common/js/moment.js',
            'jquerymd5' => $appPath . 'assets/common/js/jquery.md5.js',
            'slimscroll' => $appPath . 'assets/common/js/jquery.slimscroll.min.js',
            'resetpassword' => $appPath . 'assets/common/js/resetpassword.js',
            'wysihtml5' => $appPath . 'assets/common/bootstrap-wysihtml5/wysihtml5-0.3.0.js',
            'bootstrap_wysihtml5' => $appPath . 'assets/common/bootstrap-wysihtml5/bootstrap-wysihtml5.js',
        ],
        'datatables' => [
            'jquery' => $appPath . 'assets/admin/assets/datatables/jquery.dataTables.min.js',
            'bootstrap' => $appPath . 'assets/admin/assets/datatables/dataTables.bootstrap.js',
        ],
        'tinymce' => [
            'js' => $appPath . 'assets/admin/assets/tinymce/js/tinymce/tinymce.min.js',
        ],
        'validate' => [
            'js' => $appPath . 'assets/admin/assets/jquery.validate/jquery.validate.min.js',
        ],
        'reward' => [
            'orderhistory' => $appPath . 'assets/brand/js/reward/orderhistory.js',
            'list' => $appPath . 'assets/admin/js/reward/list.js',
        ],
        'profile' => [
            'changepassword' => $appPath . 'assets/admin/js/profile/changepassword.js',
            'view' => $appPath . 'assets/admin/js/profile/view.js',
        ],
        'ko-model' => $appPath . 'assets/common/js/ko-model.js',
        'category' => $appPath . 'assets/admin/js/category/category.js',
        'brand' => $appPath . 'assets/admin/js/brand/brand.js',
        'payment' => $appPath . 'assets/admin/js/brand/payment.js',
        'offer' => $appPath . 'assets/admin/js/offer/offer.js',
        'coupon' => $appPath . 'assets/admin/js/coupon/coupon.js',
        'corporate' => $appPath . 'assets/admin/js/corporate/corporate.js',
        'orderhistory' => $appPath . 'assets/admin/js/corporate/orderhistory.js',
        'location' => $appPath . 'assets/admin/js/location/location.js',
        'employee' => $appPath . 'assets/admin/js/employee/employee.js',
        'sms_template' => $appPath . 'assets/admin/js/sms_template/sms_template.js',
        'dashboard' => $appPath . 'assets/admin/js/dashboard.js',
        'counterup' => $appPath . 'assets/admin/js/jquery.counterup.min.js',
    ],
    [
        'css' => [
            'common' => [
                'wysihtml5' => $appPath . 'assets/common/bootstrap-wysihtml5/bootstrap-wysihtml5.css',
                'wysihtml5_color' => $appPath . 'assets/common/bootstrap-wysihtml5/bootstrap-wysihtml5.css',
            ]
        ]
    ],
    'web' => [
        'setSession' => $appPath . 'setsession',
        'updatecategory' => $appPath . 'category/id',
        'category' => [
            'brand' => $appPath . 'admin/category/brand',
            'corporate' => $appPath . 'admin/category/corporate',
            'reward' => $appPath . 'admin/category/reward',
        ],
    ],
    'image' => [
        'website' => $appPath .'assets/corporate/img/web-icon.png',
        'facebook'  => $appPath .'assets/corporate/img/fb-icon.png',
        'call'  => $appPath .'assets/corporate/img/call-icon.png',
    ],
];
